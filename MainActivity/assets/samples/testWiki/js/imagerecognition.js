var World = {
	loaded: false,

	init: function initFn(id) {
		/* 
			Disable all sensors in "IR-only" Worlds to save performance. If the property is set to true, any geo-related components (such as GeoObjects and ActionRanges) are active. If the property is set to false, any geo-related components will not be visible on the screen, and triggers will not fire.
		*/
		AR.context.services.sensors = false;
		this.createOverlays(id);
	},

	createOverlays: function createOverlaysFn(id) {
		/*
			First an AR.Tracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function worldLoaded() is called.

			Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
			Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.
		*/
		//this.tracker = new AR.Tracker("assets/magazine.wtc", {
			this.tracker = new AR.Tracker("assets/Camera3d-WikiTude.wtc", {
			onLoaded: this.worldLoaded
		});

		/*
			The next step is to create the augmentation. In this example an image resource is created and passed to the AR.ImageDrawable. A drawable is a visual component that can be connected to an IR target (AR.Trackable2DObject) or a geolocated object (AR.GeoObject). The AR.ImageDrawable is initialized by the image and its size. Optional parameters allow for position it relative to the recognized target.
		*/

		/* Create overlay for page one */
		var imgOne = new AR.ImageResource("assets/imageOne.png");
		/*var overlayOne = new AR.ImageDrawable(imgOne, 1, {
			offsetX: -0.15,
			offsetY: 0
		});*/

		/*
			The last line combines everything by creating an AR.Trackable2DObject with the previously created tracker, the name of the image target and the drawable that should augment the recognized image.
			Please note that in this case the target name is a wildcard. Wildcards can be used to respond to any target defined in the target collection. If you want to respond to a certain target only for a particular AR.Trackable2DObject simply provide the target name as specified in the target collection.
		*/
	/*	var pageOne = new AR.Trackable2DObject(this.tracker, "*", {
			drawables: {
				cam: overlayOne
			}
		});
		*/
		
		
		//var ab ="e38fkwsxja";
		  var url = "http://camera3d.apptech.com.hk/3dpic.php?item="+id;
	
        htmlDrawable = new AR.HtmlDrawable({
                                           uri:url
                                           }, 1.9,{viewportWidth:756,height:1000,width:1000}); 
                                           //}, 1.8,{viewportWidth:606,width:1000});
//,scale:0.25, updateRate:AR.HtmlDrawable.UPDATE_RATE.STATIC
//var sparkles = new AR.AnimatedImageDrawable(htmlDrawable, 1, 520, 320);
//sparkles.animate([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 100, -1);					   
										   
        htmlDrawable.clickThroughEnabled = true;
		var o1 = new AR.Trackable2DObject(this.tracker, "Wikitude1", {
                                          drawables: {
                                          cam: htmlDrawable
                                          }
                                          });
        var o2 = new AR.Trackable2DObject(this.tracker, "Wikitude2", {
                                          drawables: {
                                          cam: htmlDrawable
                                          }
                                          });
        var o2 = new AR.Trackable2DObject(this.tracker, "Wikitude3", {
                                          drawables: {
                                          cam: htmlDrawable
                                          }
                                          });
      
		/**/
		
	},

	worldLoaded: function worldLoadedFn() {
	/*	var cssDivLeft = " style='display: table-cell;vertical-align: middle; text-align: right; width: 50%; padding-right: 15px;'";
		var cssDivRight = " style='display: table-cell;vertical-align: middle; text-align: left;'";
		document.getElementById('loadingMessage').innerHTML =
			"<div" + cssDivLeft + ">Scan Target &#35;1 (surfer):</div>" +
			"<div" + cssDivRight + "><img src='assets/surfer.png'></img></div>";

		// Remove Scan target message after 10 sec.
		setTimeout(function() {
			var e = document.getElementById('loadingMessage');
			e.parentElement.removeChild(e);
		}, 10000);*/
	}
};

//World.init();