package com.siulun.Camera3D.view;

import com.siulun.Camera3D.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class DialogSubClass  extends Dialog {  
    public DialogSubClass(Context context, int theme) {  
        super(context, theme);  
    }  
  
    protected DialogSubClass(Context context, boolean cancelable,OnCancelListener cancelListener) {  
        super(context, cancelable, cancelListener);  
    }  
  
    public DialogSubClass(Context context) {  
        super(context);  
    }  
      
    @Override  
    protected void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialoglayout, null);  
        setContentView(view);  
    }  
      
    @Override  
    public void show() {  
        super.show();  
    }  
      
    @Override  
    public void dismiss() {  
        super.dismiss();  
    }  
      
    public void setMsg(String txt) {  
          
    }  
  
}  