
package com.siulun.Camera3D.view;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Stack;

import com.siulun.Camera3D.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.cameramain_mvc.CameraActivity;
import com.example.gallery.*;
import com.facebook.Session;
import com.facebook.SessionState;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.L;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.fragments.CameraFragment;
import com.siulun.Camera3D.view.fragments.ConfigFragment;
import com.siulun.Camera3D.view.fragments.FacebookLoginFragment;
import com.siulun.Camera3D.view.fragments.Followingfragment;
import com.siulun.Camera3D.view.fragments.Login;
import com.siulun.Camera3D.view.fragments.MyProgressDialogFragment;
import com.siulun.Camera3D.view.fragments.NewsfeedFragment;
import com.siulun.Camera3D.view.fragments.ProfileFragment;
import com.siulun.Camera3D.view.fragments.Profile_OnlineFragment;
import com.siulun.Camera3D.view.fragments.SearchFragment;
import com.siulun.Camera3D.view.fragments.Search_item_click_Fragment;
import com.siulun.Camera3D.view.fragments.SettingFragment;
import com.siulun.Camera3D.view.fragments.profile;

public class MainActivity extends ActionBarActivity implements OnClickListener {
	LoginSharedPreferences sharedpreferences;
	private SettingFragment settingFragment;
	private ProgressDialog progressDialog;
	private Handler UIhandler;
	private TextView requestTestTextView;

	//define a stack to store fragment and its state 
	public static Stack<Fragment> fragmentStack;
	
	public static NewsfeedFragment newsfeedFragment;
	public static Followingfragment followingFragment;
	public static profile Fragmentprofile;
	private FragmentManager myFragmentManager;
	private CameraFragment cameraFragment;
	public static GalleryFragment profileFragment;
	public static SearchFragment searchFragment;
	private ConfigFragment settingfragment;
	private FacebookLoginFragment facebookloginfragment;
	
	private FragmentManager fragmentManager;
	public static FragmentTransaction fragmentTransaction;
	MyProgressDialogFragment progressDialofrg;
	
	public static String getFragmentStack = "newsfeedFragment";
	public static Object DisplayFragment = null;
	SharedPreferences settings ;
	
	public static ImageView iv_following,iv_news,iv_camera,iv_search,iv_profile,iv_setting;
	private static final String TEST_FILE_NAME = "Universal Image Loader @#&=+-_.,!()~'%20.png";
	
	// init network checking class
		NetworkChecking checkNetwork;
	@Override
	protected void onResume() {
		
		super.onResume();
	}
	
	@Override
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
	    super.onActivityResult(requestCode, resultCode, data);
	
	    Session.getActiveSession().onActivityResult(this, requestCode,resultCode, data);
	
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}

    public static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkNetwork = new NetworkChecking(this);
		sharedpreferences= new LoginSharedPreferences(this);
		if(sharedpreferences.getToken()==null){
		Intent intent = new Intent(MainActivity.this, SettingFragment.class);
        startActivity(intent);
		}
		//progressDialog = new ProgressDialog(this);
		//Toast.makeText(this, "oncreate", Toast.LENGTH_SHORT).show();
		//progressDialog = new ProgressDialog(this);
		
		
        
		initImageLoader(getApplicationContext());
	
		//showProgressDialog();
		// NewsfeedFragment
		
		//init stack
		fragmentStack = new Stack<Fragment>();
		context = MainActivity.this;
		newsfeedFragment = new NewsfeedFragment();
		followingFragment = new Followingfragment();
		cameraFragment = new CameraFragment();
		profileFragment = new GalleryFragment();
		searchFragment = new SearchFragment();
		settingfragment  = new ConfigFragment();
		Fragmentprofile = new profile();
		facebookloginfragment = new FacebookLoginFragment();
		
		
		fragmentManager = getSupportFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		//fragmentTransaction.add(R.id.fragmentContainerFrameLayout, newsfeedFragment, TAG_NEWSFEED_FRAGMENT);
		addFragmentToTransactionAndStack(newsfeedFragment,fragmentTransaction);
		DisplayFragment = newsfeedFragment;
		fragmentTransaction.commit();
	
		// Progress Dialog
		
		UIhandler = new Handler();  
	
		iv_following = (ImageView)findViewById(R.id.followingImageView);
		iv_following.setOnClickListener(new myOnclickListener(this));
		iv_news =  (ImageView)findViewById(R.id.newsfeedImageView);
		iv_news.setOnClickListener(new myOnclickListener(this));
		iv_camera = (ImageView)findViewById(R.id.cameraImageView);
		iv_search = (ImageView)findViewById(R.id.searchImageView);
		iv_profile = (ImageView)findViewById(R.id.profileImageView);
		iv_camera.setOnClickListener(new myOnclickListener(this));
		iv_search.setOnClickListener(new myOnclickListener(this));
		iv_profile.setOnClickListener(new myOnclickListener(this));
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View action_barView = layoutInflater.inflate(R.layout.action_bar, null, false);
		iv_setting = (ImageView)action_barView.findViewById(R.id.settingsIconImageView);
		iv_setting.setOnClickListener(new myOnclickListener(this));
		iv_news.setSelected(true);
		fbClearToken(this);
		setupUI(findViewById(R.id.fragmentContainerFrameLayout));
	        
		//hideProgressDialog();
	}
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							  hideSoftKeyboard(MainActivity.this);
		                    return false;
						
						}

           
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

	public static void showFragment(Fragment fragment, FragmentTransaction transaction) {
		if(fragment!=null){
		if (fragmentStack.contains(fragment)) {
			if(DisplayFragment==fragment){
		//Toast.makeText(context, "show", Toast.LENGTH_SHORT).show();
			//	if(fragment!=NewsfeedFragment.sampleCamFragment){		
			 fragmentStack.get(fragmentStack.indexOf(fragment)).onResume();
				//}
			transaction
					.show(fragmentStack.get(fragmentStack.indexOf(fragment)));
			}
		}
		}
	}

	public static void hideFragment(Fragment fragment, FragmentTransaction transaction) {
		if(fragment!=null){
		if (fragmentStack.contains(fragment)) {
		//if(fragment!=NewsfeedFragment.sampleCamFragment){
		    fragmentStack.get(fragmentStack.indexOf(fragment)).onPause();
		//}
			transaction
					.hide(fragmentStack.get(fragmentStack.indexOf(fragment)));
			
		}
		}
	}

	public static void addFragmentToTransactionAndStack(Fragment fragment, FragmentTransaction transaction) {
		transaction
				.add(R.id.fragmentContainerFrameLayout, fragment, "");
		//transaction.addToBackStack(null);
		fragmentStack.push(fragment);
		//backfragmentStack.push(fragment);
	}

    public static void removeFragmentFromStack(Fragment fragment){
    	if(fragmentStack.contains(fragment)){
    	fragmentStack.remove(fragmentStack.indexOf(fragment));
    	}
    }

	public static void backToPreviousFragment(FragmentTransaction fragmentTransaction){
		Fragment fragment = (Fragment)MainActivity.DisplayFragment;
		if(fragment!=null){
		
			if(fragment==SearchFragment.nextFrag){
			processFragmentToShowAndHideOtherFragment(MainActivity.searchFragment,fragmentTransaction);
			}
			if(fragment==NewsfeedFragment.Newsfeed_nextFrag){
			processFragmentToShowAndHideOtherFragment(MainActivity.newsfeedFragment,fragmentTransaction);
			}
			if(fragment==Followingfragment.followingfragment_nextFrag){
				processFragmentToShowAndHideOtherFragment(MainActivity.followingFragment,fragmentTransaction);
				}
			//if(fragment==SearchFragment.nextFrag.specificFeedFragment){
			if(fragment==Search_item_click_Fragment.SearchFragment_specificFeedFragment){
				processFragmentToShowAndHideOtherFragment(SearchFragment.nextFrag,fragmentTransaction);
				}
			//if(fragment==NewsfeedFragment.Newsfeed_nextFrag.specificFeedFragment){
			if(fragment==Search_item_click_Fragment.NewsfeedFragment_specificFeedFragment){
				processFragmentToShowAndHideOtherFragment(NewsfeedFragment.Newsfeed_nextFrag,fragmentTransaction);
				}
			//if(fragment==Followingfragment.followingfragment_nextFrag.specificFeedFragment){
			if(fragment==Search_item_click_Fragment.FollowingFragment_specificFeedFragment){
				processFragmentToShowAndHideOtherFragment(Followingfragment.followingfragment_nextFrag,fragmentTransaction);
				}
			
			if(fragment==Profile_OnlineFragment.Profile_Online_specificFeedFragment){
				processFragmentToShowAndHideOtherFragment(MainActivity.Fragmentprofile,fragmentTransaction);
				}
		/*	if(fragment==NewsfeedFragment.sampleCamFragment){
				processFragmentToShowAndHideOtherFragment(MainActivity.newsfeedFragment,fragmentTransaction);
				}
				*/
			removeFragmentFromStack(fragment);
			fragmentTransaction.remove(fragment);
        	
		}
	}
    
    public static void processFragmentToShowAndHideOtherFragment(Fragment fragment, FragmentTransaction transaction){ 
		DisplayFragment=fragment;
		hideFragment(newsfeedFragment, transaction);
		hideFragment(searchFragment, transaction);
		hideFragment(profileFragment, transaction);
		hideFragment(SearchFragment.nextFrag, transaction);
		//hideFragment(SearchFragment.nextFrag.specificFeedFragment, transaction);
		hideFragment(Search_item_click_Fragment.SearchFragment_specificFeedFragment, transaction);
		hideFragment(NewsfeedFragment.Newsfeed_nextFrag, transaction);
		//hideFragment(NewsfeedFragment.Newsfeed_nextFrag.specificFeedFragment, transaction);
		hideFragment(Search_item_click_Fragment.NewsfeedFragment_specificFeedFragment, transaction);
		hideFragment(Followingfragment.followingfragment_nextFrag, transaction);
		//hideFragment(Followingfragment.followingfragment_nextFrag.specificFeedFragment, transaction);
		hideFragment(Search_item_click_Fragment.FollowingFragment_specificFeedFragment, transaction);
		hideFragment(followingFragment, transaction);
		hideFragment(Fragmentprofile, transaction);
		hideFragment(Profile_OnlineFragment.Profile_Online_specificFeedFragment, transaction);
		//hideFragment(NewsfeedFragment.sampleCamFragment, transaction);
		showFragment(fragment, transaction);
		
    }

    public void SpecificBackToPreviousFragment(Fragment fragment, FragmentTransaction transaction){   	
    		if (DisplayFragment == fragment) {
    			backToPreviousFragment(transaction);
    			transaction.commit();   			
    		} 	
    }
 
    public Fragment checkSpecificFragmentIsNullOrNot(Fragment fragment){
    	Fragment specificFragment = null;
    	if(fragment!=null){
			if(DisplayFragment==fragment){
				specificFragment=fragment;
				return specificFragment;
			}else{
				return specificFragment;
			}
			
		}else{
			return specificFragment;
		}
    }
    @Override
	public void onBackPressed() {
		Fragment specificFragment = null;
	/*	specificFragment = checkSpecificFragmentIsNullOrNot(SearchFragment.nextFrag.specificFeedFragment);
		specificFragment = checkSpecificFragmentIsNullOrNot(NewsfeedFragment.Newsfeed_nextFrag.specificFeedFragment);
		specificFragment = checkSpecificFragmentIsNullOrNot(Followingfragment.followingfragment_nextFrag.specificFeedFragment);
*/
		if(specificFragment==null){
		specificFragment = checkSpecificFragmentIsNullOrNot(Search_item_click_Fragment.SearchFragment_specificFeedFragment);
		}
		if(specificFragment==null){
			specificFragment = checkSpecificFragmentIsNullOrNot(Profile_OnlineFragment.Profile_Online_specificFeedFragment);
			}
		if(specificFragment==null){
		specificFragment = checkSpecificFragmentIsNullOrNot(Search_item_click_Fragment.FollowingFragment_specificFeedFragment);
		}
		if(specificFragment==null){
		specificFragment = checkSpecificFragmentIsNullOrNot(Search_item_click_Fragment.NewsfeedFragment_specificFeedFragment);
		}
		if(specificFragment==null){
			specificFragment = checkSpecificFragmentIsNullOrNot(Followingfragment.followingfragment_nextFrag);
			}
		if(specificFragment==null){
			specificFragment = checkSpecificFragmentIsNullOrNot(NewsfeedFragment.Newsfeed_nextFrag);
			}
		if(specificFragment==null){
			specificFragment = checkSpecificFragmentIsNullOrNot(SearchFragment.nextFrag);
			}
		/*if(specificFragment==null){
			specificFragment = checkSpecificFragmentIsNullOrNot(NewsfeedFragment.sampleCamFragment);
			}*/
		if(specificFragment!=null){
			SpecificBackToPreviousFragment(specificFragment,this.getSupportFragmentManager().beginTransaction());
		}else{
			super.onBackPressed();
		}
		
		
	}
    
	private void showProgressDialog() {

		if ( !progressDialog.isShowing())
			  progressDialog.setMessage("Loading......");
			progressDialog.show();
	}

	private void hideProgressDialog() {

        new Thread()   
        {   
            public void run()   
            {   
                try  
                {   
                    sleep(5000);   
                }catch(Exception e)   
                {   
                    e.printStackTrace();   
                }finally  
                {   
                  
                	progressDialog.dismiss();   
                    
                }   
            }   
        }.start(); 
	}

	public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 4)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(70 * 1024 * 1024) // 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

	@Override
	public void onClick(View v) {
		
		
		// SWITCH FRAGMENTS HERE
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		// Load Custom ActionBar
		getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getSupportActionBar().setCustomView(R.layout.action_bar);
		return true;
	}

	public void clickEvent(View v) {
		settingFragment = new SettingFragment();
        if (v.getId() == R.id.settingsIconImageView) {
           
	 
			Log.i("settingsIconImageView","settingsIconImageView");
	/*	  getManager();
		    fragmentTransaction = myFragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.fragmentContainerFrameLayout,settingfragment , TAG_NEWSFEED_FRAGMENT);
			fragmentTransaction.commit();*/
			Intent intent = new Intent(MainActivity.this, SettingFragment.class);
	           startActivity(intent);
			
			/*Intent intent = new Intent(MainActivity.this, Login.class);
	           startActivity(intent);*/
        }

       
    } 
	public static void fbClearToken(Context context) {
	    Session session = Session.getActiveSession();
	    if (session != null) {

	        if (!session.isClosed()) {
	            session.closeAndClearTokenInformation();
	            //clear your preferences if saved
	        }
	    } else {
	        session = new Session(context);
	        Session.setActiveSession(session);

	        session.closeAndClearTokenInformation();
	            //clear your preferences if saved
	    }

	}
	@Override
	protected void onDestroy() {

		super.onDestroy();
	}
	class myOnclickListener implements OnClickListener{
		Context c;		
		ProgressDialog progressDialog;
		/*Followingfragment followingFragment;
		FragmentManager myFragmentManager;
		 CameraFragment cameraFragment;
		 GalleryFragment profileFragment;
		SearchFragment searchFragment;
		NewsfeedFragment newfeedFragment;
		TextView requestTestTextView;
		ConfigFragment settingfragment;
		FacebookLoginFragment facebookloginfragment;
		*/
		public myOnclickListener(Context context){
			context = c;
			/*followingFragment = new Followingfragment();
			cameraFragment = new CameraFragment();
			profileFragment = new GalleryFragment();
			searchFragment = new SearchFragment();
			newfeedFragment = new NewsfeedFragment();
			settingfragment  = new ConfigFragment();
			facebookloginfragment = new FacebookLoginFragment();
			progressDialog = new ProgressDialog(MainActivity.this);
			*/
	
		}
		public void getManager(){
			myFragmentManager = getSupportFragmentManager();
			
		}
	
		public void onClick(View view){
			fragmentTransaction = fragmentManager.beginTransaction();
			if(checkNetwork.NetworkType()==1||checkNetwork.NetworkType()==4){
			}else{
				 Toast.makeText(MainActivity.this, "switch to wifi can watch 3d more perfect!", Toast.LENGTH_SHORT).show();
			}
			switch (view.getId()){
			case R.id.followingImageView :
				iv_camera.setSelected(false);
				iv_search.setSelected(false);
				iv_profile.setSelected(false);
				iv_news.setSelected(false);
				iv_following.setSelected(true);
				getManager();	
				 
				if (fragmentStack.contains(followingFragment)) {
                 if(fragmentStack.contains(Followingfragment.followingfragment_nextFrag)){
						
					
               //  if(fragmentStack.contains(Followingfragment.followingfragment_nextFrag.specificFeedFragment)){
                	 if(fragmentStack.contains(Search_item_click_Fragment.FollowingFragment_specificFeedFragment)){
						//processFragmentToShowAndHideOtherFragment(Followingfragment.followingfragment_nextFrag.specificFeedFragment,fragmentTransaction);	
                		 processFragmentToShowAndHideOtherFragment(Search_item_click_Fragment.FollowingFragment_specificFeedFragment,fragmentTransaction);	
					}else{
						processFragmentToShowAndHideOtherFragment(Followingfragment.followingfragment_nextFrag,fragmentTransaction);	
					}
					 
				}else{
					processFragmentToShowAndHideOtherFragment(followingFragment,fragmentTransaction);
				
				}
					fragmentTransaction.commit();
					
				} else {
				
					processFragmentToShowAndHideOtherFragment(followingFragment,fragmentTransaction);
					addFragmentToTransactionAndStack(followingFragment,fragmentTransaction);
					fragmentTransaction.commit();
				}
				
			
				break;
			case R.id.newsfeedImageView:
				//showProgressDialog();
				iv_camera.setSelected(false);
				iv_search.setSelected(false);
				iv_profile.setSelected(false);
				iv_news.setSelected(true);
				iv_following.setSelected(false);
	
			
				
				if (fragmentStack.contains(newsfeedFragment)) {
					if(fragmentStack.contains(NewsfeedFragment.Newsfeed_nextFrag)){
						
				
					//if(fragmentStack.contains(NewsfeedFragment.Newsfeed_nextFrag.specificFeedFragment)){
						//processFragmentToShowAndHideOtherFragment(NewsfeedFragment.Newsfeed_nextFrag.specificFeedFragment,fragmentTransaction);

						if(fragmentStack.contains(Search_item_click_Fragment.NewsfeedFragment_specificFeedFragment)){
							processFragmentToShowAndHideOtherFragment(Search_item_click_Fragment.NewsfeedFragment_specificFeedFragment,fragmentTransaction);	
					}else{
						processFragmentToShowAndHideOtherFragment(NewsfeedFragment.Newsfeed_nextFrag,fragmentTransaction);	
					}
					
				}else{
					
					processFragmentToShowAndHideOtherFragment(newsfeedFragment,fragmentTransaction);
					
				}
			/*	if(fragmentStack.contains(NewsfeedFragment.sampleCamFragment)&&DisplayFragment==NewsfeedFragment.sampleCamFragment){
						processFragmentToShowAndHideOtherFragment(NewsfeedFragment.sampleCamFragment,fragmentTransaction);	
					}else{
						
						processFragmentToShowAndHideOtherFragment(newsfeedFragment,fragmentTransaction);
						
					}
				*/
					fragmentTransaction.commit();
					
				
				} else {
					
					processFragmentToShowAndHideOtherFragment(newsfeedFragment,fragmentTransaction);
					addFragmentToTransactionAndStack(newsfeedFragment,fragmentTransaction);
					fragmentTransaction.commit();
				}
				
				
				break;
			case R.id.cameraImageView:
				iv_camera.setSelected(true);
				iv_search.setSelected(false);
				iv_profile.setSelected(false);
				iv_news.setSelected(false);
				iv_following.setSelected(false);
			
				Intent intent = new Intent(MainActivity.this,CameraActivity.class);
			    startActivity(intent);
			    break;
				
			case R.id.searchImageView:
				iv_camera.setSelected(false);
				iv_search.setSelected(true);
				iv_profile.setSelected(false);
				iv_news.setSelected(false);
				iv_following.setSelected(false);
				
				
				
				if (fragmentStack.contains(searchFragment)) {
				
					if(fragmentStack.contains(SearchFragment.nextFrag)){
					
						//if(fragmentStack.contains(SearchFragment.nextFrag.specificFeedFragment)){
							//processFragmentToShowAndHideOtherFragment(SearchFragment.nextFrag.specificFeedFragment,fragmentTransaction);
							if(fragmentStack.contains(Search_item_click_Fragment.SearchFragment_specificFeedFragment)){
								processFragmentToShowAndHideOtherFragment(Search_item_click_Fragment.SearchFragment_specificFeedFragment,fragmentTransaction);	
						}else{
							processFragmentToShowAndHideOtherFragment(SearchFragment.nextFrag,fragmentTransaction);	
						}
						
					}else{
						processFragmentToShowAndHideOtherFragment(searchFragment,fragmentTransaction);
					
					}
					fragmentTransaction.commit();
					
				} else {
				
					processFragmentToShowAndHideOtherFragment(searchFragment,fragmentTransaction);
					addFragmentToTransactionAndStack(searchFragment,fragmentTransaction);
					fragmentTransaction.commit();
				}
			
					break;
			case R.id.profileImageView:
				iv_camera.setSelected(false);
				iv_search.setSelected(false);
				iv_profile.setSelected(true);
				iv_news.setSelected(false);
				iv_following.setSelected(false);
				   // getManager();
				/*if (fragmentStack.contains(profileFragment)) {
				
					processFragmentToShowAndHideOtherFragment(profileFragment,fragmentTransaction);
					fragmentTransaction.commit();
				
				} else {
				
					processFragmentToShowAndHideOtherFragment(profileFragment,fragmentTransaction);
					addFragmentToTransactionAndStack(profileFragment,fragmentTransaction);
					fragmentTransaction.commit();
				}
				*/
				if (fragmentStack.contains(Fragmentprofile)) {
					
					if(fragmentStack.contains(Profile_OnlineFragment.Profile_Online_specificFeedFragment)){
						processFragmentToShowAndHideOtherFragment(Profile_OnlineFragment.Profile_Online_specificFeedFragment,fragmentTransaction);	
				}else{
					processFragmentToShowAndHideOtherFragment(Fragmentprofile,fragmentTransaction);	
				}
				
			
					//processFragmentToShowAndHideOtherFragment(Fragmentprofile,fragmentTransaction);
					fragmentTransaction.commit();
				
				} else {
				
					processFragmentToShowAndHideOtherFragment(Fragmentprofile,fragmentTransaction);
					addFragmentToTransactionAndStack(Fragmentprofile,fragmentTransaction);
					fragmentTransaction.commit();
				}
				   
	
			}
		}
		
	}

	
}


