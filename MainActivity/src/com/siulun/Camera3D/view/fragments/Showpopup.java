package com.siulun.Camera3D.view.fragments;


import com.siulun.Camera3D.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class Showpopup {
	public void showPopup(final Activity context,String text) {
		  //this.item2=item2;
		  final Display display = context.getWindowManager().getDefaultDisplay(); 
		   // Inflate the popup_layout.xml
		   //LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.LinearLayout1);
		   LayoutInflater layoutInflater = (LayoutInflater) context
		     .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		   final View layout = layoutInflater.inflate(R.layout.popup, null);
		   ImageView image = (ImageView)layout.findViewById(R.id.imageView1);
		   TextView textview = (TextView)layout.findViewById(R.id.textView1);
		   textview.setText(text);
		   //TextView  tv1 = (TextView)layout.findViewById(R.id.textView1);
		   
		  // image.getViewTreeObserver().removeOnPreDrawListener(context);
	     // int  finalHeight = image.getMeasuredHeight();
		  // LinearLayout ll2 = (LinearLayout)layout.findViewById(R.id.LinearLayout2);
		  // Toast.makeText(context, ""+tv1.getHeight(), Toast.LENGTH_SHORT).show();
		   //LinearLayout.LayoutParams p = (LinearLayout.LayoutParams)ll2.getLayoutParams();
		   
		   // Creating the PopupWindow
		   final PopupWindow popup = new PopupWindow(context);
		   popup.setContentView(layout);
		   popup.setWidth(display.getWidth());
		   //popup.setHeight(display.getHeight()/8);
		   popup.setHeight(image.getDrawable().getIntrinsicHeight());
		  
		  /* */
		  // popup.setWidth(140);
		  // popup.setHeight(140);
		  // popup.setHeight((display.getHeight()/100)*80);
		   popup.setFocusable(true);
		   popup.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bgpopup));
		
		
	
		 popup.setAnimationStyle(R.style.PopupWindowAnimation);
	
		
		  new Handler().postDelayed(new Runnable(){

		        public void run() {
		        	  popup.showAtLocation(layout, Gravity.BOTTOM, 0, display.getHeight()/8);
		        }

		    }, 100L);
		    new Handler().postDelayed(new Runnable(){

		        public void run() {
		        	popup.dismiss();
		        }

		    }, 5000);
		   // Displaying the popup at the specified location, + offsets.
		 
		  // popup.showAtLocation(layout, Gravity.CENTER,0,0);
		  
		 
		}
}
