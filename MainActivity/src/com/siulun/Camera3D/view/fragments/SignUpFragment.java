package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.siulun.Camera3D.R;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SignUpFragment extends Fragment{
	LayoutInflater layoutInflater;
	LinearLayout signUPLayout;
	ImageButton backbutton;
	Button signup_btn;
	JSONParser jParser = new JSONParser();
	EditText email_editText,first_name_editText,last_name_editText,nick_name_editText,pw_editText,confirmpw_editText;
	String email,first_name,last_name,nick_name,password,conform_password;
	NetworkChecking checkNetwork;
	
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layoutInflater = inflater;
		signUPLayout = (LinearLayout)layoutInflater.inflate(R.layout.sign_up_fragment, container, false);
		signUPLayout.setBackgroundColor(Color.WHITE);
		 backbutton =(ImageButton) signUPLayout.findViewById(R.id.backbutton);
		 signup_btn =(Button) signUPLayout.findViewById(R.id.signup_button);
		 email_editText  =(EditText) signUPLayout.findViewById(R.id.email_editText);
		 first_name_editText  =(EditText) signUPLayout.findViewById(R.id.first_name_editText);
		 last_name_editText  =(EditText) signUPLayout.findViewById(R.id.last_name_editText);
		 nick_name_editText  =(EditText) signUPLayout.findViewById(R.id.nick_name_editText);
		 pw_editText  =(EditText) signUPLayout.findViewById(R.id.pw_editText);
		 confirmpw_editText  =(EditText) signUPLayout.findViewById(R.id.confirmpw_editText);
		 final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		 checkNetwork = new NetworkChecking(getActivity());
		 //   imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		// InputMethodManager imm = (InputMethodManager) getSystemService(getActivity().INPUT_METHOD_SERVICE);
		    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		    backbutton.setOnClickListener  (new OnClickListener(){ 
	            @Override
	            public void onClick(View v) {
	                // TODO Auto-generated method stub
	            	getFragmentManager().popBackStack();
	            	Log.i("onclick","backbutton");
	            }         

	        });  
		    signup_btn.setOnClickListener  (new OnClickListener(){ 
	            @Override
	            public void onClick(View v) {
	            	  if(checkNetwork.isNetworkAvailable()){
	            	 password = pw_editText.getText().toString();
	            	 email = email_editText.getText().toString();
	        		 first_name = first_name_editText.getText().toString();
	        		 last_name = last_name_editText.getText().toString();
	        		 nick_name = nick_name_editText.getText().toString();	        		
	        		 conform_password = confirmpw_editText.getText().toString();
	        		 if(password.matches("")||email.matches("")||first_name.matches("")||last_name.matches("")||nick_name.matches("") ){
	        			 Toast.makeText(getActivity(),"Please Enter All The Field",Toast.LENGTH_SHORT).show();
	        		 }
	        		 else if(password.length()<5){
	            		 Toast.makeText(getActivity(),"Please Enter At Least 6 Letter For Password",Toast.LENGTH_SHORT).show();
	            	}
	            	else if(password.equals(conform_password)){
	            		String password_md5 = md5(pw_editText.getText().toString());
	             new registration().execute(email,password_md5,first_name,last_name,nick_name);
	            }
	            	else{
	            		 Toast.makeText(getActivity(),"Two passwords are not match!",Toast.LENGTH_SHORT).show();
	            	}
	            	  }else{
					    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
							
				        }
	            }
	        });  
        return signUPLayout;  
    }  
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}
	   public static final String md5(final String s) {
		    final String MD5 = "MD5";
		    try {
		        // Create MD5 Hash
		        MessageDigest digest = java.security.MessageDigest
		                .getInstance(MD5);
		        digest.update(s.getBytes());
		        byte messageDigest[] = digest.digest();

		        // Create Hex String
		        StringBuilder hexString = new StringBuilder();
		        for (byte aMessageDigest : messageDigest) {
		            String h = Integer.toHexString(0xFF & aMessageDigest);
		            while (h.length() < 2)
		                h = "0" + h;
		            hexString.append(h);
		        }
		        return hexString.toString();

		    } catch (NoSuchAlgorithmException e) {
		        e.printStackTrace();
		    }
		    return "";
		}
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class registration extends AsyncTask<String, String,String> {
		 String success ;
		 String status;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			
		
	        String username = (String)args[0];			
		    String password =(String)args[1];
		    String first_name =(String)args[2];
		    String last_name =(String)args[3];
		    String nick_name =(String)args[4];
		 
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("username", username));
			params.add(new BasicNameValuePair("password", password));
			params.add(new BasicNameValuePair("first_name", first_name));
			params.add(new BasicNameValuePair("last_name", last_name));
			params.add(new BasicNameValuePair("nick_name", nick_name));

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_REG, "POST", params);
			try{
				String success = json.getString("status");
				if (success.equals("true")) {
					status = "true";
				}
				else{
					String error_msg = json.getString("error_msg");
					status = error_msg;
				}
			}catch(Exception e){
				
			}
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			

			return status;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String status) {
			if(status.equals("true")){
				 Builder alertDialog = new AlertDialog.Builder(getActivity());					
				    alertDialog.setTitle("Registration Successful");
				    alertDialog.setMessage("Welcome to Camera3D");				   
				    alertDialog.setPositiveButton("Beck to NewsFeed", new DialogInterface.OnClickListener() {  
					    public void onClick(DialogInterface dialog, int which) {  
					    	getActivity().finish();
							
					    }  
					}); 				 
				    alertDialog.show();
			}
			else{
				 Toast.makeText(getActivity(),status,Toast.LENGTH_SHORT).show();
			}
		}

	}
}