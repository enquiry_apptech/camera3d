package com.siulun.Camera3D.view.fragments;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;


public class MyProgressDialogFragment extends DialogFragment{
    private static ProgressDialog progressDialog = null;
    
    // ���ン���タ���ス������������������使���
    public static MyProgressDialogFragment newInstance(String title, String message){
        MyProgressDialogFragment instance = new MyProgressDialogFragment();
        
        // ���������ロ���に���ラ���ー������渡���
        Bundle arguments = new Bundle();
        arguments.putString("title", title);
        arguments.putString("message", message);
        
        instance.setArguments(arguments);
        
        return instance;
    }
    
    // ProgressDialog作���
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        if (progressDialog != null)
            return progressDialog;
        
        // ���ラ���ー������������
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // ���ロ���レ���������ア���グ���ス���イ���������ス���イ���に設���
        // ���ロ���レ���������ア���グ���キ���������ル���可������������������設���（������ク���タ���で���������ロ���������ャ���セ���で���な���������に���������
        setCancelable(false);
        
        return progressDialog;
    }
    
    // progressDialog������
    @Override
    public Dialog getDialog(){
        return progressDialog;
    }
    
    // ProgressDialog������
    @Override
    public void onDestroy(){
        super.onDestroy();
        progressDialog = null;
    }
}