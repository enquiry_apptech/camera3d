package com.siulun.Camera3D.view.fragments;


import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.AbsListViewBaseFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.SearchFragment.ImageAdapter;
import com.siulun.Camera3D.view.fragments.SearchFragment.SearchForUserName;



import com.siulun.Camera3D.R;


import android.app.ActionBar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Search_item_click_Fragment extends Fragment{
	LoginSharedPreferences sharedpreferences;
	private LayoutInflater layoutInflater;
	private RelativeLayout user_item_rl;
	private TextView user_nick_nametv,follow_email_nametv;
	private ImageButton followbtn;
	private ImageView search_user_image;
	private GridView user_item_collection;
	protected AbsListView listView;
	DisplayImageOptions options;
	private ImageLoadingListener animateFirstListener;
	public ArrayList<HashMap<String, String>> itemCollectionArray;
	private UserInfo userGlobalVariable ;
	JSONParser jParser = new JSONParser();
	JSONArray ItemIdArray = null;
	private ImageButton backbutton;
	public  String token;
	public String user_uid;
	FragmentManager myFragmentManager;
	FragmentTransaction fragmentTransaction;
	public ImageButton backbtn;
	
	//public SpecificFeed specificFeedFragment = null;
	
    public static SpecificFeed SearchFragment_specificFeedFragment;
    public static SpecificFeed NewsfeedFragment_specificFeedFragment;
    public static SpecificFeed FollowingFragment_specificFeedFragment;
    
    NetworkChecking checkNetwork;
    
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		checkNetwork = new NetworkChecking(getActivity());
		
		layoutInflater = inflater;
		animateFirstListener = new AnimateFirstDisplayListener();
		user_item_rl = (RelativeLayout)layoutInflater.inflate(R.layout.search_user_fragment, container, false);
		user_nick_nametv = (TextView)user_item_rl.findViewById(R.id.follow_user_nice_nametv);
		follow_email_nametv = (TextView)user_item_rl.findViewById(R.id.follow_email_nametv);
		followbtn = (ImageButton)user_item_rl.findViewById(R.id.followbtn);
		search_user_image = (ImageView)user_item_rl.findViewById(R.id.search_user_image);
		
		user_item_rl.setBackgroundColor(Color.WHITE);
	//	user_item_collection = (GridView)user_item_rl.findViewById(R.id.user_item_collection);
		Bundle bundle = this.getArguments();
		String user_nick_name = bundle.getString("user_nick_name");
		String user_email = bundle.getString("user_email");
		String user_avatar_url = bundle.getString("user_avatar_url");
		user_uid = bundle.getString("user_uid");
		user_nick_nametv.setText(user_nick_name);
		follow_email_nametv.setText(user_email);
		setHasOptionsMenu(true);
		Log.i("user_avatar_url",user_avatar_url);
	    new DownloadImageTask(search_user_image).execute(user_avatar_url);
	    
	    //userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
	    //token = userGlobalVariable.getToken();
	    
	    token = sharedpreferences.getToken();
	    
	    itemCollectionArray = getData(token,user_uid);
	    listView = (GridView) user_item_rl.findViewById(R.id.user_item_collection);
		((GridView) listView).setAdapter(new ImageAdapter(itemCollectionArray));
		  //Listen for changes in the back stack
	 
	
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				//specificFeedFragment = new SpecificFeed();
				  if(checkNetwork.isNetworkAvailable()){
				  Bundle bundle = new Bundle();
				     bundle.putString("item_uid", itemCollectionArray.get(position).get("item_uid"));
				     bundle.putString("resource_url", itemCollectionArray.get(position).get("resource_url").toString());
				
				myFragmentManager = getFragmentManager();
				fragmentTransaction = myFragmentManager.beginTransaction();
				
				if(MainActivity.DisplayFragment==Followingfragment.followingfragment_nextFrag){
					
				FollowingFragment_specificFeedFragment = new SpecificFeed();
				MainActivity.processFragmentToShowAndHideOtherFragment(FollowingFragment_specificFeedFragment,fragmentTransaction);
				MainActivity.addFragmentToTransactionAndStack(FollowingFragment_specificFeedFragment,fragmentTransaction);			
				FollowingFragment_specificFeedFragment.setArguments(bundle);
				}
				
				if(MainActivity.DisplayFragment==NewsfeedFragment.Newsfeed_nextFrag){
					
					NewsfeedFragment_specificFeedFragment = new SpecificFeed();
					MainActivity.processFragmentToShowAndHideOtherFragment(NewsfeedFragment_specificFeedFragment,fragmentTransaction);
					MainActivity.addFragmentToTransactionAndStack(NewsfeedFragment_specificFeedFragment,fragmentTransaction);			
					NewsfeedFragment_specificFeedFragment.setArguments(bundle);
					
					}
				
				if(MainActivity.DisplayFragment==SearchFragment.nextFrag){
					
					SearchFragment_specificFeedFragment = new SpecificFeed();
					MainActivity.processFragmentToShowAndHideOtherFragment(SearchFragment_specificFeedFragment,fragmentTransaction);
					MainActivity.addFragmentToTransactionAndStack(SearchFragment_specificFeedFragment,fragmentTransaction);			
					SearchFragment_specificFeedFragment.setArguments(bundle);
					}
				
				
				// fragmentTransaction.replace(R.id.fragmentContainerFrameLayout, specificFeedFragment, TAG_NEWSFEED_FRAGMENT);
				fragmentTransaction.commit();
				  }else{
					  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
				  }
				
				     //specificFeedFragment.setArguments(bundle);
				
			}
		});
		
	
		backbutton =(ImageButton) user_item_rl.findViewById(R.id.backtosearch);
		
		backbutton.setOnClickListener  (new OnClickListener(){ 

            @Override

            public void onClick(View v) {
            	fragmentTransaction = Search_item_click_Fragment.this.getFragmentManager().beginTransaction();
            	
            	/*if(MainActivity.DisplayFragment==SearchFragment.nextFrag){
    			MainActivity.processFragmentToShowAndHideOtherFragment(MainActivity.searchFragment,fragmentTransaction);
    			MainActivity.removeFragmentFromStack(SearchFragment.nextFrag);
    			fragmentTransaction.remove(SearchFragment.nextFrag);
            	}*/
            	MainActivity.backToPreviousFragment(fragmentTransaction);
    			fragmentTransaction.commit();
            	//getFragmentManager().popBackStack();
            	Log.i("onclick","backbutton");
                // TODO Auto-generated method stub
            	
            	
            }         

        }); 
	//	try {
			//String value = new isFollowUser().execute(token,user_uid).get();
			//if (value.equals("true")){
			if(isFollowOrNot(user_uid)){
				followbtn.setImageResource(R.drawable.following_button);
				followbtn.setOnClickListener  (new OnClickListener(){ 

		            @Override

		            public void onClick(View v) {
		            	 if(checkNetwork.isNetworkAvailable()){
		            	new UNFollowUser().execute(token,user_uid);
		                // TODO Auto-generated method stub
		            	 }else{
							  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
						  }
		            	
		            }         

		        }); 
			}else
			{
				followbtn.setImageResource(R.drawable.follow_button);
				followbtn.setOnClickListener  (new OnClickListener(){ 

		            @Override

		            public void onClick(View v) {
		            	 if(checkNetwork.isNetworkAvailable()){
		            	new FollowUser().execute(token,user_uid);
		                // TODO Auto-generated method stub
		            	 }else{
							  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
						  }
		            	
		            }         

		        }); 
			}
	/*	} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	*/
 
	//	((GridView) user_item_collection).setAdapter(new ImageAdapter(getActivity(),itemCollectionArray));
		//Toast.makeText(getActivity(), user_avatar_url, Toast.LENGTH_SHORT).show();
		//  ImageLoader.getInstance().displayImage(user_avatar_url, search_user_image, optionsOfSamllIcon, animateFirstListener);
        return user_item_rl;  
    }  
	
public Boolean isFollowOrNot(String uid){
	Boolean condition = false;
	for(int i=0;i<FacebookLoginFragment.userlist.size();i++){
		if(FacebookLoginFragment.userlist.get(i).equals(uid)){
			condition = true;
		}
	}
	return condition;
}
	
	
	public class ImageAdapter extends BaseAdapter {

		

		private LayoutInflater inflater;
		private ArrayList<HashMap<String, String>> itemCollectionArray;
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		//  private BitmapCache bitmapCache;
		public ImageAdapter( ArrayList<HashMap<String, String>> itemCollectionArray){
		                //  this.inflater = LayoutInflater.from(getActivity());
			inflater = LayoutInflater.from(getActivity());
		                  this.itemCollectionArray = itemCollectionArray;
		              
		                  
		                
		//	View inflate = getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
		             }
		
		ImageAdapter() {
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return itemCollectionArray.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View view = convertView;
			if (view == null) {
				view = inflater.inflate(R.layout.search_user_itemcollection_item, parent, false);
				holder = new ViewHolder();
				assert view != null;
				holder.imageView = (ImageView) view.findViewById(R.id.image);
				holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			ImageLoader.getInstance()
					.displayImage(itemCollectionArray.get(position).get("resource_url").toString(), holder.imageView, options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							holder.progressBar.setProgress(0);
							holder.progressBar.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							holder.progressBar.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							holder.progressBar.setVisibility(View.GONE);
						}
					}, new ImageLoadingProgressListener() {
						@Override
						public void onProgressUpdate(String imageUri, View view, int current, int total) {
							holder.progressBar.setProgress(Math.round(100.0f * current / total));
						}
					});

			return view;
		}
	}

	static class ViewHolder {
		ImageView imageView;
		ProgressBar progressBar;
	}
	private  ArrayList<HashMap<String, String>> getData(String token,String user_uid) {
		
	//	if(userGlobalVariable.getToken() != null){
		if(token != null){	
			try {
				itemCollectionArray=	new SearchForUserName().execute(token,user_uid).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return itemCollectionArray;
		
	}
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class FollowUser extends AsyncTask<String, String,String> {
		 String success ;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String followee_id =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("followee_id", followee_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_FOLLOW_USER, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  success = json.getString("status");
				
			
			
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return success;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			 if(file_url.equals("true")){
           	  followbtn.setImageResource(R.drawable.following_button); 
           	FacebookLoginFragment.userlist.add(user_uid);
             }else{
            	 followbtn.setImageResource(R.drawable.follow_button);  
             }
			
		}

	}

	class UNFollowUser extends AsyncTask<String, String,String> {
		 String success ;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String followee_id =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("followee_id", followee_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_UNFOLLOW_USER, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  success = json.getString("status");
				
			
			
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return success;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
                  if(file_url.equals("true")){
                	  followbtn.setImageResource(R.drawable.follow_button); 
                	  FacebookLoginFragment.userlist.remove(user_uid);
                	
                  }else{
                 	 followbtn.setImageResource(R.drawable.following_button);  
                  }
			
		}

	}
	
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class isFollowUser extends AsyncTask<String, String,String> {
		 String success ;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String followee_id =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("followee_id", followee_id));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_FOLLOW_USER, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			String success;
			try {
				success = json.getString("status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				success = "";
				e.printStackTrace();
			}
			//String value = json.toString();

			return success;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {

			
		}

	}
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class SearchForUserName extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ArrayList<HashMap<String, String>> search_item_info = new ArrayList<HashMap<String, String>>();
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String search_user_uid =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("search_user_uid", search_user_uid));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_SEARCH_USER_ITEM, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
					
					// products found
					// Getting Array of Products
					ItemIdArray = json.getJSONArray("user_items");
					Log.i("ItemIdArray ",""+ItemIdArray.length());
					// looping through All Products
				
					for (int i = 0; i < ItemIdArray.length(); i++) {
						 HashMap<String, String> map = new HashMap<String, String>();
						JSONObject user_item_info = ItemIdArray.getJSONObject(i);
						String item_uid = user_item_info.getString("item_uid");
						String resource_url = user_item_info.getString("resource_url");
					
						map.put("item_uid", item_uid);
						map.put("resource_url", resource_url);
						
						search_item_info.add(map);
					}
					
					Log.i("search_item_info ",""+search_item_info.size());
				}
				else{
					Log.i("Exception","");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return search_item_info;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {

			
		}

	}
	  @Override  
	    public void onResume() {  
	        super.onResume();  
	        Bundle bundle = this.getArguments();
	    //    String user_avatar_url = bundle.getString("user_avatar_url");
	     //   new DownloadFilesTask().execute(user_avatar_url);
	    
	        
	      
	    }  
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		sharedpreferences = new LoginSharedPreferences(getActivity());

		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.imageloading)
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		  ImageView bmImage;

		  public DownloadImageTask(ImageView bmImage) {
		      this.bmImage = bmImage;
		  }

		  protected Bitmap doInBackground(String... urls) {
		      String urldisplay = urls[0];
		      Bitmap mIcon11 = null;
		      try {
		        InputStream in = new java.net.URL(urldisplay).openStream();
		        mIcon11 = BitmapFactory.decodeStream(in);
		      } catch (Exception e) {
		          Log.e("Error", e.getMessage());
		          e.printStackTrace();
		      }
		      return mIcon11;
		  }

		  protected void onPostExecute(Bitmap result) {
		      bmImage.setImageBitmap(result);
		  }
		}

	
}
