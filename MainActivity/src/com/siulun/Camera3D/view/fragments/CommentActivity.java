package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.maxwin.view.XListView;
import me.maxwin.view.XListView.IXListViewListener;

import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CommentActivity extends Activity implements IXListViewListener {

	LoginSharedPreferences sharedpreferences;
	
	private XListView mListView;
	private EditText ed;
	private TextView tv;
	private Handler mHandler;
 
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	// products JSONArray
	JSONArray ItemIdArray = null;

	String item_uid;
	
	public  adapter myadapter =null ;
	//define sharepreference 
    private SharedPreferences settings;
   
    String token;
    
    private static int currentIitemNum = 0;
    ArrayList<HashMap<String, String>> commentList;
    ArrayList<HashMap<String, String>> itemStringArray;
    
    private ImageLoader2 imgLoader;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment_main);
		
		sharedpreferences = new LoginSharedPreferences(this);
		
		// UserInfo info = new UserInfo();
		 
			//if(((UserInfo) this.getApplication()).getToken() !=null){
		if(sharedpreferences.getToken()!=null){
		// token=((UserInfo) this.getApplication()).getToken();
			token=sharedpreferences.getToken();
			}
			
		// init view variable
		mListView = (XListView) findViewById(R.id.comment_lv);
		ed = (EditText) findViewById(R.id.comment_et);
		tv = (TextView) findViewById(R.id.comment_done);
		settings = this.getSharedPreferences("",0);
		//token = settings.getString(TOKEN, "");
	   
		//if(((UserInfo) this.getApplication()).getToken() !=null){
		if(sharedpreferences.getToken()!=null){
		//Toast.makeText(this, ""+token.toString(), Toast.LENGTH_SHORT).show();
	   // Toast.makeText(this, ""+FacebookLoginFragment.info.getToken(), Toast.LENGTH_SHORT).show();
		Log.i("Comments: token", ""+token.toString());}
		commentList= new ArrayList<HashMap<String, String>>();
		mHandler = new Handler();
		imgLoader = new ImageLoader2(this);
		mListView.setPullLoadEnable(true);
		itemStringArray = new ArrayList<HashMap<String, String>>();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		  item_uid = extras.getString("item_uid");	
		  new GetComment(0).execute();
		}
		tv.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ed.getText().toString()!=null&&ed.getText().toString()!=""){
					Log.d("Test Create Comments: ", ed.getText().toString());
					new CreateComment(ed.getText().toString()).execute();
				}else{
					Log.d("Test Create Comments: ", "false");
				}
			}
			
		});
		
	}

	class ViewHolder{
		ImageView image;
		TextView name;
		TextView comment;
		TextView age;
	}
	
	public boolean checkItemIDIsExistOrNot(String ItemUID){
		boolean detect = false;
		if(itemStringArray.size()>0){
			for(int i=0;i<itemStringArray.size();i++){
			if(ItemUID.equalsIgnoreCase(itemStringArray.get(i).get("item_uid"))){
				detect = true;
			}
			}
		}
		
		return detect;
		
	}
	
	class GetComment extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		 
		int condition;
		
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		
		public GetComment(int condition){
		this.condition = condition;	
		}
		
		
		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
		
		
	        //String token = (String)args[0];;
	   
	  //      String item_uid = (String)args[1];;
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("item_uid", item_uid));
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("index", String.valueOf(currentIitemNum)));
			params.add(new BasicNameValuePair("batch_size", String.valueOf(10)));
			
			
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_get_comment, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Comments: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
					JSONObject jsonObject = new JSONObject(json.toString());
					JSONArray info = jsonObject
							.getJSONArray("comments"); // JSON Array
		
					// products found
					// Getting Array of Products
				for(int i=0; i<info.length();i++){
				JSONObject	comment = info.getJSONObject(i);
				//JSONObject	commentInfo = comment.getJSONObject("comment");
				JSONObject	user = comment.getJSONObject("user");
			//	JSONArray	user = comment.getJSONArray("user");	
					// looping through All Products
				
					
						HashMap<String, String> map = new HashMap<String, String>();
						 
						String  comment_item_uid = comment.getString("item_uid");
						String  comment_uid = comment.getString("comment_uid");
						String  comment_create_date = comment.getString("comment_create_date");
						String  comment_content = comment.getString("comment_content");
					/**/	
						String user_uid =  user.getString("user_uid");
						String user_first_name =  user.getString("user_first_name");
						String user_last_name =  user.getString("user_last_name");
						String user_nick_name =  user.getString("user_nick_name");
						String user_email =  user.getString("user_email");
						String user_avatar_url =  user.getString("user_avatar_url");
						
						/*String user_uid =  user.getJSONObject(0).getString("user_uid");
						String user_first_name =  user.getJSONObject(0).getString("user_first_name");
						String user_last_name =  user.getJSONObject(0).getString("user_last_name");
						String user_nick_name =  user.getJSONObject(0).getString("user_nick_name");
						String user_email =  user.getJSONObject(0).getString("user_email");
						String user_avatar_url =  user.getJSONObject(0).getString("user_avatar_url");
				*/
						//	Log.i("user_item_info",""+user_item_info.toString());
						//	Log.i("user_uid",""+user_uid.toString());
						map.put("item_uid", comment_item_uid);
						map.put("comment_uid", comment_uid);
						map.put("comment_create_date", comment_create_date);
						map.put("comment_content", comment_content);
						
						map.put("user_uid", user_uid);
						map.put("user_first_name", user_first_name);
						map.put("user_last_name", user_last_name);
						map.put("user_nick_name", user_nick_name);
						map.put("user_email", user_email);
						map.put("user_avatar_url", user_avatar_url);
						
					
						commentList.add(map);
						
				}
					
					Log.i("commentList ",""+commentList.size());
					
				}
				else{
					Log.i("Exception","");
				}
				 
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return commentList;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {
			Log.i("commentList ",""+file_url.size());
			
			
			
			switch(condition){
			case 0:
				
				if(file_url.size()>0){
					for(int i=0;i<file_url.size();i++){
						itemStringArray.add(file_url.get(i));
					}
					}
				mListView.setXListViewListener(CommentActivity.this);
				myadapter = new adapter(CommentActivity.this);
				mListView.setAdapter(myadapter);
				break;
			case 1:
				
				if(file_url.size()>0){
					for(int i=0;i<file_url.size();i++){
						itemStringArray.add(file_url.get(i));
					}
					}
				myadapter.notifyDataSetChanged();
				/**/
				onLoad();
				break;
			case 2:
				itemStringArray.clear();
				if(file_url.size()>0){
					for(int i=0;i<file_url.size();i++){
						itemStringArray.add(file_url.get(i));
					}
					}
				myadapter = new adapter(CommentActivity.this);
				mListView.setAdapter(myadapter);
				onLoad();
				break;
			default: 
				break;
			}
			
			commentList.clear();
			
			
		}

	}
	
    class CreateComment  extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		 
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
    	String comment_information;
    	public CreateComment(String comment_information){
    		this.comment_information = comment_information;
    	}
    	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
		
		
	        //String token = (String)args[0];;
	   
	  //      String item_uid = (String)args[1];;
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("item_uid", item_uid));
			params.add(new BasicNameValuePair("content", comment_information));
			
			
			// getting JSON string from URL
			//JSONObject json2 = jParser.makeHttpRequest(URL_create_comment, "POST", params);
			// getting JSON string from URL
			  JSONArray json2 = jParser.makeHttpRequestwithArray(URL_create_comment, "POST", params);
			//JSONArray json2 = jParser.makeHttpRequest(URL_create_comment, "POST", params);
			// Check your log cat for JSON reponse
			Log.d("Create Comments: ", json2.toString());
		
			 
			try {
				// Checking for SUCCESS TAG
				JSONObject jsonObj = json2.getJSONObject(0);
				  //String success = json2.getString("status");
				String success = jsonObj.getString("status");
				
				if (success.equals("true")) {
					//JSONObject jsonObject = new JSONObject(json.toString());
					//JSONArray info = jsonObject
						//	.getJSONArray("comments"); // JSON Array
					currentIitemNum = 0;
					new GetComment(2).execute();
			
			/*	for(int i=0; i<info.length();i++){
				JSONObject	comment = info.getJSONObject(i);
			
				JSONObject	user = comment.getJSONObject("user");
		
						HashMap<String, String> map = new HashMap<String, String>();
						 
						String  comment_item_uid = comment.getString("item_uid");
						String  comment_uid = comment.getString("comment_uid");
						String  comment_create_date = comment.getString("comment_create_date");
						
				
						String user_uid =  user.getString("user_uid");
						String user_first_name =  user.getString("user_first_name");
						String user_last_name =  user.getString("user_last_name");
						String user_nick_name =  user.getString("user_nick_name");
						String user_email =  user.getString("user_email");
						String user_avatar_url =  user.getString("user_avatar_url");
				
						map.put("item_uid", comment_item_uid);
						map.put("comment_uid", comment_uid);
						map.put("comment_create_date", comment_create_date);
						map.put("comment_content", comment_information);
						
						map.put("user_uid", user_uid);
						map.put("user_first_name", user_first_name);
						map.put("user_last_name", user_last_name);
						map.put("user_nick_name", user_nick_name);
						map.put("user_email", user_email);
						map.put("user_avatar_url", user_avatar_url);
						
					
						commentList.add(map);
						
				}
				*/	
					Log.i("commentList ",""+commentList.size());
					
				}
				else{
					Log.i("Exception","");
				}
				 
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return commentList;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {
			Log.i("commentList ",""+file_url.size());
			if(file_url.size()>0){
			for(int i=0;i<file_url.size();i++){
				itemStringArray.add(file_url.get(i));
			}
			
			//myadapter = new adapter(CommentActivity.this);
			//mListView.setAdapter(myadapter);
			myadapter.notifyDataSetChanged();
			}
			commentList.clear();
			
			
		}

	}
	
	public void onRefresh() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				currentIitemNum = 0;
				new GetComment(2).execute();
				Log.i("Get into here ", "");
				// get five first

				/*itemStringList.clear();
				if (!checkNetwork(1)) {
					onLoad();
				}*/
				// new LoadProductsByCondition(1).execute(currentIitemNum);

			}
		}, 0);
	}

	private void onLoad() {

		mListView.stopRefresh();
		mListView.stopLoadMore();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String currentDateandTime = sdf.format(new Date());
		mListView.setRefreshTime(currentDateandTime);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				currentIitemNum+=10;
				//currentIitemNum+=currentIitemNum;
				//onLoad();
				new GetComment(1).execute();
			/*	if (!checkNetwork(2)) {
					onLoad();
				}
				*/
				// new LoadProductsByCondition(2).execute(currentIitemNum);

				Log.i("currentIitemNum", String.valueOf(currentIitemNum));

			}
		}, 0);

		// TODO Auto-generated method stub

	}

	class adapter  extends BaseAdapter  {
		  private LayoutInflater mInflater;
			public adapter(Context context){
                this.mInflater = LayoutInflater.from(context);
                           
//	View inflate = getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
           }
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return itemStringArray.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder;
			if(convertView==null){
				 convertView = mInflater.inflate(R.layout.comment_item, null);
				 holder = new ViewHolder();
				 holder.name = (TextView)convertView.findViewById(R.id.comment_username);
				 holder.age = (TextView)convertView.findViewById(R.id.comment_ago);
				 holder.image = (ImageView)convertView.findViewById(R.id.comment_icon);
				 holder.comment = (TextView)convertView.findViewById(R.id.comment_usercomment);
				
				 convertView.setTag(holder);
			}else{
				holder = (ViewHolder)convertView.getTag();
			}
			
			 holder.name.setText(itemStringArray.get(position).get("user_nick_name"));
			 holder.age.setText(itemStringArray.get(position).get("comment_create_date"));
			 imgLoader.DisplayImage(itemStringArray.get(position).get("user_avatar_url"), holder.image);
			 holder.comment.setText(itemStringArray.get(position).get("comment_content"));
			
			return convertView;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.comment, menu);
		return true;
	}

}
