package com.siulun.Camera3D.view.fragments;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.example.gallery.GalleryFragment;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.view.MainActivity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;

public class profile extends Fragment implements OnTabChangeListener {

LoginSharedPreferences sharedpreferences;

	public static GalleryFragment galleryFragment;
	private FragmentTabHost mTabHost;
	ImageView userProfilepic;
	TextView userNickNameTv;
	private Session.StatusCallback callback;
	DisplayImageOptions optionsOfSamllIcon, optionsOfPhoto;
	private UiLifecycleHelper uiHelper;
	private UserInfo userGlobalVariable;
	private ImageLoadingListener animateFirstListener;
	private static boolean notclickeddialog = true;
	public static int page = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
		

sharedpreferences = new LoginSharedPreferences(getActivity());
		
		optionsOfSamllIcon = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.imageloading)
				.showImageForEmptyUri(R.drawable.empty_profile)
				.showImageOnFail(R.drawable.empty_profile).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();
		optionsOfPhoto = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.profile_off)
				.showImageOnFail(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.profile, container, false);

		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);
		mTabHost.setup(getActivity(), getChildFragmentManager(),
				R.id.realtabcontent);
		mTabHost.addTab(
				mTabHost.newTabSpec("fragmentb").setIndicator("Online"),
				Profile_OnlineFragment.class, null);
		mTabHost.addTab(mTabHost.newTabSpec("fragmentc").setIndicator("Local"),
				GalleryFragment.class, null);

		mTabHost.setOnTabChangedListener(this);
		mTabHost.setCurrentTab(1);

		mTabHost.getTabWidget().getChildAt(1)
				.setBackgroundColor(Color.parseColor("#FF8000"));
		mTabHost.getTabWidget().getChildAt(0)
				.setBackgroundColor(Color.parseColor("#6E6E6E"));

		// mTabHost.getTabWidget().getChildAt(0).setBackgroundColor(getResources().getColor(R.drawable.selector_tab_text));
		// mTabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.RED);
		mTabHost.getTabWidget().getChildAt(0).getLayoutParams().height = new TipoDisp()
				.alt_tabs(getActivity());
		mTabHost.getTabWidget().getChildAt(1).getLayoutParams().height = new TipoDisp()
				.alt_tabs(getActivity());
		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {

			final TextView tv = (TextView) mTabHost.getTabWidget()
					.getChildAt(i).findViewById(android.R.id.title);

			// Look for the title view to ensure this is an indicator and not a
			// divider.(I didn't know, it would return divider too, so I was
			// getting an NPE)
			if (tv == null)
				continue;
			else
				tv.setTextColor(0xFFFFFFFF);
		}
		setUserProfilePic(rootView);
		setUserNickNameTv(rootView);
		userProfilepic = getUserProfilePic();
		userProfilepic = (ImageView) rootView
				.findViewById(R.id.camera_imageview_overlayImage);

		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		uiHelper.onSaveInstanceState(bundle);
	}

	private void setUserProfilePic(View ConvertView) {
		userProfilepic = (ImageView) ConvertView
				.findViewById(R.id.camera_imageview_overlayImage);

	}

	private ImageView getUserProfilePic() {

		return userProfilepic;
	}

	private void setUserNickNameTv(View ConvertView) {
		userNickNameTv = (TextView) ConvertView.findViewById(R.id.usernickName);
	}

	private TextView getUserNickNameTv() {

		return userNickNameTv;
	}

	public void upDateFragmentUI() {
		Session session = Session.getActiveSession();
		//userGlobalVariable = ((UserInfo) getActivity().getApplicationContext());
		animateFirstListener = new AnimateFirstDisplayListener();
		if (session != null) {
			Log.i("Login statue ", "" + session.getState());
			session.getState();
			//if (((UserInfo) getActivity().getApplication()).getUser_nick_name() != null) {
				if(sharedpreferences.getUser_nick_name()!=null){
				userNickNameTv = getUserNickNameTv();
			/*	userNickNameTv.setText(((UserInfo) getActivity()
						.getApplication()).getUser_nick_name());
				*/
				userNickNameTv.setText(sharedpreferences.getUser_nick_name());
			} else {
				userNickNameTv = getUserNickNameTv();
				userNickNameTv.setText("");
			}

			Log.i("here inside if to display username", "");
			/*if (((UserInfo) getActivity().getApplication())
					.getUser_avatar_url() != null) {
*/
			if(sharedpreferences.getUserAvatarUrl()!=null){
				
				/*final String pofilpicurl = ((UserInfo) getActivity()
						.getApplication()).getUser_avatar_url();
				*/
				final String pofilpicurl = sharedpreferences.getUserAvatarUrl();
				userProfilepic = getUserProfilePic();
				userProfilepic.setTag(pofilpicurl);

				// new DownloadImagesTask().execute(userProfilepic);
				int width = userProfilepic.getWidth();
				Log.i("width", "" + width);
				/*ImageLoader
						.getInstance()
						.displayImage(
								((UserInfo) getActivity().getApplication())
										.getUser_avatar_url(),
								userProfilepic, optionsOfSamllIcon,
								animateFirstListener);
*/
				ImageLoader
				.getInstance()
				.displayImage(
						sharedpreferences.getUserAvatarUrl(),
						userProfilepic, optionsOfSamllIcon,
						animateFirstListener);
				/*Log.i("((UserInfo) getActivity().getApplication()).getUser_nick_name()",
						""
								+ ((UserInfo) getActivity().getApplication())
										.getUser_nick_name());
										*/
			} else {
				userProfilepic = getUserProfilePic();
				userProfilepic
						.setImageResource(R.drawable.com_facebook_profile_default_icon);
			}

			onSessionStateChange(session, session.getState(), null);
		}
		callback = new Session.StatusCallback() {
			@Override
			public void call(final Session session, final SessionState state,
					final Exception exception) {
				onSessionStateChange(session, state, exception);
			}
		};
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		String user_avatar_url = null;
		String user_nick_name = null;

		/*if (((UserInfo) getActivity().getApplication()).getToken() == null
				|| notclickeddialog) {
*/
		if (sharedpreferences.getToken() == null
				|| notclickeddialog) {
			Builder MyAlertDialog = new AlertDialog.Builder(getActivity());

			MyAlertDialog.setMessage("Please Login first!");

			MyAlertDialog.setPositiveButton("Login",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(getActivity(),
									SettingFragment.class);
							startActivity(intent);
							notclickeddialog = false;
						}
					});
			MyAlertDialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});

		} else {

		}

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (galleryFragment == null) {

			galleryFragment = (GalleryFragment) getChildFragmentManager()
					.findFragmentByTag("fragmentc");

		}

		else {

			galleryFragment.updateGallery();

		}
		// galleryFragment.onResume();
		// galleryFragment.TestToast();
		/* } */
		if (MainActivity.DisplayFragment == MainActivity.Fragmentprofile) {
			if (profile.page == 0) {
				Showpopup showPopup = new Showpopup();
				showPopup
						.showPopup(getActivity(),
								"Long Press photo!!!!!\nExport to gif and delete \nAfter login to upload");
			}
			if (profile.page == 1) {
				Showpopup showPopup = new Showpopup();
				showPopup.showPopup(getActivity(),
						"Long Press photo!!!!!\ndelete the item");
			}
			// uiHelper.onResume();
		}
		upDateFragmentUI();
		uiHelper.onResume();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		AnimateFirstDisplayListener.displayedImages.clear();
		ImageLoader.getInstance().clearMemoryCache();
		ImageLoader.getInstance().clearDiskCache();
		super.onDestroy();
	}

	class TipoDisp {

		public int alt_tabs(Context cont) {
			int alt;
			int dx, dy;
			DisplayMetrics metrics = cont.getResources().getDisplayMetrics();

			dx = metrics.widthPixels;
			dy = metrics.heightPixels;
			if (dx < dy)
				alt = dy / 25;
			else
				alt = dy / 20;

			return alt;
		}
	}

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
			mTabHost.getTabWidget().getChildAt(i)
					.setBackgroundColor(Color.parseColor("#6E6E6E"));
		}
		mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
				.setBackgroundColor(Color.parseColor("#FF8000"));
	}

}
