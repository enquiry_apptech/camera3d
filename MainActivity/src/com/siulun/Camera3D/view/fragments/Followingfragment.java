package com.siulun.Camera3D.view.fragments;



import static com.siulun.Camera3D.model.StringConstants.*;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.maxwin.view.XListView;
import me.maxwin.view.XListView.IXListViewListener;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.siulun.Camera3D.ARModel.SampleCamActivity;
import com.siulun.Camera3D.itemClass.itemDetail;
import com.siulun.Camera3D.itemClass.itemResources;
import com.siulun.Camera3D.itemClass.itemlist;
import com.siulun.Camera3D.itemClass.userDetail;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.*;
import com.siulun.Camera3D.view.fragments.NewsfeedFragment.LoadProductsByCondition;

import com.siulun.Camera3D.R;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Followingfragment extends Fragment implements IXListViewListener{
	
	LoginSharedPreferences sharedpreferences;
	
	private UiLifecycleHelper uiHelper;
	private static boolean notclickeddialog =true;
	private LayoutInflater layoutInflater;
	private RelativeLayout follow_user__rl;
	
	
	//Define view objects
    private ProgressBar pbar;
    private XListView mListView;
    
    //define width and height of display of array
  	int[] dis;
  	
    //define memory cache list
  	//public ArrayList<HashMap<String, String>> Item_info ;
  	
  	public ArrayList<itemlist> Item_info2 ;
  	
  	
  	public ArrayList<String> itemStringList = null;
  	
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	// products JSONArray
	JSONArray ItemIdArray = null;
  	
    // tempory list for refresh 
 	//public ArrayList<HashMap<String, String>> Item_info_tem ;
 	
	public ArrayList<itemlist> Item_info_tem2 ;
	
 	//define scoll listview handler
 	private Handler mHandler;
 	//define custom imageloader
    private ImageLoader2 imgLoader;
  //define adapter
  	public  MyAdapter myadapter =null;
    //define current number of item
	private static String currentIitemNum = "5";
	
	public static Search_item_click_Fragment followingfragment_nextFrag;
	private FragmentTransaction fragmentTransaction;
	
    //define session
    Session session;
 	
    //define sharepreference 
  //  private SharedPreferences settings;
  //init network checking class
  		NetworkChecking checkNetwork;
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		
		
		//settings = getActivity().getSharedPreferences("",0);
		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		// init view
		follow_user__rl = (RelativeLayout)layoutInflater.inflate(R.layout.following_fragment, container, false);
		follow_user__rl.setBackgroundColor(Color.WHITE); 
		//init view objects
		pbar = (ProgressBar) follow_user__rl.findViewById(R.id.follow_progress);
		mListView  = (XListView) follow_user__rl.findViewById(R.id.follow_item_list);
		//init handler for scoll listview
		mHandler = new Handler();
		 // initilize list objects
		Item_info2 = new ArrayList<itemlist>();
		Item_info_tem2 = new ArrayList<itemlist>();
		//Item_info = new ArrayList<HashMap<String, String>>();
		//Item_info_tem = new ArrayList<HashMap<String, String>>();
		itemStringList = new ArrayList<String>();
		//get device width and height
		dis = getDisplayWidthAndHeight();
		// init custom imageloader
	    imgLoader = new ImageLoader2(this.getActivity());
	    //set listview can reload
	    mListView.setPullLoadEnable(true);
	    session = Session.getActiveSession();
	   
	    
		return follow_user__rl;  
    }  
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	        @Override
	        public void call(final Session session, final SessionState state, final Exception exception) {
	            onSessionStateChange(session, state, exception);
	        }
	    };
	    
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        sharedpreferences = new LoginSharedPreferences(getActivity());
	        uiHelper = new UiLifecycleHelper(getActivity(), callback);
	        uiHelper.onCreate(savedInstanceState);
	    }
	 @Override
	    public void onResume() {
	        super.onResume();
	        
	        // For scenarios where the main activity is launched and user
			// session is not null, the session state change notification
			// may not be triggered. Trigger it if it's open/closed.
	        if(MainActivity.DisplayFragment==MainActivity.followingFragment){
	        session = Session.getActiveSession();

	        if (session != null  ) {
	        	Log.i("Login statue ", ""+session.getState());	        	
	            onSessionStateChange(session, session.getState(), null);
	        }
	       
			   uiHelper.onResume();
	        }
	        
	    }
	 
	    private Boolean onSessionStateChange(Session session, SessionState state, Exception exception) {
	    	Boolean condition = false;
	    	Log.i("Loggedin folloiwng", state.equals("CREATED")+"");
	        //if(((UserInfo) getActivity().getApplication()).getToken()==null){
	        	if(sharedpreferences.getToken()==null){
	    	//if (state.equals("CREATED_TOKEN_LOADED") || !session.isOpened() ) {
	        	if(itemStringList.size()!=0){
	        		imgLoader.clearCache();
	    			//Item_info.clear();
	    			Item_info2.clear();
	    			itemStringList.clear();
	    			currentIitemNum = "5";
	    			myadapter = new MyAdapter(getActivity(),Item_info2,mListView);
					mListView.setAdapter(myadapter);
	        	}
	    		Builder MyAlertDialog = new AlertDialog.Builder(getActivity());
	    		
	    	//	MyAlertDialog.setTitle("");
	    		notclickeddialog = true;
	    		MyAlertDialog.setMessage("Please Login first!");    		
	    		
	    	
	    		MyAlertDialog.setPositiveButton("Login", new DialogInterface.OnClickListener() {  
	    		    public void onClick(DialogInterface dialog, int which) {  
	    		    	
	    		    	Intent intent = new Intent(getActivity(),SettingFragment.class);
	    				  startActivity(intent);
	    				  notclickeddialog = false;
	    		    }  
	    		}); 
	    		MyAlertDialog.setNegativeButton("Cancel" , new DialogInterface.OnClickListener() {  
	    		    public void onClick(DialogInterface dialog, int which) {  
	    		    	
	    		    }  
	    		}); 
	    		if(notclickeddialog){
	    			condition = false;
	    			pbar.setVisibility(View.INVISIBLE);
	    			MyAlertDialog.show();
	    		}
	    		
	        } else{
	        	condition = true;
	        	if(itemStringList.size()==0){
	            pbar.setVisibility(View.VISIBLE);
	        	checkNetwork(0);
	        	}
	        }
	    	return condition;
	    }

	
	    
	 @Override
	    public void onPause() {
	        super.onPause();
	        uiHelper.onPause();
	    }

		public int[] getDisplayWidthAndHeight(){
			int[] dis = new int[2];
			Display display = getActivity().getWindowManager().getDefaultDisplay();
			dis[0] = display.getWidth();
			dis[1] = display.getHeight();
			return dis;
			
		}

		class GetUserAllAtOneBy extends AsyncTask<String, String, userDetail> {
			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			//private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
			int condition,position;
			userDetail	JSONuserDetail = null;
			itemResources JSONitemResources = null;
			itemDetail JSONitemDetail = null;
			HashMap<String, String> map;
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				map = new HashMap<String, String>();	 
			}
			public GetUserAllAtOneBy	(int condition, int position){
				this.position = position;
				this.condition = condition;}	/**/
			public itemDetail getItemDetail(String a){
				itemDetail	JSONitemDetail = null;
				
				Log.i("doInBackground in GetItemDetailByID","HEREEEEEEE");
				try {
					String item_uid = a;
					Log.i("arg",""+item_uid);
					HttpClient httpClient = new DefaultHttpClient();

					HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace with
																			// your url
					List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
					nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
					//if(((UserInfo) getActivity().getApplication()).getToken()!=null){
					if(sharedpreferences.getToken()!=null){
						nameValuePair.add(new BasicNameValuePair("token", sharedpreferences.getToken()));
					}
					// Encoding data

					try {
						httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
					} catch (UnsupportedEncodingException e) {
						// log exception
						e.printStackTrace();
					}

			//Make request

					try {
				    	StringBuilder builder = new StringBuilder();
						HttpResponse response = httpClient.execute(httpPost);
			    		StatusLine statusLine = response.getStatusLine();
			    		int statusCode = statusLine.getStatusCode();
			    		if(statusCode == 200){
			    			HttpEntity entity = response.getEntity();
			    			InputStream content = entity.getContent();
			    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			    			String line;
			    			while((line = reader.readLine()) != null){
			    				builder.append(line);
			    			}
			    		} else {
			    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
			    		}
						// write response to log
						Log.d("Http Post Response:", builder.toString());
						JSONObject jsonObject = new JSONObject(builder.toString());
						JSONArray productObj = jsonObject
								.getJSONArray("info"); // JSON Array
						
						JSONObject iteminfo = productObj.getJSONObject(0);
					
						//String items = iteminfo.getString("items");
											
						JSONObject itemJson = iteminfo.getJSONObject("items");
						Log.i("itemJson",itemJson.toString());
					//	int like_count = iteminfo.getInt("like_count");
						String like_count = iteminfo.getString("like_count");
						Log.i("like_count",""+like_count);
						String comment_count = iteminfo.getString("comment_count");
						String share_count = iteminfo.getString("share_count");
						String view_count = iteminfo.getString("view_count");
						String item_description = itemJson.getString("item_description");
						String item_isLiked = itemJson.getString("item_isLiked");
						String item_create_date = 	itemJson.getString("item_create_date");
						String item_name = itemJson.getString("item_name");
						Log.i("item_create_date",item_create_date);
						Log.i("item_description",item_description+item_create_date+item_name);
						JSONObject userinfo = itemJson.getJSONObject("user");
						Log.i("userinfo",""+userinfo);
						
						String user_id = userinfo.getString("user_uid");
					
						 JSONitemDetail = new itemDetail(item_uid,user_id,item_create_date,item_name,item_description,like_count,comment_count,share_count,view_count,item_isLiked);
						 
						
						
						
						//String userinfo = iteminfo.getString("user");
						Log.i("userinfo",userinfo.toString());
					} catch (ClientProtocolException e) {
						// Log exception
						e.printStackTrace();
					} catch (IOException e) {
						// Log exception
						e.printStackTrace();
					}

				}catch (Exception e){
					e.printStackTrace();
				}
				return JSONitemDetail;
			}
			public itemResources getItemResource(String a){
				itemResources	JSONitemRes = null;
				JSONObject jsonObject = null;;
				Log.i("doInBackground in GetItemResiurces","HEREEEEEEE");
				try {
					String item_uid = a;
					//Log.i("arg",""+item_uid);
					HttpClient httpClient = new DefaultHttpClient();

					HttpPost httpPost = new HttpPost(URL_GET_ITEM_RESOURCES); // replace with
																			// your url
					List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
					nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
					// Encoding data

					try {
						httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
					} catch (UnsupportedEncodingException e) {
						// log exception
						e.printStackTrace();
					}

			//Make request

					try {
				    	StringBuilder builder = new StringBuilder();
				    	
						HttpResponse response = httpClient.execute(httpPost);
			    		StatusLine statusLine = response.getStatusLine();
			    		int statusCode = statusLine.getStatusCode();
			    		if(statusCode == 200){
			    			HttpEntity entity = response.getEntity();
			    			InputStream content = entity.getContent();
			    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			    			String line;
			    			while((line = reader.readLine()) != null){
			    				builder.append(line);
			    			}
			    		} else {
			    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
			    		}
						// write response to log
						Log.d("Http Post Response:", builder.toString());
						if(builder.length()>0){
							jsonObject = new JSONObject(builder.toString());
							JSONArray resourcesArray = jsonObject
									.getJSONArray("resources"); // JSON Array
							
							JSONObject resources = resourcesArray.getJSONObject(0);
							String resource_index = resources.getString("resource_index");
							String resource_uid = resources.getString("resource_uid");
							String type_uid = resources.getString("type_uid");
							String resource_url = resources.getString("resource_url");
							
							JSONitemRes  = new itemResources(item_uid,resource_uid,type_uid,resource_index,resource_url);
							
						Log.i("resource",""+resources);
										//String items = iteminfo.getString("items");
						}
						else{
							JSONitemRes = null;
						}
						
					
					} catch (ClientProtocolException e) {
						// Log exception
						e.printStackTrace();
					} catch (IOException e) {
						// Log exception
						e.printStackTrace();
					}

				}catch (Exception e){
					e.printStackTrace();
				}
				return JSONitemRes;
			}
	        public userDetail getJSONuser(String a){
	        	try {
					String item_uid = a;
					Log.i("arg",""+item_uid);
					HttpClient httpClient = new DefaultHttpClient();

					HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace with
																			// your url
					List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
					nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
					// Encoding data

					try {
						httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
					} catch (UnsupportedEncodingException e) {
						// log exception
						e.printStackTrace();
					}

			//Make request

					try {
				    	StringBuilder builder = new StringBuilder();
						HttpResponse response = httpClient.execute(httpPost);
			    		StatusLine statusLine = response.getStatusLine();
			    		int statusCode = statusLine.getStatusCode();
			    		if(statusCode == 200){
			    			HttpEntity entity = response.getEntity();
			    			InputStream content = entity.getContent();
			    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			    			String line;
			    			while((line = reader.readLine()) != null){
			    				builder.append(line);
			    			}
			    		} else {
			    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
			    		}
						// write response to log
					
						JSONObject jsonObject = new JSONObject(builder.toString());
						JSONArray productObj = jsonObject
								.getJSONArray("info"); // JSON Array
						
						JSONObject iteminfo = productObj.getJSONObject(0);
					
						//String items = iteminfo.getString("items");
											
						JSONObject itemJson = iteminfo.getJSONObject("items");
						
						
						JSONObject userinfo = itemJson.getJSONObject("user");
						Log.i("userinfo",""+userinfo);
						
						String user_id = userinfo.getString("user_uid");
						String user_first_name = userinfo.getString("user_first_name");
						String user_last_name = userinfo.getString("user_last_name");
						String user_nick_name = userinfo.getString("user_nick_name");
						String user_email =  userinfo.getString("user_email");
						String user_avatar_url =   userinfo.getString("user_avatar_url");
			 
						JSONuserDetail = new userDetail(user_id,user_first_name,user_last_name,user_nick_name,user_email,user_avatar_url);
						
					
					} catch (ClientProtocolException e) {
						// Log exception
						e.printStackTrace();
					} catch (IOException e) {
						// Log exception
						e.printStackTrace();
					}

				}catch (Exception e){
					e.printStackTrace();
				}
	        	return JSONuserDetail;
	        }
			/**
			 * Getting product details in background thread
			 * */
	        
	        
public void addToMap(itemDetail idetail,itemResources iresourse, userDetail udetail){
	        	if(iresourse!=null){	
	 			   map.put("resource_url", iresourse.getResource_url());
	 			}
	 			if(udetail!=null){
	 				map.put("user_name", udetail.getUser_first_name()+" "+udetail.getUser_last_name());		
	 				map.put("user_nick_name", udetail.getUser_nick_name());
	 				map.put("user_email", udetail.getUser_email());
	 				map.put("user_avatar_url", udetail.getUser_avatar_url());
	 				}
	 			
	 				Log.i("user_avatar_url",""+udetail.getUser_avatar_url());

	 			
	 			
	 				if(idetail!=null){
	 	
	 	map.put("item_uid", idetail.getItem_uid());
	 	map.put("user_id", idetail.getUser_id());
	 	map.put("item_create_date", idetail.getstr_item_create_date());
	 	map.put("item_name", idetail.getItem_name());
	 	map.put("item_description", idetail.getItem_description());
	 	map.put("like_count", idetail.getLike_count());
	 	map.put("comment_count", idetail.getComment_count());
	 	map.put("share_count", idetail.getShare_count());
	 	map.put("view_count", idetail.getView_count());

	 				}
	 			if(condition==1){
	 				//Item_info_tem.add(map);
	 			}else{
	 				
	 //	Item_info.add(map);
	 	
	 			} 
	        }
	    
    public void addToMap2(itemDetail idetail,itemResources iresourse, userDetail udetail){
	        	if(condition==1){
	        		itemlist list = new itemlist(JSONitemDetail,JSONitemResources,JSONuserDetail);
	 				Item_info_tem2.add(list);
	 			}else{
	 				
	 				itemlist list = new itemlist(JSONitemDetail,JSONitemResources,JSONuserDetail);
					Item_info2.add(list);
	 	
	 			} 
	        }
			protected userDetail doInBackground(String... arg) {
				
				//uJSONuserDetail = null;
				JSONitemDetail = getItemDetail(arg[0]);
				JSONuserDetail = getJSONuser(arg[0]);
				JSONitemResources = getItemResource(arg[0]);
				
			

				return JSONuserDetail;
			}


			/**
			 * After completing background task Dismiss the progress dialog
			 * **/
			protected void onPostExecute(userDetail file_url) {
			//	Toast.makeText(getActivity(),"finished "+ position+Item_info2.size(), Toast.LENGTH_SHORT).show();
				if(JSONitemDetail!=null&&JSONitemResources!=null&&JSONuserDetail!=null){
				addToMap2(JSONitemDetail,JSONitemResources,JSONuserDetail);
				//Toast.makeText(getActivity(),"finished "+ position+Item_info2.size(), Toast.LENGTH_SHORT).show();
	            if(Item_info2.size()==itemStringList.size()&&condition==0){
	            	pbar.setVisibility(View.INVISIBLE);
					mListView.setVisibility(View.VISIBLE);
					myadapter = new MyAdapter(getActivity(),Item_info2,mListView);
					mListView.setAdapter(myadapter);
					mListView.setXListViewListener(Followingfragment.this);
	            }
	            if(Item_info_tem2.size()==itemStringList.size()&&condition==1){
	            	//myadapter = new MyAdapter(getActivity(),Item_info,mListView);
					//mListView.setAdapter(myadapter);
	            	Item_info2.clear();
	            	for(int i=0;i<Item_info_tem2.size();i++){
	            		Item_info2.add(Item_info_tem2.get(i));
	            	}
	            	Item_info_tem2.clear();
	            	myadapter = new MyAdapter(getActivity(),Item_info2,mListView);
					mListView.setAdapter(myadapter);
					onLoad();
	            }
	            if(Item_info2.size()==itemStringList.size()&&condition==2){
	            	myadapter.item_detail = Item_info2;
	            	myadapter.notifyDataSetChanged();
					onLoad();
	            }
			}
		}
	}
		
		class GetFolloweesItemIdByCondition extends AsyncTask<String, String, String[]>{
			
			int condition;
			public GetFolloweesItemIdByCondition(int condition){
				this.condition = condition;
			}
			protected void onPreExecute() {
				//pbar.setVisibility(View.VISIBLE);
				//mListView.setVisibility(View.INVISIBLE);
				super.onPreExecute();
			}
			protected String[] doInBackground(String... args) {
				String[] itemlist = null;
								
				//String token = settings.getString(TOKEN, "");
				//String token = FacebookLoginFragment.info.getToken();
				//String token = ((UserInfo) getActivity().getApplication()).getToken();
				String token = sharedpreferences.getToken();
				//String index = "0";
				int indexnumber = Integer.parseInt(args[0])-5;
				String index = String.valueOf(indexnumber);
				String batch_size = "5";
				
			    //String batch_size = (String)args[0];
				
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("token", token));
				params.add(new BasicNameValuePair("index", index));
				params.add(new BasicNameValuePair("batch_size", batch_size));
				// getting JSON string from URL
				JSONObject json = jParser.makeHttpRequest(URL_get_following_feeds, "POST", params);
				
				// Check your log cat for JSON reponse
				Log.d("All item: ", json.toString());
				//Log.d("All itemNumer: ", itemNumer+"");
				
				try {
					// Checking for SUCCESS TAG
				
					  String success = json.getString("status");
					
					if (success.equals("true")) {
						
						// products found
						// Getting Array of Products
						ItemIdArray = json.getJSONArray("items");
						Log.i("ItemIdArray ",""+ItemIdArray.length());
						// looping through All Products
						//itemStringArray =  new String[ItemIdArray.length()];
						itemlist  =  new String[ItemIdArray.length()];
						for (int i = 0; i < ItemIdArray.length(); i++) {
						
						//itemStringArray[i]= ItemIdArray.getString(i);
						itemlist[i]  =  ItemIdArray.getString(i);
						
								//Log.i("itemStringArray",""+itemStringArray[i]);
						Log.i("itemStringArray",""+itemlist[i]);
						}
					}
					else{
						Log.i("Exception","");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
			//	return itemStringArray;
				return itemlist;
				
			}
			protected void onPostExecute(String[] file_url) {
				if(file_url!=null){
					if(file_url.length>0){
		            for (int i=0; i<file_url.length; i++){	
		            	//for(int b = 0; b<itemStringList.size();b++){
		            	//if(file_url[i]!=itemStringList.get(b)){	
		            	itemStringList.add(file_url[i]);
		            	new GetUserAllAtOneBy(condition,itemStringList.size()-1).execute(file_url[i]);
		            	//}
		            	//}
		            }
			    }else{    
		            	if(condition==1){
		            		Item_info2.clear();
		            		myadapter = new MyAdapter(getActivity(),Item_info2,mListView);
							mListView.setAdapter(myadapter);
		            		onLoad();
		            	}
		            	if(condition==2){
		            		onLoad();
		            	}

		            	pbar.setVisibility(View.INVISIBLE);
	            	
			    }
				}else{
					if(condition==1){
						Item_info2.clear();
	            		myadapter = new MyAdapter(getActivity(),Item_info2,mListView);
						mListView.setAdapter(myadapter);
	            		onLoad();
	            	}
					if(condition==2){
	            		onLoad();
	            	}

	            	pbar.setVisibility(View.INVISIBLE);
				}
		}
		}

		 class CreateLike extends AsyncTask<String, String, Boolean> {
			    
		    	String item_uid;
		    	String like_count;
		    	ImageView img;
		    	TextView like_view;
		    	int position;
		    	
		    	public CreateLike(String item_uid, ImageView img, String like_count, TextView like_view, int position){
		    		this.item_uid = item_uid;
		    		this.img = img;
		    		this.like_count = like_count;
		    		this.like_view = like_view;
		    		this.position = position;
		    	}

		    	protected void onPreExecute() {
		    		
		    	}
		    	protected Boolean doInBackground(String... args) {
		    		String[] itemlist = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();
					
					//params.add(new BasicNameValuePair("token",  ((UserInfo) getActivity().getApplication()).getToken()));
			params.add(new BasicNameValuePair("token",  sharedpreferences.getToken()));
					params.add(new BasicNameValuePair("item_uid", item_uid));

					Boolean condition = false;
					
					// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_create_like, "POST", params);
					
					// Check your log cat for JSON reponse
					Log.d("All likes: ", json.toString());
					
					
					try {
						// Checking for SUCCESS TAG
					
						  String success = json.getString("status");
						
						if (success.equals("true")) {
						
							condition = true;
							
						}
						else{
							Log.i("Exception","");
						}
						 
					} catch (JSONException e) {
						e.printStackTrace();
					}
		    		return condition;
		    	}
		    	

		    	protected void onPostExecute(Boolean condition) {
		    		if(condition){
		    		img.setImageResource(R.drawable.like_on);
		    		Item_info2.get(position).getiDetail().setLike_count(String.valueOf(Integer.parseInt(like_count)+1));
		    		Item_info2.get(position).getiDetail().setItem_isLiked("1");
		    		like_view.setText(Item_info2.get(position).getiDetail().getLike_count());
		    		}
		    	}
		    }
		    
		    class AddViewNumber extends AsyncTask<String, String, Boolean> {
		        
		    	String item_uid;
		    	TextView view_count;
		    	String count_number;
		    	int position;
		    	
		    	public AddViewNumber(String item_uid,TextView view_count, String count_number,int position){
		    		this.item_uid = item_uid;
		    		this.view_count = view_count;
		    		this.count_number = count_number;
		    		this.position = position;
		    	}

		    	protected void onPreExecute() {
		    		
		    	}
		    	protected Boolean doInBackground(String... args) {
		    		String[] itemlist = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();
					
			params.add(new BasicNameValuePair("item_uid", item_uid));

					Boolean condition = false;
					
					// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_add_view_item_number, "POST", params);
					
					// Check your log cat for JSON reponse
					Log.d("All view_item_number : ", json.toString());
					
					
					try {
						// Checking for SUCCESS TAG
					
						  String success = json.getString("status");
						
					
						
							condition = true;
							
						
						 
					} catch (JSONException e) {
						e.printStackTrace();
						condition = false;
					}
		    		return condition;
		    	}
		    	

		    	protected void onPostExecute(Boolean condition) {
		    		if(condition){
		    			Item_info2.get(position).getiDetail().setView_count(String.valueOf(Integer.parseInt(count_number)+1));
		    			view_count.setText(String.valueOf(Integer.parseInt(count_number)+1));
		    		}
		    	}
		    }
		   
			private void showDialog(final int condition){
				new AlertDialog.Builder(getActivity()).setMessage("No Network Connection!")
				.setCancelable(false)
				.setNegativeButton("cancel", new android.content.DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						//android.os.Process.killProcess(android.os.Process.myPid());
					}
					
				})
				.setPositiveButton("reconnect",new android.content.DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						checkNetwork(condition);
					}
					
				})
				.show();
				
				}

			private Boolean checkNetwork(int condition){
				final Boolean network = checkNetwork.isNetworkAvailable();
				if (network){
					
		      	    	new GetFolloweesItemIdByCondition(condition).execute(currentIitemNum);
				  
				}			
				else {
					showDialog(condition);
					pbar.setVisibility(View.INVISIBLE);
				}
				return network;
			}
					  
		
	    public class ViewHolder {
	    	public TextView timetv;
			public TextView userNameTv;
			public TextView titletv;
			public ImageView profileImage,NewsFeedImage,CommentImage,likeImage , ViewImageView;
			public AdView adView ;
			private LinearLayout layout_ad;
			public TextView like_count;
			public TextView view_count;
			public TextView comment_count;
			public ProgressBar pbar;
			public LinearLayout layout;
			public LinearLayout itemClickLayout;
			public TextView ARModel;
			//public ImageView image;
		}

		private class MyAdapter extends BaseAdapter  {
			
			  private LayoutInflater mInflater;
			  private  ArrayList<itemlist> item_detail; 
			  private ListView listview;
			 // private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
			  WebView webView;
				private ArrayList<PublisherAdView> adList=new ArrayList<PublisherAdView>();
				private ArrayList<ViewGroup> viewList=new ArrayList<ViewGroup>();
				
			//  private BitmapCache bitmapCache;
			public MyAdapter(Context context, ArrayList<itemlist> item_detail,ListView listview){
			                  this.mInflater = LayoutInflater.from(context);
			                  this.item_detail = item_detail;
			                  this.listview = listview; 
			                  
			                
			//	View inflate = getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
			             }
			  public int getCount() {
			//return itemStringList.size();
			return item_detail.size();
			//return Integer.valueOf(currentIitemNum);
			  }
			
			  @Override
			  public Object getItem(int position) {
			  return position;
			  }
			  @Override
			  public long getItemId(int position) {
			  return position;
			  }
			
			
				  @Override
				  public View getView(int position, View convertView, ViewGroup parent) {
					  final ViewHolder holder;
					  final View view = convertView;
					  final int position2 = position;
				//	  Toast.makeText(getActivity(),""+ position, Toast.LENGTH_SHORT).show();
					  if(convertView == null ||convertView instanceof PublisherAdView){
						//	view = getLayoutInflater().inflate(R.layout.testnewfeed, parent, false);
						 convertView = mInflater.inflate(R.layout.testnewfeed, null);
					//	 Toast.makeText(getActivity(), "start"+position, Toast.LENGTH_SHORT).show();
						  holder = new ViewHolder();
						  holder.timetv = (TextView) convertView.findViewById(R.id.timestampTextView);
						  holder.userNameTv =(TextView) convertView.findViewById(R.id.usernameTextView);
						  holder.titletv = (TextView) convertView.findViewById(R.id.titleTextView);
						  holder.profileImage = (ImageView)convertView.findViewById(R.id.image);
						//  holder.profileImage = (FadeInNetworkImageView) convertView.findViewById(R.id.profilePicImageView);
						  holder.NewsFeedImage = (ImageView) convertView.findViewById(R.id.NewsFeedImage);
						holder.layout_ad = (LinearLayout) convertView.findViewById(R.id.layout_item);
						holder.likeImage = (ImageView) convertView.findViewById(R.id.likeImageView);				
						holder.like_count = (TextView) convertView.findViewById(R.id.likeCountTextView);
						holder.ViewImageView = (ImageView) convertView.findViewById(R.id.viewImageView);	 
						holder.view_count = (TextView) convertView.findViewById(R.id.viewCountTextView);	  
						holder.comment_count = (TextView) convertView.findViewById(R.id.commentCountTextView);
						holder.CommentImage = (ImageView)convertView.findViewById(R.id.commentImageView);
						holder.ARModel = (TextView) convertView.findViewById(R.id.TextAR);
						//holder.pbar = (ProgressBar) convertView.findViewById(R.id.progressBar1);
						holder.layout = (LinearLayout) convertView.findViewById(R.id.layout_item);
						holder.itemClickLayout = (LinearLayout) convertView.findViewById(R.id.itemCardLinearLayout);
						RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)holder.layout.getLayoutParams();
						param.height = dis[1]*3/4;
						holder.layout.setLayoutParams(param);
						
						  convertView.setTag(holder);
						  
						  if(position == 0 || position == 3){
							  Log.i("if statment in adview",position+"");
							
								// LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.layout_item);
								  holder.adView  = new AdView(getActivity());
								  holder.adView.setAdUnitId("ca-app-pub-8709149684611651/2135536124");
								  holder.adView.setAdSize(AdSize.BANNER);
								  AdRequest request = new AdRequest.Builder()
								    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
								    .build();
							
								  holder.layout.addView(holder.adView);
								  AdRequest adRequest = new AdRequest.Builder()
								    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
								    .addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
								    .build();

								// Start loading the ad in the background.
								  holder.adView.loadAd(adRequest);
								/*  */
								  
								  convertView.setTag(holder);
					
						  } 
						 	
			  	 		
						 /* if(itemStringList.size()>position){
							 new GetOneItemThread(holder,itemStringList.get(position),position).execute(); 
		 				  }*/
						  //new LoadOneProducts(holder,position).execute();
		 				//  new GetOneItemThread(holder,itemStringList.get(position),position).execute(); 
						 // new GetItemThread(position).start();
						 // new GetOneItemThread(holder,itemStringList.get(position),position).execute(); 
					  }
					  else{ 
						  holder = (ViewHolder) convertView.getTag();
					  }
				  /*if(itemStringList.size()>position&&item_detail.size()<=position){
			
						  new GetUserAllAtOne(holder,position).execute(itemStringList.get(position));

					  }else{
						  if(item_detail.size()>position){
					  */
								 holder.timetv.setText(item_detail.get(position2).getiDetail().getstr_item_create_date());
						  
						  String nameString = item_detail.get(position2).getuDetail().getUser_nick_name();
						  if(nameString.length() > 20) { // show part of the user name if user name is too long
							  nameString = nameString.substring(0, nameString.length()*2/3)+"...";
						  }
						  
						  holder. userNameTv.setText(nameString);
						  
						  //holder.titletv.setText(item_detail.get(position2).get("item_description").toString());
						  holder.titletv.setText(item_detail.get(position2).getiDetail().getItem_description());
						  //holder.titletv.setText("test item description "+position);
					      
						 
						  //final String item_uid = item_detail.get(position2).get("item_uid").toString();
						  final String item_uid = item_detail.get(position2).getiDetail().getItem_uid();
						  holder.NewsFeedImage.setOnClickListener(new View.OnClickListener() {
			                  public void onClick(View v) {
			                	  Intent intent = new Intent(getActivity(), WebViewActivity.class);
			                	  intent.putExtra("item_uid",item_uid );
			                	  Log.i("item_uid intent",item_uid);
			                	  startActivity(intent);
			                   
			                  }    
			                            
			              });
					

							  
							//	ImageLoader.getInstance().displayImage(item_detail.get(position2).get("user_avatar_url"), holder.profileImage, optionsOfSamllIcon, animateFirstListener);
							//	ImageLoader.getInstance().displayImage(item_detail.get(position2).get("resource_url"), holder.NewsFeedImage, optionsOfPhoto, animateFirstListener);
						  imgLoader.DisplayImage(item_detail.get(position2).getuDetail().getUser_avatar_url(), holder.profileImage);
						  imgLoader.DisplayImage(item_detail.get(position2).getiResources().getResource_url(), holder.NewsFeedImage);
						  // imgLoader.DisplayImage("", holder.NewsFeedImage);
						  holder.like_count.setText(item_detail.get(position2).getiDetail().getLike_count());
						  holder.view_count.setText(item_detail.get(position2).getiDetail().getView_count());
						  holder.comment_count.setText(item_detail.get(position2).getiDetail().getComment_count());
						  
						 // final String view_count = item_detail.get(position2).get("view_count").toString();
						  final String view_count = item_detail.get(position2).getiDetail().getView_count();
						  
						  holder.CommentImage.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									if(checkNetwork.isNetworkAvailable()){
									if(uiHelper==null){
									 uiHelper = new UiLifecycleHelper(getActivity(), callback);
									}
									  if (session != null  ) {
								        	Log.i("Login statue ", ""+session.getState());
								        	session.getState(); 
								        	
								            if(onSessionStateChange(session, session.getState(), null)){
								            	Intent intent = new Intent(getActivity(), CommentActivity.class);
												intent.putExtra("item_uid", item_uid);								
												getActivity().startActivity(intent);
								            }
								            
								        }
									  }else{
								    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
								
								        }
								}
						  });

						  holder.ViewImageView.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									if(checkNetwork.isNetworkAvailable()){
								new	AddViewNumber(item_uid, holder.view_count ,view_count,position2).execute();
								  Intent intent = new Intent(getActivity(), WebViewActivity.class);
			                	  intent.putExtra("item_uid",item_uid );
			                	  Log.i("item_uid intent",item_uid);
			                	  startActivity(intent);
									}else{
							    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
							
							        }
								}
						  });
						  if(Integer.parseInt(item_detail.get(position2).getiDetail().getItem_isLiked())==1){
						  holder.likeImage.setImageResource(R.drawable.like_on);
						  }else{
							  holder.likeImage.setImageResource(R.drawable.like_off);  
						  }
						  
						  holder.likeImage.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
								
									if(checkNetwork.isNetworkAvailable()){
								if(uiHelper==null){
									 uiHelper = new UiLifecycleHelper(getActivity(), callback);
									}
									  if (session != null  ) {
								        	Log.i("Login statue ", ""+session.getState());
								        	session.getState(); 
								        	
								            if(onSessionStateChange(session, session.getState(), null)){
								            	new	CreateLike(item_uid,holder.likeImage,item_detail.get(position2).getiDetail().getLike_count(),holder.like_count,position2).execute();
								            }
								            
								        }
									}else{
									    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
											
								        }
								}
						  });
						  
						  holder.ARModel.setOnClickListener(new OnClickListener(){

							@Override
							public void onClick(View v) {
								if(checkNetwork.isNetworkAvailable()){
								Intent intent = new Intent(getActivity(), SampleCamActivity.class);
								intent.putExtra("url", "samples"
										//+ File.separator + "1_ImageRecognition_1_ImageOnTarget"
										+ File.separator + "testWiki"
											+ File.separator + "index.html");
								intent.putExtra("id", item_uid);
								
								intent.putExtra("Title",
										"Camera3D AR");
								intent.putExtra("ir",
										true);
								intent.putExtra("geo",
										false);
								getActivity().startActivity(intent);
								}else{
							    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
									
						        }
							}
							  
						  });
						 

							  holder.itemClickLayout.setOnClickListener(new View.OnClickListener() {
				                  public void onClick(View v) {
				                	  
				                	  if(checkNetwork.isNetworkAvailable()){	  
				                	 	String user_nick_name =item_detail.get(position2).getuDetail().getUser_nick_name();
					              		String user_email =item_detail.get(position2).getuDetail().getUser_email();
					              		String user_avatar_url = item_detail.get(position2).getuDetail().getUser_avatar_url();
					              		String user_uid = item_detail.get(position2).getuDetail().getUser_id();
				              		followingfragment_nextFrag= new Search_item_click_Fragment();
				              		
				              		 Bundle bundle = new Bundle();
				              		     bundle.putString("user_nick_name", user_nick_name);
				              		     bundle.putString("user_email", user_email);
				              		     bundle.putString("user_avatar_url", user_avatar_url);
				              		     bundle.putString("user_uid", user_uid);
				              		   followingfragment_nextFrag.setArguments(bundle);
				              		     
				              			fragmentTransaction = Followingfragment.this.getFragmentManager().beginTransaction();
				              			MainActivity.processFragmentToShowAndHideOtherFragment(followingfragment_nextFrag,fragmentTransaction);
				              			MainActivity.addFragmentToTransactionAndStack(followingfragment_nextFrag,fragmentTransaction);			
				              			fragmentTransaction.commit();
				                	  }else{
									    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
											
								        }
				              		
				              		/*     this.getFragmentManager().beginTransaction()
				              		     .replace(R.id.fragmentContainerFrameLayout, nextFrag,TAG_NEWSFEED_FRAGMENT)
				              		     .addToBackStack(null)		
				              		     .commit();
				              		  */
				              		
				              		    
				                   
				                  }    
				                            
				              });
						/* }
					 } */
						
					 

					  return convertView;  
				  }
				
		 
		
			  }

		@Override
		public void onDestroy() {
			super.onDestroy();
			//Toast.makeText(getActivity(), "onDestroy", Toast.LENGTH_SHORT).show();
			//AnimateFirstDisplayListener.displayedImages.clear();
			ImageLoader.getInstance().clearMemoryCache();
			ImageLoader.getInstance().clearDiskCache();
			imgLoader.clearCache();
			//Item_info.clear();
			Item_info2.clear();
			itemStringList.clear();
			currentIitemNum = "5";
		}

	@Override
	public void onRefresh() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				currentIitemNum = "5";
				Log.i("Get into here ","");
			//	get five first  
			
	
			  itemStringList.clear();
			  checkNetwork(1);
		      //new GetFolloweesItemIdByCondition(1).execute(currentIitemNum);
			/*	*/
				//get one differently
			/*	Item_info.clear();
				itemStringList.clear();
				getAllIds();
				//myadapter = new MyAdapter(getActivity(),Item_info,mListView);
				//mListView.setAdapter(myadapter); 
				//onLoad(); 
			*/
		      
		      //  Item_info.clear();// Item_info_tem.clear();
		      		 
		      
				//new LoadAllProductsByCondition(2).execute(currentIitemNum);
			
				//Item_info=getData();
			//	 mAdapter.notifyDataSetChanged();
				// myadapter = new MyAdapter(getActivity(),Item_info,mListView);
				 //mListView.setAdapter(myadapter); 
				
			}
		}, 0);
	}
	
	private void onLoad() {
	
		mListView.stopRefresh();
		mListView.stopLoadMore();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String currentDateandTime = sdf.format(new Date());
		mListView.setRefreshTime(currentDateandTime);
	}
	
	@Override	
    public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
			
					
				currentIitemNum =String.valueOf( Integer.parseInt(currentIitemNum)+5);
				checkNetwork(2);
				//new GetFolloweesItemIdByCondition(2).execute(currentIitemNum);
				/**/
			/*	currentIitemNum =String.valueOf( Integer.parseInt(currentIitemNum)+5);
			    getAllIds();
			   // onLoad(); 
			  */  
			
				Log.i("currentIitemNum",currentIitemNum);
				
			}
		},0); 
		
	
		// TODO Auto-generated method stub
		
	}
	
	    
}
