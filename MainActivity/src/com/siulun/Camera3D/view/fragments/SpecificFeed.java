package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.UiLifecycleHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.ARModel.SampleCamActivity;
import com.siulun.Camera3D.itemClass.Specific;
import com.siulun.Camera3D.itemClass.itemlist;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.NewsfeedFragment.AddViewNumber;
import com.siulun.Camera3D.view.fragments.NewsfeedFragment.CreateLike;

import com.siulun.Camera3D.R;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SpecificFeed extends Fragment{
	
	LoginSharedPreferences sharedpreferences;
	private LayoutInflater layoutInflater;
	private RelativeLayout user_item_rl;
	private TextView user_nick_name,discriptin,post_time,liketv,viewtv,commenttv,tvar;
	private ImageView user_icon,product_image,like_img,view_img,comment_img;
	private JSONParser jParser = new JSONParser();
	private UserInfo userGlobalVariable ;
	public ArrayList<HashMap<String, String>> search_item_info;
	public Specific item;
	
	NetworkChecking checkNetwork;
	DisplayImageOptions optionsOfSamllIcon,optionsOfPhoto;
	public ImageView backbtn;
	FragmentTransaction fragmentTransaction;
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		user_item_rl = (RelativeLayout)layoutInflater.inflate(R.layout.search_item_detail, container, false);
		Bundle bundle = this.getArguments();
		final String item_uid = bundle.getString("item_uid");
		String resource_url = bundle.getString("resource_url");
		Log.i("ITEM_UID",item_uid);
		item = new Specific();
		tvar = (TextView)user_item_rl.findViewById(R.id.TextAR);
		user_nick_name = (TextView)user_item_rl.findViewById(R.id.usernameTextView);
		discriptin = (TextView)user_item_rl.findViewById(R.id.titleTextView);
		liketv = (TextView)user_item_rl.findViewById(R.id.likeCountTextView);
		viewtv = (TextView)user_item_rl.findViewById(R.id.viewCountTextView);
		commenttv = (TextView)user_item_rl.findViewById(R.id.commentCountTextView);
		post_time = (TextView)user_item_rl.findViewById(R.id.timestampTextView);
		user_icon = (ImageView)user_item_rl.findViewById(R.id.image);
		product_image = (ImageView)user_item_rl.findViewById(R.id.NewsFeedImage);
		like_img = (ImageView)user_item_rl.findViewById(R.id.likeImageView);
		view_img = (ImageView)user_item_rl.findViewById(R.id.viewImageView);
		comment_img = (ImageView)user_item_rl.findViewById(R.id.commentImageView);
		backbtn = (ImageView)user_item_rl.findViewById(R.id.search_backbutton);
		 userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
		 ArrayList<HashMap<String, String>>  search_item_info = new ArrayList<HashMap<String, String>>();
		String token = userGlobalVariable.getToken();
		
		optionsOfSamllIcon = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.imageloading)
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(20))
		.build(); 
		
		optionsOfPhoto = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.profile_off)
		.showImageOnFail(R.drawable.ic_launcher)
		.resetViewBeforeLoading(true)
		.cacheOnDisk(true)
		.imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		.build();
		ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		backbtn.setOnClickListener  (new OnClickListener(){ 
	            @Override

	            public void onClick(View v) {
	            	if(Profile_OnlineFragment.Profile_Online_specificFeedFragment!=null){
	            	//	fragmentTransaction = SpecificFeed.this.getFragmentManager().beginTransaction();
	            		
	            		SpecificFeed.this.getFragmentManager().popBackStack();
	            		Profile_OnlineFragment.Profile_Online_specificFeedFragment=null;
	            	}else{
	            	fragmentTransaction = SpecificFeed.this.getFragmentManager().beginTransaction();
	            	
	            
	            	MainActivity.backToPreviousFragment(fragmentTransaction);
	    			fragmentTransaction.commit();
	            	}
	            }         

	        }); 

		try {
			
			
			search_item_info = new ShowSpecificProduce().execute(token,item_uid).get();
		/*	user_nick_name.setText(search_item_info.get(0).get("user_nick_name"));
			discriptin.setText(search_item_info.get(0).get("item_description"));
			post_time.setText(search_item_info.get(0).get("item_create_date"));
			liketv.setText(search_item_info.get(0).get("like_count"));
			viewtv.setText(search_item_info.get(0).get("view_count"));
			commenttv.setText(search_item_info.get(0).get("comment_count"));
			ImageLoader.getInstance().displayImage(search_item_info.get(0).get("user_avatar_url"), user_icon, optionsOfSamllIcon, animateFirstListener);
			ImageLoader.getInstance().displayImage(resource_url, product_image, optionsOfPhoto, animateFirstListener);
			*/
			
			user_nick_name.setText(item.getUser_nick_name());
			discriptin.setText(item.getItem_description());
			post_time.setText(item.getItem_create_date());
			liketv.setText(item.getLike_count());
			viewtv.setText(item.getView_count());
			commenttv.setText(item.getComment_count());
			ImageLoader.getInstance().displayImage(item.getUser_avatar_url(), user_icon, optionsOfSamllIcon, animateFirstListener);
			ImageLoader.getInstance().displayImage(resource_url, product_image, optionsOfPhoto, animateFirstListener);
			view_img.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						 if(checkNetwork.isNetworkAvailable()){	
					new	AddViewNumber(item_uid, viewtv ,item.getView_count()).execute();
					  Intent intent = new Intent(getActivity(), WebViewActivity.class);
             	  intent.putExtra("item_uid",item_uid );
             	  Log.i("item_uid intent",item_uid);
             	  startActivity(intent);
					 }else{
					    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
							
				        }
					}
			  });
			  
			  if(Integer.parseInt(item.getItem_isLiked())==1){
				  like_img.setImageResource(R.drawable.like_on);
				  }else{
					  like_img.setImageResource(R.drawable.like_off);  
				  }
			  like_img.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
					
						 if(checkNetwork.isNetworkAvailable()){	
					
					            	new	CreateLike(item_uid,like_img,item.getLike_count(),liketv).execute();
						 }else{
						    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
								
					        }
					}
			  });
			  
			  tvar.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					 if(checkNetwork.isNetworkAvailable()){	
					Intent intent = new Intent(getActivity(), SampleCamActivity.class);
					intent.putExtra("url", "samples"
							//+ File.separator + "1_ImageRecognition_1_ImageOnTarget"
							+ File.separator + "testWiki"
								+ File.separator + "index.html");
					intent.putExtra("id", item_uid);
					
					intent.putExtra("Title",
							"Camera3D AR");
					intent.putExtra("ir",
							true);
					intent.putExtra("geo",
							false);
					getActivity().startActivity(intent);
					 }else{
					    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
							
				        }
				}
				  
			  });
					  
			  comment_img.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						 if(checkNetwork.isNetworkAvailable()){	
					            	Intent intent = new Intent(getActivity(), CommentActivity.class);
									intent.putExtra("item_uid", item_uid);								
									getActivity().startActivity(intent);
						 }else{
						    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
								
					        }
					}
			  });

			  
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return user_item_rl;  
    }  

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedpreferences= new LoginSharedPreferences(getActivity());
	}
    class CreateLike extends AsyncTask<String, String, Boolean> {
        
    	String item_uid;
    	String like_count;
    	ImageView img;
    	TextView like_view;
    	
    	public CreateLike(String item_uid, ImageView img, String like_count, TextView like_view){
    		this.item_uid = item_uid;
    		this.img = img;
    		this.like_count = like_count;
    		this.like_view = like_view;
    	}

    	protected void onPreExecute() {
    		
    	}
    	protected Boolean doInBackground(String... args) {
    		String[] itemlist = null;
	List<NameValuePair> params = new ArrayList<NameValuePair>();
			
		//	params.add(new BasicNameValuePair("token",  ((UserInfo) getActivity().getApplication()).getToken()));
	params.add(new BasicNameValuePair("token",  sharedpreferences.getToken()));
			params.add(new BasicNameValuePair("item_uid", item_uid));

			Boolean condition = false;
			
			// getting JSON string from URL
	JSONObject json = jParser.makeHttpRequest(URL_create_like, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All likes: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
				
					condition = true;
					
				}
				else{
					Log.i("Exception","");
				}
				 
			} catch (JSONException e) {
				e.printStackTrace();
			}
    		return condition;
    	}
    	

    	protected void onPostExecute(Boolean condition) {
    		if(condition){
    		img.setImageResource(R.drawable.like_on);
    		item.setLike_count(String.valueOf(Integer.parseInt(like_count)+1));
    		item.setItem_isLiked("1");
    		like_view.setText(item.getLike_count());
    		}
    	}
    }
    
    class AddViewNumber extends AsyncTask<String, String, Boolean> {
        
    	String item_uid;
    	TextView view_count;
    	String count_number;
    	
    	public AddViewNumber(String item_uid,TextView view_count, String count_number){
    		this.item_uid = item_uid;
    		this.view_count = view_count;
    		this.count_number = count_number;
    	}

    	protected void onPreExecute() {
    		
    	}
    	protected Boolean doInBackground(String... args) {
    		String[] itemlist = null;
	List<NameValuePair> params = new ArrayList<NameValuePair>();
			
	params.add(new BasicNameValuePair("item_uid", item_uid));

			Boolean condition = false;
			
			// getting JSON string from URL
	JSONObject json = jParser.makeHttpRequest(URL_add_view_item_number, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All view_item_number : ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
			
				
					condition = true;
					
				
				 
			} catch (JSONException e) {
				e.printStackTrace();
				condition = false;
			}
    		return condition;
    	}
    	

    	protected void onPostExecute(Boolean condition) {
    		if(condition){
    			item.setView_count(String.valueOf(Integer.parseInt(count_number)+1));
    			view_count.setText(String.valueOf(Integer.parseInt(count_number)+1));
    		}
    	}
    }
   
    
	
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class ShowSpecificProduce extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		 ArrayList<HashMap<String, String>>  search_item_info = new ArrayList<HashMap<String, String>>();
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
	   
	        String item_uid = (String)args[1];;
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("item_uid", item_uid));
			params.add(new BasicNameValuePair("token", token));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_SEPCIFIC_ITEM, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
					JSONObject jsonObject = new JSONObject(json.toString());
					JSONArray info = jsonObject
							.getJSONArray("info"); // JSON Array
		
					// products found
					// Getting Array of Products
				
				JSONObject	feed = info.getJSONObject(0);
				JSONObject	ItemInfo = feed.getJSONObject("items");
				JSONObject	user = ItemInfo.getJSONObject("user");
					
					// looping through All Products
				
					
						 HashMap<String, String> map = new HashMap<String, String>();
					
						String item_create_date = ItemInfo.getString("item_create_date");
						String item_description = ItemInfo.getString("item_description");
						String item_isLiked = ItemInfo.getString("item_isLiked");
						String item_name= ItemInfo.getString("item_name");
						
						
						
						String user_first_name = user.getString("user_first_name");
						String user_last_name = user.getString("user_last_name");
						String user_nick_name = user.getString("user_nick_name");
						String user_avatar_url = user.getString("user_avatar_url");
						
						String like_count = feed.getString("like_count");
						String comment_count = feed.getString("comment_count");
						String share_count = feed.getString("share_count");
						String view_count = feed.getString("view_count");
						
						//	Log.i("user_item_info",""+user_item_info.toString());
						//	Log.i("user_uid",""+user_uid.toString());
						item.setItem_create_date(item_create_date);
						item.setItem_description(item_description);
						item.setItem_isLiked(item_isLiked);
						item.setItem_name(item_name);
						
						item.setUser_first_name(user_first_name);
						item.setUser_last_name(user_last_name);
						item.setUser_nick_name(user_nick_name);
						item.setUser_avatar_url(user_avatar_url);
						
						item.setLike_count(like_count);
						item.setComment_count(comment_count);
						item.setShare_count(share_count);
						item.setView_count(view_count);
						
						/*map.put("item_create_date", item_create_date);
						map.put("item_description", item_description);
						map.put("item_isLiked", item_isLiked);
						map.put("item_name", item_name);
						map.put("user_nick_name", user_nick_name);
						map.put("user_avatar_url", user_avatar_url);
						map.put("like_count", like_count);
						map.put("comment_count", comment_count);
						map.put("share_count", share_count);
						map.put("view_count", view_count);
						map.put("user_first_name", user_first_name);
						map.put("user_last_name", user_last_name);
					
						search_item_info.add(map);
						*/
				
					
					Log.i("search_item_info ",""+search_item_info.size());
					
				}
				else{
					Log.i("Exception","");
				}
				 
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return search_item_info;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {

			
		}

	}
	
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	} 
}
