package com.siulun.Camera3D.view.fragments;


import java.io.File;
import android.content.Context;
 
public class FileCache {
     
    private File cacheDir;
    public String name; 
   
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FileCache(Context context){
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"TTImages_cache");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }
     
    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename=String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;
         
    }
  
    public File getFile2(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename=String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File direct = new File(getRootDirectory(), name);
        if(!direct.exists())
        	direct.mkdirs();
        String fullPath = direct.getPath() + File.separator
				+ String.format(filename);

        File f2 = new File(fullPath);
        return f2;
         
    }
     
    public File getRootDirectory(){
    	File rootDir=null;
    	if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
           rootDir=new File(android.os.Environment.getExternalStorageDirectory(),"TTImages_cache");
    	
        if(!rootDir.exists())
        	rootDir.mkdirs();
        
        return rootDir;
    }
    
    public int checkPictureNumber() {
		// TODO Auto-generated method stub
		int count = 0;
		File f = new File(getRootDirectory() + File.separator
				+ name);
		File[] files = f.listFiles();
		if (files == null) {
			return 0;
		}
		for (File inFile : files) {
			if (!inFile.isDirectory()) {
				//if (inFile.getName().contains(".jpg")) {
					count++;
				//}
			}
		}
		return count;
	}
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        if(files==null)
            return;
        for(File f:files)
            f.delete();
    }
 
}