package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import com.siulun.Camera3D.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import me.maxwin.view.XListView;
import me.maxwin.view.XListView.IXListViewListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.google.android.gms.ads.*;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.ARModel.SampleCamActivity;
import com.siulun.Camera3D.ARModel.SampleCamFragment;
import com.siulun.Camera3D.itemClass.*;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.CommentActivity.GetComment;
import com.siulun.Camera3D.view.fragments.Followingfragment.GetFolloweesItemIdByCondition;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.LruCache;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import static com.siulun.Camera3D.ARModel.SampleCamFragment.*;

public class NewsfeedFragment extends Fragment implements IXListViewListener {

	LoginSharedPreferences sharedpreferences;
	
	private XListView mListView;
	private LayoutInflater layoutInflater;

	private RelativeLayout itemCardRelativeLayout;

	//for 360
	Image360 im360 = null;
	
	
	HashMap<String ,String[]>imageUrls2;
	//
	
	// Define Data Class Variables
	private itemDetail JSONitemDetail = null;
	private userDetail JSONuserDetail = null;
	private itemResources JSONitemRes = null;
	// Define a progressbar
	private ProgressBar pbar;
	// define width and height of display of array
	int[] dis;
	// define memory cache list
	public ArrayList<HashMap<String, String>> Item_info;

	public ArrayList<itemlist> Item_info2;

	public ArrayList<String> itemStringList = null;
	public ArrayList<String> itemStringList_tem = null;

	// define session
	Session session;

	private static UiLifecycleHelper uiHelper;

	private static boolean notclickeddialog = true;

	public static int login_condition = 0;

	// tempory list for refresh
	public ArrayList<HashMap<String, String>> Item_info_tem;
	public ArrayList<itemlist> Item_info_tem2;
	// define adapter
	public MyAdapter myadapter = null;
	public ListView listView, list;
	// define custom imageloader
	private ImageLoader2 imgLoader;
	private ImageLoader2 imgSaveder;
	
	public static int itemNumer = 0;
	private int refreshCnt = 0;
	public ArrayList<String> itemsID = null;
	String[] itemStringArray = null;
	public ArrayList<String> itemsIDArray;
	private JSONArray jsonItemID = null;
	private static final String TAG_ITEMS = "items";
	private Handler mHandler, UIhandler;
	DisplayImageOptions optionsOfSamllIcon, optionsOfPhoto;
	private static String currentIitemNum = "5";

	// public static SampleCamFragment sampleCamFragment;
	// private FragmentManager fragmentManager;

	protected boolean pauseOnScroll = false;
	protected boolean pauseOnFling = true;
	// MyProgressDialogFragment progressDialofrg;
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	// products JSONArray
	JSONArray ItemIdArray = null;

	public static Search_item_click_Fragment Newsfeed_nextFrag;
	private FragmentTransaction fragmentTransaction;

	// ArrayList<HashMap<String, String>> dataSet = new
	// ArrayList<HashMap<String, String>>();;

	// init network checking class
	NetworkChecking checkNetwork;

	//FB Sharing Dialog
	//private ShareDialog shareDialog;
	
	
	ArrayList<Boolean> playStatus;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {

		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
		
			onSessionStateChange(session, state, exception);
		}
	};

	private Boolean onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		Boolean condition = false;
		Log.i("Loggedin folloiwng", state.equals("CREATED") + "");
	
	//	if (((UserInfo) getActivity().getApplication()).getToken() == null) {
			if(sharedpreferences.getToken()==null){
			
			// if (state.equals("CREATED_TOKEN_LOADED") || !session.isOpened() )
			// {
			Builder MyAlertDialog = new AlertDialog.Builder(getActivity());

			// MyAlertDialog.setTitle("");
			notclickeddialog = true;
			MyAlertDialog.setMessage("Please Login first!");

			MyAlertDialog.setPositiveButton("Login",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							Intent intent = new Intent(getActivity(),
									SettingFragment.class);
							startActivity(intent);
							notclickeddialog = false;
						}
					});
			MyAlertDialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			if (notclickeddialog) {

				MyAlertDialog.setCancelable(false);
				MyAlertDialog.show();
				condition = false;
			}

		
		}else {

			condition = true;
		}

		return condition;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		// Toast.makeText(getActivity(), "onResume", Toast.LENGTH_SHORT).show();
		// progressDialofrg.getDialog().dismiss();

		session = Session.getActiveSession();
		if (session.getState().equals("CREATED_TOKEN_LOADED")
				|| !session.isOpened()) {

			login_condition = 0;
		} else {
			//if (((UserInfo) getActivity().getApplication()).getToken() != null) {
			if(sharedpreferences.getToken()!=null){
				if (login_condition == 0) {

				}
			}
			login_condition = 1;
		}

		if (uiHelper != null) {
			uiHelper.onResume();
		}
		if (Item_info2.size() == 0) {
			try {
				checkNetwork(0);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sharedpreferences = new LoginSharedPreferences(getActivity());
		
		optionsOfSamllIcon = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.imageloading)
				.showImageForEmptyUri(R.drawable.ic_launcher)
				.showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.displayer(new RoundedBitmapDisplayer(20)).build();
		optionsOfPhoto = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.profile_off)
				.showImageOnFail(R.drawable.ic_launcher)
				.resetViewBeforeLoading(true).cacheOnDisk(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565).considerExifParams(true)
				.displayer(new FadeInBitmapDisplayer(300)).build();
		// Toast.makeText(getActivity(), "onCreate", Toast.LENGTH_SHORT).show();

	}

	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		
	
		 
		
		// Get All Id first before get item details
		// getAllIds();
		// Toast.makeText(getActivity(), "onCreateView",
		// Toast.LENGTH_SHORT).show();
		layoutInflater = inflater;
		itemCardRelativeLayout = (RelativeLayout) layoutInflater.inflate(
				R.layout.newsfeed_fragment, container, false);
		pbar = (ProgressBar) itemCardRelativeLayout
				.findViewById(R.id.progressBar1);
		mListView = (XListView) itemCardRelativeLayout
				.findViewById(R.id.xListView);
		mHandler = new Handler();

		// Item_info = new ArrayList<HashMap<String, String>>();
		Item_info2 = new ArrayList<itemlist>();
		checkNetwork = new NetworkChecking(getActivity());

		
		
		
		// Item_info_tem = new ArrayList<HashMap<String, String>>();
		Item_info_tem2 = new ArrayList<itemlist>();
		dis = getDisplayWidthAndHeight();
		// initilize list objects
		itemStringList = new ArrayList<String>();
		itemStringList_tem = new ArrayList<String>();
		// init custom imageloader
		imgLoader = new ImageLoader2(this.getActivity());
		imgSaveder = new ImageLoader2(this.getActivity());
		// Toast.makeText(getActivity(), ""+dis[0], Toast.LENGTH_SHORT).show();
		// Toast.makeText(getActivity(), ""+dis[1], Toast.LENGTH_SHORT).show();

		
		// for 360
        imageUrls2 = new HashMap<String ,String[]>();
        playStatus = new ArrayList<Boolean>();
		
		
		
		mListView.setPullLoadEnable(true);

		pbar.setVisibility(View.INVISIBLE);
		mListView.setVisibility(View.INVISIBLE);
		// checkNetwork(0);

		// new LoadProductsByCondition(0).execute(currentIitemNum);

		/*
		 * new AsyncTask<String, String, itemDetail>(){
		 * 
		 * @Override protected void onPreExecute() { super.onPreExecute();
		 * mListView.setVisibility(View.INVISIBLE); getAllIds(); }
		 * 
		 * @Override protected itemDetail doInBackground(String... params) { //
		 * TODO Auto-generated method stub
		 * 
		 * return null; } protected void onPostExecute(itemDetail file_url) {
		 * 
		 * pbar.setVisibility(View.INVISIBLE);
		 * mListView.setVisibility(View.VISIBLE); myadapter = new
		 * MyAdapter(getActivity(),Item_info,mListView);
		 * mListView.setAdapter(myadapter);
		 * mListView.setXListViewListener(NewsfeedFragment.this); } }.execute();
		 */
		return itemCardRelativeLayout;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Toast.makeText(getActivity(), "onDestroy",
		// Toast.LENGTH_SHORT).show();
		AnimateFirstDisplayListener.displayedImages.clear();
		ImageLoader.getInstance().clearMemoryCache();
		ImageLoader.getInstance().clearDiskCache();
		imgLoader.clearCache();
		// Item_info.clear();
		Item_info2.clear();
		itemStringList.clear();
		currentIitemNum = "5";
	}

	public int[] getDisplayWidthAndHeight() {
		int[] dis = new int[2];
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		dis[0] = display.getWidth();
		dis[1] = display.getHeight();
		return dis;

	}

	/**
	 * // get one data differently class LoadAllProducts extends
	 * AsyncTask<String, String, String[]> {
	 * 
	 * @Override protected void onPreExecute() { super.onPreExecute();
	 * 
	 *           }
	 * 
	 * 
	 *           protected String[] doInBackground(String... args) {
	 * 
	 *           String[] itemlist = null; //String index = "0"; int indexnumber
	 *           = Integer.parseInt(args[0])-5; String index =
	 *           String.valueOf(indexnumber); String batch_size = "5";
	 * 
	 *           //String batch_size = (String)args[0];
	 * 
	 *           // Building Parameters List<NameValuePair> params = new
	 *           ArrayList<NameValuePair>(); params.add(new
	 *           BasicNameValuePair("index", index)); params.add(new
	 *           BasicNameValuePair("batch_size", batch_size)); // getting JSON
	 *           string from URL JSONObject json =
	 *           jParser.makeHttpRequest(URL_GET_PUBLIC_FEEDS, "POST", params);
	 * 
	 *           // Check your log cat for JSON reponse Log.d("All Products: ",
	 *           json.toString()); Log.d("All itemNumer: ", itemNumer+"");
	 * 
	 *           try { // Checking for SUCCESS TAG
	 * 
	 *           String success = json.getString("status");
	 * 
	 *           if (success.equals("true")) {
	 * 
	 *           // products found // Getting Array of Products ItemIdArray =
	 *           json.getJSONArray("items");
	 *           Log.i("ItemIdArray ",""+ItemIdArray.length()); // looping
	 *           through All Products //itemStringArray = new
	 *           String[ItemIdArray.length()]; itemlist = new
	 *           String[ItemIdArray.length()]; for (int i = 0; i <
	 *           ItemIdArray.length(); i++) {
	 * 
	 *           //itemStringArray[i]= ItemIdArray.getString(i); itemlist[i] =
	 *           ItemIdArray.getString(i);
	 * 
	 *           //Log.i("itemStringArray",""+itemStringArray[i]);
	 *           Log.i("itemStringArray",""+itemlist[i]); } } else{
	 *           Log.i("Exception",""); } } catch (JSONException e) {
	 *           e.printStackTrace(); }
	 * 
	 *           // return itemStringArray; return itemlist; }
	 * 
	 * 
	 *           protected void onPostExecute(String[] file_url) {
	 *           if(file_url!=null){ for (int i=0; i<file_url.length; i++){
	 *           itemStringList.add(file_url[i]);
	 *           if(itemStringList.size()==file_url.length){ onLoad(); } }
	 * 
	 * 
	 *           }else{ onLoad(); }
	 * 
	 *           if(myadapter!=null){ // myadapter = new
	 *           MyAdapter(getActivity(),Item_info,mListView); //
	 *           mListView.setAdapter(myadapter);
	 *           //myadapter.notifyDataSetChanged();
	 * 
	 *           } }
	 * 
	 *           }
	 **/
	class GetUserAllAtOne extends AsyncTask<String, String, userDetail> {
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		ViewHolder holder;
		int position;
		userDetail JSONuserDetail = null;
		itemResources JSONitemResources = null;
		itemDetail JSONitemDetail = null;
		HashMap<String, String> map;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			map = new HashMap<String, String>();
			// this.holder.pbar.setVisibility(View.VISIBLE);
			this.holder.layout.setVisibility(View.INVISIBLE);
		}

		public GetUserAllAtOne(ViewHolder holder, int position) {
			this.holder = holder;
			this.position = position;
		}

		public itemDetail getItemDetail(String a) {
			itemDetail JSONitemDetail = null;

			Log.i("doInBackground in GetItemDetailByID", "HEREEEEEEE");
			try {
				String item_uid = a;
				Log.i("arg", "" + item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					JSONObject jsonObject = new JSONObject(builder.toString());
					JSONArray productObj = jsonObject.getJSONArray("info"); // JSON
																			// Array

					JSONObject iteminfo = productObj.getJSONObject(0);

					// String items = iteminfo.getString("items");

					JSONObject itemJson = iteminfo.getJSONObject("items");
					Log.i("itemJson", itemJson.toString());
					// int like_count = iteminfo.getInt("like_count");
					String like_count = iteminfo.getString("like_count");
					Log.i("like_count", "" + like_count);
					String comment_count = iteminfo.getString("comment_count");
					String share_count = iteminfo.getString("share_count");
					String view_count = iteminfo.getString("view_count");
					String item_description = itemJson
							.getString("item_description");
					String item_isLiked = itemJson.getString("item_isLiked");
					String item_create_date = itemJson
							.getString("item_create_date");
					String item_name = itemJson.getString("item_name");
					Log.i("item_create_date", item_create_date);
					Log.i("item_description", item_description
							+ item_create_date + item_name);
					JSONObject userinfo = itemJson.getJSONObject("user");
					Log.i("userinfo", "" + userinfo);

					String user_id = userinfo.getString("user_uid");

					JSONitemDetail = new itemDetail(item_uid, user_id,
							item_create_date, item_name, item_description,
							like_count, comment_count, share_count, view_count,
							item_isLiked);

					// String userinfo = iteminfo.getString("user");
					Log.i("userinfo", userinfo.toString());
				} catch (ClientProtocolException e) {
					// Log exception
					JSONitemDetail = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONitemDetail = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONitemDetail = null;
				e.printStackTrace();
			}
			return JSONitemDetail;
		}

		public itemResources getItemResource(String a) {
			itemResources JSONitemRes = null;
			JSONObject jsonObject = null;
			;
			Log.i("doInBackground in GetItemResiurces", "HEREEEEEEE");
			try {
				String item_uid = a;
				// Log.i("arg",""+item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_RESOURCES); // replace
																			// with
																			// your
																			// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();

					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					if (builder.length() > 0) {
						jsonObject = new JSONObject(builder.toString());
						JSONArray resourcesArray = jsonObject
								.getJSONArray("resources"); // JSON Array

						JSONObject resources = resourcesArray.getJSONObject(0);
						String resource_index = resources
								.getString("resource_index");
						String resource_uid = resources
								.getString("resource_uid");
						String type_uid = resources.getString("type_uid");
						String resource_url = resources
								.getString("resource_url");

						JSONitemRes = new itemResources(item_uid, resource_uid,
								type_uid, resource_index, resource_url);

						Log.i("resource", "" + resources);
						// String items = iteminfo.getString("items");
					} else {
						JSONitemRes = null;
					}

				} catch (ClientProtocolException e) {
					// Log exception
					JSONitemRes = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONitemRes = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONitemRes = null;
				e.printStackTrace();
			}
			return JSONitemRes;
		}

		public userDetail getJSONuser(String a) {
			userDetail JSONuserDetail = null;
			try {
				String item_uid = a;
				Log.i("arg", "" + item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log

					JSONObject jsonObject = new JSONObject(builder.toString());
					JSONArray productObj = jsonObject.getJSONArray("info"); // JSON
																			// Array

					JSONObject iteminfo = productObj.getJSONObject(0);

					// String items = iteminfo.getString("items");

					JSONObject itemJson = iteminfo.getJSONObject("items");

					JSONObject userinfo = itemJson.getJSONObject("user");
					Log.i("userinfo", "" + userinfo);

					String user_id = userinfo.getString("user_uid");
					String user_first_name = userinfo
							.getString("user_first_name");
					String user_last_name = userinfo
							.getString("user_last_name");
					String user_nick_name = userinfo
							.getString("user_nick_name");
					String user_email = userinfo.getString("user_email");
					String user_avatar_url = userinfo
							.getString("user_avatar_url");

					JSONuserDetail = new userDetail(user_id, user_first_name,
							user_last_name, user_nick_name, user_email,
							user_avatar_url);

				} catch (ClientProtocolException e) {
					// Log exception
					JSONuserDetail = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONuserDetail = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONuserDetail = null;
				e.printStackTrace();
			}
			return JSONuserDetail;
		}

		/**
		 * Getting product details in background thread
		 * */

		public void addToHodler(ViewHolder holder) {
			holder.timetv.setText(map.get("item_create_date").toString());

			String nameString = map.get("user_nick_name").toString();
			if (nameString.length() > 20) { // show part of the user name if
											// user name is too long
				nameString = nameString.substring(0,
						nameString.length() * 2 / 3) + "...";
			}

			holder.userNameTv.setText(nameString);

			holder.titletv.setText(map.get("item_description").toString());
			// holder.titletv.setText("test item description "+position);

			final String item_uid = map.get("item_uid").toString();

			holder.NewsFeedImage.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(),
							WebViewActivity.class);
					intent.putExtra("item_uid", item_uid);
					Log.i("item_uid intent", item_uid);
					startActivity(intent);

				}

			});

			imgLoader.DisplayImage(map.get("user_avatar_url"),
					holder.profileImage);
			imgLoader.DisplayImage(map.get("resource_url"),
					holder.NewsFeedImage);
			holder.like_count.setText(map.get("like_count").toString());
			holder.view_count.setText(map.get("view_count").toString());
			holder.comment_count.setText(map.get("comment_count").toString());
		}

		public void addToMap(itemDetail idetail, itemResources iresourse,
				userDetail udetail) {
			if (iresourse != null) {
				map.put("resource_url", iresourse.getResource_url());
			}
			if (udetail != null) {
				map.put("user_name", udetail.getUser_first_name() + " "
						+ udetail.getUser_last_name());
				map.put("user_nick_name", udetail.getUser_nick_name());
				map.put("user_email", udetail.getUser_email());
				map.put("user_avatar_url", udetail.getUser_avatar_url());
			}

			Log.i("user_avatar_url", "" + udetail.getUser_avatar_url());

			if (idetail != null) {

				map.put("item_uid", idetail.getItem_uid());
				map.put("user_id", idetail.getUser_id());
				map.put("item_create_date", idetail.getstr_item_create_date());
				map.put("item_name", idetail.getItem_name());
				map.put("item_description", idetail.getItem_description());
				map.put("like_count", idetail.getLike_count());
				map.put("comment_count", idetail.getComment_count());
				map.put("share_count", idetail.getShare_count());
				map.put("view_count", idetail.getView_count());

			}

			Item_info.add(position, map);
		}

		protected userDetail doInBackground(String... arg) {

			// uJSONuserDetail = null;
			JSONitemDetail = getItemDetail(arg[0]);
			JSONuserDetail = getJSONuser(arg[0]);
			JSONitemResources = getItemResource(arg[0]);

			return JSONuserDetail;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(userDetail file_url) {
			// addToMap(JSONitemDetail,JSONitemResources,JSONuserDetail);
			if (JSONitemDetail != null && JSONitemResources != null
					&& JSONuserDetail != null) {
				itemlist list = new itemlist(JSONitemDetail, JSONitemResources,
						JSONuserDetail);
				Item_info2.add(list);
			}
			// Toast.makeText(getActivity(),"finished "+ position,
			// Toast.LENGTH_SHORT).show();
			// this.holder.pbar.setVisibility(View.INVISIBLE);
			this.holder.layout.setVisibility(View.VISIBLE);
			addToHodler(holder);
		}
	}

	// get five data first
	class LoadProductsByCondition extends AsyncTask<String, String, String[]> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		int condition;
		int error = 0;

		public LoadProductsByCondition(int condition) {
			this.condition = condition;
		}/**/

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (condition == 0) {
				pbar.setVisibility(View.VISIBLE);
				mListView.setVisibility(View.INVISIBLE);
			}
		}

		/**
		 * getting All products from url
		 * */
		protected String[] doInBackground(String... args) {

			String[] itemlist = null;
			// String index = "0";
			int indexnumber = Integer.parseInt(args[0]) - 5;
			String index = String.valueOf(indexnumber);
			String batch_size = "5";

			// String batch_size = (String)args[0];

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("index", index));
			params.add(new BasicNameValuePair("batch_size", batch_size));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_GET_PUBLIC_FEEDS,
					"POST", params);

			// Check your log cat for JSON reponse
			// Log.d("All Products: ", json.toString());
			// Log.d("All itemNumer: ", itemNumer+"");

			try {
				// Checking for SUCCESS TAG

				String success;
				try {
					success = json.getString("status");
				} catch (Exception e) {
					e.printStackTrace();
					success = null;
					error = 1;
				}
				;
				if (success == null) {

				} else {
					if (success.equals("true")) {

						// products found
						// Getting Array of Products
						ItemIdArray = json.getJSONArray("items");
						// Log.i("ItemIdArray ",""+ItemIdArray.length());
						// looping through All Products
						// itemStringArray = new String[ItemIdArray.length()];
						itemlist = new String[ItemIdArray.length()];
						for (int i = 0; i < ItemIdArray.length(); i++) {

							// itemStringArray[i]= ItemIdArray.getString(i);
							itemlist[i] = ItemIdArray.getString(i);

							// Log.i("itemStringArray",""+itemStringArray[i]);
							// Log.i("itemStringArray",""+itemlist[i]);
						}
					} else {
						Log.i("Exception", "");
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				error = 1;
			}

			// return itemStringArray;
			return itemlist;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String[] file_url) {
			// Toast.makeText(getActivity(), "finished !!! ",
			// Toast.LENGTH_SHORT).show();
			if (file_url != null) {
				for (int i = 0; i < file_url.length; i++) {

					itemStringList.add(file_url[i]);
					new GetUserAllAtOneBy(condition, itemStringList.size() - 1)
							.execute(file_url[i]);
					// itemStringListB[itemStringList.size()-1] = true;
					// Toast.makeText(getActivity(), ""+itemStringList.size(),
					// Toast.LENGTH_SHORT).show();
				}
				/*
				 * if(condition==1){ // myadapter.notifyDataSetChanged();
				 * onLoad();} if(condition==2){ myadapter = new
				 * MyAdapter(getActivity(),Item_info,mListView);
				 * mListView.setAdapter(myadapter); onLoad();
				 * 
				 * 
				 * }
				 */

			} else {
				if (error == 1) {
					pbar.setVisibility(View.INVISIBLE);
					Toast.makeText(getActivity(),
							"Network is poor, Please retry later!!! ",
							Toast.LENGTH_SHORT).show();
				}

				if (condition == 1 || condition == 2) {
					onLoad();
				}
			}

			if (myadapter != null) {
				// myadapter = new MyAdapter(getActivity(),Item_info,mListView);
				// mListView.setAdapter(myadapter);
				// myadapter.notifyDataSetChanged();
			}
			/*
			 * } new AsyncTask<String, String, itemDetail>(){
			 * 
			 * @Override protected void onPreExecute() { super.onPreExecute();
			 * new GetItemDetailByID().execute(itemStringList.get(k)); new
			 * GetUserDetailByID().execute(itemStringList.get(k)); new
			 * GetItemResiurces().execute(itemStringList.get(k)); }
			 * 
			 * @Override protected itemDetail doInBackground(String... params) {
			 * // TODO Auto-generated method stub
			 * 
			 * return null; } protected void onPostExecute(itemDetail file_url)
			 * { new GetOneItemThread2(k).execute(); } }.execute();
			 */
		}

	}

	class GetUserAllAtOneBy extends AsyncTask<String, String, userDetail> {
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		int condition, position;
		userDetail JSONuserDetail = null;
		itemResources JSONitemResources = null;
		itemDetail JSONitemDetail = null;
		HashMap<String, String> map;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			map = new HashMap<String, String>();
			if (condition == 0) {
				pbar.setVisibility(View.VISIBLE);
				mListView.setVisibility(View.INVISIBLE);
			}
		}

		public GetUserAllAtOneBy(int condition, int position) {
			this.position = position;
			this.condition = condition;
		} /**/

		public itemDetail getItemDetail(String a) {
			itemDetail JSONitemDetail = null;

			Log.i("doInBackground in GetItemDetailByID", "HEREEEEEEE");
			try {
				String item_uid = a;
				Log.i("arg", "" + item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				//if (((UserInfo) getActivity().getApplication()).getToken() != null) {
					if(sharedpreferences.getToken()!=null){
					/*nameValuePair.add(new BasicNameValuePair("token",
							((UserInfo) getActivity().getApplication())
									.getToken()));
									*/
						nameValuePair.add(new BasicNameValuePair("token",
								sharedpreferences.getToken()));
				}
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					if (builder.length() > 0) {
						JSONObject jsonObject = new JSONObject(
								builder.toString());

						JSONArray productObj = jsonObject.getJSONArray("info");

						// JSON Array

						JSONObject iteminfo = productObj.getJSONObject(0);

						// String items = iteminfo.getString("items");

						JSONObject itemJson = iteminfo.getJSONObject("items");
						Log.i("itemJson", itemJson.toString());
						// int like_count = iteminfo.getInt("like_count");
						String like_count = iteminfo.getString("like_count");
						Log.i("like_count", "" + like_count);
						String comment_count = iteminfo
								.getString("comment_count");
						String share_count = iteminfo.getString("share_count");
						String view_count = iteminfo.getString("view_count");
						String item_description = itemJson
								.getString("item_description");
						String item_isLiked = itemJson
								.getString("item_isLiked");
						String item_create_date = itemJson
								.getString("item_create_date");
						String item_name = itemJson.getString("item_name");
						Log.i("item_create_date", item_create_date);
						Log.i("item_description", item_description
								+ item_create_date + item_name);
						JSONObject userinfo = itemJson.getJSONObject("user");
						Log.i("userinfo", "" + userinfo);

						String user_id = userinfo.getString("user_uid");

						JSONitemDetail = new itemDetail(item_uid, user_id,
								item_create_date, item_name, item_description,
								like_count, comment_count, share_count,
								view_count, item_isLiked);

					} else {
						JSONitemDetail = null;
					}

					// String userinfo = iteminfo.getString("user");
					// Log.i("userinfo",userinfo.toString());
				} catch (ClientProtocolException e) {
					// Log exception
					JSONitemDetail = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONitemDetail = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONitemDetail = null;
				e.printStackTrace();
			}
			return JSONitemDetail;
		}

		public itemResources getItemResource(String a) {
			itemResources JSONitemRes = null;
			JSONObject jsonObject = null;
			;
			Log.i("doInBackground in GetItemResiurces", "HEREEEEEEE");
			try {
				String item_uid = a;
				// Log.i("arg",""+item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_RESOURCES); // replace
																			// with
																			// your
																			// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();

					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					if (builder.length() > 0) {
						jsonObject = new JSONObject(builder.toString());
						JSONArray resourcesArray = jsonObject
								.getJSONArray("resources"); // JSON Array

						JSONObject resources = resourcesArray.getJSONObject(0);
						String resource_index = resources
								.getString("resource_index");
						String resource_uid = resources
								.getString("resource_uid");
						String type_uid = resources.getString("type_uid");
						String resource_url = resources
								.getString("resource_url");

						JSONitemRes = new itemResources(item_uid, resource_uid,
								type_uid, resource_index, resource_url);

						Log.i("resource", "" + resources);
						// String items = iteminfo.getString("items");
					} else {
						JSONitemRes = null;
					}

				} catch (ClientProtocolException e) {
					// Log exception
					JSONitemRes = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONitemRes = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONitemRes = null;
				e.printStackTrace();
			}
			return JSONitemRes;
		}

		public userDetail getJSONuser(String a) {
			userDetail JSONuserDetail = null;
			try {
				String item_uid = a;
				Log.i("arg", "" + item_uid);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GET_ITEM_DETAIL); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", item_uid));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					if (builder.length() > 0) {
						JSONObject jsonObject = new JSONObject(
								builder.toString());
						JSONArray productObj = jsonObject.getJSONArray("info"); // JSON
																				// Array

						JSONObject iteminfo = productObj.getJSONObject(0);

						// String items = iteminfo.getString("items");

						JSONObject itemJson = iteminfo.getJSONObject("items");

						JSONObject userinfo = itemJson.getJSONObject("user");
						Log.i("userinfo", "" + userinfo);

						String user_id = userinfo.getString("user_uid");
						String user_first_name = userinfo
								.getString("user_first_name");
						String user_last_name = userinfo
								.getString("user_last_name");
						String user_nick_name = userinfo
								.getString("user_nick_name");
						String user_email = userinfo.getString("user_email");
						String user_avatar_url = userinfo
								.getString("user_avatar_url");

						JSONuserDetail = new userDetail(user_id,
								user_first_name, user_last_name,
								user_nick_name, user_email, user_avatar_url);
					} else {
						JSONuserDetail = null;
					}

				} catch (ClientProtocolException e) {
					// Log exception
					JSONuserDetail = null;
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					JSONuserDetail = null;
					e.printStackTrace();
				}

			} catch (Exception e) {
				JSONuserDetail = null;
				e.printStackTrace();
			}
			return JSONuserDetail;
		}

		/**
		 * Getting product details in background thread
		 * */

		public void addToMap(itemDetail idetail, itemResources iresourse,
				userDetail udetail) {
			if (iresourse != null) {
				map.put("resource_url", iresourse.getResource_url());
			}
			if (udetail != null) {
				map.put("user_name", udetail.getUser_first_name() + " "
						+ udetail.getUser_last_name());
				map.put("user_nick_name", udetail.getUser_nick_name());
				map.put("user_email", udetail.getUser_email());
				map.put("user_avatar_url", udetail.getUser_avatar_url());
				Log.i("user_avatar_url", "" + udetail.getUser_avatar_url());
			}

			if (idetail != null) {

				map.put("item_uid", idetail.getItem_uid());
				map.put("user_id", idetail.getUser_id());
				map.put("item_create_date", idetail.getstr_item_create_date());
				map.put("item_name", idetail.getItem_name());
				map.put("item_description", idetail.getItem_description());
				map.put("like_count", idetail.getLike_count());
				map.put("comment_count", idetail.getComment_count());
				map.put("share_count", idetail.getShare_count());
				map.put("view_count", idetail.getView_count());

			}
			if (condition == 1) {
				Item_info_tem.add(map);
			} else {

				Item_info.add(map);

			}
		}

		public void addToMap2(itemDetail idetail, itemResources iresourse,
				userDetail udetail) {
			if (condition == 1) {
				itemlist list = new itemlist(JSONitemDetail, JSONitemResources,
						JSONuserDetail);
				Item_info_tem2.add(list);
			} else {

				itemlist list = new itemlist(JSONitemDetail, JSONitemResources,
						JSONuserDetail);
				Item_info2.add(list);

			}
		}

		protected userDetail doInBackground(String... arg) {

			// uJSONuserDetail = null;
			JSONitemDetail = getItemDetail(arg[0]);
			JSONuserDetail = getJSONuser(arg[0]);
			JSONitemResources = getItemResource(arg[0]);

			if (isCancelled()) {
				Log.i("cancel asytask", "cancel is called");
			}

			return JSONuserDetail;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(userDetail file_url) {
			if (JSONitemDetail != null && JSONitemResources != null
					&& JSONuserDetail != null) {
				addToMap2(JSONitemDetail, JSONitemResources, JSONuserDetail);
				// Toast.makeText(getActivity(),"finished "+
				// position+Item_info2.size(), Toast.LENGTH_SHORT).show();
				// Toast.makeText(getActivity(),"finished "+ position,
				// Toast.LENGTH_SHORT).show();
				if (Item_info2.size() == itemStringList.size()
						&& condition == 0) {
					pbar.setVisibility(View.INVISIBLE);
					mListView.setVisibility(View.VISIBLE);
					// myadapter = new
					// MyAdapter(getActivity(),Item_info,mListView);
					myadapter = new MyAdapter(getActivity(), Item_info2,
							mListView);
					mListView.setAdapter(myadapter);
					mListView.setXListViewListener(NewsfeedFragment.this);
				}
				if (Item_info_tem2.size() == itemStringList.size()
						&& condition == 1) {
					// myadapter = new
					// MyAdapter(getActivity(),Item_info,mListView);
					// mListView.setAdapter(myadapter);
					Item_info2.clear();
					for (int i = 0; i < Item_info_tem2.size(); i++) {
						Item_info2.add(Item_info_tem2.get(i));
					}
					Item_info_tem2.clear();
					myadapter = new MyAdapter(getActivity(), Item_info2,
							mListView);
					mListView.setAdapter(myadapter);
					onLoad();
				}
				if (Item_info2.size() == itemStringList.size()
						&& condition == 2) {
					myadapter.item_detail = Item_info2;
					myadapter.notifyDataSetChanged();
					onLoad();
				}
			} else {
				pbar.setVisibility(View.INVISIBLE);
				Toast.makeText(getActivity(),
						"Network is poor, please try it again!!!",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	/**
	 * // No Use class LoadAllProductsByCondition extends AsyncTask<String,
	 * String, String[]> {
	 * 
	 * 
	 * int condition; public LoadAllProductsByCondition(int condition){
	 * this.condition = condition; }
	 * 
	 * @Override protected void onPreExecute() { super.onPreExecute();
	 * 
	 *           }
	 * 
	 * 
	 *           protected String[] doInBackground(String... args) {
	 * 
	 *           String[] itemlist = null; //String index = "0"; int indexnumber
	 *           = Integer.parseInt(args[0])-5; String index =
	 *           String.valueOf(indexnumber); String batch_size = "5";
	 * 
	 *           //String batch_size = (String)args[0];
	 * 
	 *           // Building Parameters List<NameValuePair> params = new
	 *           ArrayList<NameValuePair>(); params.add(new
	 *           BasicNameValuePair("index", index)); params.add(new
	 *           BasicNameValuePair("batch_size", batch_size)); // getting JSON
	 *           string from URL JSONObject json =
	 *           jParser.makeHttpRequest(URL_GET_PUBLIC_FEEDS, "POST", params);
	 * 
	 *           // Check your log cat for JSON reponse Log.d("All Products: ",
	 *           json.toString()); Log.d("All itemNumer: ", itemNumer+"");
	 * 
	 *           try { // Checking for SUCCESS TAG
	 * 
	 *           String success = json.getString("status");
	 * 
	 *           if (success.equals("true")) {
	 * 
	 *           // products found // Getting Array of Products ItemIdArray =
	 *           json.getJSONArray("items");
	 *           Log.i("ItemIdArray ",""+ItemIdArray.length()); // looping
	 *           through All Products //itemStringArray = new
	 *           String[ItemIdArray.length()]; itemlist = new
	 *           String[ItemIdArray.length()]; for (int i = 0; i <
	 *           ItemIdArray.length(); i++) {
	 * 
	 *           //itemStringArray[i]= ItemIdArray.getString(i); itemlist[i] =
	 *           ItemIdArray.getString(i);
	 * 
	 *           //Log.i("itemStringArray",""+itemStringArray[i]);
	 *           Log.i("itemStringArray",""+itemlist[i]); } } else{
	 *           Log.i("Exception",""); } } catch (JSONException e) {
	 *           e.printStackTrace(); }
	 * 
	 *           // return itemStringArray; return itemlist; }
	 * 
	 * 
	 *           protected void onPostExecute(String[] file_url) {
	 *           if(file_url!=null){ for (int i=0; i<file_url.length; i++){
	 * 
	 *           itemStringList.add(file_url[i]); //
	 *           itemStringListB[itemStringList.size()-1] = true; //
	 *           Toast.makeText(getActivity(), ""+itemStringList.size(),
	 *           Toast.LENGTH_SHORT).show(); } if(condition==1){ //
	 *           myadapter.notifyDataSetChanged(); onLoad();} if(condition==2){
	 *           myadapter = new MyAdapter(getActivity(),Item_info,mListView);
	 *           mListView.setAdapter(myadapter); onLoad();
	 * 
	 * 
	 *           }
	 * 
	 *           }
	 * 
	 *           if(myadapter!=null){ // myadapter = new
	 *           MyAdapter(getActivity(),Item_info,mListView); //
	 *           mListView.setAdapter(myadapter); //
	 *           myadapter.notifyDataSetChanged(); } /* } new AsyncTask<String,
	 *           String, itemDetail>(){
	 * @Override protected void onPreExecute() { super.onPreExecute(); new
	 *           GetItemDetailByID().execute(itemStringList.get(k)); new
	 *           GetUserDetailByID().execute(itemStringList.get(k)); new
	 *           GetItemResiurces().execute(itemStringList.get(k)); }
	 * @Override protected itemDetail doInBackground(String... params) { // TODO
	 *           Auto-generated method stub
	 * 
	 *           return null; } protected void onPostExecute(itemDetail
	 *           file_url) { new GetOneItemThread2(k).execute(); } }.execute();
	 *           }
	 * 
	 *           }
	 **/
	class LoadOneProducts extends AsyncTask<String, String, String[]> {
		ViewHolder holder;
		int position;

		public LoadOneProducts(ViewHolder holder, int position) {
			this.holder = holder;
			this.position = position;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// this.holder.pbar.setVisibility(View.VISIBLE);
			this.holder.layout.setVisibility(View.INVISIBLE);
		}

		/**
		 * getting All products from url
		 * */
		protected String[] doInBackground(String... args) {

			String[] itemlist = null;
			// String index = "0";
			// String batch_size = "5";

			// String batch_size = (String)args[0];

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("index", String.valueOf(position)));
			params.add(new BasicNameValuePair("batch_size", String.valueOf(1)));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_GET_PUBLIC_FEEDS,
					"POST", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			Log.d("All itemNumer: ", itemNumer + "");

			try {
				// Checking for SUCCESS TAG

				String success = json.getString("status");

				if (success.equals("true")) {

					// products found
					// Getting Array of Products
					ItemIdArray = json.getJSONArray("items");
					Log.i("ItemIdArray ", "" + ItemIdArray.length());
					// looping through All Products
					// itemStringArray = new String[ItemIdArray.length()];
					itemlist = new String[ItemIdArray.length()];
					for (int i = 0; i < ItemIdArray.length(); i++) {

						// itemStringArray[i]= ItemIdArray.getString(i);
						itemlist[i] = ItemIdArray.getString(i);

						// Log.i("itemStringArray",""+itemStringArray[i]);
						Log.i("itemStringArray", "" + itemlist[i]);
					}
				} else {
					Log.i("Exception", "");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// return itemStringArray;
			return itemlist;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String[] file_url) {
			if (file_url != null) {
				itemStringList.add(file_url[0]);
				new GetUserAllAtOne(this.holder, position).execute(file_url[0]);
			} else {
				currentIitemNum = String.valueOf(itemStringList.size());
				// myadapter = new MyAdapter(getActivity(),Item_info,mListView);
				// mListView.setAdapter(myadapter);
				// this.holder.pbar.setVisibility(View.INVISIBLE);
				this.holder.layout.setVisibility(View.VISIBLE);
				myadapter.notifyDataSetChanged();
			}
			// new GetOneItemThread(this.holder,file_url[0],position).execute();
		}

	}

	class DetectLoadMore extends AsyncTask<String, String, String[]> {
		int position;

		public DetectLoadMore(int position) {
			this.position = position;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		/**
		 * getting All products from url
		 * */
		protected String[] doInBackground(String... args) {
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
			}
			String[] itemlist = null;
			// String index = "0";
			// String batch_size = "5";

			// String batch_size = (String)args[0];

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("index", String.valueOf(position)));
			params.add(new BasicNameValuePair("batch_size", String.valueOf(1)));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_GET_PUBLIC_FEEDS,
					"POST", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			Log.d("All itemNumer: ", itemNumer + "");

			try {
				// Checking for SUCCESS TAG

				String success = json.getString("status");

				if (success.equals("true")) {

					// products found
					// Getting Array of Products
					ItemIdArray = json.getJSONArray("items");
					Log.i("ItemIdArray ", "" + ItemIdArray.length());
					// looping through All Products
					// itemStringArray = new String[ItemIdArray.length()];
					itemlist = new String[ItemIdArray.length()];
					for (int i = 0; i < ItemIdArray.length(); i++) {

						// itemStringArray[i]= ItemIdArray.getString(i);
						itemlist[i] = ItemIdArray.getString(i);

						// Log.i("itemStringArray",""+itemStringArray[i]);
						Log.i("itemStringArray", "" + itemlist[i]);
					}
				} else {
					Log.i("Exception", "");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// return itemStringArray;
			return itemlist;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String[] file_url) {
			if (file_url != null) {
				currentIitemNum = String.valueOf(Integer
						.parseInt(currentIitemNum) + 5);
				onLoad();
			} else {
				onLoad();
			}
			// new GetOneItemThread(this.holder,file_url[0],position).execute();
		}

	}

	class CreateLike extends AsyncTask<String, String, Boolean> {

		String item_uid;
		String like_count;
		ImageView img;
		TextView like_view;
		int position;

		public CreateLike(String item_uid, ImageView img, String like_count,
				TextView like_view, int position) {
			this.item_uid = item_uid;
			this.img = img;
			this.like_count = like_count;
			this.like_view = like_view;
			this.position = position;
		}

		protected void onPreExecute() {

		}

		protected Boolean doInBackground(String... args) {
			String[] itemlist = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			/*params.add(new BasicNameValuePair("token",
					((UserInfo) getActivity().getApplication()).getToken()));
					*/
			params.add(new BasicNameValuePair("token",
					sharedpreferences.getToken()));
			params.add(new BasicNameValuePair("item_uid", item_uid));

			Boolean condition = false;

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_create_like, "POST",
					params);

			// Check your log cat for JSON reponse
			Log.d("All likes: ", json.toString());

			try {
				// Checking for SUCCESS TAG

				String success = json.getString("status");

				if (success.equals("true")) {

					condition = true;

				} else {
					Log.i("Exception", "");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return condition;
		}

		protected void onPostExecute(Boolean condition) {
			if (condition) {
				img.setImageResource(R.drawable.like_on);
				Item_info2
						.get(position)
						.getiDetail()
						.setLike_count(
								String.valueOf(Integer.parseInt(like_count) + 1));
				Item_info2.get(position).getiDetail().setItem_isLiked("1");
				like_view.setText(simplifyCount(Item_info2.get(position).getiDetail()
						.getLike_count()));
			}
		}
	}

	class AddViewNumber extends AsyncTask<String, String, Boolean> {

		String item_uid;
		TextView view_count;
		String count_number;
		int position;

		public AddViewNumber(String item_uid, TextView view_count,
				String count_number, int position) {
			this.item_uid = item_uid;
			this.view_count = view_count;
			this.count_number = count_number;
			this.position = position;
		}

		protected void onPreExecute() {

		}

		protected Boolean doInBackground(String... args) {
			String[] itemlist = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("item_uid", item_uid));

			Boolean condition = false;

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_add_view_item_number,
					"POST", params);

			// Check your log cat for JSON reponse
			Log.d("All view_item_number : ", json.toString());

			try {
				// Checking for SUCCESS TAG

				String success = json.getString("status");

				condition = true;

			} catch (JSONException e) {
				e.printStackTrace();
				condition = false;
			}
			return condition;
		}

		protected void onPostExecute(Boolean condition) {
			if (condition) {
				if(Item_info2.size()>position)
				{
				Item_info2
						.get(position)
						.getiDetail()
						.setView_count(
								String.valueOf(Integer.parseInt(count_number) + 1));
				view_count.setText(simplifyCount(String.valueOf(Integer
						.parseInt(count_number) + 1)));
				}
			}
		}
	}

	/*private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Toast.makeText(getActivity(), "error occur in sharing", Toast.LENGTH_SHORT).show();
        }
 
        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            //Log.d("facebook share", "Success!");
        	Toast.makeText(getActivity(), "Success to share", Toast.LENGTH_SHORT).show();
        }
    };
 */
	
	public void PostToFacebookWall(String title,String Url,String market){
	if (FacebookDialog.canPresentShareDialog(getActivity(),
				FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
				    // Publish the post using the Share Dialog
				    FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
				   
				  
				   .setDescription(title)
				    .setLink(market)
				    .setName("Download App Link is here --->>>")
				        .setPicture(Url)    
				            .build();
				    if (uiHelper == null) {
						uiHelper = new UiLifecycleHelper(getActivity(),
								callback);
					}
				    uiHelper.trackPendingDialogCall(shareDialog.present());
				    
			
				}
	else{
		Toast.makeText(getActivity(), "No facebook App is installed", Toast.LENGTH_SHORT).show();
	}/**/
	
				
			/*	     else{
				     
				    	 if (Session.getActiveSession() == null || !Session.getActiveSession().isOpened()) {
				    	        Session.openActiveSession(getActivity(), true, callback2);
				    	    } else {
				    	    
				    	        publishFeedDialog();
				    	    }
					
				  
			}
			*/
		/*
		Bundle params = new Bundle();
		params.putString("caption", "caption");
		params.putString("message", "message");
		params.putString("link", "link_url");
		params.putString("picture", "picture_url");

		Request request = new Request(Session.getActiveSession(), "me/feed", params, HttpMethod.POST);
		request.setCallback(new Request.Callback() {
		    @Override
		    public void onCompleted(Response response) {
		        if (response.getError() == null) {
		            // Tell the user success!
		        }
		    }
		});
		request.executeAsync();
		*/
		
		
		
	/*	Bundle bundle = new Bundle();
		bundle.putString("caption", "Harlem Shake Launcher for Android");
		bundle.putString("description", "Your android can do the Harlem Shake. Download it from google play");
		bundle.putString("link", "https://play.google.com/store/apps/details?id=mobi.shush.harlemlauncher");
		bundle.putString("name", "Harlem Shake Launcher");
		bundle.putString("picture", "http://shush.mobi/bla.png");
		new WebDialog.FeedDialogBuilder(getActivity(), session, bundle).build().show();
		*/
	}
	 
	 private Session.StatusCallback callback2 = new Session.StatusCallback() {
			
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				//Toast.makeText(getActivity(), "start get session", Toast.LENGTH_SHORT).show();
				if (state.isOpened()) {
					publishFeedDialog();
				}
			}
		};
	public void publishFeedDialog(){
		Bundle params = new Bundle();
	    params.putString("name", "Facebook SDK for Android");
	   params.putString("caption", "Build great social apps and get more installs.");
	    params.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
	    params.putString("link", "https://developers.facebook.com/android");
	    params.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

	    WebDialog feedDialog = (
	            new WebDialog.FeedDialogBuilder(getActivity(),
	                    Session.getActiveSession(),
	                    params)).build();
	    feedDialog.show();
	}
	
	
	public class ViewHolder {
		public TextView timetv;
		public TextView userNameTv;
		public TextView titletv;
		public ImageView profileImage, NewsFeedImage, CommentImage, likeImage,
				ViewImageView, ShareImage, loader;
		public AdView adView;
		private LinearLayout layout_ad;
		public TextView like_count;
		public TextView view_count;
		public TextView comment_count;
		public ProgressBar pbar;
		public LinearLayout layout;
		public LinearLayout itemClickLayout;
		public TextView ARModel;
		public WebView web;
		public ProgressBar preWeb;
		// public ImageView image;
	}

	private class MyAdapter extends BaseAdapter {

		private LayoutInflater mInflater;
		private ArrayList<itemlist> item_detail;
		private ListView listview;
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		WebView webView;
		private ArrayList<PublisherAdView> adList = new ArrayList<PublisherAdView>();
		private ArrayList<ViewGroup> viewList = new ArrayList<ViewGroup>();

		// private BitmapCache bitmapCache;
		public MyAdapter(Context context, ArrayList<itemlist> item_detail,
				ListView listview) {
			this.mInflater = LayoutInflater.from(context);
			this.item_detail = item_detail;
			this.listview = listview;

			// View inflate =
			// getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
		}

		public int getCount() {
			// return itemStringList.size();
			return item_detail.size();
			// return Integer.valueOf(currentIitemNum);
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			final View view = convertView;
			final int position2 = position;
			// Toast.makeText(getActivity(),""+ position,
			// Toast.LENGTH_SHORT).show();

			/*
			 * if(viewlist.size()==position){ convertView =
			 * mInflater.inflate(R.layout.testnewfeed, null); ViewHolder
			 * holder2= new ViewHolder(); holder2.likeImage = (ImageView)
			 * convertView.findViewById(R.id.likeImageView);
			 * viewlist.add(holder2); }
			 */
			if (convertView == null || convertView instanceof PublisherAdView) {
				// view = getLayoutInflater().inflate(R.layout.testnewfeed,
				// parent, false);
				
				
				// Toast.makeText(getActivity(), "start"+position,
				// Toast.LENGTH_SHORT).show();
				holder = new ViewHolder();
				//if(checkNetwork.NetworkType()==1||checkNetwork.NetworkType()==4){
					convertView = mInflater.inflate(R.layout.testnewfeed2, null);
					holder.web = (WebView) convertView
							.findViewById(R.id.NewsFeedImage);
					holder.preWeb = (ProgressBar)convertView
							.findViewById(R.id.preload_webview);
				/*}else{
				convertView = mInflater.inflate(R.layout.testnewfeed, null);
				holder.NewsFeedImage = (ImageView) convertView
						.findViewById(R.id.NewsFeedImage);
				}*/
				holder.timetv = (TextView) convertView
						.findViewById(R.id.timestampTextView);
				holder.userNameTv = (TextView) convertView
						.findViewById(R.id.usernameTextView);
				holder.titletv = (TextView) convertView
						.findViewById(R.id.titleTextView);
				holder.profileImage = (ImageView) convertView
						.findViewById(R.id.image);
				// holder.profileImage = (FadeInNetworkImageView)
				// convertView.findViewById(R.id.profilePicImageView);
				
				holder.layout_ad = (LinearLayout) convertView
						.findViewById(R.id.layout_item);
				holder.likeImage = (ImageView) convertView
						.findViewById(R.id.likeImageView);
				holder.like_count = (TextView) convertView
						.findViewById(R.id.likeCountTextView);
				holder.ViewImageView = (ImageView) convertView
						.findViewById(R.id.viewImageView);
				holder.view_count = (TextView) convertView
						.findViewById(R.id.viewCountTextView);
				holder.comment_count = (TextView) convertView
						.findViewById(R.id.commentCountTextView);
				holder.CommentImage = (ImageView) convertView
						.findViewById(R.id.commentImageView);
				holder.ARModel = (TextView) convertView
						.findViewById(R.id.TextAR);
				holder.ShareImage = (ImageView)convertView.findViewById(R.id.shareImageView);
				// holder.pbar = (ProgressBar)
				// convertView.findViewById(R.id.progressBar1);
				holder.layout = (LinearLayout) convertView
						.findViewById(R.id.layout_item);
				holder.itemClickLayout = (LinearLayout) convertView
						.findViewById(R.id.itemCardLinearLayout);
				RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) holder.layout
						.getLayoutParams();
				param.height = dis[1] * 4 / 5;
				holder.layout.setLayoutParams(param);

				convertView.setTag(holder);

				if (position == 0 || position == 3) {
					Log.i("if statment in adview", position + "");

					// LinearLayout layout = (LinearLayout)
					// convertView.findViewById(R.id.layout_item);
					holder.adView = new AdView(getActivity());
					holder.adView
							.setAdUnitId("ca-app-pub-8709149684611651/2135536124");
					holder.adView.setAdSize(AdSize.BANNER);
					AdRequest request = new AdRequest.Builder().addTestDevice(
							AdRequest.DEVICE_ID_EMULATOR).build();

					holder.layout.addView(holder.adView);
					AdRequest adRequest = new AdRequest.Builder()
							.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
							.addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
							.build();

					// Start loading the ad in the background.
					holder.adView.loadAd(adRequest);
					/*  */

					convertView.setTag(holder);

				}

				/*
				 * if(itemStringList.size()>position){ new
				 * GetOneItemThread(holder
				 * ,itemStringList.get(position),position).execute(); }
				 */
				// new LoadOneProducts(holder,position).execute();
				// new
				// GetOneItemThread(holder,itemStringList.get(position),position).execute();
				// new GetItemThread(position).start();
				// new
				// GetOneItemThread(holder,itemStringList.get(position),position).execute();
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			/*
			 * if(itemStringList.size()>position&&item_detail.size()<=position){
			 * 
			 * new
			 * GetUserAllAtOne(holder,position).execute(itemStringList.get(position
			 * ));
			 * 
			 * }else{ if(item_detail.size()>position){
			 */

			//Toast.makeText(getActivity(), "position: "+position2, Toast.LENGTH_SHORT).show();
			
			// for 360
		/*	
			
			if (im360 != null) {

				im360.stop();
				im360 = null;

			}

			 String uid = item_detail.get(position).getiDetail().getItem_uid();
			 
			 
			 if(imageUrls2.containsKey(uid)){
				 imgLoader.fileCache.setName(uid);
					 if(checkImagesFilesIsDownloaded(imgLoader,uid)){					 
						 String[] Urls = imageUrls2.get(uid);
						 updateView(holder.NewsFeedImage, Urls);
					 }
			
			 }else{
				 
				 if(playStatus.size()==position)
				         { 
						 playStatus.add(true);
						 }
				 
				 switch(position){
				 case 0: 
					 
					 if(playStatus.get(position)){
					 imgSaveder.fileCache.setName(uid);
					 new GetImageURList(position,uid).execute();
					 playStatus.set(position, false);
					 }
					 break;
				 default:
					 String previous_uid = item_detail.get(position-1).getiDetail().getItem_uid();
					 if(imageUrls2.containsKey(previous_uid)){
						 imgLoader.fileCache.setName(previous_uid);
						 if(checkImagesFilesIsDownloaded(imgLoader,previous_uid)){
							 if(playStatus.get(position)){
							 imgSaveder.fileCache.setName(uid);
							 new GetImageURList(position,uid).execute();
							 playStatus.set(position, false);
							 }
						 }
					 }
					 break;
				 }
				 
			 }
			 */
			 
			 
			 
			holder.timetv.setText(item_detail.get(position2).getiDetail()
					.getstr_item_create_date());

			String nameString = item_detail.get(position2).getuDetail()
					.getUser_nick_name();
			if (nameString.length() > 20) { // show part of the user name if
											// user name is too long
				nameString = nameString.substring(0,
						nameString.length() * 2 / 3) + "...";
			}

			holder.userNameTv.setText(nameString);

			// holder.titletv.setText(item_detail.get(position2).get("item_description").toString());
			holder.titletv.setText(item_detail.get(position2).getiDetail()
					.getItem_description());
			// holder.titletv.setText("test item description "+position);

			final String view_count = item_detail.get(position2).getiDetail()
					.getView_count();
			// final String item_uid =
			// item_detail.get(position2).get("item_uid").toString();
			final String item_uid = item_detail.get(position2).getiDetail()
					.getItem_uid();
			
			holder.ShareImage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				//PostToFacebookWall();
				
				 String[] arr = {"Facebook ","others..."};
	                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	                builder.setTitle("Share To Your Friends!")
	                        .setItems(arr, new DialogInterface.OnClickListener() {
	                            public void onClick(DialogInterface dialog, int which) {
	                                // The 'which' argument contains the index position
	                                // of the selected item
	                                 if(which==0){
	                               
	                                	 PostToFacebookWall(item_detail.get(position2).getiDetail()
		                        					.getItem_description(),item_detail.get(position2).getiResources()
		                        					.getResource_url(),"http://www.apptech.com.hk/camera3d.html");
	                                }
	                                if(which==1){
	                                    onShareClick(item_detail.get(position2).getiDetail()
	                        					.getItem_description(),item_detail.get(position2).getiResources()
	                        					.getResource_url(),"http://www.apptech.com.hk/camera3d.html");
	                                }
	                            }
	                        });
	                AlertDialog dialog = builder.create();
	                dialog.show();
				
	                
				}
			
			});
			
		
			
			
			
				 WebSettings websettings = holder.web.getSettings();  
			        websettings.setSupportZoom(true);  
			        websettings.setBuiltInZoomControls(true);   
			        websettings.setJavaScriptEnabled(true);
			        holder.web.loadUrl("about:blank");
			        String ProductURL ="http://camera3d.apptech.com.hk/3dpic.php?item="+item_uid;
					holder.web.loadUrl(ProductURL);
				holder.web.setWebViewClient(new WebViewClient(){
					 @Override
					    public boolean shouldOverrideUrlLoading(WebView view, String url) {
							
					        return true;
					    }
					 @Override
					public void onPageStarted(WebView view, String url, Bitmap favicon) {
						 holder.web.setVisibility(View.INVISIBLE);
						holder.preWeb.setVisibility(View.VISIBLE);
						super.onPageStarted(view, url, favicon);
					}
					 
					@Override
					public void onPageFinished(WebView view, String url) {
						holder.web.setVisibility(View.VISIBLE);
						holder.preWeb.setVisibility(View.INVISIBLE);
						super.onPageFinished(view, url);
						if (checkNetwork.isNetworkAvailable()) {
							new AddViewNumber(item_uid, holder.view_count,
									view_count, position2).execute();}
					}
		
				});
				
			holder.web.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					/*if (checkNetwork.isNetworkAvailable()) {
						new AddViewNumber(item_uid, holder.view_count,
								view_count, position2).execute();
						Intent intent = new Intent(getActivity(),
								WebViewActivity.class);
						intent.putExtra("item_uid", item_uid);
						
						Log.i("item_uid intent", item_uid);
						//startActivity(intent);
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();
					}
					Intent intent = new Intent(getActivity(),
							WebViewActivity.class);
					intent.putExtra("item_uid", item_uid);
					Log.i("item_uid intent", item_uid);
					startActivity(intent);
					*/

				}

			});	 /**/
		  
		        
			
			/*}else{
                Toast.makeText(getActivity(), "switch to wifi can watch 3d in that feed!", Toast.LENGTH_SHORT).show();
				holder.NewsFeedImage.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						if (checkNetwork.isNetworkAvailable()) {
							new AddViewNumber(item_uid, holder.view_count,
									view_count, position2).execute();
						/*	Intent intent = new Intent(getActivity(),
									WebViewActivity.class);
							intent.putExtra("item_uid", item_uid);
							
							Log.i("item_uid intent", item_uid);
							//startActivity(intent);
						} else {
							Toast.makeText(
									getActivity(),
									"No network connection, please try it again!!!",
									Toast.LENGTH_SHORT).show();
						}
						Intent intent = new Intent(getActivity(),
								WebViewActivity.class);
						intent.putExtra("item_uid", item_uid);
						Log.i("item_uid intent", item_uid);
						startActivity(intent);

					}

				});
				imgLoader.DisplayImage(item_detail.get(position2).getiResources()
						.getResource_url(), holder.NewsFeedImage);
			}
			*/
			

			// ImageLoader.getInstance().displayImage(item_detail.get(position2).get("user_avatar_url"),
			// holder.profileImage, optionsOfSamllIcon, animateFirstListener);
			// ImageLoader.getInstance().displayImage(item_detail.get(position2).get("resource_url"),
			// holder.NewsFeedImage, optionsOfPhoto, animateFirstListener);
			/*
			 * imgLoader.DisplayImage(item_detail.get(position2).get(
			 * "user_avatar_url"), holder.profileImage);
			 * imgLoader.DisplayImage(item_detail
			 * .get(position2).get("resource_url"), holder.NewsFeedImage);
			 * holder
			 * .like_count.setText(item_detail.get(position2).get("like_count"
			 * ).toString());
			 * holder.view_count.setText(item_detail.get(position2
			 * ).get("view_count").toString());
			 * holder.comment_count.setText(item_detail
			 * .get(position2).get("comment_count").toString());
			 */
			
			imgLoader.DisplayImage(item_detail.get(position2).getuDetail()
					.getUser_avatar_url(), holder.profileImage);
		

			holder.like_count.setText(simplifyCount(item_detail.get(position2).getiDetail()
					.getLike_count()));
			holder.view_count.setText(simplifyCount(item_detail.get(position2).getiDetail()
					.getView_count()));
			holder.comment_count.setText(simplifyCount(item_detail.get(position2)
					.getiDetail().getComment_count()));

			// final String view_count =
			// item_detail.get(position2).get("view_count").toString();

			holder.CommentImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkNetwork.isNetworkAvailable()) {
						if (uiHelper == null) {
							uiHelper = new UiLifecycleHelper(getActivity(),
									callback);
						}
						if (session != null) {
							Log.i("Login statue ", "" + session.getState());
							session.getState();

							if (onSessionStateChange(session,
									session.getState(), null)) {
								Intent intent = new Intent(getActivity(),
										CommentActivity.class);
								intent.putExtra("item_uid", item_uid);
								getActivity().startActivity(intent);
							}

						}
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();

					}
				}
			});

			holder.ViewImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkNetwork.isNetworkAvailable()) {
						new AddViewNumber(item_uid, holder.view_count,
								view_count, position2).execute();
						Intent intent = new Intent(getActivity(),
								WebViewActivity.class);
						intent.putExtra("item_uid", item_uid);
						Log.i("item_uid intent", item_uid);
						startActivity(intent);
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();
					}
				}
			}); 

			if (Integer.parseInt(item_detail.get(position2).getiDetail()
					.getItem_isLiked()) == 1) {
				holder.likeImage.setImageResource(R.drawable.like_on);
			} else {
				holder.likeImage.setImageResource(R.drawable.like_off);
			}

			holder.likeImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkNetwork.isNetworkAvailable()) {

						if (uiHelper == null) {
							uiHelper = new UiLifecycleHelper(getActivity(),
									callback);
						}
						if (session != null) {
							Log.i("Login statue ", "" + session.getState());
							session.getState();

							if (onSessionStateChange(session,
									session.getState(), null)) {
								
								new CreateLike(item_uid, holder.likeImage,
										item_detail.get(position2).getiDetail()
												.getLike_count(),
										holder.like_count, position2).execute();
							}

						}
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();
					}
				}
			});

			holder.ARModel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkNetwork.isNetworkAvailable()) {
						Intent intent = new Intent(getActivity(),
								SampleCamActivity.class);
						intent.putExtra("url", "samples"
								// + File.separator +
								// "1_ImageRecognition_1_ImageOnTarget"
								+ File.separator + "testWiki" + File.separator
								+ "index.html");
						intent.putExtra("id", item_uid);

						intent.putExtra("Title", "Camera3D AR");
						intent.putExtra("ir", true);
						intent.putExtra("geo", false);
						getActivity().startActivity(intent);
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();
					}
				}

			});

			holder.itemClickLayout
					.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {

							if (checkNetwork.isNetworkAvailable()) {
								if (uiHelper == null) {
									uiHelper = new UiLifecycleHelper(
											getActivity(), callback);
								}
								if (session != null) {
									Log.i("Login statue ",
											"" + session.getState());
									session.getState();

									if (onSessionStateChange(session,
											session.getState(), null)) {

										/*
										 * String user_nick_name
										 * =item_detail.get
										 * (position2).get("user_nick_name"
										 * ).toString(); String user_email
										 * =item_detail
										 * .get(position2).get("user_email"
										 * ).toString(); String user_avatar_url
										 * = item_detail.get(position2).get(
										 * "user_avatar_url").toString(); String
										 * user_uid =
										 * item_detail.get(position2).
										 * get("user_id").toString();
										 */
										String user_nick_name = item_detail
												.get(position2).getuDetail()
												.getUser_nick_name();
										String user_email = item_detail
												.get(position2).getuDetail()
												.getUser_email();
										String user_avatar_url = item_detail
												.get(position2).getuDetail()
												.getUser_avatar_url();
										String user_uid = item_detail
												.get(position2).getuDetail()
												.getUser_id();
										Newsfeed_nextFrag = new Search_item_click_Fragment();

										Bundle bundle = new Bundle();
										bundle.putString("user_nick_name",
												user_nick_name);
										bundle.putString("user_email",
												user_email);
										bundle.putString("user_avatar_url",
												user_avatar_url);
										bundle.putString("user_uid", user_uid);
										Newsfeed_nextFrag.setArguments(bundle);

										fragmentTransaction = NewsfeedFragment.this
												.getFragmentManager()
												.beginTransaction();
										MainActivity
												.processFragmentToShowAndHideOtherFragment(
														Newsfeed_nextFrag,
														fragmentTransaction);
										MainActivity
												.addFragmentToTransactionAndStack(
														Newsfeed_nextFrag,
														fragmentTransaction);
										fragmentTransaction.commit();

									}

								}
							} else {
								Toast.makeText(
										getActivity(),
										"No network connection, please try it again!!!",
										Toast.LENGTH_SHORT).show();
							}
						}

					});/* */
			/*
			 * } }
			 */

			return convertView;
		}

	}

	private String simplifyCount(String count)
	{
		int num_of_count = Integer.parseInt(count);
		if(num_of_count>=1000)
		{
			long number = num_of_count/1000;
			return number+"k";
		}
		return count;
	}
	
	private void updateView(ImageView im, String[] itemList) {

		if (im360 != null) {
			im360.stop();
			//im360 = null;

		}

		else {

			im360 = new Image360(im, itemList);
		}

	}

	class Image360 {
		// img state
		private int iFragment;
		private int iCurrent, iFirstImg, iLastImg;

		// playing config.
		private long PLAY_PERIOD = 4000; // ms
		private long lImgDisplayTime; // ms per img

		// deceleration
		float SLOW_RATE = 1.1f; // FPS incresing ratio (resistence of rotation)
		final float CONVERSION_CONSTANT = 10000.0f; // NEED TO TUNE
		final long MIN_SPF = 50; // ms per img (fastest)
		final long MAX_SPF = 200; // ms per img (slowest)

		// UI
		private ImageView ivPreview;

		String[] ImageItemList;

		// about touch
		// private VelocityTracker mVelocityTracker;
		private float fLastX;// = -1;
		private float fLastUpdatedX;// = -1;
		private float fDeltaUpdatedX;// = -1;
		private boolean bToLeft;
		final int SENSITIVITY = 20; // dp

		// threads
		private RotateThread threadRotate;
		private DecelerateThread threadDecelerate;

		public Image360(ImageView ivPreview, String[] ImageItemList) {
			this.ivPreview = ivPreview;
			this.ImageItemList = ImageItemList;
			iFragment = ImageItemList.length;
			iCurrent = 0;
			iFirstImg = 0;
			iLastImg = iFragment - 1;
			// Log.d("Folder stat", "#: " + countFiles());

			if (iFragment != 0)
				lImgDisplayTime = PLAY_PERIOD / iFragment;

			if (ImageItemList.length > 1) {
				threadRotate = new RotateThread();
				threadRotate.start();
			}
		}

		public void stop() {
			
			if (threadRotate != null) {
				threadRotate.stopRotating();
			}
			if (threadDecelerate != null) {
				threadDecelerate.stopDecelerating();
			}
		
		}

		private void prevImg() {
			if (iCurrent <= iFirstImg) { // from first img to last img
				iCurrent = iLastImg;
                  ivPreview.setImageBitmap(imgLoader.getBitmap2(ImageItemList[iCurrent]));
			} else {
				iCurrent -= 1;
				ivPreview.setImageBitmap(imgLoader.getBitmap2(ImageItemList[iCurrent]));
			}

		}

		private void nextImg() {
			if (iCurrent >= iLastImg) { // from last img to first img
				iCurrent = iFirstImg;
				ivPreview.setImageBitmap(imgLoader.getBitmap2(ImageItemList[iCurrent]));
			} else {
				iCurrent += 1;
				ivPreview.setImageBitmap(imgLoader.getBitmap2(ImageItemList[iCurrent]));
			}

		}

		// ===================== thread ===================
		class RotateThread extends Thread {
			boolean bKeepRotating = true;

			public void run() {
				while (bKeepRotating) {
					ivPreview.post(new Runnable() {
						public void run() {
							nextImg();
						}
					});
					try {
						Thread.sleep(lImgDisplayTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			public void stopRotating() {
				bKeepRotating = false;
			}
		}

		class DecelerateThread extends Thread {
			boolean bKeepDecelerating = true;
			float fDisplayTimeInDece;
			boolean bToLeft;

			public DecelerateThread(float lInitialDisplayTime,
					boolean bInitialToLeft) {
				fDisplayTimeInDece = lInitialDisplayTime;
				if (fDisplayTimeInDece < MIN_SPF) { // too short
					fDisplayTimeInDece = MIN_SPF;
					Log.d("decelerateThread", "Too short: "
							+ lInitialDisplayTime + " --> "
							+ fDisplayTimeInDece);
				}

				bToLeft = bInitialToLeft;
			}

			public void run() {
				while (bKeepDecelerating) {
					if (fDisplayTimeInDece >= MAX_SPF) {
						Log.d("decelerateThread", "Too slow: "
								+ fDisplayTimeInDece);
						break;
					}

					ivPreview.post(new Runnable() {
						@Override
						public void run() {
							if (bToLeft)
								nextImg();
							else
								prevImg();
						}
					});

					Log.d("decelerateThread", "Display Time: "
							+ fDisplayTimeInDece);

					try {
						Thread.sleep((long) fDisplayTimeInDece);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// update next display time
					fDisplayTimeInDece *= SLOW_RATE;
				}
			}

			public void stopDecelerating() {
				bKeepDecelerating = false;
			}
		}

	}

	/** AsyncTask to get the detail from PHP **/
	class GetImageURList extends AsyncTask<Object, String, Object> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		int position;
		String item_uid;
		public GetImageURList(int position,String item_uid){
			this.position = position;
			this.item_uid = item_uid;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(Object... params) {
			String jsonString = "";
			try {

				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(
						//"http://camera3d.apptech.com.hk/index.php/xidong/resource/get_imageUrlList2");
						"http://camera3d.apptech.com.hk/index.php/xidong/resource/get_imageUrlList");
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("item_uid", this.item_uid));

				// Encoding data
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request
				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
						jsonString = builder.toString();
					}

				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return jsonString;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/

		protected void onPostExecute(Object s) {
			super.onPostExecute(s);

			Boolean status = null;
			JSONArray jsonArray = null;
			JSONObject object = null;

			if (((String) s != null) && (((String) s).equals("") == false)) {
				try {
					Log.d("Image URL", (String) s);
					object = new JSONObject((String) s);
					status = object.getBoolean("status");
					Log.d("Status Info", status.toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}

				if (status) {
					try {

						jsonArray = object.getJSONArray("resources");
						ConvertData(jsonArray,this.item_uid);

					} catch (JSONException e) {
						e.printStackTrace();
					}

				}

			}

		}

	}

	
	public void ConvertData(JSONArray jsonArray,String item_uid) {
		try {
			
			Log.d("User Info", jsonArray.toString());
			if (jsonArray != null) {
				String[] imageUrls=new String[jsonArray.length()];
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject obj = jsonArray.getJSONObject(i);
					imageUrls[i] = obj.getString("resource_url").toString();
					Log.d("User " + i, obj.getString("resource_url").toString());
				}
				if(imageUrls.length>0){
					
					imageUrls2.put(item_uid, imageUrls);
					imgLoader.fileCache.setName(item_uid);
					if(!checkImagesFilesIsDownloaded(imgLoader,item_uid)){
						imgSaveder.fileCache.setName(item_uid);
						saveImageToFile(imageUrls);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	

	public void saveImageToFile(String[] imageUrls){
		//urls = imageUrls;
		for(int i=0;i<imageUrls.length;i++)
					{
			imgSaveder.savePhoto(imageUrls[i]);
					}
		
	}
	
	public Boolean checkImagesFilesIsDownloaded(ImageLoader2 imloder,String item_uid){
		
		String[] Urls = imageUrls2.get(item_uid);
	 
        if(imloder.getSize()>=Urls.length){
        	return true;
	    }
        return false;
        
	}
	public void onShareClick(String title, String Url, String store) {
	       // Resources resources = getResources();

	        Intent emailIntent = new Intent();
	        emailIntent.setAction(Intent.ACTION_SEND);
	        emailIntent.setType("message/rfc822");

	        PackageManager pm = getActivity().getPackageManager();
	        Intent sendIntent = new Intent(Intent.ACTION_SEND);
	        sendIntent.setType("text/plain");
	        Intent openInChooser = Intent.createChooser(emailIntent, "");

	        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
	        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();

	        for (int i = 0; i < resInfo.size(); i++) {
	            // Extract the label, append it, and repackage it in a LabeledIntent
	            ResolveInfo ri = resInfo.get(i);
	            String packageName = ri.activityInfo.packageName;
	            Log.d("pakageName",packageName);
	            if(packageName.contains("android.email")) {
	                emailIntent.setPackage(packageName);
	            } else if(packageName.contains("twitter")|| packageName.contains("plus")|| packageName.contains("bluetooth")|| packageName.contains("mms") || packageName.contains("android.gm")) {
	                Intent intent = new Intent();
	                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
	                intent.setAction(Intent.ACTION_SEND);
	                intent.setType("text/plain");
	                intent.putExtra(Intent.EXTRA_TEXT, title+"\n"+ Url+"\nDownload App link click this link --->"+"\n"+store);
	                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
	            }
	        }

	        // convert intentList to array
	        //  if(!facebook_choose) {
	        LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);
	        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
	        startActivity(openInChooser);
	        //}
	    }
	
	@Override
	public void onRefresh() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				currentIitemNum = "5";
				Log.i("Get into here ", "");
				// get five first

				itemStringList.clear();
				try {
					if (!checkNetwork(1)) {
						onLoad();
					}
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// new LoadProductsByCondition(1).execute(currentIitemNum);

			}
		}, 0);
	}

	private void onLoad() {

		mListView.stopRefresh();
		mListView.stopLoadMore();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String currentDateandTime = sdf.format(new Date());
		mListView.setRefreshTime(currentDateandTime);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {

				currentIitemNum = String.valueOf(Integer
						.parseInt(currentIitemNum) + 5);
				try {
					if (!checkNetwork(2)) {
						onLoad();
					}
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// new LoadProductsByCondition(2).execute(currentIitemNum);

				Log.i("currentIitemNum", currentIitemNum);

			}
		}, 0);

		// TODO Auto-generated method stub

	}

	private void applyScrollListener() {
		mListView.setOnScrollListener(new PauseOnScrollListener(ImageLoader
				.getInstance(), pauseOnScroll, pauseOnFling));
	}

	private void showDialog(final int condition) {
		new AlertDialog.Builder(getActivity())
				.setMessage("No Network Connection!")
				.setCancelable(false)
				.setNegativeButton("cancel",
						new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								// android.os.Process.killProcess(android.os.Process.myPid());
							}

						})
				.setPositiveButton("reconnect",
						new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								try {
									checkNetwork(condition);
								} catch (ClientProtocolException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						}).show();

	}

	
	
	private Boolean checkNetwork(int condition) throws ClientProtocolException,
			IOException {
		final Boolean network = checkNetwork.isNetworkAvailable();
		if (network) {
			try {
				new LoadProductsByCondition(condition).execute(currentIitemNum);
			} catch (Exception e) {
				e.printStackTrace();
			}
			;
		} else {
			showDialog(condition);
		}
		return network;
	}

	public class LoadAdview extends AsyncTask<Object, AdView, AdView> {

		private AdView adView;
		LinearLayout layout;
		private int position;

		@Override
		protected AdView doInBackground(Object... parameters) {
			adView = (AdView) parameters[0];

			// Create an ad request. Check logcat output for the hashed device
			// ID to
			// get test ads on a physical device.
			AdRequest adRequest = new AdRequest.Builder()
					.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
					.addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE").build();

			// Start loading the ad in the background.
			adView.loadAd(adRequest);

			return adView;
		}

		@Override
		protected void onPostExecute(AdView result) {

		}
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}
