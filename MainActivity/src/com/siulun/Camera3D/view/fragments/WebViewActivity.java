package com.siulun.Camera3D.view.fragments;




import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import com.siulun.Camera3D.R;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.siulun.Camera3D.model.*;

public class WebViewActivity extends Activity {
	public  String item_uid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_fragment);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		  item_uid = extras.getString("item_uid");
		   
		}
       
        WebView myBrowser=(WebView)findViewById(R.id.webView1);  
  	  //String ProductURL = "http://54.251.110.85/camera3d/www/3dpic.php?item="+item_uid;
  	String ProductURL ="http://camera3d.apptech.com.hk/3dpic.php?item="+item_uid;
        StringConstants constant = new StringConstants();
  	    //String ProductURL = constant.URL_WEBVIEW +item_uid;
        WebSettings websettings = myBrowser.getSettings();  
        websettings.setSupportZoom(true);  
        websettings.setBuiltInZoomControls(true);   
        websettings.setJavaScriptEnabled(true);  
        
        myBrowser.setWebViewClient(new WebViewClient());  
      
        
        myBrowser.loadUrl(ProductURL);
        

		  AdView adView = new AdView(this);
		  adView.setAdUnitId("ca-app-pub-8709149684611651/2135536124");
		  adView.setAdSize(AdSize.BANNER);
		  AdRequest request = new AdRequest.Builder()
		    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		    .build();
		  LinearLayout layout = (LinearLayout)findViewById(R.id.WebViewAd);
		  layout.addView(adView);
		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device.
		AdRequest adRequest = new AdRequest.Builder()
		    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		    .addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
		    .build();

	}
	}
// Start loading the ad in the background.