package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;


import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.android.gms.plus.Plus;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.ppierson.t4jtwitterlogin.T4JTwitterLoginActivity;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.FacebookLoginFragment.LoginWithEmail;
import com.siulun.Camera3D.view.fragments.SignUpFragment.registration;



public class User_Info_Fragment extends Fragment{
	
	
	
	public static final int SELECT_PICTURE = 1;

	LoginSharedPreferences sharedpreferences;
	
	public LayoutInflater layoutInflater;
	public LinearLayout user_info_ll;
	public ImageButton backbutton;
	public TextView usernicknameTV,user_email_tv;
	public EditText newpassword,confiempw;
	public Button loglout_btn,change_pw,upload_profilepic;
	public ImageView user_profie_pic;
	private FragmentManager fragmentManager;
	private FragmentTransaction fragmentTransaction;
	private String selectedImagePath;
	 int serverResponseCode = 0;
	private DisplayImageOptions optionsOfSamllIcon;
	
	NetworkChecking checkNetwork;
	
	private  ImageLoadingListener animateFirstListener ;
	
		@Override
	    public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			Log.d("Gallery", "Start: onCreate()");
		//	setContentView(R.layout.gallery);

		
			
			sharedpreferences= new LoginSharedPreferences(getActivity());
			
			optionsOfSamllIcon = new DisplayImageOptions.Builder()
			.showImageOnLoading(R.drawable.imageloading)
			.showImageForEmptyUri(R.drawable.empty_profile)
			.showImageOnFail(R.drawable.empty_profile)
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.considerExifParams(true)
			.displayer(new RoundedBitmapDisplayer(20))
			.build(); 
			 
	
		    	animateFirstListener = new AnimateFirstDisplayListener();
	

	    }
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		user_info_ll = (LinearLayout)layoutInflater.inflate(R.layout.user_info_fragment, container, false);
		user_info_ll.setBackgroundColor(Color.WHITE);
		 backbutton =(ImageButton) user_info_ll.findViewById(R.id.user_info_back);
		usernicknameTV = (TextView)user_info_ll.findViewById(R.id.user_nick_name_tv);
		user_email_tv = (TextView)user_info_ll.findViewById(R.id.user_email_tv);
		loglout_btn = (Button)user_info_ll.findViewById(R.id.logout); 
		change_pw = (Button)user_info_ll.findViewById(R.id.save_button); 
		newpassword  =(EditText) user_info_ll.findViewById(R.id.Password_edit);
		confiempw  = (EditText)user_info_ll.findViewById(R.id.confirm_pw_edit);
		upload_profilepic = (Button)user_info_ll.findViewById(R.id.upload_profilePic_btn); 
		 user_profie_pic = (ImageView)user_info_ll.findViewById(R.id.user_icon_img); 
		setUserProfilePic(user_info_ll);
		//new GetXMLTask().execute(((UserInfo) getActivity().getApplication()).getToken());
		//Log.i("user Info Url",((UserInfo) getActivity().getApplication()).getUser_avatar_url());
		//ImageLoader.getInstance().displayImage(((UserInfo) getActivity().getApplication()).getUser_avatar_url(), user_profie_pic, optionsOfSamllIcon, animateFirstListener);
		ImageLoader.getInstance().displayImage(sharedpreferences.getUserAvatarUrl(), user_profie_pic, optionsOfSamllIcon, animateFirstListener);
		change_pw.setOnClickListener  (new OnClickListener(){ 
			String password,confirmpassword;
	            @Override
	            public void onClick(View v) {
	            	password = newpassword.getText().toString();
	            	confirmpassword = confiempw.getText().toString();
	        		
	        		 if(password.matches("")||confirmpassword.matches("")){
	        			 Toast.makeText(getActivity(),"Please Enter All The Field",Toast.LENGTH_SHORT).show();
	        		 }
	        		 else if(password.length()<5){
	            		 Toast.makeText(getActivity(),"Please Enter At Least 6 Letter For Password",Toast.LENGTH_SHORT).show();
	            	}
	            	else if(password.equals(confirmpassword)){
	            		//String token  =  ((UserInfo) getActivity().getApplication()).getToken();
	            		String token  = sharedpreferences.getToken();
	            		String password_md5 = md5(newpassword.getText().toString());
	            		  if(checkNetwork.isNetworkAvailable()){	
	             new ChangePassword().execute(token,password_md5);
	            		  }else{
						    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
								
					        }
	            }
	            	else{
	            		 Toast.makeText(getActivity(),"Two passwords are not match!",Toast.LENGTH_SHORT).show();
	            	}
	            }
	        });  
		
		upload_profilepic.setOnClickListener  (new OnClickListener(){ 
			
            @Override

            public void onClick(View v) {
            	//  Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                 // startActivityForResult(intent, SELECT_PICTURE);
          if(checkNetwork.isNetworkAvailable()){	  
            	 Intent intent = new Intent();
            	 intent.setType("image/*");
                 intent.setAction(Intent.ACTION_GET_CONTENT);
                 startActivityForResult(intent, SELECT_PICTURE);
            	  }else{
				    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
						
			        }
            }         

        });   
		
		 backbutton.setOnClickListener  (new OnClickListener(){ 
	
	            @Override

	            public void onClick(View v) {
	            	Log.i("onclick","backbutton");
	            	//fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	            	getActivity().finish();
	          
	            }         

	        });   
			loglout_btn.setOnClickListener  (new OnClickListener(){ 

	            @Override

	            public void onClick(View v) {
	            
	            	  Session session = Session.getActiveSession();
	            	  session.closeAndClearTokenInformation();
	            	 /* ((UserInfo) getActivity().getApplication()).setToken(null);
	            	  ((UserInfo) getActivity().getApplication()).setFinished(false);
	            	  ((UserInfo) getActivity().getApplication()).setUser_nick_name(null);
	            	  ((UserInfo) getActivity().getApplication()).setUser_avatar_url(null);
	            	 
	            	  */ 
	            	  getActivity().finish();
	            	  
	            	  FacebookLoginFragment.count=0;
	            	  
	            	  T4JTwitterLoginActivity.logOutOfTwitter(getActivity());
	            	  
	            	  signOutFromGplus();
	            	  
	            	  sharedpreferences.logoutUser();
	            		Intent intent = new Intent(getActivity(), SettingFragment.class);
	     	           startActivity(intent);
	     	          
	          
	            }         

	        });   
	   
		/* usernicknameTV.setText(((UserInfo) getActivity().getApplication()).getUser_nick_name());
		  user_email_tv.setText(((UserInfo) getActivity().getApplication()).getEmail());
		  */
			 usernicknameTV.setText(sharedpreferences.getUser_nick_name());
			  user_email_tv.setText(sharedpreferences.getEmail());
        return user_info_ll;  
    }
	
	 /* Sign-out from google
	 * */
	private void signOutFromGplus() {
		//Toast.makeText(getActivity(), "sign out is called", Toast.LENGTH_SHORT).show();
		if (FacebookLoginFragment.mGoogleApiClient.isConnected()) {
		//	Toast.makeText(getActivity(), "inside sign out is called", Toast.LENGTH_SHORT).show();
			Plus.AccountApi.clearDefaultAccount(FacebookLoginFragment.mGoogleApiClient);
			FacebookLoginFragment.mGoogleApiClient.disconnect();
			FacebookLoginFragment.mGoogleApiClient.connect();
			//updateUI(false);
		}
	}   
	
	 private void setUserProfilePic (View ConvertView){
		 user_profie_pic = (ImageView)ConvertView.findViewById(R.id.user_icon_img); 
	 }
	 private ImageView getUserProfilePic() {

			return user_profie_pic;
		}
	  private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    	if (state.isOpened()) {
	    	

        		Log.i("state",state+"");
	    	
	        } else if (!state.isClosed()) {
	        	
	        }
	    }

		/** AsyncTask to get the detail from PHP **/
		class ChangePassword extends AsyncTask<String, String, String> {
			
			//GalleryFragment  fragement;
			String usertoken = null;

			String success = null;
			
			private ProgressDialog pdia;
			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				 pdia = new ProgressDialog(getActivity());
			        pdia.setMessage("Loading...");
			        pdia.show();  
				
			}

			/**
			 * Getting product details in background thread
			 * */
			protected String doInBackground(String... arg) {
				
				
			//	itemDetail	JSONitemDetail = null;
			
				try {
					String token  = arg[0];
					String new_password = arg[1];
			
					HttpClient httpClient = new DefaultHttpClient();
					
					HttpPost httpPost = new HttpPost(URL_CHANGE_PW); // replace with
																			// your url
					List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
					nameValuePair.add(new BasicNameValuePair("token", token));
					nameValuePair.add(new BasicNameValuePair("new_password", new_password));
					// Encoding data

					try {
						httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
					} catch (UnsupportedEncodingException e) {
						// log exception
						e.printStackTrace();
					}

			//Make request

					try {
				    	StringBuilder builder = new StringBuilder();
						HttpResponse response = httpClient.execute(httpPost);
			    		StatusLine statusLine = response.getStatusLine();
			    		int statusCode = statusLine.getStatusCode();
			    		if(statusCode == 200){
			    			HttpEntity entity = response.getEntity();
			    			InputStream content = entity.getContent();
			    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			    			String line;
			    			while((line = reader.readLine()) != null){
			    				builder.append(line);
			    			}
			    		} else {
			    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
			    		}
						// write response to log
						Log.d("Http Post Response:", builder.toString());
						
						JSONObject jsonObject = new JSONObject(builder.toString());
					//	JSONArray productObj = jsonObject
					//			.getJSONArray("user"); // JSON Array
						boolean userStatus = jsonObject.getBoolean("status");
						
						if (userStatus==true){

							success = "true";
				
						}
						else{
						
						
						}
					
						
					} catch (ClientProtocolException e) {
						// Log exception
						e.printStackTrace();
					} catch (IOException e) {
						// Log exception
						e.printStackTrace();
					}

				}catch (Exception e){
					e.printStackTrace();
				}
				return success;
				
			}


			/**
			 * After completing background task Dismiss the progress dialog
			 * **/
			protected void onPostExecute(String file_url) {
				// dismiss the dialog once got all details
				if(file_url!=null){
					pdia.dismiss();
					 Toast.makeText(getActivity(),"The password is changed successfully",Toast.LENGTH_SHORT).show();
					 newpassword.setText("");
					 confiempw.setText("");
				}else{
	       		
	       	}
				
		
				
				
					}
			
			
		}
		   public static final String md5(final String s) {
			    final String MD5 = "MD5";
			    try {
			        // Create MD5 Hash
			        MessageDigest digest = java.security.MessageDigest
			                .getInstance(MD5);
			        digest.update(s.getBytes());
			        byte messageDigest[] = digest.digest();

			        // Create Hex String
			        StringBuilder hexString = new StringBuilder();
			        for (byte aMessageDigest : messageDigest) {
			            String h = Integer.toHexString(0xFF & aMessageDigest);
			            while (h.length() < 2)
			                h = "0" + h;
			            hexString.append(h);
			        }
			        return hexString.toString();

			    } catch (NoSuchAlgorithmException e) {
			        e.printStackTrace();
			    }
			    return "";
			}
		   
		   @Override
		   public void onActivityResult(int requestCode, int resultCode, Intent data) {
			 //  Toast.makeText(getActivity(), "User info fragment is called"+resultCode, Toast.LENGTH_SHORT).show();
		    	   if (resultCode == getActivity().RESULT_OK) {
		               if (requestCode == SELECT_PICTURE) {
		            	   Uri selectedImage = data.getData();
		            //	   Toast.makeText(getActivity(), "selectedImage data:"+selectedImage, Toast.LENGTH_SHORT).show();
		            	//   Log.d("selectedImage data:", selectedImage+"");
		            	  //File imageFile = new File(getRealPathFromURI(selectedImage));
		                   
		              //    Log.i("image path",getRealPathFromURI(selectedImage));
		                 // String token = ((UserInfo) getActivity().getApplication()).getToken();
		            	   
		                  String token = sharedpreferences.getToken();
		                //  File f =new File(getRealPathFromURI(selectedImage));
		                  //new UploadImage().execute(token,getRealPathFromURI(selectedImage));
		                
		              
		                  
		               }
		           }
		       
		   }
		    
		   public String getRealPathFromURI(Uri contentUri) {
			    String res = null;
			    String[] proj = { MediaStore.Images.Media.DATA };
			   
			    Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
			    if(cursor.moveToFirst()){;
			       int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			       res = cursor.getString(column_index);
			    }
			    cursor.close();
			    return res;
			}
		   
		   /** AsyncTask to get the detail from PHP **/
			class UploadImage extends AsyncTask<String, String, String> {
				
				//GalleryFragment  fragement;
			
				String success = null;
				
				private ProgressDialog pdia;
				 HttpURLConnection conn = null;
		          DataOutputStream dos = null;  
		          String lineEnd = "\r\n";
		          String twoHyphens = "--";
		          String boundary = "*****";
		          int bytesRead, bytesAvailable, bufferSize;
		          byte[] buffer;
		          int maxBufferSize = 1 * 1024 * 1024; 
		          File sourceFile;
		          Bitmap bm;
		          File outputFile ;
		          String userImage;
				/**
				 * Before starting background thread Show Progress Dialog
				 * */
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
					
				}

				
				/**
				 * Getting product details in background thread
				 * */
				protected String doInBackground(String... arg) {
					
					String uploadedfile  = arg[1];
					  String token  = arg[0];
			
					   sourceFile = new File(uploadedfile); 
					  
				//	itemDetail	JSONitemDetail = null;
					
					try {
						
						Environment.getExternalStorageDirectory();
						File folder = new File(Environment.getExternalStorageDirectory() + "/image");
						boolean success = true;
						if (!folder.exists()) {
						    success = folder.mkdir();
						    Log.i("folder",folder.getAbsolutePath());
						}
						if (success) {
							Log.i("folder",folder.getAbsolutePath());
						} else {
						    // Do something else on failure 
						}
						// create a File object for the parent directory
							
							// create a File object for the output file
							 outputFile = new File(folder, "tem.jpeg");
							// now attach the OutputStream to the file object, instead of a String representation
							try {
								FileOutputStream fos = new FileOutputStream(outputFile);
								
								
							    bm = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
								
								bm.compress(Bitmap.CompressFormat.JPEG, 75, fos);
								
								
								  InputStream in = new FileInputStream(sourceFile);
								    OutputStream out = new FileOutputStream(outputFile);

								    // Transfer bytes from in to out
								    byte[] buf = new byte[1024];
								    int len;
								    while ((len = in.read(buf)) > 0) {
								        out.write(buf, 0, len);
								    }
								    in.close();
								    out.close();
								    Log.i("outputFile",outputFile.getAbsolutePath());			   
								  			                
						              

					                
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						  
					  
						    // open a URL connection to the Servlet
		                   FileInputStream fileInputStream = new FileInputStream(outputFile);
		                   URL url = new URL(URL_ADD_PROFILEPIC);
		                    
		                   // Open a HTTP  connection to  the URL
		                   conn = (HttpURLConnection) url.openConnection(); 
		                   conn.setDoInput(true); // Allow Inputs
		                   conn.setDoOutput(true); // Allow Outputs
		                   conn.setUseCaches(false); // Don't use a Cached Copy
		                   conn.setRequestMethod("POST");
		                   conn.setRequestProperty("Connection", "Keep-Alive");
		                   conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		                   conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
		                   conn.setRequestProperty("uploadedfile", outputFile.getAbsolutePath()); 
		                   conn.setRequestProperty("token", token);
		                    
		                   dos = new DataOutputStream(conn.getOutputStream());
		          
		                   dos.writeBytes(twoHyphens + boundary + lineEnd); 
		                   dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"tmp.png\"" + lineEnd);
		                    
		                   dos.writeBytes(lineEnd);
		          
		                   // create a buffer of  maximum size
		                   bytesAvailable = fileInputStream.available(); 
		          
		                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
		                   buffer = new byte[bufferSize];
		          
		                   // read file and write it into form...
		                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);  
		                      
		                   while (bytesRead > 0) {
		                        
		                     dos.write(buffer, 0, bufferSize);
		                     bytesAvailable = fileInputStream.available();
		                     bufferSize = Math.min(bytesAvailable, maxBufferSize);
		                     bytesRead = fileInputStream.read(buffer, 0, bufferSize);   
		                      
		                    }
		          
		                   // send multipart form data necesssary after file data...
		                   dos.writeBytes(lineEnd);
		                   
		                   dos.writeBytes(twoHyphens + boundary + lineEnd);
		                   dos.writeBytes("Content-Disposition: form-data; name=\"token\"" + lineEnd);
		                   dos.writeBytes(lineEnd);
		                   dos.writeBytes(token);
		                   dos.writeBytes(lineEnd);
		                   dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		                   
		                  
		          
		                   // Responses from the server (code and message)
		                   serverResponseCode = conn.getResponseCode();
		                   String serverResponseMessage = conn.getResponseMessage();
		                     
		                   Log.i("uploadFile", "HTTP Response is : "
		                           + serverResponseMessage + ": " + serverResponseCode);
		               
		                   if(serverResponseCode == 200){
		                        
		                	   String result = null;
		                	    StringBuffer sb = new StringBuffer();
		                	    InputStream is = null;

		                	    try {
		                	        is = new BufferedInputStream(conn.getInputStream());
		                	        BufferedReader br = new BufferedReader(new InputStreamReader(is));
		                	        String inputLine = "";
		                	        while ((inputLine = br.readLine()) != null) {
		                	            sb.append(inputLine);
		                	        }
		                	        result = sb.toString();
		                	    	JSONObject jsonObject = new JSONObject(result.toString());
		                	    	boolean userStatus = jsonObject.getBoolean("status");
		                	    	if (userStatus==true){
		                	    	 userImage = jsonObject.getString("user_avatar_url");
		                	    		//((UserInfo) getActivity().getApplication()).setUser_avatar_url(userImage.toString());
		                	    	 sharedpreferences.putUserAvatarUrl(userImage.toString());
		                	    		  Log.i("result of the respone",result);
		                	    	}
		                	    }
		                	    catch (Exception e) {
		                	        Log.i("Error", "Error reading InputStream");
		                	        result = null;
		                	    }
		                	    finally {
		                	        if (is != null) {
		                	            try { 
		                	                is.close(); 
		                	            } 
		                	            catch (IOException e) {
		                	                Log.i("Error", "Error closing InputStream");
		                	            }
		                	        }   
		                	    }

		                	  
		                   }    
		                    
		                   //close the streams //
		                   fileInputStream.close();
		                   dos.flush();
		                   dos.close();
		                   boolean deleted = outputFile.delete(); 
		               
		                   
					/*	
						  
						 HttpClient client = new DefaultHttpClient();
		                    HttpPost post = new HttpPost(URL_ADD_PROFILEPIC);

		                    MultipartEntity entity = new MultipartEntity();
		                    try {
		                        entity.addPart("token", new StringBody(token));
		                    } catch (UnsupportedEncodingException e) {
		                        e.printStackTrace();
		                    }
		                   FileBody fb = new FileBody(outputFile);
		                  Log.i("filebody", ""+fb.getFile());
		                    entity.addPart("uploadedfile",fb);
		                   
		                    post.setEntity(entity);
		                    try {
		                        HttpResponse response = client.execute(post);
		                  //      final String responseEntityString = EntityUtils.toString(response.getEntity());
		                   //     Log.d("Http Post Response:", responseEntityString);
		                        
		                        BufferedReader bufferedReader = new BufferedReader(
		                        		  new InputStreamReader(
		                        		   response.getEntity().getContent()));
		                        		 StringBuffer stringBuffer = new StringBuffer("");
		                        		 String line = "";
		                        		 String LineSeparator = System.getProperty("line.separator");
		                        		 while ((line = bufferedReader.readLine()) != null) {
		                        		  stringBuffer.append(line + LineSeparator);
		                        		 }
		                        		 bufferedReader.close();
		                        		 
		                        		Log.i("json result",stringBuffer.toString()) ;
		                    } catch (IOException e) {
		                        e.printStackTrace();
		                    }
		                */
					/*	 HttpClient httpclient = new DefaultHttpClient();
					     
					      httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
						  String uploadedfile  = arg[1];
						  String token  = arg[0];
						 File sourceFile = new File(uploadedfile); 						
						 URL url = new URL(URL_ADD_PROFILEPIC);
						  HttpPost httppost = new HttpPost(URL_ADD_PROFILEPIC);
					     
					   
					      MultipartEntity mpEntity = new MultipartEntity(); 
					      ContentBody tokenString = new StringBody(token);
					      ContentBody cbFile = new FileBody(sourceFile);
					      mpEntity.addPart("token", tokenString);
					      mpEntity.addPart("uploadedfile", cbFile);
					    
					      
					      httppost.setEntity(mpEntity);
					      System.out.println("executing request " + httppost.getRequestLine());
					       
					      HttpResponse response = httpclient.execute(httppost);
					      HttpEntity resEntity = response.getEntity();
					   
					      Log.i("response",""+response.getStatusLine());
					      StringBuilder builder = new StringBuilder();
					      StatusLine statusLine = response.getStatusLine();
					      int statusCode = statusLine.getStatusCode();
					  	if(statusCode == 200){
			    			HttpEntity entity = response.getEntity();
			    			InputStream content = entity.getContent();
			    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			    			String line;
			    			while((line = reader.readLine()) != null){
			    				builder.append(line);
			    			}
			    			Log.d("Http Post Response:", builder.toString());
					}*/
					} 
				/*HttpPost httpPost = new HttpPost(URL_ADD_PROFILEPIC); // replace with
																				// your url
						List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
						nameValuePair.add(new BasicNameValuePair("token", token));
						nameValuePair.add(new BasicNameValuePair("uploadedfile", uploadedfile));
						// Encoding data

						try {
							httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
						} catch (UnsupportedEncodingException e) {
							// log exception
							e.printStackTrace();
						}

				//Make request

						try {
					    	StringBuilder builder = new StringBuilder();
							HttpResponse response = httpClient.execute(httpPost);
				    		StatusLine statusLine = response.getStatusLine();
				    		int statusCode = statusLine.getStatusCode();
				    		if(statusCode == 200){
				    			HttpEntity entity = response.getEntity();
				    			InputStream content = entity.getContent();
				    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				    			String line;
				    			while((line = reader.readLine()) != null){
				    				builder.append(line);
				    			}
				    		} else {
				    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
				    		}
							// write response to log
							Log.d("Http Post Response:", builder.toString());
							
							JSONObject jsonObject = new JSONObject(builder.toString());
						//	JSONArray productObj = jsonObject
						//			.getJSONArray("user"); // JSON Array
							boolean userStatus = jsonObject.getBoolean("status");
							
							if (userStatus==true){

								success = "true";
					
							}
							else{
							
							
							}
						
							
						} catch (ClientProtocolException e) {
							// Log exception
							e.printStackTrace();
						} catch (IOException e) {
							// Log exception
							e.printStackTrace();
						}*/

					catch (Exception e){
						e.printStackTrace();
					}
					return userImage;
					
				}


				/**
				 * After completing background task Dismiss the progress dialog
				 * **/
				protected void onPostExecute(String file_url) {
					// dismiss the dialog once got all details
					if(file_url!=null){						
						 Toast.makeText(getActivity(),"Image Updated",Toast.LENGTH_SHORT).show();
						  getActivity().finish();
		          		Intent intent = new Intent(getActivity(), SettingFragment.class);
		   	          startActivity(intent);
		   	       
					}else{
						 Toast.makeText(getActivity(),"Fail to upload the file",Toast.LENGTH_SHORT).show();
		       	}					
					 // Bitmap myBitmap = BitmapFactory.decodeFile(outputFile.getAbsolutePath());

					   // ImageView myImage = (ImageView) findViewById(R.id.user_icon_img);

					//  user_profie_pic.setImageBitmap(myBitmap);
						}
				
			}
			@Override
		    public void onDestroy() {
		        super.onDestroy();
		        AnimateFirstDisplayListener.displayedImages.clear();
				ImageLoader.getInstance().clearMemoryCache();
				ImageLoader.getInstance().clearDiskCache();
		        
		    }
			private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

				static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					if (loadedImage != null) {
						ImageView imageView = (ImageView) view;
						boolean firstDisplay = !displayedImages.contains(imageUri);
						if (firstDisplay) {
							FadeInBitmapDisplayer.animate(imageView, 500);
							displayedImages.add(imageUri);
						}
					}
				}
			} 
}


