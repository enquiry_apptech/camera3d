package com.siulun.Camera3D.view.fragments;


import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.gallery.GalleryFragment;
import com.facebook.Session;
import com.ppierson.t4jtwitterlogin.T4JTwitterLoginActivity;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.view.MainActivity;

import com.siulun.Camera3D.R;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class SettingFragment extends FragmentActivity{

LoginSharedPreferences sharedpreferences;
	private UserInfo userGlobalVariable ;
	private FacebookLoginFragment mainFragment;
	private String sessionToken;
	  private SharedPreferences settings;
		JSONArray ItemIdArray = null;

 	//	 Log.i("sessionToken", session.getAccessToken());
	 @Override
	 
	    public void onResume() {
	        super.onResume();
	        userGlobalVariable = ((UserInfo)getApplicationContext());
	      
	        Session session = Session.getActiveSession();
	        if (session!= null && !session.isClosed()) {
	        	sessionToken = session.getAccessToken();
	        	//new RegisterNewAccount().execute(sessionToken);
	       	 Log.i("sessionToken in setting ", sessionToken);
	       	 
	       
	        }
	 }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = new LoginSharedPreferences(this);
    
        if (savedInstanceState == null) {
        	// Add the fragment on initial activity setup
        	mainFragment = new FacebookLoginFragment();
            getSupportFragmentManager()
            .beginTransaction()
            .add(android.R.id.content, mainFragment)
            //.addToBackStack(null)
            .commit();
        } else {
        	// Or set the fragment from restored state info
        	mainFragment = (FacebookLoginFragment) getSupportFragmentManager()
        	.findFragmentById(android.R.id.content);
        	
        
        }
        
       
     
    }
 
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
          //Toast.makeText(this, "Setting fragment is called"+resultCode+"result ok:"+this.RESULT_OK, Toast.LENGTH_SHORT).show();
        
    	if (requestCode == mainFragment.RC_SIGN_IN) {
        	FacebookLoginFragment fragment = (FacebookLoginFragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
            fragment.onActivityResult(requestCode, resultCode, data);
        } else {
        	
          super.onActivityResult(requestCode, resultCode, data);
        }
        
        if(requestCode == User_Info_Fragment.SELECT_PICTURE){
        	User_Info_Fragment fragment = (User_Info_Fragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
        	fragment.onActivityResult(requestCode, resultCode, data);
        }
        else {
        	
            super.onActivityResult(requestCode, resultCode, data);
          }
       /*
        if(requestCode == FacebookLoginFragment.TWITTER_LOGIN_REQUEST_CODE){
        	FacebookLoginFragment fragment = (FacebookLoginFragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
            fragment.onActivityResult(requestCode, resultCode, data);
	    }
        else {
        	
            super.onActivityResult(requestCode, resultCode, data);
          }
        */
        
        
    } 
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
   
        return true;
    }
    
    
    @Override
  	public void onBackPressed() {
    	
    	//if (((UserInfo) this.getApplication()).getToken() == null) {
    	if(sharedpreferences.getToken()==null){
    		new AlertDialog.Builder(this).setMessage("Login first! or Want to quit app?")
			.setCancelable(false).setPositiveButton("No", null)
			.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
				/*	android.os.Process.killProcess(android.os.Process
							.myPid());
							*/
					Intent intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			}).show();

    	}else{
    		
    		finish();
    	}
    	//super.onBackPressed();
    	
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /**
	
	class RegisterNewAccount extends AsyncTask<String, String, String> {
		String access_token;
		String usertoken;
		private UserInfo userGlobalVariable ;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
		}

		protected String doInBackground(String... arg) {
			
		//	itemDetail	JSONitemDetail = null;
			GalleryFragment fragment;
			userGlobalVariable = ((UserInfo)getApplicationContext());
			try {
				 access_token = arg[0];
				Log.i("arg",""+access_token);
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_REGISTER_NEW_ACC_WITH_FB); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token", access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==false){
						
					new	LoginWithFacebook().execute(access_token);
			
					}
					else{
					
					JSONObject iteminfo = jsonObject.getJSONObject("user");
				      usertoken = iteminfo.getString("token");
				
				  	String userImage = iteminfo.getString("user_avatar_url");
					String usernickName = iteminfo.getString("user_nick_name");
					
					((UserInfo)getApplicationContext()).setUser_avatar_url(userImage.toString());
				     ((UserInfo)getApplicationContext()).setUser_nick_name(usernickName);				 
				     ((UserInfo)getApplicationContext()).setToken(usertoken.toString());
				((UserInfo)getApplicationContext()).setFinished(true);
				     settings = getSharedPreferences(USERINFO,0);
				     settings.edit()
			            .putString(USER_IMAGE_URL,usernickName)
			            .putString(USERNICKNAME, userImage)
			            .putString(TOKEN, usertoken)
			            .commit();
					}
				
										
				//	JSONObject itemJson = iteminfo.getJSONObject("items");
					Log.i("iteminfo in Setting",userStatus+"");
				
					
					
					//String userinfo = iteminfo.getString("user");
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return null;
		}


		
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			
				}
	}
	
	class LoginWithFacebook extends AsyncTask<String, String, String> {
		private UserInfo userGlobalVariable ;
		GalleryFragment  fragement;
		String usertoken;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
		}

		
		protected String doInBackground(String... arg) {
			userGlobalVariable = ((UserInfo)getApplicationContext());
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
				String access_token = arg[0];
				Log.i("arg",""+access_token);
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_LOGIN_WITH_FB); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token", access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==true){
						JSONObject iteminfo = jsonObject.getJSONObject("user");
						String userImage = iteminfo.getString("user_avatar_url");
						String usernickName = iteminfo.getString("user_nick_name");
						
				
					      usertoken = jsonObject.getString("token");
						
			
						Log.i("usertoken in Setting",usertoken+"");
						
						((UserInfo)getApplicationContext()).setUser_avatar_url(userImage.toString());
					     ((UserInfo)getApplicationContext()).setUser_nick_name(usernickName);				 
					     ((UserInfo)getApplicationContext()).setToken(usertoken.toString());
					((UserInfo)getApplicationContext()).setFinished(true);
						
						  settings = getSharedPreferences(USERINFO,0);
						     settings.edit()
					            .putString(USER_IMAGE_URL,usernickName)
					            .putString(USERNICKNAME, userImage)
					            .putString(TOKEN, usertoken)
					            .commit();
						     
			
					}
					else{
					
					
					}
					//String items = iteminfo.getString("items");
										
				//	JSONObject itemJson = iteminfo.getJSONObject("items");
				
				
					
					
					//String userinfo = iteminfo.getString("user");
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return usertoken;
		}


		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			file_url = usertoken;
	
				}
	}
**/
	
	/** AsyncTask to get the detail from PHP **/
	class getFollower extends AsyncTask<String, String, String> {
		private UserInfo userGlobalVariable ;
		GalleryFragment  fragement;
		String usertoken;
		ArrayList<HashMap<String, String>> search_item_info = new ArrayList<HashMap<String, String>>();
		Set<HashMap<String, String>> set = new HashSet<HashMap<String, String>>();
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			userGlobalVariable = ((UserInfo)getApplicationContext());
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
				settings = getSharedPreferences(USERINFO,0);
				String token = settings.getString(TOKEN, "");
				
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_GER_FOLLOWEE); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("token", token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==true){
						
						ItemIdArray = jsonObject.getJSONArray("users");
						for (int i = 0; i < ItemIdArray.length(); i++) {
						
							 HashMap<String, String> map = new HashMap<String, String>();
							JSONObject user_item_info = ItemIdArray.getJSONObject(i);
							String user_uid = user_item_info.getString("user_uid");
							String user_nick_name = user_item_info.getString("user_nick_name");
							String user_first_name = user_item_info.getString("user_first_name");
							String user_last_name = user_item_info.getString("user_last_name");
							String user_avatar_url = user_item_info.getString("user_avatar_url");
							String user_email = user_item_info.getString("user_email");
							//	Log.i("user_item_info",""+user_item_info.toString());
							//	Log.i("user_uid",""+user_uid.toString());
							
					
							map.put("user_uid", user_uid);
							map.put("user_nick_name", user_nick_name);
							map.put("user_avatar_url", user_avatar_url);
							map.put("user_email", user_email);
							set.add(map);
							
							
						
							
						}
						SharedPreferences  settings = getSharedPreferences(FOLLOWEE_INFO,0);
						Editor editor= settings.edit();  
					
					}
					else{
					
					
					}
					//String items = iteminfo.getString("items");
										
				//	JSONObject itemJson = iteminfo.getJSONObject("items");
				
				
					
					
					//String userinfo = iteminfo.getString("user");
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return usertoken;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			file_url = usertoken;
	
				}
	}
}

