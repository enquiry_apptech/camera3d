package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.siulun.Camera3D.R;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;

public class ForgotPasswordFragment extends Fragment{
	LayoutInflater layoutInflater;
	LinearLayout forgotPassword_ll;
	EditText email_edit;
	Button send_btn;
	ImageButton backbutton;
	private FragmentTransaction fragmentTransaction;
	private FragmentManager fragmentManager;
	NetworkChecking checkNetwork;
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		forgotPassword_ll = (LinearLayout)layoutInflater.inflate(R.layout.forgot_password, container, false);
		forgotPassword_ll.setBackgroundColor(Color.WHITE);
		email_edit = (EditText) forgotPassword_ll.findViewById(R.id.email_edit);
		send_btn =(Button) forgotPassword_ll.findViewById(R.id.Send_email_btn);
		backbutton  =(ImageButton) forgotPassword_ll.findViewById(R.id.forgot_backbutton);
		send_btn.setOnClickListener  (new OnClickListener(){ 
			String email;
	            @Override
	            public void onClick(View v) {
	            	 if(checkNetwork.isNetworkAvailable()){
	            	email = email_edit.getText().toString();
	            	new SendEmail().execute(email);
	            	 }else{
					    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
							
				        }
	            }      

	        });  
		

	    backbutton.setOnClickListener  (new OnClickListener(){ 
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	getFragmentManager().popBackStack();
            	Log.i("onclick","backbutton");
            }         

        }); 
        return forgotPassword_ll;  
    }  
	
	/** AsyncTask to get the detail from PHP **/
	class SendEmail extends AsyncTask<String, String, String> {
		
		//GalleryFragment  fragement;
	
		String success = null;
		
		private ProgressDialog pdia;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
				String email  = arg[0];
		
		
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_FORGOT_PW); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				
				nameValuePair.add(new BasicNameValuePair("email", email));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					
					if (userStatus==true){

						success = "true";
			
					}
					else{
					
					
					}
				
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}
			return success;
			
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			if(file_url!=null){
				
				 Toast.makeText(getActivity(),"Please check your email to reset the password",Toast.LENGTH_SHORT).show();
				  getActivity().finish();
          		Intent intent = new Intent(getActivity(), SettingFragment.class);
   	           startActivity(intent);
			}else{
				 Toast.makeText(getActivity(),"Email Not Exist!",Toast.LENGTH_SHORT).show();
       	}
		
				}
		
	}
}

