package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.URL_GER_FOLLOWEE;
import static com.siulun.Camera3D.model.StringConstants.URL_LOGIN_WITH_EMAIL;
import static com.siulun.Camera3D.model.StringConstants.URL_LOGIN_WITH_FB;
import static com.siulun.Camera3D.model.StringConstants.URL_REGISTER_NEW_ACC_WITH_FB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;



import com.example.gallery.GalleryFragment;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.R.layout;
import com.siulun.Camera3D.R.menu;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.FacebookLoginFragment.Get_Followees;
import com.siulun.Camera3D.view.fragments.FacebookLoginFragment.LoginWithEmail;
import com.siulun.Camera3D.view.fragments.FacebookLoginFragment.LoginWithFacebook;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends FragmentActivity {

    LoginSharedPreferences sharedpreferences;
    
	public static ArrayList<String> userlist = new ArrayList<String>();
	private View view;
	private UiLifecycleHelper uiHelper;
	private TextView alerttv,fb_login_button;
	private ImageButton backbutton;
	private EditText email_login , password_login;
	private Button login_btn,registration_button,loglout_btn,forgot_pw_btn;
	private FragmentTransaction fragmentTransaction;
	private FragmentManager fragmentManager;
	private RelativeLayout setting_rl;
	private  String user_email_login,user_password_login;
	private String sessionToken;
	private ProgressBar pb_loading;
	NetworkChecking checkNetwork;
	//fb
	static Session.StatusCallback statusCallback;
	public static Session session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		sharedpreferences = new LoginSharedPreferences(this);
		
		checkNetwork = new NetworkChecking(Login.this);
		//info = new UserInfo();
	//	Log.i("userGlobalVariable",""+ ((UserInfo) getActivity().getApplication()).getFinished());
		// Session session = Session.getActiveSession();
		 fb_login_button = (TextView)findViewById(R.id.fb_login_button);
		 backbutton =(ImageButton) this.findViewById(R.id.backbutton);
		   login_btn = (Button)findViewById(R.id.login_button);
		   registration_button = (Button)findViewById(R.id.registration_button);
		   fragmentManager = Login.this.getSupportFragmentManager();
		   setting_rl = (RelativeLayout)findViewById(R.id.setting_rl);
		   email_login = (EditText)findViewById(R.id.login_email);
		   password_login = (EditText)findViewById(R.id.login_password);
		   forgot_pw_btn =(Button)findViewById(R.id.forget_password_btn);
		   pb_loading = (ProgressBar)findViewById(R.id.fb_progressbar);
			statusCallback = new SessionStatusCallback();
			fb_login_button.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					fblogin();
					
				}
				
			});
			if (session == null) {
				if (savedInstanceState != null) {
					session = Session.restoreSession(this, null, statusCallback,
							savedInstanceState);
				}
				if (session == null) {
					session = new Session(Login.this);
					Session.setActiveSession(session);
				}
				
		
				if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
					session.openForRead(new Session.OpenRequest(this)
							.setCallback(statusCallback));
				}/**/
			}
		   //if(((UserInfo) Login.this.getApplication()).getFinished()){

if(sharedpreferences.getLoginStatus()){
				//LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
				//authButton.setFragment(this);
				//loglout_btn = (Button) view.findViewById(R.id.logout);
				Log.i("Loggedin facebook", session.getState()+"");
				
				
				//authButton.setReadPermissions(Arrays.asList("user_likes", "user_status"));
			//	authButton.setReadPermissions(Arrays.asList("public_profile"));
				fragmentManager = Login.this.getSupportFragmentManager();
				User_Info_Fragment user_info_frag = new User_Info_Fragment();
				fragmentTransaction = fragmentManager.beginTransaction();
	        	fragmentTransaction.replace(R.id.setting_fragmentContainer,user_info_frag , "");
	        	fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
				
			/*	loglout_btn.setOnClickListener  (new OnClickListener(){ 

			            @Override

			            public void onClick(View v) {
			            	String tmp = null;
			            	  user_email_login = email_login.getText().toString();
			                  user_password_login = password_login.getText().toString();
			              	Log.i("email",user_email_login);
			                // TODO Auto-generated method stub
			             
							new LoginWithEmail().execute(user_email_login,md5(user_password_login));
						
			            	
			          
			            }         

			        });   
			   */
			
			}
			else{
				
			//	LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
				//authButton.setFragment(this);
				
				Log.i("Loggedin facebook", session.getState()+"");
			
				//authButton.setReadPermissions(Arrays.asList("user_likes", "user_status"));
				
			//	authButton.setReadPermissions(Arrays.asList("public_profile"));
				   
				   forgot_pw_btn.setOnClickListener  (new OnClickListener(){ 

			            @Override

			            public void onClick(View v) {
			            	setting_rl.setBackgroundColor(Color.WHITE);
			            	ForgotPasswordFragment forgotpw_fragment = new ForgotPasswordFragment();
			            	
			                // TODO Auto-generated method stub
			            	fragmentTransaction = fragmentManager.beginTransaction();
			            	fragmentTransaction.replace(R.id.setting_fragmentContainer,forgotpw_fragment , "");
			            	fragmentTransaction.addToBackStack(null);
			    			fragmentTransaction.commit();
			    			
			    			
			            }         

			        });  
				   login_btn.setOnClickListener  (new OnClickListener(){ 

				            @Override

				            public void onClick(View v) {
				            	 if(checkNetwork.isNetworkAvailable()){	  
				            	String tmp = null;
				            	  user_email_login = email_login.getText().toString();
				                  user_password_login = password_login.getText().toString();
				              	Log.i("email",user_email_login);
				                // TODO Auto-generated method stub
				             
								new LoginWithEmail().execute(user_email_login,md5(user_password_login));
							
				            	 }else{
								    	Toast.makeText(Login.this, "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
										
							        } 	
				          
				            }         

				        });   
				   
			
			        backbutton.setOnClickListener  (new OnClickListener(){ 

			            @Override

			            public void onClick(View v) {

			                // TODO Auto-generated method stub
			            	finish();
			            	Log.i("onclick","backbutton");
			            }         

			        });   
			        registration_button.setOnClickListener  (new OnClickListener(){ 

			            @Override

			            public void onClick(View v) {
			            	setting_rl.setBackgroundColor(Color.WHITE);
			            	SignUpFragment signupFragment = new SignUpFragment();
			            	
			                // TODO Auto-generated method stub
			            	fragmentTransaction = fragmentManager.beginTransaction();
			            	fragmentTransaction.replace(R.id.setting_fragmentContainer,signupFragment , "");
			            	fragmentTransaction.addToBackStack(null);
			    			fragmentTransaction.commit();
			    			
			    			
			            }         

			        });  
			       
			}
			 setupUI(findViewById(R.id.setting_fragmentContainer));
			   
			    
			       
			
		
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}
	public void fblogin() {
		session = Session.getActiveSession();
		if (session.isOpened()) {
			onClickLogout();
		} else {
			onClickLogin();
		}
	} 
	
	@Override
	public void onStart() {
		super.onStart();
		Session.getActiveSession().addCallback(statusCallback);
	}

	@Override
	public void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
		System.gc();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	 public static void hideSoftKeyboard(Activity activity) {
	        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	    }
	  public void setupUI(View view) {

	        //Set up touch listener for non-text box views to hide keyboard.
	        if(!(view instanceof EditText)) {

	            view.setOnTouchListener(new OnTouchListener() {

	                public boolean onTouch(View v, MotionEvent event) {
	                    hideSoftKeyboard(Login.this);
	                    return false;
	                }

	            });
	        }

	        //If a layout container, iterate over children and seed recursion.
	        if (view instanceof ViewGroup) {

	            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

	                View innerView = ((ViewGroup) view).getChildAt(i);

	                setupUI(innerView);
	            }
	        }
	    }
	   public static final String md5(final String s) {
		    final String MD5 = "MD5";
		    try {
		        // Create MD5 Hash
		        MessageDigest digest = java.security.MessageDigest
		                .getInstance(MD5);
		        digest.update(s.getBytes());
		        byte messageDigest[] = digest.digest();

		        // Create Hex String
		        StringBuilder hexString = new StringBuilder();
		        for (byte aMessageDigest : messageDigest) {
		            String h = Integer.toHexString(0xFF & aMessageDigest);
		            while (h.length() < 2)
		                h = "0" + h;
		            hexString.append(h);
		        }
		        return hexString.toString();

		    } catch (NoSuchAlgorithmException e) {
		        e.printStackTrace();
		    }
		    return "";
		}
	/** AsyncTask to get the detail from PHP **/
	class LoginWithEmail extends AsyncTask<String, String, String> {
		
		//GalleryFragment  fragement;
		String usertoken = null;
		String username;
		private ProgressDialog pdia;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/* pdia = new ProgressDialog(getActivity());
		        pdia.setMessage("Loading...");
		        pdia.show();  
			*/
			loadingViewVisibilityTrue();
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
				username = arg[0];
				String password = arg[1];
		
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_LOGIN_WITH_EMAIL); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("username", username));
				nameValuePair.add(new BasicNameValuePair("password", password));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==true){
						
						JSONObject iteminfo = jsonObject.getJSONObject("user");
						String userImage = iteminfo.getString("user_avatar_url");
						String usernickName = iteminfo.getString("user_nick_name");
						
					     //((UserInfo) Login.this.getApplication()).setUser_avatar_url(userImage.toString());
					     
					     sharedpreferences.putUserAvatarUrl(userImage.toString());
					     
					     //((UserInfo) Login.this.getApplication()).setUser_nick_name(usernickName);
					     
					     sharedpreferences.putUser_nick_name(usernickName);
					     
					      usertoken = jsonObject.getString("token");
					      //((UserInfo) Login.this.getApplication()).setToken(usertoken.toString());
					      sharedpreferences.putToken(usertoken.toString());
					      
						//((UserInfo) Login.this.getApplication()).setFinished(true);
					      sharedpreferences.startLogin();
						Log.i("usertoken in Setting",usertoken+"");
						//((UserInfo) Login.this.getApplication()).setEmail(username);
						sharedpreferences.putEmail(username);
			
					}
					else{
					
					
					}
				
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return usertoken;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			if(file_url!=null){
				/*pdia.dismiss(); 
				User_Info_Fragment user_info_frag = new User_Info_Fragment();
				fragmentTransaction = fragmentManager.beginTransaction();
	        	fragmentTransaction.replace(R.id.setting_fragmentContainer,user_info_frag , "");
	        	fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
				*/
				//new Get_Followees(((UserInfo) Login.this.getApplication()).getToken()).execute();
				new Get_Followees(sharedpreferences.getToken()).execute();
				
			}else{
				loadingViewVisibilityFalse();
    		 Toast.makeText(Login.this,"Email or Password is wrong",Toast.LENGTH_SHORT).show();
    	}
			
	
			
			
				}
	}
	
    public void loadingViewVisibilityTrue(){
    	setting_rl.setVisibility(View.INVISIBLE);
	    pb_loading.setVisibility(View.VISIBLE);
    }
    public void loadingViewVisibilityFalse(){
    	setting_rl.setVisibility(View.VISIBLE);
	    pb_loading.setVisibility(View.INVISIBLE);
    }
	@SuppressWarnings("deprecation")
	public void getuser() {

		session = Session.getActiveSession();
		// TODO Auto-generated method stub
		Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

			@Override
			public void onCompleted(GraphUser user, Response response) {
				// TODO Auto-generated method stub
				
			
					Toast.makeText(Login.this, "Login with " + user.getName(),
							Toast.LENGTH_SHORT).show();
				
			
			}
		});
	}	

	    
	/** AsyncTask to get the detail from PHP **/
	class RegisterNewAccount extends AsyncTask<String, String, String> {
		String access_token;
		String usertoken;
		private UserInfo userGlobalVariable ;
		boolean condition=true;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loadingViewVisibilityTrue();
			
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			
		//	itemDetail	JSONitemDetail = null;
			GalleryFragment fragment;
			//userGlobalVariable =   ((UserInfo) getActivity().getApplication());
			try {
				 access_token = arg[0];
				Log.i("arg",""+access_token);
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_REGISTER_NEW_ACC_WITH_FB); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token", access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==false){
						
					new	LoginWithFacebook().execute(access_token);
					condition=false;
					}
					else{
					
					JSONObject iteminfo = jsonObject.getJSONObject("user");
				      usertoken = iteminfo.getString("token");
				
				  	String userImage = iteminfo.getString("user_avatar_url");
					String usernickName = iteminfo.getString("user_nick_name");
					
				/*	  ((UserInfo)Login.this.getApplication()).setUser_avatar_url(userImage.toString());
					  ((UserInfo)Login.this.getApplication()).setUser_nick_name(usernickName);				 
					  ((UserInfo) Login.this.getApplication()).setToken(usertoken.toString());
					  ((UserInfo)Login.this.getApplication()).setFinished(true);
					*/  
					  sharedpreferences.startLogin();
					  sharedpreferences.putToken(usertoken.toString());
					  sharedpreferences.putUser_nick_name(usernickName);
					 sharedpreferences.putUserAvatarUrl(userImage.toString());
					  
					  
					  condition=true;
				
					}
				
										
				//	JSONObject itemJson = iteminfo.getJSONObject("items");
					Log.i("iteminfo in Setting",userStatus+"");
				
					
					
					//String userinfo = iteminfo.getString("user");
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return null;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			if(condition){
				loadingViewVisibilityFalse();
			}
				}
	}
	/** AsyncTask to get the detail from PHP **/
	class LoginWithFacebook extends AsyncTask<String, String, String> {
		//private UserInfo userGlobalVariable ;
		GalleryFragment  fragement;
		String usertoken;
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			//userGlobalVariable =   ((UserInfo) getActivity().getApplication());
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
				String access_token = arg[0];
				Log.i("arg",""+access_token);
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_LOGIN_WITH_FB); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token", access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==true){
						JSONObject iteminfo = jsonObject.getJSONObject("user");
						String userImage = iteminfo.getString("user_avatar_url");
						String usernickName = iteminfo.getString("user_nick_name");
						
				
					      usertoken = jsonObject.getString("token");
						
			
						Log.i("usertoken in Setting",usertoken+"");
						
						  ((UserInfo)Login.this.getApplication()).setUser_avatar_url(userImage.toString());
						  ((UserInfo)Login.this.getApplication()).setUser_nick_name(usernickName);				 
						  ((UserInfo)Login.this.getApplication()).setToken(usertoken.toString());
						  ((UserInfo)Login.this.getApplication()).setFinished(true);
					/*	
							info.setUser_avatar_url(userImage.toString());
							info.setUser_nick_name(usernickName);				 
							info.setToken(usertoken.toString());*/
							//info.setFinished(true);
						     
			
					}
					else{
					
					
					}
					//String items = iteminfo.getString("items");
										
				//	JSONObject itemJson = iteminfo.getJSONObject("items");
				
				
					
					
					//String userinfo = iteminfo.getString("user");
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return usertoken;
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			file_url = usertoken;
			//new Get_Followees(((UserInfo) Login.this.getApplication()).getToken()).execute();
			new Get_Followees(sharedpreferences.getToken()).execute();
			//loadingViewVisibilityFalse();
			//jumpToUserInfoPage();
				}
	}
	
	
	public void jumpToUserInfoPage(){
		 fragmentManager = Login.this.getSupportFragmentManager();
			User_Info_Fragment user_info_frag = new User_Info_Fragment();
			fragmentTransaction = fragmentManager.beginTransaction();
      	fragmentTransaction.replace(R.id.setting_fragmentContainer,user_info_frag , "");
      	fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
	}
	
	
    class Get_Followees extends AsyncTask<String, String, String> {
		
	
		
		String token;
		
			public Get_Followees(String token){
				this.token = token;
			}
			/**
			 * Before starting background thread Show Progress Dialog
			 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			
			
		//	itemDetail	JSONitemDetail = null;
		
			try {
			
				HttpClient httpClient = new DefaultHttpClient();
				
				HttpPost httpPost = new HttpPost(URL_GER_FOLLOWEE); // replace with
																		// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("token", token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

		//Make request

				try {
			    	StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
		    		StatusLine statusLine = response.getStatusLine();
		    		int statusCode = statusLine.getStatusCode();
		    		if(statusCode == 200){
		    			HttpEntity entity = response.getEntity();
		    			InputStream content = entity.getContent();
		    			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		    			String line;
		    			while((line = reader.readLine()) != null){
		    				builder.append(line);
		    			}
		    		} else {
		    			Log.e(MainActivity.class.toString(),"Failedet JSON object");
		    		}
					// write response to log
					Log.d("Http Post Response:", builder.toString());
					
					JSONObject jsonObject = new JSONObject(builder.toString());
				//	JSONArray productObj = jsonObject
				//			.getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus==true){
						JSONArray array = jsonObject.getJSONArray("user");
						for(int i=0;i<array.length();i++){
							JSONObject list = array.getJSONObject(i);
							String user_uid = list.getString("user_uid");
							userlist.add(user_uid);
						}
						
					}
					else{
					
					
					}
				
					
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			}catch (Exception e){
				e.printStackTrace();
			}

			return "";
		}


		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			
			
			loadingViewVisibilityFalse();
			jumpToUserInfoPage();
			
			
				}
	}
       
	public void onClickLogin() {
		session = Session.getActiveSession();

		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(this)
					.setCallback(statusCallback));
		} else {
			Session.openActiveSession(this, true, statusCallback);
		}
	}
 
	public void onClickLogout() {
	
		session = Session.getActiveSession();
		// session = Session.getActiveSession();
		if (!session.isClosed()) {
			session.closeAndClearTokenInformation();
		
			Toast.makeText(Login.this, "Logout", Toast.LENGTH_SHORT)
					.show();
		}
	}
	public class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if (session.isOpened()) {
				getuser();
			
			
			}
		}
	}
 
}
