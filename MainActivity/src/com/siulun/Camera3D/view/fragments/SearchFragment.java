package com.siulun.Camera3D.view.fragments;


import static com.siulun.Camera3D.model.StringConstants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.nostra13.universalimageloader.BaseFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;

import com.siulun.Camera3D.R;

import android.app.ActionBar;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.SyncStateContract.Constants;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SearchFragment extends Fragment implements OnClickListener ,OnItemClickListener {

LoginSharedPreferences sharedpreferences;

	private EditText searchInfo;
	private ImageButton searchbtn;
	private LayoutInflater layoutInflater;
	private RelativeLayout searchLayout;
	private ListView userInfolv;
	public String search_user_name;
	public ArrayList<HashMap<String, String>> search_item_info;
	private UserInfo userGlobalVariable ;
	JSONParser jParser = new JSONParser();
	JSONArray ItemIdArray = null;
	String[] itemStringArray  =null;
	DisplayImageOptions optionsOfSamllIcon;
	View action_barView ;
	 boolean mDualPane;
	    int mCurCheckPosition = 0;
		private FragmentManager fragmentManager;
		public static Search_item_click_Fragment nextFrag;
		private FragmentTransaction fragmentTransaction;
		SearchFragment mContent;
		public ImageButton backbtn;
		NetworkChecking checkNetwork;
	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		searchLayout = (RelativeLayout)layoutInflater.inflate(R.layout.search_freagment, container, false);
		searchInfo = (EditText)searchLayout.findViewById(R.id.userName);
		searchbtn = (ImageButton)searchLayout.findViewById(R.id.searchbtn);
		userInfolv = (ListView)searchLayout.findViewById(R.id.userInfolv);
		search_item_info = new ArrayList<HashMap<String, String>>();
		 userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
		searchbtn.setOnClickListener(this);
		ActionBar ab = getActivity().getActionBar();
    	
		action_barView = layoutInflater.inflate(R.layout.action_bar, null, false);
		
		backbtn = (ImageButton)action_barView.findViewById(R.id.back);
	
		userInfolv.setOnItemClickListener(this);
        return searchLayout;  
    }

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//if(((UserInfo) getActivity().getApplication()).getToken() == null){
		
			if(sharedpreferences.getToken()==null){
			userInfolv.setAdapter(new ImageAdapter(getActivity(),new ArrayList<HashMap<String, String>>()));
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		sharedpreferences = new LoginSharedPreferences(getActivity());
		
		optionsOfSamllIcon = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.imageloading)
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(20))
		.build(); 
	}


	@Override
	public void onClick(View v) {
		 if(checkNetwork.isNetworkAvailable()){	 
		if(v.getId()==R.id.searchbtn){
			search_user_name = searchInfo.getText().toString();
			search_item_info = getData(search_user_name);
			if(search_item_info.size()==0){
				
				userInfolv.setAdapter(new ImageAdapter(getActivity(),search_item_info));
				//if(((UserInfo) getActivity().getApplication()).getToken()==null){
				if(sharedpreferences.getToken()==null){
				Toast.makeText(getActivity(), "Please Login first!!!!!!!!!!!!!", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getActivity(), "No this person!!!!!!!!!!!!!", Toast.LENGTH_SHORT).show();	
				}
				
			}else{
			userInfolv.setAdapter(new ImageAdapter(getActivity(),search_item_info));
			}
		
			}
		 }else{
		    	Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
				
	        }
		
	}  



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		String user_nick_name =search_item_info.get(position).get("user_nick_name").toString();
		String user_email =search_item_info.get(position).get("user_email").toString();
		String user_avatar_url = search_item_info.get(position).get("user_avatar_url").toString();
		String user_uid = search_item_info.get(position).get("user_uid").toString();
		nextFrag= new Search_item_click_Fragment();
		
		fragmentTransaction = this.getFragmentManager().beginTransaction();
		MainActivity.processFragmentToShowAndHideOtherFragment(nextFrag,fragmentTransaction);
		MainActivity.addFragmentToTransactionAndStack(nextFrag,fragmentTransaction);			
		fragmentTransaction.commit();

	
	/*     this.getFragmentManager().beginTransaction()
	     .replace(R.id.fragmentContainerFrameLayout, nextFrag,TAG_NEWSFEED_FRAGMENT)
	     .addToBackStack(null)		
	     .commit();
	  */
	
	     Bundle bundle = new Bundle();
	     bundle.putString("user_nick_name", user_nick_name);
	     bundle.putString("user_email", user_email);
	     bundle.putString("user_avatar_url", user_avatar_url);
	     bundle.putString("user_uid", user_uid);
	     nextFrag.setArguments(bundle);
	
		   
	}
	
	private  ArrayList<HashMap<String, String>> getData(String search_user_name) {
		
		//if(((UserInfo) getActivity().getApplication()).getToken() != null){
		if(sharedpreferences.getToken()!=null){
			//String token = ((UserInfo) getActivity().getApplication()).getToken();
			String token = sharedpreferences.getToken();
			try {
				search_item_info=	new SearchForUserName().execute(token,search_user_name).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			search_item_info = new ArrayList<HashMap<String, String>>();
		}
		return search_item_info;
		
	}
	
	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class SearchForUserName extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ArrayList<HashMap<String, String>> search_item_info = new ArrayList<HashMap<String, String>>();
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String keywords =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("keywords", keywords));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_SEARCH_USER, "POST", params);
			
			// Check your log cat for JSON reponse
			//Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
					
					// products found
					// Getting Array of Products
					ItemIdArray = json.getJSONArray("users");
					Log.i("ItemIdArray ",""+ItemIdArray.length());
					// looping through All Products
				
					for (int i = 0; i < ItemIdArray.length(); i++) {
						 HashMap<String, String> map = new HashMap<String, String>();
						JSONObject user_item_info = ItemIdArray.getJSONObject(i);
						String user_uid = user_item_info.getString("user_uid");
						String user_nick_name = user_item_info.getString("user_nick_name");
						String user_avatar_url = user_item_info.getString("user_avatar_url");
						String user_email = user_item_info.getString("user_email");
						//	Log.i("user_item_info",""+user_item_info.toString());
						//	Log.i("user_uid",""+user_uid.toString());
						map.put("user_uid", user_uid);
						map.put("user_nick_name", user_nick_name);
						map.put("user_avatar_url", user_avatar_url);
						map.put("user_email", user_email);
						search_item_info.add(map);
						
					}
					
					Log.i("search_item_info ",""+search_item_info.size());
				}
				else{
					Log.i("Exception","");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return search_item_info;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {

			
		}

	}
	private static class ViewHolder {
		TextView user_nick_name;
		TextView user_email;
		ImageView image;
	}

	class ImageAdapter extends BaseAdapter {

		private LayoutInflater inflater;
		private ArrayList<HashMap<String, String>> search_item_info;
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		//  private BitmapCache bitmapCache;
		public ImageAdapter(Context context, ArrayList<HashMap<String, String>> search_item_info){
		                  this.inflater = LayoutInflater.from(getActivity());
		                  this.search_item_info = search_item_info;
		              
		                  
		                
		//	View inflate = getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
		             }
		

		@Override
		public int getCount() {
			return search_item_info.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			
			
			if (convertView == null) {
				view = inflater.inflate(R.layout.search_item, parent, false);
				holder = new ViewHolder();
				holder.user_nick_name = (TextView) view.findViewById(R.id.item_user_name);
				holder.user_email = (TextView) view.findViewById(R.id.item_user_email);
				holder.image = (ImageView) view.findViewById(R.id.item_user_profilepic);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.user_nick_name.setText(search_item_info.get(position).get("user_nick_name").toString());
			holder.user_email.setText(search_item_info.get(position).get("user_email").toString());
			ImageLoader.getInstance().displayImage(search_item_info.get(position).get("user_avatar_url").toString(), holder.image, optionsOfSamllIcon, animateFirstListener);
        
			return view;
		}
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	
	

}