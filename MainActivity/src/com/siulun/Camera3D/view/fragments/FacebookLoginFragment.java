package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.TOKEN;
import static com.siulun.Camera3D.model.StringConstants.URL_GER_FOLLOWEE;
import static com.siulun.Camera3D.model.StringConstants.URL_LOGIN_WITH_EMAIL;
import static com.siulun.Camera3D.model.StringConstants.URL_LOGIN_WITH_FB;
import static com.siulun.Camera3D.model.StringConstants.URL_REGISTER_NEW_ACC_WITH_FB;
import static com.siulun.Camera3D.model.StringConstants.USERINFO;
import static com.siulun.Camera3D.model.StringConstants.USERNICKNAME;
import static com.siulun.Camera3D.model.StringConstants.USER_IMAGE_URL;
import static com.siulun.Camera3D.model.StringConstants.URL_REGISTER_NEW_ACC_WITH_GPLUS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gallery.GalleryFragment;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ppierson.t4jtwitterlogin.T4JTwitterLoginActivity;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;

import com.facebook.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class FacebookLoginFragment extends Fragment implements
ConnectionCallbacks, OnConnectionFailedListener{

	LoginSharedPreferences sharedpreferences;
	
	public static final int TWITTER_LOGIN_REQUEST_CODE = 333;
	
	private static final String TAG = "MainFragment";
	public static ArrayList<String> userlist = new ArrayList<String>();
	private View view;
	private UiLifecycleHelper uiHelper;
	private TextView alerttv,twitter_login;
	private ImageButton backbutton;
	private EditText email_login, password_login;
	private Button login_btn, registration_button, loglout_btn, forgot_pw_btn;
	private FragmentTransaction fragmentTransaction;
	private FragmentManager fragmentManager;
	private RelativeLayout setting_rl;
	private String user_email_login, user_password_login;
	private String sessionToken;
	private ProgressBar pb_loading;
	NetworkChecking checkNetwork;

	// For GPlus Login
	private SignInButton btnSignIn;
	// Google client to interact with Google API
    public static GoogleApiClient mGoogleApiClient;
    //Get Connection result
    private ConnectionResult mConnectionResult;
    //result code
    public static final int RC_SIGN_IN = 0;
    
    public static int count = 0;
    
    private boolean mIntentInProgress;
    
    private boolean mSignInClicked;
    
	// public static UserInfo info;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		checkNetwork = new NetworkChecking(getActivity());
		// info = new UserInfo();
		// Log.i("userGlobalVariable",""+ ((UserInfo)
		// getActivity().getApplication()).getFinished());
		
		Session session = Session.getActiveSession();
		view = inflater.inflate(R.layout.setting_fragment, container, false);
		
		//GPlus Login Button
	 //   btnSignIn = (SignInButton) view.findViewById(R.id.btn_sign_in);
	    // Button click listeners
     /*   btnSignIn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//new RegisterNewAccountWithGPlus("ya29.nwEALiSvgHCqYfpTOnTC4uTNuhJKE8JKy70YK6GUbJOetlc61X8v_ArlgiYHX8gS7BWGImXKoOS-QQ","singsing1633@gmail.com").execute();
				/*if (mGoogleApiClient.isConnected()) {
					signOutFromGplus();	
				}else{
				signInWithGplus();
				}
				Toast.makeText(getActivity(), "Coming soon........", Toast.LENGTH_SHORT).show();
			}
			
        });
        */
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
        .addConnectionCallbacks(FacebookLoginFragment.this)
        .addOnConnectionFailedListener(FacebookLoginFragment.this).addApi(Plus.API, null)
        .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        
        
        twitter_login = (TextView)view.findViewById(R.id.twt);
		backbutton = (ImageButton) view.findViewById(R.id.backbutton);
		login_btn = (Button) view.findViewById(R.id.login_button);
		registration_button = (Button) view
				.findViewById(R.id.registration_button);
		fragmentManager = getActivity().getSupportFragmentManager();
		setting_rl = (RelativeLayout) view.findViewById(R.id.setting_rl);
		email_login = (EditText) view.findViewById(R.id.login_email);
		password_login = (EditText) view.findViewById(R.id.login_password);
		forgot_pw_btn = (Button) view.findViewById(R.id.forget_password_btn);
		pb_loading = (ProgressBar) view.findViewById(R.id.fb_progressbar);

		//if (((UserInfo) getActivity().getApplication()).getFinished()) {
         if(sharedpreferences.getLoginStatus()){
        	 
			LoginButton authButton = (LoginButton) view
					.findViewById(R.id.authButton);
			authButton.setFragment(this);
			loglout_btn = (Button) view.findViewById(R.id.logout);
			Log.i("Loggedin facebook", session.getState() + "");

			// authButton.setReadPermissions(Arrays.asList("user_likes",
			// "user_status"));
			authButton.setReadPermissions(Arrays.asList("public_profile"));
			fragmentManager = getActivity().getSupportFragmentManager();
			User_Info_Fragment user_info_frag = new User_Info_Fragment();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.setting_fragmentContainer,
					user_info_frag, "");
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

			/*
			 * loglout_btn.setOnClickListener (new OnClickListener(){
			 * 
			 * @Override
			 * 
			 * public void onClick(View v) { String tmp = null; user_email_login
			 * = email_login.getText().toString(); user_password_login =
			 * password_login.getText().toString();
			 * Log.i("email",user_email_login); // TODO Auto-generated method
			 * stub
			 * 
			 * new
			 * LoginWithEmail().execute(user_email_login,md5(user_password_login
			 * ));
			 * 
			 * 
			 * 
			 * }
			 * 
			 * });
			 */

		} else {

			LoginButton authButton = (LoginButton) view
					.findViewById(R.id.authButton);
			authButton.setFragment(this);

			Log.i("Loggedin facebook", session.getState() + "");

			// authButton.setReadPermissions(Arrays.asList("user_likes",
			// "user_status"));

			authButton.setReadPermissions(Arrays.asList("public_profile"));

			forgot_pw_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setting_rl.setBackgroundColor(Color.WHITE);
					ForgotPasswordFragment forgotpw_fragment = new ForgotPasswordFragment();

					// TODO Auto-generated method stub
					fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.setting_fragmentContainer,
							forgotpw_fragment, "");
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				}

			});
			
			twitter_login.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					//Toast.makeText(getActivity(), "twt", Toast.LENGTH_SHORT).show();
					if (checkNetwork.isNetworkAvailable()) {
					TwitterLogin();
					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();

					}
				}
				
			});
			
			login_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (checkNetwork.isNetworkAvailable()) {
						String tmp = null;
						user_email_login = email_login.getText().toString();
						user_password_login = password_login.getText()
								.toString();
						Log.i("email", user_email_login);
						// TODO Auto-generated method stub

						new LoginWithEmail().execute(user_email_login,
								md5(user_password_login));

					} else {
						Toast.makeText(
								getActivity(),
								"No network connection, please try it again!!!",
								Toast.LENGTH_SHORT).show();

					}

				}

			});

			backbutton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					//if (((UserInfo) getActivity().getApplication()).getToken() == null) {
					if(sharedpreferences.getToken()==null||!sharedpreferences.getLoginStatus()){
						new AlertDialog.Builder(getActivity())
								.setMessage("Login first! or Want to quit app?")
								.setCancelable(false)
								.setPositiveButton("No", null)
								.setNegativeButton("Quit",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												Intent intent = new Intent(
														Intent.ACTION_MAIN);
												intent.addCategory(Intent.CATEGORY_HOME);
												intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
												startActivity(intent);
											}
										}).show();
					} else {
						getActivity().finish();
					}

					Log.i("onclick", "backbutton");
				}

			});
			registration_button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setting_rl.setBackgroundColor(Color.WHITE);
					SignUpFragment signupFragment = new SignUpFragment();

					// TODO Auto-generated method stub
					fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.setting_fragmentContainer,
							signupFragment, "");
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

				}

			});

		}
		setupUI(view.findViewById(R.id.setting_fragmentContainer));
		return view;
	}

	public static void hideSoftKeyboard(Fragment activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getActivity()
				.getCurrentFocus().getWindowToken(), 0);
	}

	public void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new OnTouchListener() {

				public boolean onTouch(View v, MotionEvent event) {
					hideSoftKeyboard(FacebookLoginFragment.this);
					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView);
			}
		}
	}

	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	/** AsyncTask to get the detail from PHP **/
	class LoginWithEmail extends AsyncTask<String, String, String> {

		// GalleryFragment fragement;
		String usertoken = null;
		String username;
		private ProgressDialog pdia;

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*
			 * pdia = new ProgressDialog(getActivity());
			 * pdia.setMessage("Loading..."); pdia.show();
			 */
			loadingViewVisibilityTrue();
		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {

			// itemDetail JSONitemDetail = null;

			try {
				username = arg[0];
				String password = arg[1];

				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_LOGIN_WITH_EMAIL); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("username", username));
				nameValuePair.add(new BasicNameValuePair("password", password));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());

					JSONObject jsonObject = new JSONObject(builder.toString());
					// JSONArray productObj = jsonObject
					// .getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus == true) {

						JSONObject iteminfo = jsonObject.getJSONObject("user");
						String userImage = iteminfo
								.getString("user_avatar_url");
						String usernickName = iteminfo
								.getString("user_nick_name");
						String user_uid = iteminfo.getString("user_uid");
						usertoken = jsonObject.getString("token");
						
						 if(!sharedpreferences.getLoginStatus()){
                        	 sharedpreferences.startLogin();
                        	 sharedpreferences.putEmail(username);
                        	 sharedpreferences.putToken(usertoken.toString());
                        	 sharedpreferences.putUser_nick_name(usernickName);
                        	 sharedpreferences.putUser_uid(user_uid);
                        	 sharedpreferences.putUserAvatarUrl(userImage.toString());
                         }
						/*
						((UserInfo) getActivity().getApplication())
								.setUser_avatar_url(userImage.toString());
						((UserInfo) getActivity().getApplication())
								.setUser_nick_name(usernickName);						
						((UserInfo) getActivity().getApplication())
								.setToken(usertoken.toString());
						((UserInfo) getActivity().getApplication())
								.setUser_uid(user_uid);
						((UserInfo) getActivity().getApplication())
								.setFinished(true);			
						((UserInfo) getActivity().getApplication())
								.setEmail(username);
						*/
                      Log.i("usertoken in Setting", usertoken + "");
                      
					} else {

					}

				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return usertoken;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			if (file_url != null) {
				/*
				 * pdia.dismiss(); User_Info_Fragment user_info_frag = new
				 * User_Info_Fragment(); fragmentTransaction =
				 * fragmentManager.beginTransaction();
				 * fragmentTransaction.replace
				 * (R.id.setting_fragmentContainer,user_info_frag , "");
				 * fragmentTransaction.addToBackStack(null);
				 * fragmentTransaction.commit();
				 */
			/*	new Get_Followees(
						((UserInfo) getActivity().getApplication()).getToken())
						.execute();
						*/
				new Get_Followees(
						sharedpreferences.getToken())
						.execute();
			} else {
				loadingViewVisibilityFalse();
				Toast.makeText(getActivity(), "Email or Password is wrong",
						Toast.LENGTH_SHORT).show();
			}

		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
		sharedpreferences = new LoginSharedPreferences(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();

		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			// onSessionStateChange(session, session.getState(), null);
		}
		if (session != null && !session.isClosed()) {
			sessionToken = session.getAccessToken();
			//if (((UserInfo) getActivity().getApplication()).getToken() == null) {
				if(sharedpreferences.getToken()==null){
				new RegisterNewAccount().execute(sessionToken);
			}

			Log.i("sessionToken in setting ", sessionToken);
		}
		if (session != null
				&& sharedpreferences.getToken() != null) {
			Log.i("onResume onResume != null token", session.getState() + "");

		}

		uiHelper.onResume();
	}

	 private void resolveSignInError() {
	        if (mConnectionResult.hasResolution()) {
	            try {
	            //	Toast.makeText(getActivity(), "resolveSignInError try is called", Toast.LENGTH_SHORT).show();
	                mIntentInProgress = true;
	                mConnectionResult.startResolutionForResult(getActivity(), RC_SIGN_IN);
	              //  startActivityForResult(getActivity(), RC_SIGN_IN);
	            } catch (SendIntentException e) {
	            //	Toast.makeText(getActivity(), "resolveSignInError catch is called", Toast.LENGTH_SHORT).show();
	                mIntentInProgress = false;
	                mGoogleApiClient.connect();
	            }
	        }
	    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//Toast.makeText(getActivity(), "requestCode is "+requestCode, Toast.LENGTH_SHORT).show();
		//Toast.makeText(getActivity(), "onActivityResult is called", Toast.LENGTH_SHORT).show();
		 if (requestCode == RC_SIGN_IN) {
	            if (resultCode != getActivity().RESULT_OK) {
	                mSignInClicked = false;
	            }
	 
	            mIntentInProgress = false;
	 
	            if (!mGoogleApiClient.isConnecting()) {
	            	//getGPlusUserInformation();
	                mGoogleApiClient.connect();
	            }
	        }
		 else if(requestCode == FacebookLoginFragment.TWITTER_LOGIN_REQUEST_CODE){
			   Log.d("TAG", "TWITTER LOGIN REQUEST CODE");
		        if(resultCode == T4JTwitterLoginActivity.TWITTER_LOGIN_RESULT_CODE_SUCCESS){
		            Log.d("TAG", "TWITTER LOGIN SUCCESS");
		          //  Toast.makeText(getActivity(), "getAccessToken"+T4JTwitterLoginActivity.getAccessToken(getActivity()), Toast.LENGTH_LONG).show();
					//Toast.makeText(getActivity(), "First time"+count, Toast.LENGTH_LONG).show();
		         if(count==0){
					new RegisterNewAccountWithTwitter(T4JTwitterLoginActivity.getAccessToken(getActivity()), T4JTwitterLoginActivity.getAccessTokenSecret(getActivity())).execute();
		         }
		         count++;
		        }else if(resultCode == T4JTwitterLoginActivity.TWITTER_LOGIN_RESULT_CODE_FAILURE){
		            Log.d("TAG", "TWITTER LOGIN FAIL");
		        }
		 }
		 else{
		uiHelper.onActivityResult(requestCode, resultCode, data);
		 }
		
		
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	public void loadingViewVisibilityTrue() {
		setting_rl.setVisibility(View.GONE);
		pb_loading.setVisibility(View.VISIBLE);
	}

	public void loadingViewVisibilityFalse() {
		setting_rl.setVisibility(View.VISIBLE);
		pb_loading.setVisibility(View.GONE);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	
	/** AsyncTask to get the detail from PHP **/
	class RegisterNewAccountWithGPlus extends AsyncTask<Object, String, Object> {
		String access_token;
		String email;
		private UserInfo userGlobalVariable;

		public RegisterNewAccountWithGPlus(String access_token,	String email){
			this.access_token = access_token;
			this.email = email;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loadingViewVisibilityTrue();

		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(Object... params) {
              String jsonString="";
			try {
	
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_REGISTER_NEW_ACC_WITH_GPLUS); 
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token",
						access_token));
				nameValuePair.add(new BasicNameValuePair("email",
						email));
				
				// Encoding data
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
						jsonString = builder.toString();
					} 				
				
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return jsonString;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(Object s) {
			super.onPostExecute(s);
		
		Boolean status=null;
		JSONArray jsonArray=null;
		JSONObject object=null;
		JSONObject jsonObject = null;
			 if (((String) s != null) && (((String) s).equals("") == false)) {
                 try {
                	 Log.d("GPlus Info", (String) s);
                     object = new JSONObject((String) s);
                     status = object.getBoolean("status");
                    
                     Log.d("Status Info", status.toString());
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

			if(status){
				 try {   
			     jsonObject = object.getJSONObject("user");
				 Log.d("User Info", jsonObject.toString());
				 } catch (JSONException e) {
                     e.printStackTrace();
                 }
				 
				 if (jsonObject != null){
				
                         try {   
                             Log.d("User uid", jsonObject.getString("user_uid").toString());
                             
                             if(!sharedpreferences.getLoginStatus()){
                            	 sharedpreferences.startLogin();
                            	// sharedpreferences.putEmail(jsonObject.getString("email"));
                            	 sharedpreferences.putToken(jsonObject.getString("access_token").toString());
                            	 sharedpreferences.putUser_nick_name(jsonObject.getString("user_nick_name").toString());
                            	 sharedpreferences.putUser_uid(jsonObject.getString("user_uid").toString());
                            	 sharedpreferences.putUserAvatarUrl(jsonObject.getString("user_avatar_url").toString());
                             }
                             
                      /*       ((UserInfo) getActivity().getApplication())
								.setUser_avatar_url(jsonObject.getString("user_avatar_url").toString());
						((UserInfo) getActivity().getApplication())
								.setUser_nick_name(jsonObject.getString("user_nick_name").toString());
						((UserInfo) getActivity().getApplication())
								.setUser_uid(jsonObject.getString("user_uid").toString());
						((UserInfo) getActivity().getApplication())
								.setToken(jsonObject.getString("access_token").toString());
						((UserInfo) getActivity().getApplication())
								.setFinished(true);
                             
                             */
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
				 
					/* new Get_Followees(
								((UserInfo) getActivity().getApplication()).getToken())
								.execute();
								*/
                         new Get_Followees(
                        		 sharedpreferences.getToken())
 								.execute();
			}
			
			}
			
		}
				loadingViewVisibilityFalse();
	}

	}
	
	/** AsyncTask to get the detail from PHP **/
	class RegisterNewAccount extends AsyncTask<String, String, String> {
		String access_token;
		String usertoken;
		private UserInfo userGlobalVariable;
		boolean condition = true;

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loadingViewVisibilityTrue();

		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {

			// itemDetail JSONitemDetail = null;
			GalleryFragment fragment;
			// userGlobalVariable = ((UserInfo) getActivity().getApplication());
			try {
				access_token = arg[0];
				Log.i("arg", "" + access_token);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_REGISTER_NEW_ACC_WITH_FB); // replace
																				// with
				// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token",
						access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());

					JSONObject jsonObject = new JSONObject(builder.toString());
					// JSONArray productObj = jsonObject
					// .getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus == false) {

						new LoginWithFacebook().execute(access_token);
						condition = false;
					} else {

						JSONObject iteminfo = jsonObject.getJSONObject("user");
						usertoken = iteminfo.getString("token");

						String userImage = iteminfo
								.getString("user_avatar_url");
						String usernickName = iteminfo
								.getString("user_nick_name");
						String user_uid = iteminfo.getString("user_uid");
						
						 if(!sharedpreferences.getLoginStatus()){
                        	 sharedpreferences.startLogin();

                        	 sharedpreferences.putToken(usertoken.toString());
                        	 sharedpreferences.putUser_nick_name(usernickName);
                        	 sharedpreferences.putUser_uid(user_uid);
                        	 sharedpreferences.putUserAvatarUrl(userImage.toString());
                         }
						
						/*
						((UserInfo) getActivity().getApplication())
								.setUser_avatar_url(userImage.toString());
						((UserInfo) getActivity().getApplication())
								.setUser_nick_name(usernickName);
						((UserInfo) getActivity().getApplication())
								.setUser_uid(user_uid);
						((UserInfo) getActivity().getApplication())
								.setToken(usertoken.toString());
						((UserInfo) getActivity().getApplication())
								.setFinished(true);
*/
						condition = true;

					}

					// JSONObject itemJson = iteminfo.getJSONObject("items");
					Log.i("iteminfo in Setting", userStatus + "");

					// String userinfo = iteminfo.getString("user");

				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			if (condition) {
				loadingViewVisibilityFalse();
				/*new Get_Followees(
						((UserInfo) getActivity().getApplication()).getToken())
						.execute();
						*/
				new Get_Followees(
						sharedpreferences.getToken())
						.execute();
				/*		*/
			}
		}
	}

	/** AsyncTask to get the detail from PHP **/
	class LoginWithFacebook extends AsyncTask<String, String, String> {
		// private UserInfo userGlobalVariable ;
		GalleryFragment fragement;
		String usertoken;

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {
			// userGlobalVariable = ((UserInfo) getActivity().getApplication());

			// itemDetail JSONitemDetail = null;

			try {
				String access_token = arg[0];
				Log.i("arg", "" + access_token);
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_LOGIN_WITH_FB); // replace
																		// with
																		// your
																		// url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("access_token",
						access_token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());

					JSONObject jsonObject = new JSONObject(builder.toString());
					// JSONArray productObj = jsonObject
					// .getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus == true) {
						JSONObject iteminfo = jsonObject.getJSONObject("user");
						String userImage = iteminfo
								.getString("user_avatar_url");
						String usernickName = iteminfo
								.getString("user_nick_name");
						String user_uid = iteminfo.getString("user_uid");

						usertoken = jsonObject.getString("token");

						Log.i("usertoken in Setting", usertoken + "");
						Log.i("user_uid in Setting", user_uid + "");

						 if(!sharedpreferences.getLoginStatus()){
                        	 sharedpreferences.startLogin();

                        	 sharedpreferences.putToken(usertoken.toString());
                        	 sharedpreferences.putUser_nick_name(usernickName);
                        	 sharedpreferences.putUser_uid(user_uid);
                        	 sharedpreferences.putUserAvatarUrl(userImage.toString());
                         }
						
					/*	((UserInfo) getActivity().getApplication())
								.setUser_avatar_url(userImage.toString());
						((UserInfo) getActivity().getApplication())
								.setUser_nick_name(usernickName);
						((UserInfo) getActivity().getApplication())
								.setUser_uid(user_uid);
						((UserInfo) getActivity().getApplication())
								.setToken(usertoken.toString());
						((UserInfo) getActivity().getApplication())
								.setFinished(true);
								*/
						/*
						 * info.setUser_avatar_url(userImage.toString());
						 * info.setUser_nick_name(usernickName);
						 * info.setToken(usertoken.toString());
						 */
						// info.setFinished(true);

					} else {

					}
					// String items = iteminfo.getString("items");

					// JSONObject itemJson = iteminfo.getJSONObject("items");

					// String userinfo = iteminfo.getString("user");

				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return usertoken;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			file_url = usertoken;
			/*new Get_Followees(
					((UserInfo) getActivity().getApplication()).getToken())
					.execute();
					*/
			new Get_Followees(
					sharedpreferences.getToken())
					.execute();
			// loadingViewVisibilityFalse();
			// jumpToUserInfoPage();
		}
	}

	public void jumpToUserInfoPage() {
		fragmentManager = getActivity().getSupportFragmentManager();
		User_Info_Fragment user_info_frag = new User_Info_Fragment();
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.setting_fragmentContainer,
		//fragmentTransaction.replace(android.R.id.content,
				user_info_frag, "");
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	private void onClickLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(this).setPermissions(
					Arrays.asList("public_profile")).setCallback(callback));
			Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
					this,
					Arrays.asList("user_checkins", "email", "publish_actions",
							"public_profile", "user_friends"));
			session.requestNewReadPermissions(newPermissionsRequest);

		} else {
			Session.openActiveSession(getActivity(), this, true, callback);
		}
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			fragmentManager = getActivity().getSupportFragmentManager();
			User_Info_Fragment user_info_frag = new User_Info_Fragment();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.setting_fragmentContainer,
					user_info_frag, "");
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();

		} else if (!state.isClosed()) {

		}
	}

	class Get_Followees extends AsyncTask<String, String, String> {

		String token;

		public Get_Followees(String token) {
			this.token = token;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(String... arg) {

			// itemDetail JSONitemDetail = null;

			try {

				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost(URL_GER_FOLLOWEE); // replace
																	// with
																	// your url
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("token", token));
				// Encoding data

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
					} else {
						Log.e(MainActivity.class.toString(),
								"Failedet JSON object");
					}
					// write response to log
					Log.d("Http Post Response:", builder.toString());

					JSONObject jsonObject = new JSONObject(builder.toString());
					// JSONArray productObj = jsonObject
					// .getJSONArray("user"); // JSON Array
					boolean userStatus = jsonObject.getBoolean("status");
					if (userStatus == true) {
						JSONArray array = jsonObject.getJSONArray("user");
						for (int i = 0; i < array.length(); i++) {
							JSONObject list = array.getJSONObject(i);
							String user_uid = list.getString("user_uid");
							userlist.add(user_uid);
						}

					} else {

					}

				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return "";
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details

			loadingViewVisibilityFalse();
			jumpToUserInfoPage();

		}
	}

	
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(),
                    0).show();
            return;
        }
 
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;
 
            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
		
	}

	
	public void getGPlusUserInformation(){
		 //Toast.makeText(getActivity(), "getGPlusUserInformation is called", Toast.LENGTH_SHORT).show();
		if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            
        	AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
        		String sAccessToken="";
			    @Override
			    protected String doInBackground(Void... params) {
			    	try{
					
						
						String scope = "oauth2:"+Scopes.PLUS_ME;
						sAccessToken = GoogleAuthUtil.getToken(
		                        getActivity(),
		                        Plus.AccountApi.getAccountName(mGoogleApiClient),
		                        scope);
						
						
						Log.d("Token ", sAccessToken);
						}catch(Exception e){
							Log.d("Token error", "error in get token");
							
						}
					return "";
			    	
			    }
			    @Override
			    protected void onPostExecute(String info) {
			      //Toast.makeText(getActivity(), "Token: "+sAccessToken, Toast.LENGTH_SHORT).show();
			    	String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
			    	new RegisterNewAccountWithGPlus(sAccessToken,email).execute();
			    }

			};

			task.execute();
            
		}
	}
	
	
	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		//Toast.makeText(getActivity(), "onConnected is called!", Toast.LENGTH_LONG).show();
		if(!sharedpreferences.getLoginStatus()){
		getGPlusUserInformation();
		}
		/**/
	}

	
	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();
	}
	
	
	 
	 /**
	     * Sign-in into google
	     * */
	    private void signInWithGplus() {
	    	//Toast.makeText(getActivity(), "sign in is called", Toast.LENGTH_SHORT).show();
	        if (!mGoogleApiClient.isConnecting()) {
	            mSignInClicked = true;
	            resolveSignInError();
	        }
	    }
	 
	    /* Sign-out from google
		 * */
		private void signOutFromGplus() {
			//Toast.makeText(getActivity(), "sign out is called", Toast.LENGTH_SHORT).show();
			if (mGoogleApiClient.isConnected()) {
				Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
				mGoogleApiClient.disconnect();
				mGoogleApiClient.connect();
				//updateUI(false);
			}
		}   
	    
	@Override
	public void onStart() {
	        super.onStart();
	        mGoogleApiClient.connect();
	    }
	
	@Override 
	public void onStop() {
	        super.onStop();
	        if (mGoogleApiClient.isConnected()) {
	            mGoogleApiClient.disconnect();
	        }
	    }
	
	public void TwitterLogin(){
		if (!T4JTwitterLoginActivity.isConnected(getActivity())){
		    Intent twitterLoginIntent = new Intent(getActivity(), T4JTwitterLoginActivity.class);
		    twitterLoginIntent.putExtra(T4JTwitterLoginActivity.TWITTER_CONSUMER_KEY, "FAuMZFlIqhRksn1Tz9HK2FXPA");
		    twitterLoginIntent.putExtra(T4JTwitterLoginActivity.TWITTER_CONSUMER_SECRET, "WeXXXTgoXrPCoerlFhcG82fB6noZujlz62sBShGQbrwDv6YDnK");
		    startActivityForResult(twitterLoginIntent, TWITTER_LOGIN_REQUEST_CODE);
		}
		
		
	}
	
	/** AsyncTask to get the detail from PHP **/
	class RegisterNewAccountWithTwitter extends AsyncTask<Object, String, Object> {
		String token;
		String secret_token;
	

		public RegisterNewAccountWithTwitter(String token,	String secret_token){
			this.token = token;
			this.secret_token = secret_token;
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loadingViewVisibilityTrue();

		}

		/**
		 * Getting product details in background thread
		 * */
		protected String doInBackground(Object... params) {
              String jsonString="";
			try {
	
				HttpClient httpClient = new DefaultHttpClient();

				HttpPost httpPost = new HttpPost("http://camera3d.apptech.com.hk/index.php/xidong/user/login_with_Twitter"); 
				//HttpPost httpPost = new HttpPost("http://localhost/camera3d/www/index.php/xidong/user/login_with_Twitter"); 
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
				nameValuePair.add(new BasicNameValuePair("token",
						this.token));
				nameValuePair.add(new BasicNameValuePair("secret_token",
						this.secret_token));
				
				// Encoding data
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
				} catch (UnsupportedEncodingException e) {
					// log exception
					e.printStackTrace();
				}

				// Make request

				try {
					StringBuilder builder = new StringBuilder();
					HttpResponse response = httpClient.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					Log.d("statusCode: ", ""+statusCode);
					if (statusCode == 200) {
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							builder.append(line);
						}
						jsonString = builder.toString();
					} 	
					
				
				} catch (ClientProtocolException e) {
					// Log exception
					e.printStackTrace();
				} catch (IOException e) {
					// Log exception
					e.printStackTrace();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return jsonString;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(Object s) {
			super.onPostExecute(s);
		
		Boolean status=null;
		JSONObject object=null;
		JSONObject jsonObject = null;
		String jsonToken = null;
			 if (((String) s != null) && (((String) s).equals("") == false)) {
                 try {
                	 Log.d("Twitter Info", (String) s);
                     object = new JSONObject((String) s);
                     status = object.getBoolean("status");
                    
                     Log.d("Status Info", status.toString());
                     
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

			
			
			if(status){
				 try {   
			     jsonObject = object.getJSONObject("user");
			     jsonToken = object.getString("token");
				 Log.d("User Info", jsonObject.toString());
				 } catch (JSONException e) {
                    e.printStackTrace();
                }
				 
				 if (jsonObject != null){
				
                        try {   
                            Log.d("User uid", jsonObject.getString("user_uid").toString());
                            
                            if(!sharedpreferences.getLoginStatus()){
                           	 sharedpreferences.startLogin();
                           	// sharedpreferences.putEmail(jsonObject.getString("email"));
                           	 sharedpreferences.putToken(jsonToken.toString());
                           	 sharedpreferences.putUser_nick_name(jsonObject.getString("user_nick_name").toString());
                           	 sharedpreferences.putUser_uid(jsonObject.getString("user_uid").toString());
                           	 sharedpreferences.putUserAvatarUrl(jsonObject.getString("user_avatar_url").toString());
                            }
                            
                   
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
				 
					/* new Get_Followees(
								((UserInfo) getActivity().getApplication()).getToken())
								.execute();
								*/
                        new Get_Followees(
                       		 sharedpreferences.getToken())
								.execute();
			}
			
			}
			
			
		}else{
			 Log.d("Twitter Info", "it is null");
			 loadingViewVisibilityFalse();
		}
				
	}

	}
	
	
	
}
