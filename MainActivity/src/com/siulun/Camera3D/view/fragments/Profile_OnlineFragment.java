package com.siulun.Camera3D.view.fragments;

import static com.siulun.Camera3D.model.StringConstants.*;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.R;
import com.siulun.Camera3D.model.LoginSharedPreferences;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.Search_item_click_Fragment.ImageAdapter;
import com.siulun.Camera3D.view.fragments.Search_item_click_Fragment.SearchForUserName;
import com.siulun.Camera3D.view.fragments.Search_item_click_Fragment.ViewHolder;


import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Profile_OnlineFragment extends Fragment{
	LoginSharedPreferences sharedpreferences;
	private LayoutInflater layoutInflater;
	private RelativeLayout user_item_rl;
	private ImageView search_user_image;
	protected AbsListView listView;
	DisplayImageOptions options;
	private ImageLoadingListener animateFirstListener;
	public ArrayList<HashMap<String, String>> itemCollectionArray;
	//private UserInfo userGlobalVariable ;
	JSONParser jParser = new JSONParser();
	JSONArray ItemIdArray = null;
	public  String token;
	public String user_uid;
	FragmentManager myFragmentManager;
	FragmentTransaction fragmentTransaction;
    public static SpecificFeed Profile_Online_specificFeedFragment;
    NetworkChecking checkNetwork;
    AlertDialog alert;
    ImageAdapter adapter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		sharedpreferences= new LoginSharedPreferences(getActivity());
		/*if(userGlobalVariable==null){
		userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
		}*/
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.imageloading)
		.showImageForEmptyUri(R.drawable.ic_launcher)
		.showImageOnFail(R.drawable.ic_launcher)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
	}
	
 
	public Profile_OnlineFragment() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
        checkNetwork = new NetworkChecking(getActivity());
		
		layoutInflater = inflater;
		animateFirstListener = new AnimateFirstDisplayListener();
		/*if(userGlobalVariable==null){
			userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
			}*/
		user_item_rl = (RelativeLayout)layoutInflater.inflate(R.layout.profile_online, container, false);
		 listView = (GridView) user_item_rl.findViewById(R.id.profile_user_item_collection);
		// if(userGlobalVariable.getToken()!=null){
			 if(sharedpreferences.getToken()!=null){
			 if(checkNetwork.isNetworkAvailable()){
		 itemCollectionArray = getData(sharedpreferences.getToken(),sharedpreferences.getUser_uid());
		 adapter = new ImageAdapter(itemCollectionArray);
		 
			((GridView) listView).setAdapter(adapter);
			 }else{
				  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
			  }
		 }
			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					
					//specificFeedFragment = new SpecificFeed();
					  if(checkNetwork.isNetworkAvailable()){
					  Bundle bundle = new Bundle();
					     bundle.putString("item_uid", itemCollectionArray.get(position).get("item_uid"));
					     bundle.putString("resource_url", itemCollectionArray.get(position).get("resource_url").toString());
					
					myFragmentManager = getFragmentManager();
					fragmentTransaction = myFragmentManager.beginTransaction();
					
				Profile_Online_specificFeedFragment = new SpecificFeed();
					
					/*if(MainActivity.DisplayFragment==MainActivity.Fragmentprofile){
						
					    
						MainActivity.processFragmentToShowAndHideOtherFragment(Profile_Online_specificFeedFragment,MainActivity.fragmentTransaction);
						MainActivity.addFragmentToTransactionAndStack(Profile_Online_specificFeedFragment,fragmentTransaction);			
						
						}
					*/
					Profile_Online_specificFeedFragment.setArguments(bundle);
					fragmentTransaction.replace(R.id.realtabcontent, Profile_Online_specificFeedFragment, "");
		        	fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
					  }else{
						  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
					  }
					
					     //specificFeedFragment.setArguments(bundle);
					
				}
			});
			listView.setOnItemLongClickListener(new OnItemLongClickListener(){

				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					showDialog("Delete item","Delete","cancel",position);
					return false;
				}
				
			});
		return user_item_rl;
	}
	private  ArrayList<HashMap<String, String>> getData(String token,String user_uid) {
		
	//	if(userGlobalVariable.getToken() != null){
			if(sharedpreferences.getToken()!=null){
			try {
				itemCollectionArray=	new GetUserItem().execute(token,user_uid).get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return itemCollectionArray;
	}
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	static class ViewHolder {
		ImageView imageView;
		ProgressBar progressBar;
	}
	public class ImageAdapter extends BaseAdapter {

		

		private LayoutInflater inflater;
		private ArrayList<HashMap<String, String>> itemCollectionArray;
		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
		//  private BitmapCache bitmapCache;
		public ImageAdapter( ArrayList<HashMap<String, String>> itemCollectionArray){
		                //  this.inflater = LayoutInflater.from(getActivity());
			inflater = LayoutInflater.from(getActivity());
		                  this.itemCollectionArray = itemCollectionArray;
		              
		                  
		                
		//	View inflate = getLayoutInflater().inflate(R.layout.schedule_detail_item, null);
		             }
		
		ImageAdapter() {
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return itemCollectionArray.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			View view = convertView;
			if (view == null) {
				view = inflater.inflate(R.layout.search_user_itemcollection_item, parent, false);
				holder = new ViewHolder();
				assert view != null;
				holder.imageView = (ImageView) view.findViewById(R.id.image);
				holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			ImageLoader.getInstance()
					.displayImage(itemCollectionArray.get(position).get("resource_url").toString(), holder.imageView, options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingStarted(String imageUri, View view) {
							holder.progressBar.setProgress(0);
							holder.progressBar.setVisibility(View.VISIBLE);
						}

						@Override
						public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
							holder.progressBar.setVisibility(View.GONE);
						}

						@Override
						public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
							holder.progressBar.setVisibility(View.GONE);
						}
					}, new ImageLoadingProgressListener() {
						@Override
						public void onProgressUpdate(String imageUri, View view, int current, int total) {
							holder.progressBar.setProgress(Math.round(100.0f * current / total));
						}
					});

			return view;
		}
	}

	class GetUserItem extends AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ArrayList<HashMap<String, String>> search_item_info = new ArrayList<HashMap<String, String>>();
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting All products from url
		 * */
		protected ArrayList<HashMap<String, String>> doInBackground(String... args) {
			
		
	        String token = (String)args[0];;
			
		    String search_user_uid =(String)args[1];
		   
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", token));
			params.add(new BasicNameValuePair("search_user_uid", search_user_uid));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_SEARCH_USER_ITEM, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
			
			
			try {
				// Checking for SUCCESS TAG
			
				  String success = json.getString("status");
				
				if (success.equals("true")) {
					
					// products found
					// Getting Array of Products
					ItemIdArray = json.getJSONArray("user_items");
					Log.i("ItemIdArray ",""+ItemIdArray.length());
					// looping through All Products
				
					for (int i = 0; i < ItemIdArray.length(); i++) {
						 HashMap<String, String> map = new HashMap<String, String>();
						JSONObject user_item_info = ItemIdArray.getJSONObject(i);
						String item_uid = user_item_info.getString("item_uid");
						String resource_url = user_item_info.getString("resource_url");
					
						map.put("item_uid", item_uid);
						map.put("resource_url", resource_url);
						
						search_item_info.add(map);
					}
					
					Log.i("search_item_info ",""+search_item_info.size());
				}
				else{
					Log.i("Exception","");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return search_item_info;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(ArrayList<HashMap<String, String>> file_url) {

			
		}

	}
	public void showDialog(String title, String Positive, String Negative, final int position){
		    Builder MyAlertDialog = new AlertDialog.Builder(getActivity());
		
    	//	MyAlertDialog.setTitle("");
    		
    		MyAlertDialog.setMessage(title);    		
    		if(Positive.equals("Login")){
    	
    		MyAlertDialog.setPositiveButton(Positive, new DialogInterface.OnClickListener() {  
    		    public void onClick(DialogInterface dialog, int which) {  
    		    	Intent intent = new Intent(getActivity(),SettingFragment.class);
  				    startActivity(intent);
    		    	
    		    }  
    		}); 
    		MyAlertDialog.setNegativeButton(Negative , new DialogInterface.OnClickListener() {  
    		    public void onClick(DialogInterface dialog, int which) {  
    		    
    		    }  
    		}); 
    		}
    		if(Positive.equals("Delete")){
    	    	
        		MyAlertDialog.setPositiveButton(Positive, new DialogInterface.OnClickListener() {  
        		    public void onClick(DialogInterface dialog, int which) {  
        		    	 if(checkNetwork.isNetworkAvailable()){
        		    	new DeleteUserItem(position).execute();
        		    	 }else{
        					  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
        				  }
        		    }  
        		}); 
        		MyAlertDialog.setNegativeButton(Negative , new DialogInterface.OnClickListener() {  
        		    public void onClick(DialogInterface dialog, int which) {  
        		    
        		    }  
        		}); 
        		}
    		    MyAlertDialog.setCancelable(false);
    		    alert = MyAlertDialog.create();
    		    alert.show();
    		
	}
	
    class DeleteUserItem extends AsyncTask<String, String, String[]> {
		int position;
		String success ;
		public DeleteUserItem(int position){
			this.position = position;
		}
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog("Loading...","","",0);
		}

		/**
		 * getting All products from url
		 * */
		protected String[] doInBackground(String... args) {
		
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("token", sharedpreferences.getToken()));
			params.add(new BasicNameValuePair("item_uid", itemCollectionArray.get(position).get("item_uid")));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(URL_Delete_Users_Items, "POST", params);
			
			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());
		
			
			try {
				// Checking for SUCCESS TAG
			      
				  success = json.getString("status");
				} catch (JSONException e) {	
					success=null;
				e.printStackTrace();
			    }catch (Exception e){
			    	success=null;
			    e.printStackTrace();	
			    }
			
			   


			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String[] file_url) {
			alert.cancel();
			if(success==null){
				Toast.makeText(getActivity(), "Network is poor, Please retry later!!!",Toast.LENGTH_SHORT).show();
			}
			 if(success!=null){
					if (success.equals("true")) {
						Toast.makeText(getActivity(), "success",Toast.LENGTH_SHORT).show();	
						itemCollectionArray.remove(position);
						adapter.notifyDataSetChanged();
						
			}else{
				Toast.makeText(getActivity(), "Fail to delete, Please retry later!!!",Toast.LENGTH_SHORT).show();	
			}
		}
		      
		}

	}

    @Override  	
public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		 if(MainActivity.DisplayFragment==MainActivity.Fragmentprofile){
			    
	        	profile.page=1;
	        //	 Showpopup showPopup = new Showpopup();
			  //      showPopup.showPopup(getActivity(),"Long Press photo!!!!!\ndelete the item");
				//   uiHelper.onResume();
		        }
		/*if(userGlobalVariable==null){
			userGlobalVariable = ((UserInfo)getActivity().getApplicationContext());
			}*/
		 //if(userGlobalVariable.getToken()!=null&&itemCollectionArray==null){
		 if(sharedpreferences.getToken()!=null&&itemCollectionArray==null){
			 if(checkNetwork.isNetworkAvailable()){

			 itemCollectionArray = getData(sharedpreferences.getToken(),sharedpreferences.getUser_uid());
			 adapter = new ImageAdapter(itemCollectionArray);		 
				((GridView) listView).setAdapter(adapter);
			 }else{
				  Toast.makeText(getActivity(), "No network connection, please try it again!!!",  Toast.LENGTH_SHORT).show();
			  }
			 }
	//	 if(userGlobalVariable.getToken()==null){

if(sharedpreferences.getToken()==null){
			 showDialog("Please Login first!","Login","cancel",0);
		 }
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

}
