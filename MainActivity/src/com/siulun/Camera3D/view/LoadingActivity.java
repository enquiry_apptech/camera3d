package com.siulun.Camera3D.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class LoadingActivity extends Activity{
    private ProgressDialog dialog;  
    @Override  
    protected void onCreate(Bundle savedInstanceState) {  
            // TODO Auto-generated method stub  
            super.onCreate(savedInstanceState);  
            //设置�����progressdialog���弹��� 
            dialog = ProgressDialog.show(this, "Loading...", "Please wait");  
            Thread thread = new Thread(new Runnable() {  
                    public void run() {  
                                                                 //do...  
                                                              
                            Message message = new Message();  
                            message.what = 0;  
                            mHandler.sendMessage(message);  
                    }  
            });  
            thread.start();  
  
    }  
      
   
    private Handler mHandler = new Handler() {  
            @Override  
            public void handleMessage(Message msg) {  
                    // TODO Auto-generated method stub  
                     
                    Intent mIntent = new Intent();  
                    mIntent.setClass(LoadingActivity.this, MainActivity.class);  
                    startActivity(mIntent);  
                    LoadingActivity.this.finish();  
                   
                    if (msg.what == 0) {  
                        dialog.dismiss();  
                }  
            }  
    };  
}
