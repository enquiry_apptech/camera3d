package com.siulun.Camera3D.itemClass;

import java.text.SimpleDateFormat;
import java.util.Date;

public class itemDetail {
	
	private String item_uid;
	private String user_id;
	private Date item_create_date;
	private String str_item_create_date;
	private String item_name;
	private String item_description;
	private String like_count;
	private String item_isLiked;
	private String comment_count;
	private String share_count;
	private String view_count;
	

	
	
	public itemDetail(String item_uid,String user_id,String str_item_create_date,String item_name,String item_description,String like_count,String comment_count,String share_count,String view_count,String item_isLiked){
		this.item_uid = item_uid;
		this.str_item_create_date = str_item_create_date;
		this.item_description = item_description;
		this.item_name = item_name;
		this.user_id = user_id;
		this.like_count = like_count;
		this.comment_count =comment_count;
		this.share_count =share_count;
		this.view_count =view_count;
		this.item_isLiked = item_isLiked;
		
		Date date = null;
		SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		try {  
		     date = format.parse(str_item_create_date);  
		  
		} catch (Exception e) {  
		    // TODO Auto-generated catch block  
		    e.printStackTrace();  
		}
		
		
		this.item_create_date = date;
	}


public String getItem_isLiked() {
		return item_isLiked;
	}


	public void setItem_isLiked(String item_isLiked) {
		this.item_isLiked = item_isLiked;
	}


public String getstr_item_create_date(){
	return str_item_create_date;
}


	public String getItem_uid() {
		return item_uid;
	}



	public void setItem_uid(String item_uid) {
		this.item_uid = item_uid;
	}



	public String getUser_id() {
		return user_id;
	}



	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}



	public Date getItem_create_date() {
	
		return item_create_date;
	}



	public void setItem_create_date() {
		
		
		Date date = null;
		SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");  
		try {  
		     date = format.parse(str_item_create_date);  
		  
		} catch (Exception e) {  
		    // TODO Auto-generated catch block  
		    e.printStackTrace();  
		}
		this.item_create_date = date;
	}



	public String getItem_name() {
		return item_name;
	}



	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}



	public String getItem_description() {
		return item_description;
	}



	public void setItem_description(String item_description) {
		this.item_description = item_description;
	}
	public String getLike_count() {
		return like_count;
	}
	
	public String getComment_count() {
		return comment_count;
	}
	public String getShare_count() {
		return share_count;
	}
	public String getView_count() {
		return view_count;
	}
	public void setLike_count(String like_count) {
		this.like_count = like_count;
	}
	public void setComment_count(String comment_count) {
		this.comment_count = comment_count;
	}
	public void setShare_count(String share_count) {
		this.share_count = share_count;
	}
	public void setView_count(String view_count) {
		this.view_count = view_count;
	}
}
