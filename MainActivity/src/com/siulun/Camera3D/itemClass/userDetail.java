package com.siulun.Camera3D.itemClass;

public class userDetail {
	private String user_id;
	private String user_first_name;
	private String user_last_name;
	private String user_nick_name;
	private String user_email;
	private String user_avatar_url;

	
	public userDetail(String user_id,  String user_first_name,String user_last_name,String user_nick_name, String user_email, String user_avatar_url){
		this.user_id  = user_id;
		this.user_first_name = user_first_name;
		this.user_last_name	=user_last_name;
		this.user_nick_name =user_nick_name;
		this.user_email =user_email;
		this.user_avatar_url =user_avatar_url;
		
		}
	

	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_first_name() {
		return user_first_name;
	}
	public void setUser_first_name(String user_first_name) {
		this.user_first_name = user_first_name;
	}
	public String getUser_last_name() {
		return user_last_name;
	}
	public void setUser_last_name(String user_last_name) {
		this.user_last_name = user_last_name;
	}
	public String getUser_nick_name() {
		return user_nick_name;
	}
	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_avatar_url() {
		return user_avatar_url;
	}
	public void setUser_avatar_url(String user_avatar_url) {
		this.user_avatar_url = user_avatar_url;
	}
}
