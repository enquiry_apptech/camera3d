package com.siulun.Camera3D.itemClass;

public class itemResources {
	private String item_uid;
	private String resource_uid;
	private String type_uid;
	private String resource_index;
	private String resource_url;
	public itemResources(String item_uid,String resource_uid,String type_uid,String resource_index,String resource_url){
		this.item_uid = item_uid;
		this.resource_index = resource_index;
		this.resource_uid = resource_uid;
		this.resource_url = resource_url;
		this.type_uid = type_uid;
	
}
	public String getItem_uid() {
		return item_uid;
	}
	public void setItem_uid(String item_uid) {
		this.item_uid = item_uid;
	}
	public String getResource_uid() {
		return resource_uid;
	}
	public void setResource_uid(String resource_uid) {
		this.resource_uid = resource_uid;
	}
	public String getType_uid() {
		return type_uid;
	}
	public void setType_uid(String type_uid) {
		this.type_uid = type_uid;
	}
	public String getResource_index() {
		return resource_index;
	}
	public void setResource_index(String resource_index) {
		this.resource_index = resource_index;
	}
	public String getResource_url() {
		return resource_url;
	}
	public void setResource_url(String resource_url) {
		this.resource_url = resource_url;
	}
	
	
}
