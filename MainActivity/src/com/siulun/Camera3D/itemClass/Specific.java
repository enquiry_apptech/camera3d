package com.siulun.Camera3D.itemClass;

public class Specific {
private String item_create_date;
private String item_description;
private String item_isLiked;
private String item_name;



private String user_first_name;
private String user_last_name;
private String user_nick_name;
private String user_avatar_url;

private String like_count;
private String comment_count;
private String share_count;
private String view_count;

public Specific(String item_create_date, String item_description,
		String item_isLiked, String item_name, String user_first_name,
		String user_last_name, String user_nick_name, String user_avatar_url,
		String like_count, String comment_count, String share_count,
		String view_count) {
	super();
	this.item_create_date = item_create_date;
	this.item_description = item_description;
	this.item_isLiked = item_isLiked;
	this.item_name = item_name;
	this.user_first_name = user_first_name;
	this.user_last_name = user_last_name;
	this.user_nick_name = user_nick_name;
	this.user_avatar_url = user_avatar_url;
	this.like_count = like_count;
	this.comment_count = comment_count;
	this.share_count = share_count;
	this.view_count = view_count;
}
public Specific() {
	super();
}
public String getItem_create_date() {
	return item_create_date;
}
public void setItem_create_date(String item_create_date) {
	this.item_create_date = item_create_date;
}
public String getItem_description() {
	return item_description;
}
public void setItem_description(String item_description) {
	this.item_description = item_description;
}
public String getItem_isLiked() {
	return item_isLiked;
}
public void setItem_isLiked(String item_isLiked) {
	this.item_isLiked = item_isLiked;
}
public String getItem_name() {
	return item_name;
}
public void setItem_name(String item_name) {
	this.item_name = item_name;
}
public String getUser_first_name() {
	return user_first_name;
}
public void setUser_first_name(String user_first_name) {
	this.user_first_name = user_first_name;
}
public String getUser_last_name() {
	return user_last_name;
}
public void setUser_last_name(String user_last_name) {
	this.user_last_name = user_last_name;
}
public String getUser_nick_name() {
	return user_nick_name;
}
public void setUser_nick_name(String user_nick_name) {
	this.user_nick_name = user_nick_name;
}
public String getUser_avatar_url() {
	return user_avatar_url;
}
public void setUser_avatar_url(String user_avatar_url) {
	this.user_avatar_url = user_avatar_url;
}
public String getLike_count() {
	return like_count;
}
public void setLike_count(String like_count) {
	this.like_count = like_count;
}
public String getComment_count() {
	return comment_count;
}
public void setComment_count(String comment_count) {
	this.comment_count = comment_count;
}
public String getShare_count() {
	return share_count;
}
public void setShare_count(String share_count) {
	this.share_count = share_count;
}
public String getView_count() {
	return view_count;
}
public void setView_count(String view_count) {
	this.view_count = view_count;
}
}
