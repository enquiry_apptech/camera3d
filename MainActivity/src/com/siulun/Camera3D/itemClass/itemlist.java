package com.siulun.Camera3D.itemClass;

public class itemlist {
itemDetail iDetail;
itemResources iResources;
userDetail uDetail;
public itemlist(itemDetail iDetail,itemResources iResources,userDetail uDetail){
	this.iDetail = iDetail;
	this.iResources = iResources;
	this.uDetail = uDetail;
}
public itemDetail getiDetail() {
	return iDetail;
}
public void setiDetail(itemDetail iDetail) {
	this.iDetail = iDetail;
}
public itemResources getiResources() {
	return iResources;
}
public void setiResources(itemResources iResources) {
	this.iResources = iResources;
}
public userDetail getuDetail() {
	return uDetail;
}
public void setuDetail(userDetail uDetail) {
	this.uDetail = uDetail;
}

}
