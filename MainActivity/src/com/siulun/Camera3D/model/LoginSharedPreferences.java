package com.siulun.Camera3D.model;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class LoginSharedPreferences {
	 // Shared Preferences
    SharedPreferences pref;
    
    // Editor for Shared preferences
    Editor editor;
     
    // Context
    Context _context;
     
    // Shared pref mode
    int PRIVATE_MODE = 0;
     
    // Sharedpref file name
    private static final String PREF_NAME = "Camera3dPref_";
	
	 // User avatar url (make variable public to access from outside)
    public static final String KEY_USER_AVATAR_URL = "user_avatar_url";
    // User token (make variable public to access from outside)
    public static final String KEY_TOKEN = "token";
    // User nick name (make variable public to access from outside)
    public static final String KEY_USER_NICK_NAME = "user_nick_name";
    // User login status (make variable public to access from outside)
    public static final String KEY_FINISHED = "finished";
    // User email (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
 // User uid (make variable public to access from outside)
    public static final String KEY_USER_UID = "user_uid";
	
    
  
     
    // Constructor
    public LoginSharedPreferences(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
 

    
    public void putUserAvatarUrl(String AvatarUrl){
    	editor.putString(KEY_USER_AVATAR_URL, AvatarUrl);
    	// commit changes
        editor.commit();
    }
    public void putToken(String token){
    	editor.putString(KEY_TOKEN, token);
    	// commit changes
        editor.commit();
    }
    public void putUser_nick_name(String user_nick_name){
    	editor.putString(KEY_USER_NICK_NAME, user_nick_name);
    	// commit changes
        editor.commit();
    }
    public void startLogin(){
    	editor.putBoolean(KEY_FINISHED, true);
    	// commit changes
        editor.commit();
    }
    public void putEmail(String email){
    	editor.putString(KEY_EMAIL, email);
    	// commit changes
        editor.commit();
    }
    public void putUser_uid(String user_uid){
    	editor.putString(KEY_USER_UID, user_uid);
    	// commit changes
        editor.commit();
    }

    public String getUserAvatarUrl(){
    	return pref.getString(KEY_USER_AVATAR_URL, null);
    }
    public String getToken(){
    	return pref.getString(KEY_TOKEN, null);
    }
    public String getUser_nick_name(){
    	return pref.getString(KEY_USER_NICK_NAME, null);
    }
    public Boolean getLoginStatus(){
    	return pref.getBoolean(KEY_FINISHED, false);
    }
    public String getEmail(){
    	return pref.getString(KEY_EMAIL, null);
    }
    public String getUser_uid(){
    	return pref.getString(KEY_USER_UID, null);
    }
    
    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
         
    }
   
}
