package com.siulun.Camera3D.model;
import com.appjolt.winback.Winback;
import android.app.Application;

public class MainApplication extends Application {
	 @Override
	    public void onCreate() {
	        super.onCreate();

	        // Appjolt - Init SDK
	        Winback.init(this);
	    }
}
