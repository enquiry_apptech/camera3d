package com.siulun.Camera3D.model;

import com.appjolt.winback.Winback;
import com.appjolt.winback.Winback;
import android.app.Application;

public class UserInfo extends Application {
	private String user_avatar_url = null;
	private String token = null;
	private String user_nick_name;
	private Boolean finished = false;
	private String email;
	private String user_uid;

	 @Override
	    public void onCreate() {
	        super.onCreate();

	        // Appjolt - Init SDK
	        Winback.init(this);
	    }
	
	public String getUser_uid() {
		return user_uid;
	}

	public void setUser_uid(String user_uid) {
		this.user_uid = user_uid;
	}

	public String getUser_avatar_url() {
		return user_avatar_url;
	}

	public void setUser_avatar_url(String user_avatar_url) {
		this.user_avatar_url = user_avatar_url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUser_nick_name() {
		return user_nick_name;
	}

	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}

	public Boolean getFinished() {
		return finished;
	}

	public void setFinished(Boolean finished) {
		this.finished = finished;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
