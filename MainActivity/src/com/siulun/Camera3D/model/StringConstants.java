
package com.siulun.Camera3D.model;

public final class StringConstants {

	// URLS
  //  public static final String URL_BASE = "http://54.251.110.85/camera3d/www/index.php/xidong/";
	public static final String URL_BASE = "http://camera3d.apptech.com.hk/index.php/xidong/";
	public static final String URL_GET_PUBLIC_FEEDS = URL_BASE + "resource/get_public_feeds";
	public static final String URL_GET_ITEM_DETAIL = URL_BASE + "resource/get_item_info";
	public static final String URL_GET_ITEM_RESOURCES = URL_BASE + "resource/get_item_resources";
//	public static final String URL_GET_WEBVIEW = "http://54.251.110.85/camera3d_sharepage/index.php?item=";
	public static final String URL_REGISTER_NEW_ACC_WITH_FB = URL_BASE + "user/register_new_account_with_FB";
	public static final String URL_LOGIN_WITH_FB = URL_BASE + "user/login_with_FB";
	
	public static final String URL_REGISTER_NEW_ACC_WITH_GPLUS = URL_BASE + "user/register_new_account_with_gplus";
	
	public static final String URL_SEARCH_USER = URL_BASE + "user/search_user";
	public static final String URL_SEARCH_USER_ITEM = URL_BASE + "user/search_user_items";
	public static final String URL_FOLLOW_USER = URL_BASE + "user/follow_user";
	public static final String URL_UNFOLLOW_USER = URL_BASE + "user/unfollow_user";
	public static final String URL_GER_FOLLOWEE = URL_BASE + "user/get_followees";
	public static final String URL_LOGIN_WITH_EMAIL = URL_BASE+ "user/login_with_password";
	public static final String URL_REG = URL_BASE+ "user/register";
	public static final String URL_IS_FOLLOWEE = URL_BASE + "user/is_following";
	public static final String URL_SEPCIFIC_ITEM = URL_BASE + "resource/get_item_info";
	public static final String URL_CHANGE_PW = URL_BASE + "user/change_password";
	public static final String URL_get_following_feeds = URL_BASE + "resource/get_following_feeds";
//	public static final String URL_WEBVIEW = "http://54.251.110.85/camera3d_sharepage/index.php?item=";
	
	//Delete User Item
	public static final String URL_Delete_Users_Items = URL_BASE + "resource/delete_user_items";
	
	public static final String URL_get_comment = URL_BASE + "resource/get_comments";
	public static final String URL_create_comment = URL_BASE + "resource/create_comments";
	
	public static final String URL_create_like = URL_BASE + "resource/create_like";
	
	public static final String URL_add_view_item_number = URL_BASE + "resource/add_view_item_number";
	
	

	public static final String URL_FORGOT_PW = URL_BASE + "user/forget_password";
	public static final String URL_ADD_PROFILEPIC = URL_BASE + "user/add_profile_pic";
	public static final String URL_ADD_RESOURCE = URL_BASE + "resource/add_resource";
	public static final String BASEURL = URL_BASE + "resource/";

	// TAGS
	public static final String TAG_JSON_STRING_REQ = "JSON_STRING_REQ";
	public static final String TAG_JSON_IMAGE_REQ = "JSON_IMAGE_REQ";
	// FRAGMENTS
	public static final String TAG_NEWSFEED_FRAGMENT = "NEWSFEED_FRAGMENT";
	public static final String TAG_SEARCH_FRAGMENT = "SEARCH_FRAGMENT";
	// DEBUG temp
	public static final String TAG_DEBUG = "DD";
	
	//For SharedPreferences 
	public static final String USERINFO = "USERINFO";
	public static final String USER_IMAGE_URL = "USER_IMAGE_URL";
	public static final String USERNICKNAME = "USERNICKNAME";
	public static final String TOKEN = "TOKEN";
	public static final String FOLLOWEE_INFO = "FOLLOWEE_INFO";
}
