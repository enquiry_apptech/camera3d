
package com.siulun.Camera3D.model.common;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = 666L;
	private Integer uID;
	private String firstName;
	private String lastName;
	private String nickName;
	private String email;
	private String profilePicURL;

	public Integer getuID() {

		return uID;
	}

	public void setuID(Integer uID) {

		this.uID = uID;
	}

	public String getFirstName() {

		return firstName;
	}

	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	public String getLastName() {

		return lastName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public String getNickName() {

		return nickName;
	}

	public void setNickName(String nickName) {

		this.nickName = nickName;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getProfilePicURL() {

		return profilePicURL;
	}

	public void setProfilePicURL(String profilePicURL) {

		this.profilePicURL = profilePicURL;
	}

	@Override
	public String toString() {

		return "User [uID="
		       + uID
		       + ", firstName="
		       + firstName
		       + ", lastName="
		       + lastName
		       + ", nickName="
		       + nickName
		       + ", email="
		       + email
		       + ", profilePicURL="
		       + profilePicURL
		       + "]";
	}
}
