package com.example.gallery;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.gallery.Gallery.CreateItemPost;
import com.siulun.Camera3D.view.fragments.Showpopup;
import com.siulun.Camera3D.view.fragments.profile;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.siulun.Camera3D.model.UserInfo;
import com.siulun.Camera3D.view.MainActivity;
import com.siulun.Camera3D.view.fragments.SettingFragment;

import com.siulun.Camera3D.R;

import android.app.AlertDialog;

import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.Fragment;
//import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import com.siulun.Camera3D.model.*;
import com.siulun.Camera3D.networkconfig.volley.NetworkChecking;

public class GalleryFragment extends Fragment implements OnItemClickListener,
		OnItemLongClickListener, OnClickListener {
	
	LoginSharedPreferences sharedpreferences;
	
	private LayoutInflater layoutInflater;
	private static boolean notclickeddialog = true;
	private static final int REAUTH_ACTIVITY_CODE = 100;
	// Configuration
	// private final String BASEURL =
	// "http://54.251.110.85/camera3d/www/index.php/xidong/resource/";
	private final String BASEURL = "http://camera3d.apptech.com.hk/index.php/xidong/resource/";
	// Append Url
	private final String CREATE_ITEM = "create_item";

	private static AsyncHttpClient client = new AsyncHttpClient();
	// String url =
	// "http://54.251.110.85/camera3d/www/index.php/xidong/resource/add_resource";
	String url = "http://camera3d.apptech.com.hk/index.php/xidong/resource/add_resource";
	boolean canItemSelected = true;
	private LinearLayout profileRelativeLayout;
	private ArrayList<Bitmap> galleryPicture = new ArrayList<Bitmap>();
	private ArrayList<String> directoryName = new ArrayList<String>();
	private ArrayList<String> directoryPath = new ArrayList<String>();
	private ArrayList<String> eachImagePath = new ArrayList<String>();
	String[] eachfilePath;
	LinearLayout llDelete;
	LinearLayout llGIF;
	LinearLayout llGIFFrameRate;
	LinearLayout llOptionPanel;
	Button btnDeleteYes;
	Button btnDeleteNo;
	Button btnGIF;
	Button btnGIFYes;
	Button btnGIFNo;
	Button btnGIFFrameRate200;
	Button btnGIFFrameRate500;
	Button btnGIFFrameRate800;
	Button btnDelete;
	Button btnUpload;
	LinearLayout cameraprofile_linearlayout;
	GalleryAdapter adapter;
	int targetPosition = -1;
	ProgressDialog progress;
	private Session.StatusCallback callback;
	int iFrameDuration = 200; // ms
	int current = 0;
	int lastCurrent = 0;
	int picNumber = 0;
	boolean nextUpload = true;
	// file storage
	GridView gridview;

	final String FOLDER_NAME = "/Camera3D_MVC";// ".Camera3D";
	final String FOLDER_NAME2 = "Camera3D";
	// File mediaStorageDir = new File(
	// Environment
	// .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
	// FOLDER_NAME);
	File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
			FOLDER_NAME);
	File galleryDir = Environment.getExternalStorageDirectory();
	// String strBasePath = galleryDir.getPath();
	String strBasePath = mediaStorageDir.getPath() + "/Camera3DGallery";
	private UiLifecycleHelper uiHelper;
	private UserInfo userGlobalVariable;
	DisplayImageOptions optionsOfSamllIcon, optionsOfPhoto;
	NetworkChecking checkNetwork;
	private ImageLoadingListener animateFirstListener;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Gallery", "Start: onCreate()");
		sharedpreferences= new LoginSharedPreferences(getActivity());
	/*	if (userGlobalVariable == null) {
			userGlobalVariable = ((UserInfo) getActivity()
					.getApplicationContext());
		}*/
		// setContentView(R.layout.gallery);

	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		checkNetwork = new NetworkChecking(getActivity());
		layoutInflater = inflater;
		/*if (userGlobalVariable == null) {
			userGlobalVariable = ((UserInfo) getActivity()
					.getApplicationContext());
		}*/
		
		profileRelativeLayout = (LinearLayout) layoutInflater.inflate(
				R.layout.gallery, container, false);

		cameraprofile_linearlayout = (LinearLayout) profileRelativeLayout
				.findViewById(R.id.cameraprofile_linearlayout);
		llDelete = (LinearLayout) profileRelativeLayout
				.findViewById(R.id.llDeleteLayout);
		btnDeleteYes = (Button) profileRelativeLayout
				.findViewById(R.id.btnDeleteYes);
		btnDeleteYes.setOnClickListener(this);
		btnDeleteNo = (Button) profileRelativeLayout
				.findViewById(R.id.btnDeleteNo);
		btnDeleteNo.setOnClickListener(this);
		llGIF = (LinearLayout) profileRelativeLayout.findViewById(R.id.llGIF);
		btnGIFYes = (Button) profileRelativeLayout.findViewById(R.id.btnGIFYes);
		btnGIFYes.setOnClickListener(this);
		btnGIFNo = (Button) profileRelativeLayout.findViewById(R.id.btnGIFNo);
		btnGIFNo.setOnClickListener(this);
		llGIFFrameRate = (LinearLayout) profileRelativeLayout
				.findViewById(R.id.llGIFFrameRate);
		btnGIFFrameRate200 = (Button) profileRelativeLayout
				.findViewById(R.id.btnGIFFrameRate_200);
		btnGIFFrameRate200.setOnClickListener(this);
		btnGIFFrameRate500 = (Button) profileRelativeLayout
				.findViewById(R.id.btnGIFFrameRate_500);
		btnGIFFrameRate500.setOnClickListener(this);
		btnGIFFrameRate800 = (Button) profileRelativeLayout
				.findViewById(R.id.btnGIFFrameRate_800);
		btnGIFFrameRate800.setOnClickListener(this);
		llOptionPanel = (LinearLayout) profileRelativeLayout
				.findViewById(R.id.llOptionPanel);
		btnDelete = (Button) profileRelativeLayout.findViewById(R.id.btnDelete);
		btnDelete.setOnClickListener(this);
		btnGIF = (Button) profileRelativeLayout.findViewById(R.id.btnGIF);
		btnGIF.setOnClickListener(this);
		btnUpload = (Button) profileRelativeLayout.findViewById(R.id.btnUpload);

		btnUpload.setOnClickListener(this);
		// btnUpload.setVisibility(View.GONE);
		gridview = (GridView) profileRelativeLayout
				.findViewById(R.id.gvGallery);

		// userProfilepic.setImageResource(R.drawable.ic_launcher);
		directoryName.clear();
		getDirectotyName();
		if (galleryPicture.size() == 0) {
			getBitmap();
		}

		adapter = new GalleryAdapter(getActivity(), galleryPicture);
		gridview.setAdapter(adapter);
		gridview.setOnItemClickListener(this);
		gridview.setOnItemLongClickListener(this);

		return profileRelativeLayout;
	}

	public void TestToast() {
		Toast.makeText(getActivity(), "problem happen", Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// userProfilepic.setImageResource(R.drawable.ic_launcher); // Load
		// image into ImageView
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REAUTH_ACTIVITY_CODE) {
			uiHelper.onActivityResult(requestCode, resultCode, data);
		}
	}

	public void updateGallery() {
		if (adapter != null) {
			directoryName.clear();
			galleryPicture.clear();
			directoryPath.clear();
			getDirectotyName();
			getBitmap();
			adapter = new GalleryAdapter(getActivity(), galleryPicture);
			gridview.setAdapter(adapter);

		} else {
			TestToast();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		updateGallery();

		if (MainActivity.DisplayFragment == MainActivity.Fragmentprofile) {

			profile.page = 0;
			// Showpopup showPopup = new Showpopup();
			// showPopup.showPopup(getActivity(),"Long Press photo!!!!!\nExport to gif and delete \nAfter login to upload");
			// uiHelper.onResume();
		}
		/*if (userGlobalVariable == null) {
			userGlobalVariable = ((UserInfo) getActivity()
					.getApplicationContext());
		}*/
		//if (((UserInfo) getActivity().getApplication()).getToken() == null) {

if(sharedpreferences.getToken()==null){
			btnUpload.setVisibility(View.GONE);
		} else {
			btnUpload.setVisibility(View.VISIBLE);
		}
		if (targetPosition != -1 && canItemSelected) {
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();
			targetPosition = -1;
			canItemSelected = false;
		}

	}

	/**
	 * public class DownloadImagesTask extends AsyncTask<ImageView, Void,
	 * Bitmap> {
	 * 
	 * ImageView imageView = null;
	 * 
	 * @Override protected Bitmap doInBackground(ImageView... imageViews) {
	 *           this.imageView = imageViews[0]; return
	 *           download_Image((String)imageView.getTag()); }
	 * @Override protected void onPostExecute(Bitmap result) {
	 *           imageView.setImageBitmap(result); }
	 * 
	 *           private Bitmap download_Image(String url) {
	 * 
	 *           Bitmap bmp =null; try{ URL ulrn = new URL(url);
	 *           HttpURLConnection con =
	 *           (HttpURLConnection)ulrn.openConnection(); InputStream is =
	 *           con.getInputStream(); bmp = BitmapFactory.decodeStream(is); if
	 *           (null != bmp) return bmp;
	 * 
	 *           }catch(Exception e){} return bmp; } }
	 **/
	private void getDirectotyName() {
		Log.d("Gallery", "Start: getDirectotyName()");

		File f = new File(strBasePath);
		File[] files = f.listFiles();
		File f2;
		File[] files2;

		if (files == null) {
			return;
		}
		for (File inFile : files) {
			if (inFile.isDirectory()) {
				// is directory
				f2 = new File(strBasePath + File.separator + inFile.getName());
				files2 = f2.listFiles();

				if (files2 != null) {
					if (files2.length > 0) {
						directoryName.add(inFile.getName());
						Log.d("Gallery",
								"getDirectotyName(): " + inFile.getName());
					} else {
						Log.d("Gallery",
								"Empty: getDirectotyName()" + inFile.getName());
						inFile.delete();
					}
				} else {
					Log.d("Gallery", "Null: getDirectotyName()");
				}
			}
		}

		Log.d("Gallery", "End: getDirectotyName()");
	}

	private void getBitmap() {
		// TODO Auto-generated method stub
		for (int i = 0; i < directoryName.size(); i++) {
			String uri = getUri(0, directoryName.get(i)).toString();
			File imgFile = new File(uri);
			if (imgFile.exists()) {
				directoryPath.add(strBasePath + File.separator
						+ directoryName.get(i));
				int rotate = getCameraPhotoOrientation(getActivity(),
						getUri(0, directoryName.get(i)),
						imgFile.getAbsolutePath());
				Bitmap myBitmap = null;
			/*	try{
				myBitmap = BitmapFactory.decodeFile(imgFile
						.getAbsolutePath());
				galleryPicture.add(myBitmap);
				}catch(Exception e){
					 BitmapFactory.Options options = new BitmapFactory.Options();
					 options.inJustDecodeBounds = false;
					 options.inPreferredConfig = Config.RGB_565;
					 options.inDither = true;
					 myBitmap = BitmapFactory.decodeFile(imgFile
								.getAbsolutePath(),options);
					 galleryPicture.add(myBitmap);
					 
				}*/
				
				 BitmapFactory.Options options = new BitmapFactory.Options();
				 options.inJustDecodeBounds = false;
				 options.inPreferredConfig = Config.RGB_565;
				 options.inDither = true;
				 /*options.outHeight = 200;
				 options.outWidth = 200;
				 */
			     options.inSampleSize = 5;
				 myBitmap = BitmapFactory.decodeFile(imgFile
							.getAbsolutePath(),options);
				 galleryPicture.add(myBitmap);
				/* */
				
			}
		}
		// Log.d("Gallery", "Bitmap path: "+directoryPath);
	}

	public static int getCameraPhotoOrientation(Context context, Uri imageUri,
			String imagePath) {
		int rotate = 0;
		try {
			context.getContentResolver().notifyChange(imageUri, null);
			File imageFile = new File(imagePath);
			ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rotate;
	}

	private Uri getUri(int iIndex, String append) {
		return Uri.parse(String.format("%s%s%03d.jpg", strBasePath + "/"
				+ append, File.separator, iIndex));
	}

	private int countFiles() {
		int count = 0;

		File f = new File(strBasePath);
		File[] files = f.listFiles();
		// Log.d("s","see  "+f.listFiles());
		if (files == null) {
			return count;
		}
		for (File inFile : files) {
			if (inFile.isDirectory()) {
				// is directory
				count++;
			}
		}
		return count;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnDelete:
			llOptionPanel.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			llGIFFrameRate.setVisibility(View.GONE);
			llDelete.setVisibility(View.VISIBLE);
			break;
		case R.id.btnGIF:
			llOptionPanel.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIFFrameRate.setVisibility(View.VISIBLE);

			break;
		case R.id.btnGIFFrameRate_200:
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.VISIBLE);
			iFrameDuration = 200;
			break;
		case R.id.btnGIFFrameRate_500:
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.VISIBLE);
			iFrameDuration = 500;
			break;
		case R.id.btnGIFFrameRate_800:
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.VISIBLE);
			iFrameDuration = 800;
			break;
		case R.id.btnUpload:
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();

			current = 0;

			// get prompts.xml view
			LayoutInflater li = LayoutInflater.from(getActivity());

			View promptsView = li.inflate(R.layout.upload_dialogbox, null);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					getActivity());

			// set prompts.xml to alertdialog builder
			alertDialogBuilder.setView(promptsView);

			final EditText description = (EditText) promptsView
					.findViewById(R.id.upload_item_des_ed);

			// set dialog message
			alertDialogBuilder
					.setCancelable(false)
					.setPositiveButton("Upload",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// get user input and set it to result
									// edit text
									if (checkNetwork.isNetworkAvailable()) {
										/*String token = userGlobalVariable
												.getToken();*/
										String token = sharedpreferences.getToken();
										Log.i("token", token);
										String[] input = {
												token,
												directoryName
														.get(targetPosition),
												description.getText()
														.toString() };
										new CreateItemPost().execute(input);
									} else {
										Toast.makeText(
												getActivity(),
												"No network connection, please try it again!!!",
												Toast.LENGTH_SHORT).show();

									}
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();

			// new CreateItemPost().execute(input);

			// picNumber = checkPictureNumber(targetPosition);
			// for(int i=0;i<picNumber;i++){
			// nextUpload=false;
			// RequestParams params = new RequestParams();
			// String uri =
			// getUri(i,directoryName.get(targetPosition)).toString();
			// File myFile = new File(uri);
			// try {
			// params.put("item_uid", "888");
			// params.put("resource_type", "image");
			// params.put("token", "7ec2b4e93c2107b972452ab0d6b74159");
			// params.put("resource_index", i);
			// params.put("uploadedfile", myFile);
			// } catch(FileNotFoundException e) {
			// Log.e("TAF","File Not Found");
			// }
			// client.post(url, params, new AsyncHttpResponseHandler(){
			//
			// @Override
			// public void onFailure(int arg0, Header[] arg1, byte[] arg2,
			// Throwable arg3) {
			// // TODO Auto-generated method stub
			// Log.e("tag","progress:FAIL");
			//
			// }
			//
			// @Override
			// public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
			// // TODO Auto-generated method stub
			//
			// Log.e("TAG","progress123:"+arg2.toString()+current);
			// current++;
			// if(current==picNumber){
			// Toast.makeText(getBaseContext(),"Upload Success",
			// Toast.LENGTH_SHORT).show();
			// }
			//
			// }
			//
			// });
			//
			// }
			//
			// targetPosition = -1;
			// progress = ProgressDialog.show(this, "Upload",
			// "Uploading", true);
			// new Thread(new Runnable() {
			// @Override
			// public void run()
			// {
			// // do the thing that takes a long time
			//
			// upload();
			//
			//
			//
			// runOnUiThread(new Runnable() {
			// @Override
			// public void run()
			// {
			// // progress.dismiss();
			//
			// }
			// });
			// }
			//
			//
			// }).start();

			break;
		case R.id.btnDeleteYes:
			llDelete.setVisibility(View.GONE);
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			adapter.removePicture(targetPosition);
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();
			deleteFile();
			galleryPicture.remove(targetPosition);
			directoryPath.remove(targetPosition);
			directoryName.remove(targetPosition);
			targetPosition = -1;
			break;
		case R.id.btnDeleteNo:
			llDelete.setVisibility(View.GONE);
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			targetPosition = -1;
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();
			break;
		case R.id.btnGIFYes:
			progress = ProgressDialog.show(getActivity(), "Converting to GIF",
					"Exporting: " + getAppRootFile().toString(), true);
			new Thread(new Runnable() {
				@Override
				public void run() {
					// do the thing that takes a long time

					File destinationDirectory = getAppRootFile();
					FileOutputStream outStream = null;
					try {
						outStream = new FileOutputStream(destinationDirectory
								+ File.separator
								+ directoryName.get(targetPosition) + ".gif");
						outStream.write(generateGIF());
						outStream.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							progress.dismiss();
							llGIF.setVisibility(View.GONE);
							llGIFFrameRate.setVisibility(View.GONE);
							llOptionPanel.setVisibility(View.GONE);
							llDelete.setVisibility(View.GONE);
							targetPosition = -1;
							adapter.setColorPosition(-1);
							adapter.notifyDataSetChanged();
						}
					});
				}
			}).start();

			break;
		case R.id.btnGIFNo:
			llGIF.setVisibility(View.GONE);
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			targetPosition = -1;
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();
			break;
		}

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int arg2,
			long arg3) {
		canItemSelected = false;
		llOptionPanel.setVisibility(View.VISIBLE);
		llGIF.setVisibility(View.GONE);
		llGIFFrameRate.setVisibility(View.GONE);
		llDelete.setVisibility(View.GONE);
		adapter.setColorPosition(arg2);
		targetPosition = arg2;
		adapter.notifyDataSetChanged();
		new CountDownTimer(500, 500) {

			public void onTick(long millisUntilFinished) {
			}

			public void onFinish() {
				canItemSelected = true;
			}

		}.start();
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (position != targetPosition) {
			Intent intent = new Intent(getActivity(), Preview.class);

			intent.putExtra("Picture", directoryName.get(position));
			startActivity(intent);
		} else if (position == targetPosition && canItemSelected) {
			llGIFFrameRate.setVisibility(View.GONE);
			llOptionPanel.setVisibility(View.GONE);
			llDelete.setVisibility(View.GONE);
			llGIF.setVisibility(View.GONE);
			adapter.setColorPosition(-1);
			adapter.notifyDataSetChanged();
			targetPosition = -1;
		}

	}

	private void deleteFile() {
		// TODO Auto-generated method stub
		File galleryFile = new File(directoryPath.get(targetPosition));
		if (galleryFile.exists()) {
			String[] children = galleryFile.list();
			for (int i = 0; i < children.length; i++) {
				new File(galleryFile, children[i]).delete();
			}

		}
		galleryFile.delete();
	}

	public byte[] generateGIF() {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		AnimatedGifEncoder encoder = new AnimatedGifEncoder();
		encoder.start(bos);
		encoder.setDelay(iFrameDuration);
		ArrayList<Bitmap> gif = new ArrayList<Bitmap>();
		ArrayList<Bitmap> gifOut = new ArrayList<Bitmap>();
		gif = getGIFBitmap();
		for (int i = 0; i < gif.size(); i++) {
			gifOut.add(gif.get(i));
		}
		for (int i = 0; i < gifOut.size(); i++) {
			encoder.addFrame(gifOut.get(i));
		}
		Log.d("hsdjf", "cat:" + gifOut.size());

		encoder.finish();
		Log.d("hoidsfj", "jsorkfj" + bos.toByteArray());
		return bos.toByteArray();
	}

	private ArrayList<Bitmap> getGIFBitmap() {

		ArrayList<Bitmap> gif = new ArrayList<Bitmap>();
		int pictureNumber = 0;
		pictureNumber = checkPictureNumber(targetPosition);
		for (int i = 0; i < pictureNumber; i++) {
			String uri = getUri(i, directoryName.get(targetPosition))
					.toString();
			Log.d("uri", "uri:" + uri);
			File imgFile = new File(uri);
			if (imgFile.exists()) {
				Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
						.getAbsolutePath());
				gif.add(myBitmap);
			}

		}

		return gif;
	}

	private int checkPictureNumber(int target) {
		// TODO Auto-generated method stub
		int count = 0;
		File f = new File(strBasePath + File.separator
				+ directoryName.get(target));
		File[] files = f.listFiles();
		if (files == null) {
			return 0;
		}
		for (File inFile : files) {
			if (!inFile.isDirectory()) {
				if (inFile.getName().contains(".jpg")) {
					count++;
				}
			}
		}
		return count;
	}

	private File getAppRootFile() {
		// File appRootFile = new File(
		// Environment
		// .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		// FOLDER_NAME);

		File appRootFile = new File(Environment.getExternalStorageDirectory(),
				FOLDER_NAME2);

		if (!appRootFile.exists()) {
			if (!appRootFile.mkdir()) {
				Log.d("SAVE_PHOTO", "Required storage does not exist");
				return null;
			}
		}

		return appRootFile;
	}

	class CreateItemPost extends AsyncTask<String, String, String> {
		// ProgressDialog pdLoading = new ProgressDialog(getBaseContext());
		String status;

		// Before running
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			// pdLoading.setMessage("\tCreating Items...");
			// pdLoading.show();
		}

		// After running
		@Override
		protected void onPostExecute(String result) {
			// super.onPostExecute(result);

			// this method will be running on UI thread
			// pdLoading.dismiss();
			// check how many pic in the file at that position
			picNumber = checkPictureNumber(targetPosition);

			for (int i = 0; i < picNumber; i++) {
				nextUpload = false;
				RequestParams data = new RequestParams();
				String uri = getUri(i, directoryName.get(targetPosition))
						.toString();

				File myFile = new File(uri);
				try {
					//data.put("token", userGlobalVariable.getToken()); // IMPORTANT
					data.put("token", sharedpreferences.getToken());
					data.put("item_uid", status);
					data.put("resource_type", "image");
					data.put("resource_index", i);
					data.put("uploadedfile", myFile);
				} catch (FileNotFoundException e) {
					Log.e("TAF", "File Not Found");
					Toast.makeText(getActivity(), "File Not Found",
							Toast.LENGTH_SHORT).show();
				}
				
				client.post(url, data, new AsyncHttpResponseHandler() {

					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						// TODO Auto-generated method stub
						Log.e("tag", "progress:FAIL");
						Toast.makeText(getActivity(), "Upload Fail",
								Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub

						Log.e("TAG", "progress123:" + arg2.toString() + current);
						current++;
						if (current == picNumber) {
							Toast.makeText(getActivity(), "Upload Success",
									Toast.LENGTH_SHORT).show();
						}

					}

				});

			}

			targetPosition = -1;

		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String token = params[0];
			String name = params[1];
			String description = params[2];
			String response = "";
			response = postCreate(BASEURL + CREATE_ITEM, token, name,
					description);
			// jsonStr =
			// getJSON(BASEURL+REGISTER_NEW_ACCOUNT+"/"+user.getAccessToken()+"/"+user.getAccessTokenSecret());
			Log.d("URL", "> " + BASEURL + CREATE_ITEM);
			// Log.d("Response: ", "> " + jsonStr);

			if (response != null) {
				try {
					// JSONArray jsonArray = new JSONArray(jsonStr);
					JSONObject item = new JSONObject(response);

					// Getting JSON Array node
					// jsonData = jsonObj.getJSONArray("");

					// looping through All Contacts
					// for (int i = 0; i < item.length(); i++) {
					// JSONObject c = jsonArray.getJSONObject(i);
					// JSONArray user = c.getJSONArray("user");
					// JSONObject userObject = user.getJSONObject(0);

					status = item.getString("item_id");
					Log.d("registration", "reg " + status);

					// }

					//

				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				Log.e("ServiceHandler", "Couldn't get any data from the url");
			}

			return response.toString();
		}
	}

	public String postCreate(String address, String token, String name,
			String des) {
		HttpClient httpClient = new DefaultHttpClient();
		StringBuilder builder = new StringBuilder();
		// replace with your url
		HttpPost httpPost = new HttpPost(address);

		// Post Data
		List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
		nameValuePair.add(new BasicNameValuePair("token", token));
		nameValuePair.add(new BasicNameValuePair("item_name", name));
		nameValuePair.add(new BasicNameValuePair("item_description", des));

		// Encoding POST data
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
		} catch (UnsupportedEncodingException e) {
			// log exception
			e.printStackTrace();
		}

		// making POST request.
		try {
			HttpResponse response = httpClient.execute(httpPost);
			// write response to log
			Log.d("Http Post Response:", response.toString());

			HttpEntity entity = response.getEntity();
			Log.d("jake", "entity: " + entity);
			InputStream content = entity.getContent();
			Log.d("ahkf", "content: " + content);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					content));
			Log.d("sdjfh", "reader: " + reader);
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				// }
				// } else {
				// Log.e(MainActivity.class.toString(),"Failed to get JSON object");
			}

			Log.d("seofoisj", "return: " + builder.toString());
			return builder.toString();
		} catch (ClientProtocolException e) {
			// Log exception
			e.printStackTrace();
		} catch (IOException e) {
			// Log exception
			e.printStackTrace();
		}
		return builder.toString();
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}

/**
 * A class that implements the Java FileFilter interface.
 */
class ImageFileFilter implements FileFilter {
	private final String[] okFileExtensions = new String[] { "jpg", "png",
			"gif", "jpeg" };

	public boolean accept(File file) {
		for (String extension : okFileExtensions) {
			if (file.getName().toLowerCase().endsWith(extension)) {
				return true;
			}
		}
		return false;
	}
}
