package com.example.gallery;

import java.io.File;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.siulun.Camera3D.R;

public class Preview extends Activity implements OnTouchListener {
	
	// img state
	private int iFragment;
	private int iCurrent, iFirstImg, iLastImg;

	// playing config.
	private long PLAY_PERIOD = 4000; // ms
	private long lImgDisplayTime; // ms per img

	// deceleration
	float SLOW_RATE = 1.1f; // FPS incresing ratio (resistence of rotation)
	final float CONVERSION_CONSTANT = 10000.0f; // NEED TO TUNE
	final long MIN_SPF = 50; // ms per img (fastest)
	final long MAX_SPF = 200; // ms per img (slowest)

	// file storage
	final String FOLDER_NAME = "/Camera3D_MVC";//".Camera3D";
//	File mediaStorageDir = new File(
//			Environment
//					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//			FOLDER_NAME);
	File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),FOLDER_NAME);
	String strBasePath = mediaStorageDir.getPath()+"/Camera3DGallery/";

	// UI
	private ImageView ivPreview;
	private TextView tvDisplay;

	// about touch
//	private VelocityTracker mVelocityTracker;
	private float fLastX;// = -1;
	private float fLastUpdatedX;// = -1;
	private float fDeltaUpdatedX;// = -1;
	private boolean bToLeft;
	final int SENSITIVITY = 20; // dp

	// threads
	private RotateThread threadRotate;
	private DecelerateThread threadDecelerate;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String result=getIntent().getStringExtra("Picture");
		
		strBasePath += result;
		
		// no title
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// keep screen on
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.preview);

		initializeUI();

		iFragment = countFiles();
		iCurrent = 0;
		iFirstImg = 0;
		iLastImg = iFragment - 1;
		// Log.d("Folder stat", "#: " + countFiles());

		if (iFragment != 0)
			lImgDisplayTime = PLAY_PERIOD / iFragment;

	}

	private void initializeUI() {
		ivPreview = (ImageView) findViewById(R.id.ivPreview);
		ivPreview.setOnTouchListener(this);
		ivPreview.setImageURI(getUri(0));
		
		tvDisplay = (TextView) findViewById(R.id.tvDisplay);
		tvDisplay.setText(String.format("%d/%d", iCurrent+1, iFragment));
	}

	public void onPause() {
		super.onPause();
		// stop the rotating thread
		if (threadRotate != null) {
			threadRotate.stopRotating();
		}
		// stop the decelerating thread
		if (threadDecelerate != null) {
			threadDecelerate.stopDecelerating();
		}
	}

	public void onResume() {
		super.onResume();
		threadRotate = new RotateThread();
		threadRotate.start();
	}

	private void prevImg() {
		if (iCurrent <= iFirstImg) { // from first img to last img
			iCurrent = iLastImg;
			ivPreview.setImageURI(getUri(iCurrent));
		} else {
			iCurrent -= 1;
			ivPreview.setImageURI(getUri(iCurrent));
		}
		Log.d("Preview", "prevImg() " + iCurrent);
		tvDisplay.setText(String.format("%d/%d", iCurrent+1, iFragment));
	}

	private void nextImg() {
		if (iCurrent >= iLastImg) { // from last img to first img
			iCurrent = iFirstImg;
			ivPreview.setImageURI(getUri(iCurrent));
		} else {
			iCurrent += 1;
			ivPreview.setImageURI(getUri(iCurrent));
		}
		Log.d("Preview", "nextImg() " + iCurrent);
		tvDisplay.setText(String.format("%d/%d", iCurrent+1, iFragment));
	}

	private Uri getUri(int iIndex) {
		return Uri.parse(String.format("%s%s%03d.jpg", strBasePath,
				File.separator, iIndex));
	}

	private int countFiles() {
		return new File(strBasePath).list().length;
	}

	// ===================== event ====================
	public boolean onTouch(View v, MotionEvent event) {

		float fX, fDeltaX;
		float fInitiailDisplayTime;

		// stop the rotating thread
		if (threadRotate != null) {
			threadRotate.stopRotating();
		}

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// initialize
			fLastUpdatedX = event.getX();
			fLastX = event.getX();

			// initialize velocity tracker
//			if (mVelocityTracker == null) { // new tracker
//				mVelocityTracker = VelocityTracker.obtain();
//			} else { // reset existing tracker
//				mVelocityTracker.clear();
//			}
//			mVelocityTracker.addMovement(event);

			// stop the decelerating thread
			if (threadDecelerate != null) {
				threadDecelerate.stopDecelerating();
			}
			break;
		case MotionEvent.ACTION_MOVE:
			fX = event.getX();
			fDeltaX = fX - fLastX;
			fDeltaUpdatedX = fX - fLastUpdatedX;

//			mVelocityTracker.addMovement(event);
//			mVelocityTracker.computeCurrentVelocity(200);

			if (fX > fLastX) { // towards right
				bToLeft = false;
				// Log.d("ACTION_MOVE",
				// "fX: "+fX+", fLastX: "+fLastX+", fLastUpdatedX"+fLastUpdatedX);
				if (fDeltaUpdatedX >= fDeltaX) { // same direction
					// Log.d("ACTION_MOVE",
					// "RR fX: "+fX+", fLastX: "+fLastX+", UpdatedX: "+fLastUpdatedX);
					if (fDeltaUpdatedX > SENSITIVITY) {
						// Log.d("ACTION_MOVE", "prevImg()");
						prevImg();
						fLastUpdatedX = fX; // set updated point
					}
				} else { // different direction
					// Log.d("ACTION_MOVE",
					// "RL fX: "+fX+", fLastX: "+fLastX+", UpdatedX: "+fLastUpdatedX);
					fLastUpdatedX = fLastX; // reset updated point
				}
			} else { // towards left
				bToLeft = true;
				// Log.d("ACTION_MOVE",
				// "fX: "+fX+", fLastX: "+fLastX+", UpdatedX: "+fLastUpdatedX);
				if (fDeltaUpdatedX <= fDeltaX) { // same direction
					// Log.d("ACTION_MOVE",
					// "LL fX: "+fX+", fLastX: "+fLastX+", UpdatedX: "+fLastUpdatedX);
					if (fDeltaUpdatedX < -SENSITIVITY) {
						// Log.d("ACTION_MOVE", "nextImg()");
						nextImg();
						fLastUpdatedX = fX; // set updated point
					}
				} else { // different direction
					// Log.d("ACTION_MOVE",
					// "LR fX: "+fX+", fLastX: "+fLastX+", UpdatedX: "+fLastUpdatedX);
					fLastUpdatedX = fLastX; // reset updated point
				}
			}

			// update
			fLastX = fX;

			break;
		case MotionEvent.ACTION_UP:
			// start the decelerate thread
//			fInitiailDisplayTime = bToLeft ? -CONVERSION_CONSTANT
//					/ mVelocityTracker.getXVelocity() : CONVERSION_CONSTANT
//					/ mVelocityTracker.getXVelocity();
			fInitiailDisplayTime = 100;
			threadDecelerate = new DecelerateThread(fInitiailDisplayTime,
					bToLeft);
			threadDecelerate.start();
//			mVelocityTracker.recycle();
			break;
		}
		return true;
	}

	// ===================== event ================ends

	// ===================== thread ===================
	class RotateThread extends Thread {
		boolean bKeepRotating = true;

		public void run() {
			while (bKeepRotating) {
				ivPreview.post(new Runnable() {
					public void run() {
						nextImg();
					}
				});
				try {
					Thread.sleep(lImgDisplayTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		public void stopRotating() {
			bKeepRotating = false;
		}
	}

	class DecelerateThread extends Thread {
		boolean bKeepDecelerating = true;
		float fDisplayTimeInDece;
		boolean bToLeft;

		public DecelerateThread(float lInitialDisplayTime,
				boolean bInitialToLeft) {
			fDisplayTimeInDece = lInitialDisplayTime;
			if (fDisplayTimeInDece < MIN_SPF) { // too short
				fDisplayTimeInDece = MIN_SPF;
				Log.d("decelerateThread", "Too short: " + lInitialDisplayTime
						+ " --> " + fDisplayTimeInDece);
			}

			bToLeft = bInitialToLeft;
		}

		public void run() {
			while (bKeepDecelerating) {
				if (fDisplayTimeInDece >= MAX_SPF) {
					Log.d("decelerateThread", "Too slow: " + fDisplayTimeInDece);
					break;
				}

				ivPreview.post(new Runnable() {
					@Override
					public void run() {
						if (bToLeft)
							nextImg();
						else
							prevImg();
					}
				});

				Log.d("decelerateThread", "Display Time: " + fDisplayTimeInDece);

				try {
					Thread.sleep((long) fDisplayTimeInDece);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// update next display time
				fDisplayTimeInDece *= SLOW_RATE;
			}
		}

		public void stopDecelerating() {
			bKeepDecelerating = false;
		}
	}

}
// ===================== thread ===============ends