package com.example.gallery;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.siulun.Camera3D.R;

public class GalleryAdapter extends BaseAdapter {
	
    private RelativeLayout row;
    private Context context;
    private ImageView ivGalleryPicture;
    private ArrayList<Bitmap> galleryBitmap = new ArrayList<Bitmap>();
    private int colorPosition=-1;;

    
    public GalleryAdapter(Context context, ArrayList<Bitmap> bitmap)
    { 	
    	this.context = context;
    	for(int i=0; i<bitmap.size(); i++){
    		galleryBitmap.add(bitmap.get(i));
    	}
    }

    public int getCount() {
        return galleryBitmap.size();  
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    
    public void removePicture(int position){
    	galleryBitmap.remove(position);
    }

    public int getColorPosition() {
		return colorPosition;
	}

	public void setColorPosition(int colorPosition) {
		this.colorPosition = colorPosition;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
        	
        	row = (RelativeLayout) (convertView == null
                    ? LayoutInflater.from(context).inflate(R.layout.grid_content, parent, false)
                    : convertView);
        ivGalleryPicture = (ImageView) row.findViewById(R.id.ivGalleryPicture);
        ivGalleryPicture.setImageBitmap(galleryBitmap.get(position));
        //ivGalleryPicture.setAlpha(100);
        //row.setBackgroundColor(Color.parseColor("#dddddd"));
         if(colorPosition == position){
        	 //row.setBackgroundColor(Color.GREEN);
        	 ivGalleryPicture.setAlpha(70);
         }
         else{
        	 ivGalleryPicture.setAlpha(1000);
         }
         
        return row;
    }
}
