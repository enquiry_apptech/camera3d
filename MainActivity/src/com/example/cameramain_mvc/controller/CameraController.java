package com.example.cameramain_mvc.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cameramain_mvc.CameraActivity;
import com.siulun.Camera3D.R;
import com.example.cameramain_mvc.model.CameraCanvasModel;
import com.example.cameramain_mvc.model.CameraModel;
import com.example.cameramain_mvc.view.CameraCanvasView;
import com.example.cameramain_mvc.view.CameraModeSpinnerAdapter;

public class CameraController implements OnClickListener, OnTouchListener,
		SensorEventListener, OnItemSelectedListener {

	// parent activity
	private Activity camActivity;

	// Model
	private CameraModel camModel;
	private CameraCanvasModel camCanvasModel;
	private Camera mCamera;
	

	// View - interactive components
	private LinearLayout llPreview, llOverlayImage;
	private SurfaceView svCameraPreview;
	private SurfaceHolder shCameraPreview;
	private CameraCanvasView cameraCanvas;
	private ImageView ivOverlayImage;

	private ImageView ivFlash, ivOverlay, ivCrop, ivInfo;
	private CameraModeSpinnerAdapter saMode;
	private Spinner sMode;

	private LinearLayout llBottomControl;
	private TextView tvFeedback;
	private ProgressBar pbCompletion;
	private ImageView ivTick, ivShoot, ivCross;

	// Sensor
    final float RAD2DEG = (float) (180 / Math.PI);
    private SensorManager mSensorManager;
    private float[] sensor_mags = new float[3];
    private float[] sensor_accels = new float[3];
    private float[] sensor_rotationMat = new float[9];
    private float[] sensor_inclinationMat = new float[9];
    private float[] sensor_attitude = new float[3];
    private float fInstantX, fInstantY, fInstantZ;

    int photo_count = 0;
    
	// touch event
	boolean bTwoFingers = false;
	float fDownDistance_X = 0;
	float fDownDistance_Y = 0;
	float fMoveDistance_X, fMoveDistance_Y;
	float iDelta_X, iDelta_Y;

	// for saving
	private BitmapFactory.Options bfOptions = new BitmapFactory.Options();
	static private Bitmap bmOriginal, bmNew, bmOverlayImage;

	// thread
	private BurstThread threadBurst;
	private DrawCanvasThread threadDrawCanvas;
	
	//motion
	private long previewTime;
	private long lastTime;
	Bitmap copyBitmap = null;
	Bitmap compareBitmap = null;


	public CameraController(Activity activity, final CameraModel camModel,
			CameraCanvasModel camCanvasModel) {
		this.camActivity = activity;
		this.camModel = camModel;
		this.camCanvasModel = camCanvasModel;

		mSensorManager = (SensorManager) camActivity
				.getSystemService(Context.SENSOR_SERVICE);

		checkDeviceConfig();

		initializeView();
		camModel.setFlashOn(false);
		camModel.setOverlayOn(false);
		sMode.setSelection(camModel.MODE_MANUAL);
		updateViewIcon();

		setSize();

		cleanBufferFolder();
	}

	// assume that the the default preview size and the screen sizes have
	// same ratio, then select the right photo size and adjust the shape of the
	// preview to fit the width of the screen and the height to be in scale
	private void setSize() {
		safeCameraOpen(); // for the access of Camera.Parameters
		Camera.Parameters camPara = mCamera.getParameters();
		DisplayMetrics theDisplayMetrics = camActivity.getResources()
				.getDisplayMetrics();
		int iPreviewScreenHeight; // px
		int iStatusBarHeight = 0; // px

		int resourceId = camActivity.getResources().getIdentifier(
				"status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			iStatusBarHeight = camActivity.getResources()
					.getDimensionPixelSize(resourceId);
		}

		// about the default preview
		camModel.setPreviewResolutionWidth(camPara.getPreviewSize().width);
		camModel.setPreviewResolutionHeight(camPara.getPreviewSize().height);
		camModel.setPreviewRatio((float) camModel.getPreviewResolutionWidth()
				/ camModel.getPreviewResolutionHeight());
		Log.d("Size",
				String.format("Preview: %dx%d (%.03f)",
						camModel.getPreviewResolutionWidth(),
						camModel.getPreviewResolutionHeight(),
						camModel.getPreviewRatio()));

		// about the screen
		camModel.setScreenDensity(theDisplayMetrics.density);
		camModel.setScreenWidth(theDisplayMetrics.widthPixels);
		camModel.setScreenHeight(theDisplayMetrics.heightPixels
				- iStatusBarHeight);
		Log.d("Size", String.format("Screen: %dx%d (%.03f)",
				camModel.getScreenWidth(), camModel.getScreenHeight(),
				(float) camModel.getScreenWidth() / camModel.getScreenHeight()));

		// record the width of preview on screen
		camModel.setPreviewScreenWidth(camModel.getScreenWidth());

		// calculate the height for preview when its width fits the screen
		iPreviewScreenHeight = (int) (camModel.getScreenWidth() * camModel
				.getPreviewRatio());
		camModel.setPreviewScreenHeight(iPreviewScreenHeight);
		Log.d("Size", "iPreviewHeightOnScreen: " + iPreviewScreenHeight);

		// set the shape of the preview so that it is in scale
		llPreview.setLayoutParams(new FrameLayout.LayoutParams(camModel
				.getScreenWidth(), iPreviewScreenHeight));

		// set bottom control height
		camModel.setBottomControlHeight(camModel.getScreenHeight()
				/ camModel.BOTTOM_CONTROL_WEIGHT);

		// fill space exits between preview and the bottom control area
		if (camModel.getPreviewScreenHeight()
				+ camModel.getBottomControlHeight() < camModel
					.getScreenHeight()) {
			camModel.setBottomControlHeight(camModel.getScreenHeight()
					- camModel.getPreviewScreenHeight());
		}
		Log.d("Size",
				"BottomControlHeight: " + camModel.getBottomControlHeight());

		// adjust the bottom control height
		ViewGroup.LayoutParams vglpBottomControl = llBottomControl
				.getLayoutParams();
		vglpBottomControl.height = camModel.getBottomControlHeight();
		llBottomControl.setLayoutParams(new LinearLayout.LayoutParams(
				vglpBottomControl));

		camCanvasModel.setCanvasVerticalOffset(camModel
				.getBottomControlHeight());

		//setCameraPictureSize();

		// set the shape of the overlay image so that it is in place
		llOverlayImage.setLayoutParams(new FrameLayout.LayoutParams(camModel
				.getScreenWidth(), camModel.getScreenHeight()
				- camModel.getBottomControlHeight()));
		Log.d("Size",
				"llOverlayImage: " + camModel.getScreenWidth() + "x"
						+ camModel.getScreenHeight() + "-"
						+ camModel.getBottomControlHeight());
		Log.d("Size", "llPreview: " + camModel.getScreenWidth() + "x"
				+ camModel.getPreviewScreenHeight());

	}

	private void initializeView() {
		// set camera preview as base of the view
	
		llPreview = new LinearLayout(camActivity.getBaseContext());
		svCameraPreview = new SurfaceView(camActivity.getBaseContext());
		llPreview.addView(svCameraPreview, new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		camActivity.setContentView(llPreview, new LayoutParams(400, 300));
		svCameraPreview.setZOrderMediaOverlay(false);
		shCameraPreview = svCameraPreview.getHolder();
		shCameraPreview.addCallback(new Callback() {

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				// empty
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				safeCameraOpen();
				startPreviewOnTheSurface();
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				stopPreviewAndFreeCamera();
			}
		});
		// required for Android <3.0
		shCameraPreview.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		// add the canvas
		cameraCanvas = new CameraCanvasView(camActivity.getBaseContext(),
				camModel.X_ACCEPTANCE, camModel.Z_ACCEPTANCE);
		camActivity.addContentView(cameraCanvas, new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		cameraCanvas.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		cameraCanvas.setZOrderMediaOverlay(true);
		cameraCanvas.getHolder().addCallback(new Callback() {
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				camCanvasModel.setCanvasWidth(width);
				camCanvasModel.setCanvasHeight(height);
				Log.d("Size", "Canvas: " + width + "x" + height);
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				// empty
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				// empty
			}
		});
		cameraCanvas.setOnTouchListener(this);

		// link up with the xml file
		LayoutInflater inflater = camActivity.getLayoutInflater();
		camActivity.getWindow().addContentView(
				inflater.inflate(R.layout.camera, null),
				new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT));

		llOverlayImage = (LinearLayout) camActivity
				.findViewById(R.id.camera_linearlayout_overlayImage);
		ivOverlayImage = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_overlayImage);

		ivFlash = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_flash);
		ivFlash.setOnClickListener(this);
		ivOverlay = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_overlay);
		ivOverlay.setOnClickListener(this);
		ivCrop = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_crop);
		ivCrop.setOnClickListener(this);
		ivInfo = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_info);
		ivInfo.setOnClickListener(this);
		
		saMode = new CameraModeSpinnerAdapter(camActivity, 0);
		sMode = (Spinner) camActivity.findViewById(R.id.camera_spinner_mode);
		sMode.setAdapter(saMode);
		sMode.setOnItemSelectedListener(this);

		llBottomControl = (LinearLayout) camActivity
				.findViewById(R.id.camera_linearlayout_bottomControl);

		tvFeedback = (TextView) camActivity
				.findViewById(R.id.camera_textview_feedback);
		pbCompletion = (ProgressBar) camActivity
				.findViewById(R.id.camera_progressbar_completion);

		ivTick = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_tick);
		ivTick.setOnClickListener(this);
		
		// close save photo button at first
		ivTick.setVisibility(View.INVISIBLE);
		llOverlayImage.setOnTouchListener(new android.view.View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 if(event.getAction() == MotionEvent.ACTION_DOWN){
					 //Toast.makeText(camActivity, "onTouch", Toast.LENGTH_SHORT).show();
				//mCamera.autoFocus(autoFocusCallback);
					 
					 float x = event.getX();
			            float y = event.getY();
			      
			            Rect touchRect = new Rect(
			                (int)(x - 100), 
			                (int)(y - 100), 
			                (int)(x + 100), 
			                (int)(y + 100));
			            

			            final Rect targetFocusRect = new Rect(
			                touchRect.left * 2000/svCameraPreview.getWidth() - 1000,
			                touchRect.top * 2000/svCameraPreview.getHeight() - 1000,
			                touchRect.right * 2000/svCameraPreview.getWidth() - 1000,
			                touchRect.bottom * 2000/svCameraPreview.getHeight() - 1000);
			      
			            doTouchFocus(targetFocusRect);
					 
				 }
				return false;
			}
		
		});
		/*svCameraPreview.setOnTouchListener(new android.view.View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
			    if(event.getAction() == MotionEvent.ACTION_DOWN){
		            float x = event.getX();
		            float y = event.getY();
		      
		            Rect touchRect = new Rect(
		                (int)(x - 100), 
		                (int)(y - 100), 
		                (int)(x + 100), 
		                (int)(y + 100));
		            

		            final Rect targetFocusRect = new Rect(
		                touchRect.left * 2000/svCameraPreview.getWidth() - 1000,
		                touchRect.top * 2000/svCameraPreview.getHeight() - 1000,
		                touchRect.right * 2000/svCameraPreview.getWidth() - 1000,
		                touchRect.bottom * 2000/svCameraPreview.getHeight() - 1000);
		      
		            doTouchFocus(targetFocusRect);
			    }
				return false;
			}
		});
		*/	
		
		// set animation for save photo button
		ivTick.setOnTouchListener(new android.view.View.OnTouchListener() {
			
		
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction()==MotionEvent.ACTION_DOWN){		
					Animation animation1 = AnimationUtils.loadAnimation(camActivity, R.anim.pop_out_cancel);
					ivTick.startAnimation(animation1);
					ivTick.setAlpha(50);
					return false;
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					ivTick.setAlpha(255);
					ivTick.clearAnimation();
					
				}
				return false;
			}
		});
		ivShoot = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_shoot);
        
		// set animation for take photo button
		ivShoot.setOnTouchListener(new android.view.View.OnTouchListener() {
			
		
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction()==MotionEvent.ACTION_DOWN){
					ivShoot.setImageResource(R.drawable.camera_shoot_on);
					Animation animation1 = AnimationUtils.loadAnimation(camActivity, R.anim.pop_out);
					ivShoot.startAnimation(animation1);
					ivShoot.setAlpha(50);
					return false;
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					ivShoot.setImageResource(R.drawable.camera_shoot_off);
					ivShoot.setAlpha(255);
					ivShoot.clearAnimation();
					
				}
				return false;
			}
		});
		
		
		ivShoot.setOnClickListener(this);
		ivCross = (ImageView) camActivity
				.findViewById(R.id.camera_imageview_cross);
		ivCross.setOnClickListener(this);
		ivCross.setVisibility(View.INVISIBLE);
		
		// set animation for cancel photo button
		ivCross.setOnTouchListener(new android.view.View.OnTouchListener() {
			
		
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				
				if(event.getAction()==MotionEvent.ACTION_DOWN){
		
					Animation animation1 = AnimationUtils.loadAnimation(camActivity, R.anim.pop_out_cancel);
					ivCross.startAnimation(animation1);
					ivCross.setAlpha(50);
					return false;
				}
				else if(event.getAction() == MotionEvent.ACTION_UP){
					ivCross.setAlpha(255);
					ivCross.clearAnimation();
					
				}
				return false;
			}
		});
	}
	
	private void updateViewIcon() {
		if (camModel.getFlashOn()) {
			ivFlash.setImageResource(R.drawable.camera_flash_on);
		} else {
			ivFlash.setImageResource(R.drawable.camera_flash_off);
		}

		if (camModel.getOverlayOn()) {
			ivOverlay.setImageResource(R.drawable.camera_overlay_on);
			ivOverlayImage.setVisibility(View.VISIBLE);
		} else {
			ivOverlay.setImageResource(R.drawable.camera_overlay_off);
			ivOverlayImage.setVisibility(View.INVISIBLE);
		} 

		if (camModel.getCropOn()) {
			ivCrop.setImageResource(R.drawable.camera_crop_on);
		} else {
			ivCrop.setImageResource(R.drawable.camera_crop_off);
		}
	}

	private void checkDeviceConfig() {
		try {
			boolean bHasCamera, bHasFlash, bHasAccSensor, bHasMagSensor;

			bHasCamera = safeCameraOpen();
			camModel.setHasCamera(bHasCamera);
			if (bHasCamera) {
				Log.d("Device", "Camera: YES");
			} else {
				Log.d("Device", "Camera: NO");
			}

			bHasFlash = hasFlash();
			camModel.setHasFlash(bHasFlash);
			if (bHasFlash) {
				Log.d("Device", "Flash: YES");
			} else {
				Log.d("Device", "Flash: NO");
			}

            bHasAccSensor = mSensorManager.registerListener(this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    CameraModel.SENSE_DELAY);
            camModel.setHasAccSensor(bHasAccSensor);
            if (bHasAccSensor) {
                Log.d("Device", "Acc Sensor: YES");
            } else {
                Log.d("Device", "Acc Sensor: NO");
            }

            bHasMagSensor = mSensorManager
                    .registerListener(this, mSensorManager
                                    .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                            CameraModel.SENSE_DELAY);
            camModel.setHasMagSensor(bHasMagSensor);
            if (bHasMagSensor) {
                Log.d("Device", "Mag Sensor: YES");
            } else {
                Log.d("Device", "Mag Sensor: NO");
            }

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stopSensor();
			stopPreviewAndFreeCamera();
		}

	}

	// ================== life cycle ==================
	public void onDestroy() {

	}

	public void onPause() {

		camModel.setbMotionStart(false);
		// draw canvas
		stopCanvasThread();

		// burst thread
		stopBurstThread();

        // sensor
        stopSensor();
	}

	public void onResume() {
		// draw canvas
		threadDrawCanvas = new DrawCanvasThread();
		threadDrawCanvas.setRun(true);
		threadDrawCanvas.start();

        if (camModel.getMode() == CameraModel.MODE_AUTO) {
            startSensor();
        }
	}

	// ================== life cycle ================== end
	//
	//
	//
	// =================== gesture ====================
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.camera_imageview_flash:

			// prevent turning off in progress of a photo set
			if (camModel.getCurrentPhotoIndex() != 0) {
				tvFeedback.setText("Can only turn on/off at first photo");
			} else {
				if (camModel.getHasFlash()) {
					camModel.setFlashOn(!camModel.getFlashOn());
					updateFlashWithMode();
				} else {
					tvFeedback.setText("Flash Not Available");
					camModel.setFlashOn(false);
				}

				if (camModel.getFlashOn()) {
					ivFlash.setImageResource(R.drawable.camera_flash_on);
				} else {
					ivFlash.setImageResource(R.drawable.camera_flash_off);
				}
			}
			break;
		case R.id.camera_imageview_overlay:
            // force on for auto mode
			camModel.setOverlayOn(!camModel.getOverlayOn());

			if (camModel.getOverlayOn()) {
				ivOverlayImage.setVisibility(View.VISIBLE);
				ivOverlay.setImageResource(R.drawable.camera_overlay_on);
			} else {
				ivOverlayImage.setVisibility(View.INVISIBLE);
				ivOverlay.setImageResource(R.drawable.camera_overlay_off);
			}

			break;
		case R.id.camera_imageview_crop:
			// force on for auto mode
			if (camModel.getMode() == camModel.MODE_AUTO) {
				tvFeedback.setText("Cannot turn off in this mode");
				return;
			}

			if (camModel.getCurrentPhotoIndex() == 0) {
				camModel.setCropOn(!camModel.getCropOn());
				if (camModel.getCropOn()) {
					ivCrop.setImageResource(R.drawable.camera_crop_on);
				} else {
					ivCrop.setImageResource(R.drawable.camera_crop_off);
				}
			} else { // prevent turning off in progress
				tvFeedback.setText("Can only turn on/off at first photo");
			}

			break;
		case R.id.camera_imageview_info:
			tvFeedback.setText("info");

			break;
        case R.id.camera_imageview_cross:
        	photo_count = 0;
        	ivTick.setVisibility(View.INVISIBLE);
    		ivCross.setVisibility(View.INVISIBLE);
                if (camModel.getMode() == camModel.MODE_BURST) {
                    stopBurstThread();
                } else if (camModel.getMode() == camModel.MODE_AUTO) {
                    pbCompletion.setProgress(0);
                    camModel.setAutoStarted(false);
                    camModel.setFirstInRangeTime(-1);
                }

                camModel.setbMotionStart(false);

                camModel.setCurrentPhotoIndex(0);

                tvFeedback.setText("Reset to ZERO");
                ivOverlayImage.setImageResource(android.R.color.transparent);
                ivShoot.setImageResource(R.drawable.camera_shoot_off);

                break;
		case R.id.camera_imageview_tick:
            camModel.setModePause(true);

			if (camModel.getMode() == camModel.MODE_BURST) {
				stopBurstThread();
			} else if (camModel.getMode() == camModel.MODE_AUTO) {

			}

			if (camModel.getCurrentPhotoIndex() != 0) {
				movePhotoToNewDir();
			}

			// START NEW PREVIEW ACTIVITY HERE (IF ANY)

			camActivity.finish();

			break;
		case R.id.camera_imageview_shoot:
			
			Animation animation1 = AnimationUtils.loadAnimation(camActivity, R.anim.blink);
			llPreview.startAnimation(animation1);
			//ivOverlayImage.startAnimation(animation1);
			//container.startAnimation(animation1);			
			cameraCanvas.startAnimation(animation1);
			
			photo_count++;
			
			if(photo_count>1){
				ivTick.setVisibility(View.VISIBLE);
			}			
			ivCross.setVisibility(View.VISIBLE);
			
			switch (camModel.getMode()) {
			case CameraModel.MODE_MANUAL:
				takePhoto();
				break;
			case CameraModel.MODE_BURST:
				if (threadBurst == null) {
					threadBurst = new BurstThread();
					threadBurst.setRun(true);
					threadBurst.start();
					camModel.setModePause(false);
				} else {
					stopBurstThread();
					camModel.setModePause(true);
				}

				break;
			case CameraModel.MODE_AUTO:
                if (!camModel.getAutoStarted()) {
                    camModel.setAutoStarted(true);
                    camModel.setModePause(false);
                } else {
                    camModel.setModePause(!camModel.getModePause());
                }

				break;
			case CameraModel.MODE_MOTION:
				if (!camModel.getbMotionStart()) {
					if (camModel.getIsFirstTime()) {
						Log.d("???","aaa");
						takePhoto();
						camModel.setIsFirstTime(false);
					}
					camModel.setbMotionStart(true);
					camModel.setModePause(false);
				} else {
					camModel.setbMotionStart(false);
					camModel.setModePause(true);
				}
				break;
			} // inner switch ends

			// update icon
			if (camModel.getModePause()) {
				ivShoot.setImageResource(R.drawable.camera_shoot_off);
			} else {
				ivShoot.setImageResource(R.drawable.camera_shoot_on);
			}

			break;
		}
	}
	
	
	public void Action_TakePhoto(){
		Animation animation1 = AnimationUtils.loadAnimation(camActivity, R.anim.blink);
		llPreview.startAnimation(animation1);
		//ivOverlayImage.startAnimation(animation1);
		//container.startAnimation(animation1);			
		cameraCanvas.startAnimation(animation1);
		
		photo_count++;
		
		if(photo_count>1){
			ivTick.setVisibility(View.VISIBLE);
		}			
		ivCross.setVisibility(View.VISIBLE);
		
		switch (camModel.getMode()) {
		case CameraModel.MODE_MANUAL:
			takePhoto();
			break;
		case CameraModel.MODE_BURST:
			if (threadBurst == null) {
				threadBurst = new BurstThread();
				threadBurst.setRun(true);
				threadBurst.start();
				camModel.setModePause(false);
			} else {
				stopBurstThread();
				camModel.setModePause(true);
			}
		}
	}
	
	
	
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long id) {
		camModel.setbMotionStart(false);
		
		camModel.setMode(position);
		updateFlashWithMode();
		camModel.setModePause(true);
        ivShoot.setImageResource(R.drawable.camera_shoot_off);

		camModel.setCurrentPhotoIndex(0);
		tvFeedback.setText("");
		ivOverlayImage.setImageResource(android.R.color.transparent);
        pbCompletion.setProgress(0);

		if (camModel.getMode() != CameraModel.MODE_BURST) {
			stopBurstThread();
		}

        if (camModel.getMode() != CameraModel.MODE_AUTO) {
            stopSensor();
        }
		
		Camera.Parameters camPara = mCamera.getParameters();
		switch (camModel.getMode()) {
		case CameraModel.MODE_MANUAL:
			camModel.setCropOn(false);
			ivCrop.setImageResource(R.drawable.camera_crop_off);

			break;
		case CameraModel.MODE_BURST: 
			camModel.setCropOn(false);
			ivCrop.setImageResource(R.drawable.camera_crop_off); 

			break;
		case CameraModel.MODE_AUTO:
            if (camModel.getHasSensor()) {
                startSensor();

                // force on cropping
                camModel.setCropOn(true);
                ivCrop.setImageResource(R.drawable.camera_crop_on);

                camModel.setAutoStarted(false);
                camModel.setFirstInRangeTime(-1);
            } else {
                sMode.setSelection(CameraModel.MODE_MANUAL);
                tvFeedback.setText("No Sensor Available");
            }

			break;
		case CameraModel.MODE_MOTION:
			camModel.setbMotionStart(false);
			camModel.setIsFirstTime(true);
			camModel.setCropOn(false);
			ivCrop.setImageResource(R.drawable.camera_crop_off);
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// empty
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// check if cropping is on
		if (!camModel.getCropOn()) {
			return true;
		}

		// check if it is the first photo
		if (camModel.getCurrentPhotoIndex() != 0) {
			tvFeedback.setText("Can only change size at first photo");
			return true;
		}

		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			bTwoFingers = true;

			fDownDistance_X = Math.abs(event.getX(0) - event.getX(1));
			fDownDistance_Y = Math.abs(event.getY(0) - event.getY(1));

			break;
		case MotionEvent.ACTION_MOVE:

			if (bTwoFingers) {
				fMoveDistance_X = Math.abs(event.getX(0) - event.getX(1));
				fMoveDistance_Y = Math.abs(event.getY(0) - event.getY(1));

				iDelta_X = (int) (fMoveDistance_X - fDownDistance_X);
				iDelta_Y = (int) (fMoveDistance_Y - fDownDistance_Y);

				// update the down distance
				fDownDistance_X = fMoveDistance_X;
				fDownDistance_Y = fMoveDistance_Y;

				camCanvasModel.setCropHalfWidth((int) (iDelta_X / 2)
						+ camCanvasModel.getCropHalfWidth());
				camCanvasModel.setCropHalfHeight((int) (iDelta_Y / 2)
						+ camCanvasModel.getCropHalfHeight());
			}

			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
			bTwoFingers = false;
			break;
		}

		return true;
	}

	// =================== gesture ==================== end
	//
	//
	//
    // ==================== sensor ====================
    private void startSensor() {
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                CameraModel.SENSE_DELAY);
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                CameraModel.SENSE_DELAY);
    }

    private void stopSensor() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int type = event.sensor.getType();

        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                sensor_accels = event.values.clone();
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                sensor_mags = event.values.clone();
                break;
        }

        SensorManager.getRotationMatrix(sensor_rotationMat,
                sensor_inclinationMat, sensor_accels, sensor_mags);
        SensorManager.getOrientation(sensor_rotationMat, sensor_attitude);

        fInstantZ = sensor_attitude[0] * RAD2DEG;
        fInstantX = sensor_attitude[1] * RAD2DEG;
        fInstantY = sensor_attitude[2] * RAD2DEG;

//        Log.d("sensor", "raw: " + fInstantX + ", " + fInstantY + ", " +
//        fInstantZ);

        if (camModel.getCurrentPhotoIndex() >= camModel.SAMPLE_TARGET) {

            stopSensor();
            movePhotoToNewDir();

            // START PREVIEW ACTIVITY HERE
            camActivity.finish();

            return;
        }

        if (!camModel.getAutoStarted()) {
            camModel.setStartXYZ(fInstantX, fInstantY, fInstantZ);

        }

        // input the average instead
        int iOffSet_X = (camModel.getAutoStarted()) ? (int) (fInstantX - camModel
                .getAutoStartX()) : 0;
        int iOffSet_Z = (camModel.getAutoStarted()) ? (int) (fInstantZ - camModel
                .getAutoTargetZ()) : 0;
        int iOffSet_Y = (int) (fInstantY - 0);

        if (iOffSet_Z > 180) {
            iOffSet_Z -= 360;
        } else if (iOffSet_Z < -180) {
            iOffSet_Z += 360;
        }

        camCanvasModel.pushXYZ(iOffSet_X, iOffSet_Y, iOffSet_Z);

        if (!camModel.getModePause()) {
            if (camModel.getAutoStarted()) {
                if (inRange()) {
                    takePhoto();
                    camCanvasModel.clearXYZ();
                }
            }
        }
    }

    private boolean inRange() {
        boolean bValidX, bValidZ;
        // boolean bValidY; // deprecated, no longer checking Y

        // prevent double photo taking due to concurrency issue
        if (!camModel.getTakePhotoReady()) {
            return false;
        }

        Log.d("auto dif check", "X: " + camCanvasModel.getX());
        if (camCanvasModel.getX() < -CameraModel.X_ACCEPTANCE
                || camCanvasModel.getX() > CameraModel.X_ACCEPTANCE) {
            bValidX = false;
        } else {
            bValidX = true;
        }

        Log.d("auto dif check", "Z: " + camCanvasModel.getZ());
        if (camCanvasModel.getZ() < -CameraModel.Z_ACCEPTANCE
                || camCanvasModel.getZ() > CameraModel.Z_ACCEPTANCE) {
            bValidZ = false;
        } else {
            bValidZ = true;
        }

        // update the UI
        if (bValidX && bValidZ) {

            if (camModel.getFirstInRangeTime() == -1) {
                camModel.setFirstInRangeTime(System.currentTimeMillis());
            } else {
                if (System.currentTimeMillis() - camModel.getFirstInRangeTime() > CameraModel.REQUIRE_IN_RANGE_TIME) {
                    Log.d("takephoto", camModel.getFirstInRangeTime() + " - "
                            + System.currentTimeMillis());
                    Log.d("takephoto", "X:" + camCanvasModel.getZ() + ", Z: "
                            + camCanvasModel.getZ());

                    camModel.setFirstInRangeTime(-1);
                    return true;
                }
            }
            return false;
        } else {
            camModel.setFirstInRangeTime(-1);
            return false;
        }
    }

    // ==================== sensor ==================== end
	//
	//
	//
	// ==================== camera ====================
	private boolean safeCameraOpen() {
		boolean qOpened = false;
		try {
			stopPreviewAndFreeCamera();
			mCamera = Camera.open();
			mCamera.setPreviewCallback(previewCallback);
			qOpened = (mCamera != null);
		} catch (Exception e) {
			e.printStackTrace();

			AlertDialog alertDialog = new AlertDialog.Builder(camActivity)
					.setTitle("No Camera Available!")
					.setNegativeButton("Exit",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									camActivity.finish();
								}
							}).create();
			alertDialog.show();
		}
		return qOpened;
	}

	// VERY IMPORTANT, if not released, camera of the device is stuck
	private void stopPreviewAndFreeCamera() {
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	private void setCameraPictureSize(Camera.Parameters camPara) { // !!! assume camera is opened!!!

		//Camera.Parameters camPara = mCamera.getParameters();
		//List<Size> picSizes = camPara.getSupportedPictureSizes();
		
		List<Size> picSizes = camPara.getSupportedPreviewSizes();

		Size sizePicBest;

		int iResult = -1;
		int iCurrentWidth, iCurrentHeight;
		float fCurrentRatio;
		int iCurrentAbsoluteDifference;
		int iBestAbsoluteDifference = Integer.MAX_VALUE;

		int iPreWidth = mCamera.getParameters().getPreviewSize().width;
		int iPreHeight = mCamera.getParameters().getPreviewSize().height;
		
		Log.d("PreviewSize", "width: "+iPreWidth+" height: "+iPreHeight);
		float fPreviewRatio = (float) iPreWidth / iPreHeight;

		// find the best picture size
		for (int i = 0; i < picSizes.size(); i++) {
			iCurrentWidth = picSizes.get(i).width;
			iCurrentHeight = picSizes.get(i).height;
			fCurrentRatio = (float) iCurrentWidth / iCurrentHeight;

			if (iCurrentWidth >= camModel.TARGET_PICTURE_WIDTH
					&& iCurrentHeight >= camModel.TARGET_PICTURE_HEIGHT) {
				if (fCurrentRatio == fPreviewRatio) {
					iCurrentAbsoluteDifference = (iCurrentWidth - camModel.TARGET_PICTURE_WIDTH)
							+ (iCurrentHeight - camModel.TARGET_PICTURE_HEIGHT);

					// update to a better match
					if (iCurrentAbsoluteDifference < iBestAbsoluteDifference) {
						iBestAbsoluteDifference = iCurrentAbsoluteDifference;
						iResult = i;
					}
				}
			}
		}

		if (iResult == -1) {// no matching found
			return;
		} else {
			sizePicBest = picSizes.get(iResult);
		}
 
		// size of picture
		camPara.setPictureSize(sizePicBest.width, sizePicBest.height);
		Log.d("PictureSize: height:", camPara.getPictureSize().height+" width:"+camPara.getPictureSize().width);
		// submit change
		mCamera.setParameters(camPara);
		Log.d("end set picture", "end set para");
	}

	private void startPreviewOnTheSurface() {
		try {
			

			// auto-focus
			Camera.Parameters camPara = mCamera.getParameters();
			
			/* List<Integer> formats = camPara.getSupportedPictureFormats();
		        if (formats.contains(PixelFormat.RGB_565))
		        	camPara.setPictureFormat(PixelFormat.RGB_565);
		        else
		        	camPara.setPictureFormat(PixelFormat.JPEG);

		        // Choose the biggest picture size supported by the hardware
		        List<Size> sizes = camPara.getSupportedPictureSizes();
		        Camera.Size size = sizes.get(sizes.size()-2);
		        
		        Log.d("SupoortedPictureSize: height:", size.height+" width:"+size.width);
		       
			*/
			//camPara.setPictureSize(1920, 1088);
			setCameraPictureSize(camPara);
			Log.d("PictureSize: height:", camPara.getPictureSize().height+" width:"+camPara.getPictureSize().width);
			//Log.d("PreviewPictureSize: height:", camPara.getPreviewSize().height+" width:"+camPara.getPreviewSize().width);
            List<?> focus = camPara.getSupportedFocusModes();
//			camPara.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            if (focus != null
                    && focus.contains(android.hardware.Camera.Parameters.FOCUS_MODE_AUTO)) {
            	camPara.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            List<?> whiteMode = camPara.getSupportedWhiteBalance();
            if (whiteMode != null
                    && whiteMode
                            .contains(android.hardware.Camera.Parameters.WHITE_BALANCE_AUTO)) {
            	camPara.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
            }

            //camPara.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
           
			mCamera.setParameters(camPara);

			updateFlashWithMode();

			mCamera.setPreviewDisplay(shCameraPreview);

			mCamera.setDisplayOrientation(90); // rotate the preview
			mCamera.startPreview();
			mCamera.autoFocus(autoFocusCallback);
			camModel.setTakePhotoReady(true);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	 private AutoFocusCallback autoFocusCallback = new AutoFocusCallback() {

	        @Override
	        public void onAutoFocus(boolean autoFocusSuccess, Camera arg1) {
	        }
	    };

	 /* */  public void doTouchFocus(final Rect tfocusRect) {
	         try {
	             List<Camera.Area> focusList = new ArrayList<Camera.Area>();
	             Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
	             focusList.add(focusArea);
	       
	             Camera.Parameters param = mCamera.getParameters();
	             param.setFocusAreas(focusList);
	             param.setMeteringAreas(focusList);
	             mCamera.setParameters(param);
	       
	             mCamera.autoFocus(autoFocusCallback);
	         } catch (Exception e) {
	             e.printStackTrace();
	           //  Log.i(TAG, "Unable to autofocus");
	         }
	     }

	 
	    
	// ==================== camera ==================== end
	//
	//
	//
	//
	//
	// ================== Save Photo ==================
	private void takePhoto() {
		if (camModel.getTakePhotoReady()) {
			camModel.setTakePhotoReady(false);
			mCamera.takePicture(shutterCallback, null, mPicture);
		}
	}

	private Camera.ShutterCallback shutterCallback = new ShutterCallback() {
		@Override
		public void onShutter() {

		}
	};

	private PictureCallback mPicture = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {

			Matrix matrix = new Matrix();
			int iNewWidth;

			int iStartCropX, iStartCropY; // where to start cropping
			int iCroppedWidth, iCroppedHeight; // crop for how many px

			File pictureFile = getOutputMediaFile();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			FileOutputStream fos;

			try {
				fos = new FileOutputStream(pictureFile);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				Log.e("Save Photo", "File retrieval failed");
				return;
			}
			Log.d("Save Photo", "Getting output media file");

			try {
				// inPurgeable is used to free up memory while required
				bfOptions.inPurgeable = true;
				bfOptions.inJustDecodeBounds = true;

				// make data stream into bitmap
				bmOriginal = BitmapFactory
						.decodeByteArray(data, 0, data.length);
				Log.d("Preview Photo", "width: " + mCamera.getParameters().getPictureSize().width
						+ "x" + mCamera.getParameters().getPictureSize().height);
				

				Log.d("Save Photo", "bmOriginal: " + bmOriginal.getWidth()
						+ "x" + bmOriginal.getHeight()+"data length"+ data.length);

				// prepare matrix to rotate the bitmap
				matrix.postRotate(90);

				// apply the matrix to get a new bitmap
				bmNew = Bitmap.createBitmap(bmOriginal, 0, 0,
						bmOriginal.getWidth(), bmOriginal.getHeight(), matrix,
						true);

				Log.d("Save Photo",
						"bmNew: " + bmNew.getWidth() + "x" + bmNew.getHeight());

				// cropping
				if (camModel.getCropOn()) {
					iStartCropX = bmNew.getWidth()
							/ 2
							* (camModel.getPreviewScreenWidth() - 2 * camCanvasModel
									.getCropHalfWidth())
							/ camModel.getPreviewScreenWidth();
					
					iStartCropY = bmNew.getHeight()
							* (camCanvasModel.getCropMaxHeight() / 2 - camCanvasModel
									.getCropHalfHeight())
							/ camModel.getPreviewScreenHeight();
					
					iCroppedWidth = bmNew.getWidth() * 2
							* camCanvasModel.getCropHalfWidth()
							/ camModel.getPreviewScreenWidth();
					iCroppedHeight = bmNew.getHeight() * 2
							* camCanvasModel.getCropHalfHeight()
							/ camModel.getPreviewScreenHeight();
				} else {
					iStartCropX = 0;
					iStartCropY = 0;
					iCroppedWidth = bmNew.getWidth();
					iCroppedHeight = bmNew.getHeight()
							* camCanvasModel.getCropMaxHeight()
							/ camModel.getPreviewScreenHeight();
				}

			/*	Log.d("Save Photo", String
						.format("Crop: %dx%d w=%d h=%d", iStartCropX,
								iStartCropY, iCroppedWidth, iCroppedHeight));
*/
				bmNew = Bitmap.createBitmap(bmNew, iStartCropX, iStartCropY,
						iCroppedWidth, iCroppedHeight);
					/*	*/
/**/
				// resize if too big (on scale)
				if (bmNew.getHeight() > camModel.SAVE_TARGET_HEIGHT) {
					iNewWidth = (int) (((float) bmNew.getWidth() / (float) bmNew
							.getHeight()) * camModel.SAVE_TARGET_HEIGHT);
					if (iNewWidth % 2 != 0) // ensure is even number
						iNewWidth += 1;

					bmNew = Bitmap.createScaledBitmap(bmNew, iNewWidth,
							camModel.SAVE_TARGET_HEIGHT, true);
				}/**/

				// create the ghost bitmap
				bmOverlayImage = bmNew.copy(bmNew.getConfig(), true);
				if (camModel.getCropOn()) {
					bmOverlayImage = Bitmap.createScaledBitmap(bmNew,
							camCanvasModel.getCropHalfWidth() * 2,
							camCanvasModel.getCropHalfHeight() * 2, true);
				} else {
					bmOverlayImage = Bitmap.createScaledBitmap(bmNew,
							camCanvasModel.getCanvasWidth(),
							camCanvasModel.getCropMaxHeight(), true);
				}
				ivOverlayImage.setImageBitmap(bmOverlayImage);

				Log.d("Save Photo",
						"OverlayImageArea: " + llOverlayImage.getWidth() + "x"
								+ llOverlayImage.getHeight());

				// create jpeg file from the bitmap
				bmNew.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				
				// write stream to file
				baos.writeTo(fos);
    
				
				// room clearance //IMPORTANT
				clearBitmap();
				baos.flush();
				fos.flush();
				baos.close();
				fos.close();
				
				Log.d("Save Photo", "just wrote the file");
			} catch (FileNotFoundException e) {
				Log.e("Save Photo", e.getMessage());
			} catch (IOException e) {
				Log.e("Save Photo", e.getMessage());
			}

			// continue to preview
			startPreviewOnTheSurface();
		}
	};

	
	
	private File getAppRootFile() {
		// File appRootFile = new File(
		// Environment
		// .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		// ROOT_FOLDER_NAME);
		File appRootFile = new File(Environment.getExternalStorageDirectory(),
				camModel.ROOT_FOLDER_NAME);
		if (!appRootFile.exists()) {
			if (!appRootFile.mkdir()) {
				Log.d("Save Photo", "Required storage does not exist");
				return null;
			}
		}

		return appRootFile;
	}

	// prepare an empty file for output
	private File getOutputMediaFile() {
		File mediaStorageDir = new File(getAppRootFile(),
				camModel.BUFFER_FOLDER_NAME);

		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdir()) {
				Log.d("Save Photo", "Required media storage does not exist");
				return null;
			}
		}

		String fullPath = mediaStorageDir.getPath() + File.separator
				+ String.format("%03d.jpg", camModel.getCurrentPhotoIndex());

		// update UI
		camModel.setCurrentPhotoIndex(camModel.getCurrentPhotoIndex() + 1);
		// tvProgress.setText(String.format("%03d.jpg", iCurrentTargetIndex));

		if (camModel.getMode() == camModel.MODE_AUTO) {
			pbCompletion.setProgress(100 * camModel.getCurrentPhotoIndex()
					/ camModel.SAMPLE_TARGET - 1);
			tvFeedback.setText(100 * camModel.getCurrentPhotoIndex()
					/ camModel.SAMPLE_TARGET - 1 + "% Complete");
		} else {
			tvFeedback.setText(String.format("Photo Taken: %d",
					camModel.getCurrentPhotoIndex()));
		}

		File mediaFile;
		mediaFile = new File(fullPath);

		Log.d("Save Photo", "jpg saved in " + fullPath);

		return mediaFile;
	}

	private void clearBitmap() {
		if (bmOriginal != null) {
			bmOriginal.recycle();
		}

		if (bmNew != null) {
			bmNew.recycle();
		}
		System.gc();
	}

	
	private void cleanBufferFolder() {
		File mediaStorageDir = new File(getAppRootFile(),
				camModel.BUFFER_FOLDER_NAME);
		if (mediaStorageDir.exists()) {
			String[] children = mediaStorageDir.list();
			for (int i = 0; i < children.length; i++) {
				new File(mediaStorageDir, children[i]).delete();
			}
			Log.d("Save Photo", "Remove everything in buffer folder");
		}
	}

	private void movePhotoToNewDir() {
		String strSaveTime = String.format("%d", System.currentTimeMillis());

		File sourceLocation = new File(getAppRootFile(),
				camModel.BUFFER_FOLDER_NAME);

		File targetLocation = new File(getAppRootFile().getAbsolutePath()
				+ File.separator + camModel.GALLERY_FOLDER_NAME
				+ File.separator + strSaveTime);

		try {
			copyDirectory(sourceLocation, targetLocation);
			cleanBufferFolder();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// If targetLocation does not exist, it will be created.
	public void copyDirectory(File sourceLocation, File targetLocation)
			throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists() && !targetLocation.mkdirs()) {
				throw new IOException("Cannot create dir "
						+ targetLocation.getAbsolutePath());
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(
						targetLocation, children[i]));
			}
		} else {

			// make sure the directory we plan to store the recording in exists
			File directory = targetLocation.getParentFile();
			if (directory != null && !directory.exists() && !directory.mkdirs()) {
				throw new IOException("Cannot create dir "
						+ directory.getAbsolutePath());
			}

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	// ================== Save Photo ============== end
	//
	//
	//
	//
	// ==================== flash =====================

	
	public boolean hasFlash() {
		if (mCamera == null) {
			return false;
		}

		Camera.Parameters parameters = mCamera.getParameters();

		if (parameters.getFlashMode() == null) {
			return false;
		}

		List<String> supportedFlashModes = parameters.getSupportedFlashModes();
		if (supportedFlashModes == null
				|| supportedFlashModes.isEmpty()
				|| supportedFlashModes.size() == 1
				&& supportedFlashModes.get(0).equals(
						Camera.Parameters.FLASH_MODE_OFF)) {
			return false;
		}
		return true;
	}

	private void updateFlashWithMode() {
		Camera.Parameters camPara = mCamera.getParameters();

		if (camModel.getFlashOn()) {
			switch (camModel.getMode()) {
			case CameraModel.MODE_MANUAL:
			case CameraModel.MODE_BURST:
			case CameraModel.MODE_AUTO:
				try {
					camPara.setFlashMode(camPara.FLASH_MODE_ON);
					mCamera.setParameters(camPara);
				} catch (Exception e) {
					e.printStackTrace();
					tvFeedback.setText("Can't turn on flash");
					ivFlash.setImageResource(R.drawable.camera_flash_off);
					camModel.setFlashOn(false);
				}
				break;
			case CameraModel.MODE_MOTION:
			case CameraModel.MODE_VIDEO:
				try {
					camPara.setFlashMode(camPara.FLASH_MODE_TORCH);
					mCamera.setParameters(camPara);
				} catch (Exception e) {
					e.printStackTrace();
					tvFeedback.setText("Can't turn on flash");
					ivFlash.setImageResource(R.drawable.camera_flash_off);
					camModel.setFlashOn(false);
				}
				break;
			}
		} else {
			try {
				camPara.setFlashMode(camPara.FLASH_MODE_OFF);
				mCamera.setParameters(camPara);
			} catch (Exception e) {
				e.printStackTrace();
				tvFeedback.setText("Can't turn off flash");
				ivFlash.setImageResource(R.drawable.camera_flash_on);
				camModel.setFlashOn(true);
			}
		}
	}

	// ==================== flash ===================== end
	//
	//
	//
	//
	// ================== burst thread ================
	private class BurstThread extends Thread {
		final int INTERVAL = 500; // ms

		private boolean bRun = true;
		private long lLastBurstTime = 0;
		private long lDeltaTime = -1;

		public BurstThread() {
			setName("BurstThread");
		}

		public void setRun(boolean b) {
			bRun = b;
		}

		public void run() {
			while (bRun) {

				if (!camModel.getTakePhotoReady()) {
					try {
						Thread.sleep(INTERVAL);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					lDeltaTime = System.currentTimeMillis() - lLastBurstTime;
					takePhoto();
					try {
						Thread.sleep(INTERVAL);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} // while - end

		}
	} // private class end

	private void stopBurstThread() {
		if (threadBurst != null) {
			threadBurst.setRun(false);
		}
		while (true) {
			try {
				if (threadBurst != null)
					threadBurst.join();
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		threadBurst = null;
	}

	// ================== burst thread ================ end
	//
	//
	//
	//
	// ================= canvas thread ================
	private class DrawCanvasThread extends Thread {
		final int INITIAL_WAIT_TIME = 1500; // ms
		private boolean bRun = true;

		public DrawCanvasThread() {
			setName("DrawCanvasThread");
		}

		public void setRun(boolean b) {
			bRun = b;
		}

		public void run() {

			// wait for a period of time preventing exhaustion
			try {
				Thread.sleep(INITIAL_WAIT_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			while (bRun) {
				try {
					if (cameraCanvas != null) {
						if (camModel.getMode() == camModel.MODE_AUTO) {
							cameraCanvas.draw((int) camCanvasModel.getX(),
									(int) camCanvasModel.getY(),
									(int) camCanvasModel.getZ(),
									camCanvasModel.getCropHalfWidth(),
									camCanvasModel.getCropHalfHeight(),
									camCanvasModel.getCanvasVerticalOffset());
						} else {
							if (camModel.getCropOn()) {
								cameraCanvas.draw(camCanvasModel
										.getCropHalfWidth(), camCanvasModel
										.getCropHalfHeight(), camCanvasModel
										.getCanvasVerticalOffset());
							} else {
								cameraCanvas.draw(cameraCanvas.getWidth() / 2,
										cameraCanvas.getHeight() / 2,
										camCanvasModel
												.getCanvasVerticalOffset());
							}
						}
					}
//					Log.d("canvas", camModel.getMode() + ": "
//							+ (int) camCanvasModel.getX() + ", "
//							+ (int) camCanvasModel.getY() + ", "
//							+ (int) camCanvasModel.getZ() + ", "
//							+ camCanvasModel.getCropHalfWidth() + ", "
//							+ camCanvasModel.getCropHalfHeight() + ", "
//							+ camCanvasModel.getCanvasVerticalOffset());
					sleep(150);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

	} // private class end

	private void stopCanvasThread() {
		if (threadDrawCanvas != null) {
			threadDrawCanvas.setRun(false);
		}
		while (true) {
			try {
				if (threadDrawCanvas != null)
					threadDrawCanvas.join();
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		threadDrawCanvas = null;
	}
	// ================= canvas thread ================ end
	
	// motion detection 
	// =============== Camera preview ==============
		Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
			public void onPreviewFrame(byte[] data, Camera camera) {
			
				if (camModel.getbMotionStart()) { 
					
					int width = camModel.getPreviewResolutionWidth();
					int height = camModel.getPreviewResolutionHeight();
				
					// convert preview data to RGB
					final int[] rgb = decodeYUV420SP(data, width, height);
					// convert RGB to bitmap
					Bitmap bmp = Bitmap.createBitmap(rgb, width, height,
							Bitmap.Config.ARGB_8888);
					Bitmap bmpNew = null;
					if (camModel.getMode() == CameraModel.MODE_MOTION) {
						// compress bitmap to smaller size
						bmpNew = Bitmap.createScaledBitmap(bmp, (int)(width / 3.5),
								(int)(height / 3.5), true); 
					}
					
					Log.d("see","see"+width/3.5 + " "+(int)(width/3.5));
					
					// first photo
					if (camModel.getIndex() < 2) {
						Log.d("jhjghjgjhg", "range");
						camModel.setIndex(camModel.getIndex()+1);
						lastTime = System.currentTimeMillis();
						previewTime = System.currentTimeMillis();
//						camModel.setCopyBitmap(bmpNew.copy(bmpNew.getConfig(), true));
//						camModel.setCompareBitmap(bmpNew.copy(bmpNew.getConfig(), true));
						copyBitmap = bmpNew.copy(bmpNew.getConfig(), true);
						compareBitmap = bmpNew.copy(bmpNew.getConfig(), true);

					}
					// after taken first photo 
					if (camModel.getIndex() >= 2) {
						
							long compareTime = System.currentTimeMillis();
							// ensure the checking not too fast
							if ((compareTime - previewTime) > 50) {
							previewTime = compareTime;
							// compare the preview with previous preview continuously
//							boolean range = comparePic(camModel.getCompareBitmap(), bmpNew,
//									"update");
							boolean range = comparePic(compareBitmap, bmpNew,
									"update");

							Log.d("ha", "range compare  " + range + camModel.getCompareCount());
							// ensure the preview is not changing
							switch (camModel.getCompareCount()) {
							case 0:
								if (range) {
//									camModel.setCompareBitmap(bmpNew.copy(bmpNew.getConfig(),
//											true));
									compareBitmap = bmpNew.copy(bmpNew.getConfig(),
											true);

									camModel.setCompareCount(camModel.getCompareCount()+1);
								} else {
//									camModel.setCompareBitmap(bmpNew.copy(bmpNew.getConfig(),
//											true));

									compareBitmap = bmpNew.copy(bmpNew.getConfig(),
											true);
									camModel.setCompareCount(0);
									camModel.setIsFirstChange(false);
								}
								break;
							case 1:
								if (range) {
//									camModel.setCompareBitmap(bmpNew.copy(bmpNew.getConfig(),
//											true));
									compareBitmap = bmpNew.copy(bmpNew.getConfig(),
											true);

									camModel.setCompareCount(camModel.getCompareCount()+1);
								} else {
//									camModel.setCompareBitmap(bmpNew.copy(bmpNew.getConfig(),
//											true));
									compareBitmap = bmpNew.copy(bmpNew.getConfig(),
											true);
									camModel.setCompareCount(0);
									camModel.setIsFirstChange(false);
								}
								break; 

							}
						}
						
						if (camModel.getMode() == CameraModel.MODE_MOTION) {
							// ensure the checking not too fast
							if ((compareTime - lastTime) > 50) {
								// compare preview with last taken photo
//								boolean rangeComp = comparePic(camModel.getCopyBitmap(), bmpNew,
//										"original");
								boolean rangeComp = comparePic(copyBitmap, bmpNew,
										"original");

								if (camModel.getIsFirstChange() == false) {
									camModel.setCompareCount(0);
								}
								if (!rangeComp) {
									camModel.setIsFirstChange(true);
								}
								lastTime = compareTime;
								Log.d("onno", "range? " + rangeComp );
								// if it fulfills both comparisons (preview with previous previews and last taken photo),
								// it takes photo
								if (!rangeComp && camModel.getCompareCount() == 2 && camModel.getbMotionStart()) {
//									camModel.setCopyBitmap(bmpNew.copy(bmpNew.getConfig(),
//											true));
									copyBitmap = bmpNew.copy(bmpNew.getConfig(),
											true);
									Log.d("aaa","aaabbb");
									takePhoto();
									lastTime += 3000;
									previewTime += 3000;
									camModel.setCompareCount(0);
									camModel.setIsFirstChange(false);
								}

								else if (camModel.getCompareCount() == 2) {
									camModel.setCompareCount(0);
								}
							}
						} 

					}

					
				}
				
			}

		};

		// =============== Camera preview ==============ends
	
	
	// =============== Convert to RGB ==============
		public int[] decodeYUV420SP(byte[] yuv420sp, int width, int height) {

			final int frameSize = width * height;

			int rgb[] = new int[width * height];
			for (int j = 0, yp = 0; j < height; j++) {
				int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
				for (int i = 0; i < width; i++, yp++) {
					int y = (0xff & ((int) yuv420sp[yp])) - 16;
					if (y < 0)
						y = 0;
					if ((i & 1) == 0) {
						v = (0xff & yuv420sp[uvp++]) - 128;
						u = (0xff & yuv420sp[uvp++]) - 128;
					}

					int y1192 = 1192 * y;
					int r = (y1192 + 1634 * v);
					int g = (y1192 - 833 * v - 400 * u);
					int b = (y1192 + 2066 * u);

					if (r < 0)
						r = 0;
					else if (r > 262143)
						r = 262143;
					if (g < 0)
						g = 0;
					else if (g > 262143)
						g = 262143;
					if (b < 0)
						b = 0;
					else if (b > 262143)
						b = 262143;

					rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000)
							| ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);

				}
			}
			return rgb;
		}
		// =============== Convert to RGB ==============ends
		
		// =============== Compare bitmap ==============
		public boolean comparePic(Bitmap oldBm, Bitmap newBm, String type) {

			double error = 0;
			int accurateColor = 0;
			double accuratePercent = 0;
			if (camModel.getMode() == CameraModel.MODE_MOTION) {
				accurateColor = 130;
				accuratePercent = 0.0002;
			}
			

			boolean inRange = true;
			int oldWidth = oldBm.getWidth();
			int oldHeight = oldBm.getHeight();
			int newWidth = newBm.getWidth();
			int newHeight = newBm.getHeight();

			Log.d("old", "old  " + oldWidth + " " + oldHeight);
			Log.d("new", "new  " + newWidth + " " + newHeight);

			if (oldWidth == newWidth && oldHeight == newHeight) {
				int[] oldPixel = new int[oldWidth * oldHeight];
				int[] newPixel = new int[newWidth * newHeight];

				oldBm.getPixels(oldPixel, 0, oldWidth, 0, 0, oldWidth, oldHeight);
				newBm.getPixels(newPixel, 0, newWidth, 0, 0, newWidth, newHeight);

				for (int i = 0; i < oldWidth * oldHeight; i++) {

					int oldCurrentR = Color.red(oldPixel[i]);
					int oldCurrentG = Color.green(oldPixel[i]);
					int oldCurrentB = Color.blue(oldPixel[i]);

					int newCurrentR = Color.red(newPixel[i]);
					int newCurrentG = Color.green(newPixel[i]);
					int newCurrentB = Color.blue(newPixel[i]);

					if (Math.abs(oldCurrentR - newCurrentR) > accurateColor) {
						error++;
					} else if (Math.abs(oldCurrentG - newCurrentG) > accurateColor) {
						error++;
					} else if (Math.abs(oldCurrentB - newCurrentB) > accurateColor) {
						error++;
					}

				}
				if ((error / (oldWidth * oldHeight)) > accuratePercent) {
					Log.d("value","range"+" "+type+" "+ (error / (oldWidth * oldHeight)));
					inRange = false;
				}
			}

			Log.d("error", "error:  " +type+"  "
					+ (error / (oldWidth * oldHeight)) + " " + oldWidth);

			return inRange; 
		}

		// =============== Compare bitmap ==============ends
}
