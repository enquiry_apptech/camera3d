package com.example.cameramain_mvc.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraCanvasView extends SurfaceView {

	// how many degree half the canvas represents
	// e.g. from left-most to the middle
	final int DRAW_RANGE = 60; // deg

	final int X_ACCEPTANCE, Z_ACCEPTANCE;

	private Canvas canvas;
	private SurfaceHolder theHolder;

	// crop mask
	private Paint paintCropMask;
	private Path pathCrop;

	// aim
	private Paint paintInRange, paintOutRange, paintAimCross;
	final int AIM_RADIUS = 70; // px
	final int AIM_CROSS_LENGTH = 40; // px
    final int AIM_CROSS_WIDTH = 4;

	// rotate cross
	final int ROTATE_CROSS_LENGTH = 90; // px
    final int ROTATE_CROSS_WIDTH = 2;
	private Paint paintRotateCross;

	public CameraCanvasView(Context context, final int X_ACCEPTANCE,
			final int Z_ACCEPTANCE) {
		super(context);

		this.X_ACCEPTANCE = X_ACCEPTANCE;
		this.Z_ACCEPTANCE = Z_ACCEPTANCE;

		theHolder = getHolder();

		// initialize paints
		paintCropMask = new Paint();
		paintCropMask.setColor(Color.WHITE);
		paintCropMask.setAlpha(205);

		paintInRange = new Paint();
		paintInRange.setColor(Color.argb(128, 173, 240, 45));

		paintOutRange = new Paint();
		paintOutRange.setColor(Color.argb(128, 237, 100, 100));

		paintAimCross = new Paint();
		paintAimCross.setColor(Color.WHITE);
		paintAimCross.setStyle(Paint.Style.STROKE);
		paintAimCross.setStrokeWidth(AIM_CROSS_WIDTH);

		paintRotateCross = new Paint();
		paintRotateCross.setColor(Color.WHITE);
		paintRotateCross.setStyle(Paint.Style.STROKE);
		paintRotateCross.setStrokeWidth(ROTATE_CROSS_WIDTH);
	}

	public void draw(final int iCrop_HalfWidth, final int iCrop_HalftHeight,
			final int iCanvasVerticalOffset) {
		try {
			canvas = theHolder.lockCanvas();

			synchronized (theHolder) {
				// clear the background
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

				// center canvas - middle as (0,0)
				canvas.translate(this.getWidth() / 2,
						(this.getHeight() - iCanvasVerticalOffset) / 2);

				// draw the crop mask
				drawCropMask(canvas, iCrop_HalfWidth, iCrop_HalftHeight);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (canvas != null)
				theHolder.unlockCanvasAndPost(canvas);
		}
	}

	public void draw(final int iOffset_X, final int iOffset_Y,
			final int iOffset_Z, final int iCrop_HalfWidth,
			final int iCrop_HalftHeight, final int iCanvasVerticalOffset) {
		try {
			canvas = theHolder.lockCanvas();

			synchronized (theHolder) {
				// clear the background
				canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

				// center canvas - middle as (0,0)
				canvas.translate(this.getWidth() / 2,
						(this.getHeight() - iCanvasVerticalOffset) / 2);

				// draw the crop mask
				drawCropMask(canvas, iCrop_HalfWidth, iCrop_HalftHeight);

				// draw the aim
				drawAim(canvas, iOffset_X, iOffset_Z);

				// draw the rotate cross
				drawRotateCross(canvas, iOffset_Y);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (canvas != null)
				theHolder.unlockCanvasAndPost(canvas);
		}
	}

	private void drawCropMask(Canvas c, final int iCrop_HalfWidth,
			final int iCrop_HalftHeight) {
		pathCrop = new Path();
		pathCrop.moveTo(-iCrop_HalfWidth, -iCrop_HalftHeight);
		pathCrop.lineTo(iCrop_HalfWidth, -iCrop_HalftHeight);
		pathCrop.lineTo(iCrop_HalfWidth, iCrop_HalftHeight);
		pathCrop.lineTo(-iCrop_HalfWidth, iCrop_HalftHeight);
		pathCrop.close();
		pathCrop.setFillType(Path.FillType.INVERSE_EVEN_ODD);
		c.drawPath(pathCrop, paintCropMask);
	}

	private void drawAim(Canvas c, final int iOffset_X, final int iOffset_Z) {
		// draw the Aim - ball
		int iAim_X, iAim_Y;

		if (iOffset_Z > DRAW_RANGE) {
			iAim_X = this.getWidth() / 2;
		} else if (iOffset_Z < -DRAW_RANGE) {
			iAim_X = -this.getWidth() / 2;
		} else {
			iAim_X = this.getWidth() / 2 * iOffset_Z / DRAW_RANGE;
		}

		if (iOffset_X > DRAW_RANGE) {
			iAim_Y = this.getHeight() / 2;
		} else if (iOffset_X < -DRAW_RANGE) {
			iAim_Y = -this.getHeight() / 2;
		} else {
			iAim_Y = this.getHeight() / 2 * iOffset_X / DRAW_RANGE;
		}

		iAim_Y *= -1;

		if (iOffset_X < -X_ACCEPTANCE || iOffset_X > X_ACCEPTANCE
				|| iOffset_Z < -Z_ACCEPTANCE || iOffset_Z > Z_ACCEPTANCE) {
			c.drawCircle(iAim_X, iAim_Y, AIM_RADIUS, paintOutRange);

		} else {
			c.drawCircle(iAim_X, iAim_Y, AIM_RADIUS, paintInRange);
		}

		// draw the Aim - cross
		c.drawLine(iAim_X - AIM_CROSS_LENGTH, iAim_Y,
				iAim_X + AIM_CROSS_LENGTH, iAim_Y, paintAimCross);
		c.drawLine(iAim_X, iAim_Y - AIM_CROSS_LENGTH, iAim_X, iAim_Y
				+ AIM_CROSS_LENGTH, paintAimCross);
	}

	private void drawRotateCross(Canvas c, final int iOffset_Y) {
		c.rotate(iOffset_Y);

		c.drawLine(-ROTATE_CROSS_LENGTH, 0, ROTATE_CROSS_LENGTH, 0,
				paintAimCross);
		c.drawLine(0, -ROTATE_CROSS_LENGTH, 0, ROTATE_CROSS_LENGTH,
				paintAimCross);
	}
}