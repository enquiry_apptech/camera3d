package com.example.cameramain_mvc.model;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.List;

public class CameraModel {

    // mode
    public static final int MODE_MANUAL = 0;
    public static final int MODE_BURST = 1;
    public static final int MODE_AUTO = 2;
    public static final int MODE_MOTION = 3;
    public static final int MODE_VIDEO = 4;

    // target sample number
    public final int SAMPLE_TARGET = 32;

    // e.g. input 10 for 1/10 of the screen
    public final int BOTTOM_CONTROL_WEIGHT = 6;
    // bottom control area height
    public int iBottomControlHeight; // px

    // options
    private boolean bFlashOn;
    private boolean bOverlayOn;
    private boolean bCropOn;

    // saving picture
    public final String ROOT_FOLDER_NAME = "Camera3D_MVC";
    public final String GALLERY_FOLDER_NAME = "Camera3DGallery";
    public final String BUFFER_FOLDER_NAME = "Camera3DBuffer";
    public final int SAVE_TARGET_HEIGHT = 720; // or smaller
    public final int TARGET_PICTURE_WIDTH = 1920; // dp
    public final int TARGET_PICTURE_HEIGHT = 1080; // dp

    // photo taking state
    private boolean bTakePhotoReady;
    private boolean bModePause;
    private int iMode;
    private int iCurrentPhotoIndex;


    // device condition
    private boolean bHasCamera;
    private boolean bHasFlash;
    private boolean bHasAccSensor;
    private boolean bHasMagSensor;


    // screen
    private float fScreenDensity; // density = px/dp
    private int iScreenWidth; // px
    private int iScreenHeight; // px

    // preview
    private float fPreviewRatio;
    private int iPreviewResolutionWidth; // px
    private int iPreviewResolutionHeight; // px
    private int iPreviewScreenWidth; // px
    private int iPreviewScreenHeight; // px

    // sensor
    public static final int SENSE_DELAY = 10000; // microsecond
    public static final int X_ACCEPTANCE = 5;
    public static final int Z_ACCEPTANCE = 3;
    public static final int AVERAGING_SIZE = 10;
    public static final int REQUIRE_IN_RANGE_TIME = 1000;
    private final int X_AXIS = 0;
    private final int Y_AXIS = 1;
    private final int Z_AXIS = 2;
    private boolean bAutoStarted;
    private float fAutoStartX, fAutoStartY, fAutoStartZ;
    private float[] faAutoTargetZ; // all the expected Z (corresponding to fAutoStartZ)
    private List<Float>[] dataSet;
    private float fAverageX, fAverageY, fAverageZ;
    private long lFirstInRangeTime;

    //peter motion
    private boolean bMotionStart;
    Bitmap copyBitmap = null;
    Bitmap compareBitmap = null;
    private int compareCount = 0;
    private int index = 1;
    private boolean isFirstChange = false;
    private boolean isFirstTime = true;

    public CameraModel() {
        iMode = 0;
        iCurrentPhotoIndex = 0;

        bFlashOn = false;
        bOverlayOn = false;
        bCropOn = false;

        faAutoTargetZ = new float[SAMPLE_TARGET];
    }

    public void setBottomControlHeight(int h) {
        iBottomControlHeight = h;
    }

    public int getBottomControlHeight() {
        return iBottomControlHeight;
    }

    public void setFlashOn(boolean bOn) {
        bFlashOn = bOn;
    }

    public boolean getFlashOn() {
        return bFlashOn;
    }

    public void setOverlayOn(boolean bOn) {
        bOverlayOn = bOn;
    }

    public boolean getOverlayOn() {
        return bOverlayOn;
    }

    public void setCropOn(boolean bOn) {
        bCropOn = bOn;
    }

    public boolean getCropOn() {
        return bCropOn;
    }

    public void setTakePhotoReady(boolean b) {
        bTakePhotoReady = b;
    }

    public boolean getTakePhotoReady() {
        return bTakePhotoReady;
    }

    public void setModePause(boolean b) {
        bModePause = b;
    }

    public boolean getModePause() {
        return bModePause;
    }

    public void setMode(int i) {
        iMode = i;
    }

    public int getMode() {
        return iMode;
    }

    public void setCurrentPhotoIndex(int i) {
        iCurrentPhotoIndex = i;
    }

    public int getCurrentPhotoIndex() {
        return iCurrentPhotoIndex;
    }

    public void setHasCamera(boolean bHas) {
        bHasCamera = bHas;
    }

    public boolean getHasCamera() {
        return bHasCamera;
    }

    public void setHasFlash(boolean bHas) {
        bHasFlash = bHas;
    }

    public boolean getHasFlash() {
        return bHasFlash;
    }

    public void setHasAccSensor(boolean bHas) {
        bHasAccSensor = bHas;
    }

    public boolean getHasAccSensor() {
        return bHasAccSensor;
    }

    public void setHasMagSensor(boolean bHas) {
        bHasMagSensor = bHas;
    }

    public boolean getHasSensor() {
        return (bHasAccSensor && bHasMagSensor);
    }

    public float getScreenDensity() {
        return fScreenDensity;
    }

    public void setScreenDensity(float density) {
        fScreenDensity = density;
    }

    public int getScreenWidth() {
        return iScreenWidth;
    }

    public void setScreenWidth(int width) {
        iScreenWidth = width;
    }

    public int getScreenHeight() {
        return iScreenHeight;
    }

    public void setScreenHeight(int height) {
        iScreenHeight = height;
    }

    public float getPreviewRatio() {
        return fPreviewRatio;
    }

    public void setPreviewRatio(float r) {
        fPreviewRatio = r;
    }

    public int getPreviewResolutionWidth() {
        return iPreviewResolutionWidth;
    }

    public void setPreviewResolutionWidth(int w) {
        iPreviewResolutionWidth = w;
    }

    public int getPreviewResolutionHeight() {
        return iPreviewResolutionHeight;
    }

    public void setPreviewResolutionHeight(int h) {
        iPreviewResolutionHeight = h;
    }

    public int getPreviewScreenWidth() {
        return iPreviewScreenWidth;
    }

    public void setPreviewScreenWidth(int w) {
        iPreviewScreenWidth = w;
    }

    public int getPreviewScreenHeight() {
        return iPreviewScreenHeight;
    }

    public void setPreviewScreenHeight(int h) {
        iPreviewScreenHeight = h;
    }

    public void setAutoStarted(boolean b) {
        bAutoStarted = b;
    }

    public boolean getAutoStarted() {
        return bAutoStarted;
    }

    public void setStartXYZ(float x, float y, float z) {
        float fDifference;

        fAutoStartX = x;
        fAutoStartY = y;
        fAutoStartZ = z;

        // calculate the expected Z
        fDifference = (float) (360.0 / SAMPLE_TARGET);

        faAutoTargetZ[0] = fAutoStartZ;
        for (int i = 1; i < SAMPLE_TARGET; i++) {
            faAutoTargetZ[i] = (faAutoTargetZ[i - 1] + fDifference);
            if (faAutoTargetZ[i] > 180)
                faAutoTargetZ[i] -= 360;
        }
    }

    public float getAutoStartX() {
        return fAutoStartX;
    }

    public float getAutoStartY() {
        return fAutoStartY;
    }

    public float getAutoStartZ() {
        return fAutoStartZ;
    }

    public float getAutoTargetZ() {
        return faAutoTargetZ[iCurrentPhotoIndex];
    }

    public void setFirstInRangeTime(long t) {
        lFirstInRangeTime = t;
    }

    public long getFirstInRangeTime() {
        return lFirstInRangeTime;
    }

// peter
    public boolean getIsFirstTime() {
        return isFirstTime;
    }

    public void setIsFirstTime(boolean isFirstTime) {
        this.isFirstTime = isFirstTime;
    }

    public boolean getbMotionStart() {
        return bMotionStart;
    }

    public void setbMotionStart(boolean bMotionStart) {
        this.bMotionStart = bMotionStart;
    }

    public Bitmap getCopyBitmap() {
        return copyBitmap;
    }

    public void setCopyBitmap(Bitmap copyBitmap) {
        this.copyBitmap = copyBitmap;
    }

    public Bitmap getCompareBitmap() {
        return compareBitmap;
    }

    public void setCompareBitmap(Bitmap compareBitmap) {
        this.compareBitmap = compareBitmap;
    }

    public int getCompareCount() {
        return compareCount;
    }

    public void setCompareCount(int compareCount) {
        this.compareCount = compareCount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean getIsFirstChange() {
        return isFirstChange;
    }

    public void setIsFirstChange(boolean isFirstChange) {
        this.isFirstChange = isFirstChange;
    }
//peter motion ends

}