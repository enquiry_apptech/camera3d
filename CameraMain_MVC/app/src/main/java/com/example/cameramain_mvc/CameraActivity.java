package com.example.cameramain_mvc;

import com.example.cameramain_mvc.controller.CameraController;
import com.example.cameramain_mvc.model.CameraCanvasModel;
import com.example.cameramain_mvc.model.CameraModel;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class CameraActivity extends Activity {

	private CameraController camController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// no title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		try {
			super.onCreate(savedInstanceState);

			final CameraModel camModel = new CameraModel();
			final CameraCanvasModel camCanvasModel = new CameraCanvasModel();
			camController = new CameraController(this, camModel, camCanvasModel);
		} catch (Exception e) {
			Log.e("ERROR", "ERROR IN CODE: " + e.toString());
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(camController!=null){
			camController.onDestroy();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(camController!=null){
			camController.onPause();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(camController!=null){
			camController.onResume();
		}
	}
}
