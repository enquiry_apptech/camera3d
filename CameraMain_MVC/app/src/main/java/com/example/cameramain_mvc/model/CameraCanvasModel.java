package com.example.cameramain_mvc.model;

import java.util.ArrayList;
import java.util.List;

public class CameraCanvasModel {

	final int AVERAGING_SIZE = 10;
	final int X_AXIS = 0;
	final int Y_AXIS = 1;
	final int Z_AXIS = 2;

	// how many degree the canvas represents
	public final int SHOW_RANGE = 60; // deg

	// canvas size
	private int iCanvasWidth;
	private int iCanvasHeight;
	private int iCanvas_HalfWidth, iCanvas_HalfHeight;

	// canvas vertical offset (height of bottom control area)
	private int iCanvasVerticalOffset;

	// cropping
	final int CROP_MIN_HALF_WIDTH = 100;
	final int CROP_MIN_HALF_HEIGHT = 100;
	private int iCrop_HalfWidth;
	private int iCrop_HalfHeight;

	// data set from sensor
	List<Float>[] dataSet;

	public CameraCanvasModel() {
		dataSet = new List[3];
		dataSet[X_AXIS] = new ArrayList<Float>();
		dataSet[Y_AXIS] = new ArrayList<Float>();
		dataSet[Z_AXIS] = new ArrayList<Float>();

		iCrop_HalfWidth = 150;
		iCrop_HalfHeight = 200;
	}

	public void setCanvasWidth(int w) {
		iCanvasWidth = w;
		iCanvas_HalfWidth = w / 2;
	}

	public void setCanvasHeight(int h) {
		iCanvasHeight = h;
		iCanvas_HalfHeight = h / 2;
	}

	public int getCanvasWidth() {
		return iCanvasWidth;
	}

	public int getCanvasHeight() {
		return iCanvasHeight;
	}
	
	public void setCanvasVerticalOffset(int i){
		iCanvasVerticalOffset = i;
	}
	
	public int getCanvasVerticalOffset(){
		return iCanvasVerticalOffset;
	}

	public void setCropHalfWidth(int w) {
		iCrop_HalfWidth = w;
		if (iCrop_HalfWidth > iCanvas_HalfWidth) {
			iCrop_HalfWidth = iCanvas_HalfWidth;
		} else if (iCrop_HalfWidth < CROP_MIN_HALF_WIDTH) {
			iCrop_HalfWidth = CROP_MIN_HALF_WIDTH;
		}
	}

	public void setCropHalfHeight(int h) {
		iCrop_HalfHeight = h;
		if (iCrop_HalfHeight > iCanvas_HalfHeight - iCanvasVerticalOffset/2) {
			iCrop_HalfHeight = iCanvas_HalfHeight - iCanvasVerticalOffset/2;
		} else if (iCrop_HalfHeight < CROP_MIN_HALF_HEIGHT) {
			iCrop_HalfHeight = CROP_MIN_HALF_HEIGHT;
		}
	}

	public int getCropHalfWidth() {
		return iCrop_HalfWidth;
	}

	public int getCropHalfHeight() {
		return iCrop_HalfHeight;
	}
	
	public int getCropMaxHeight(){
		return iCanvasHeight - iCanvasVerticalOffset;
	}

	public void pushXYZ(float x, float y, float z) {
		if (dataSet[X_AXIS].size() == AVERAGING_SIZE) {
			dataSet[X_AXIS].remove(0);
			dataSet[Y_AXIS].remove(0);
			dataSet[Z_AXIS].remove(0);
		}

		dataSet[X_AXIS].add(x);
		dataSet[Y_AXIS].add(y);
		dataSet[Z_AXIS].add(z);
	}

	public void clearXYZ() {
		dataSet[X_AXIS].clear();
		dataSet[Y_AXIS].clear();
		dataSet[Z_AXIS].clear();
	}

	public float getAverage(List<Float> theList) {
		float fSum = 0;

		for (float f : theList) {
			fSum += f;
		}

		return fSum / theList.size();
	}

	public float getX() {
		return getAverage(dataSet[X_AXIS]);
	}

	public float getY() {
		return getAverage(dataSet[Y_AXIS]);
	}

	public float getZ() {
		return getAverage(dataSet[Z_AXIS]);
	}
}
