package com.example.cameramain_mvc.view;

import com.example.cameramain_mvc.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CameraModeSpinnerAdapter extends BaseAdapter {

	private Context context;
	private RelativeLayout spinner_mode;
	int positionSelected = 10;
	boolean colorChange = false;

	final String[] strMode = { "Manual", "Burst", "Auto", "Motion" };
	final int[] iaImages = { R.drawable.camera_mode_manual_off,
			R.drawable.camera_mode_burst_off, R.drawable.camera_mode_auto_off,
			R.drawable.camera_mode_motion_off };
	final int[] iaOnImages = { R.drawable.camera_mode_manual_on,
			R.drawable.camera_mode_burst_on, R.drawable.camera_mode_auto_on,
			R.drawable.camera_mode_motion_on };

	public CameraModeSpinnerAdapter(Context context, int positionSelected) {
		this.context = context;
		// this.positionSelected = positionSelected;
	}

	public int getCount() {
		return strMode.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public boolean isColorChange() {
		return colorChange;
	}

	public void setColorChange(boolean colorChange) {
		this.colorChange = colorChange;
	}

	public int getPositionSelected() {
		return positionSelected;
	}

	public void setPositionSelected(int positionSelected) {
		this.positionSelected = positionSelected;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		spinner_mode = (RelativeLayout) (convertView == null ? LayoutInflater
				.from(context).inflate(R.layout.camera_spinner_mode, parent, false)
				: convertView);
		/*
		 * if(position == 0){ manual = ((ImageView)
		 * spinner_mode.findViewById(R.id.ivMode));
		 * manual.setImageResource(iaImages[position]); a=spinner_mode;
		 * a.setId(position); a.setOnClickListener(this); } else if(position ==
		 * 1){ burst = ((ImageView) spinner_mode.findViewById(R.id.ivMode));
		 * burst.setImageResource(iaImages[position]); b=spinner_mode;
		 * b.setId(position); b.setOnClickListener(this); } else if(position ==
		 * 2){ auto = ((ImageView) spinner_mode.findViewById(R.id.ivMode));
		 * auto.setImageResource(iaImages[position]); c=spinner_mode;
		 * c.setId(position); c.setOnClickListener(this); } else if(position ==
		 * 3){ motion = ((ImageView) spinner_mode.findViewById(R.id.ivMode));
		 * motion.setImageResource(iaImages[position]); d=spinner_mode;
		 * d.setId(position); d.setOnClickListener(this); }
		 */
		((ImageView) spinner_mode.findViewById(R.id.ivMode))
				.setImageResource(iaOnImages[position]);

		/*
		 * if((!(positionSelected == position)) && colorChange == true){
		 * ((ImageView)
		 * spinner_mode.findViewById(R.id.ivMode)).setImageResource(
		 * iaImages[position]); }
		 */

		((TextView) spinner_mode.findViewById(R.id.tvMode))
				.setVisibility(View.GONE);

		return spinner_mode;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		spinner_mode = (RelativeLayout) (convertView == null ? LayoutInflater
				.from(context).inflate(R.layout.camera_spinner_mode, parent, false)
				: convertView);
		((ImageView) spinner_mode.findViewById(R.id.ivMode))
				.setImageResource(iaImages[position]);
		((TextView) spinner_mode.findViewById(R.id.tvMode))
				.setText(strMode[position]);

		if (positionSelected == position) {
			((ImageView) spinner_mode.findViewById(R.id.ivMode))
					.setImageResource(iaOnImages[position]);
		}
		return spinner_mode;
	}

}